$(function(){
	permissionLocation();
	if($("meta[name=location_user]").attr('neigh') == 0) {
			$("#modalLocation").modal('show');
	}
});

function habilitarFiltroLocalizacao(ev) {
	ev.preventDefault();
	permissionLocation()
	$("#modalLocation").modal('show');
}

function permissionLocation() {
  var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  function success(pos) {
    var latitude, longitude
    var crd = pos.coords;
   	getLocationUser(crd.latitude, crd.longitude)
  };

  function error(err) {
    resetRange()
    console.warn('ERROR q(' + err.code + '): ' + err.message);
  };

  navigator.geolocation.getCurrentPosition(success, error, options);
}

function LoadUsersByLocation (filter_lcation) {
	if(filter_lcation == '') return false;

	var options = {
	  enableHighAccuracy: true,
	  timeout: 5000,
	  maximumAge: 0
	};

	var option = filter_lcation.replace(/\D/g, '');
	option = parseInt(option, 10);
	
	switch(option) {
		case 1:
			Wo_StorePosts(1)
		break;
		case 2:
			Wo_StorePosts(2)
		break;
		case 3:
			Wo_StorePosts(3)
		break;
		case 4:
			Wo_StorePosts(4)
		break;
		case 5:
			Wo_StorePosts(5)
		break;
		case 6:
			Wo_StorePosts(0)
		break;
		default:
			Wo_StorePosts(1)
	}
}

function resetRange() {
	if(document.getElementById('nearby-users-distance'))
		document.getElementById('nearby-users-distance').value = 1
}

function getLocationUser(lat, lng) {
	$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&sensor=true&key=AIzaSyCmJYukwuT6vYNG1YExqNmHwQGfh4GnGfk', function(data){
		if(data) {
			var neighborhood = data.results[3].formatted_address.split(',');
			neighborhood     = neighborhood[0];
            var city 		 = data.results[0].address_components[3].long_name;
            var state        = data.results[0].address_components[4].short_name;
            var country      = data.results[0].address_components[5].short_name;

            $.post(Wo_Ajax_Requests_File() + '?f=update_location_user', {
            	neighborhood: neighborhood, 
            	city: city, 
            	state: state, 
            	country: country
            }, function (data) {
	            //console.log(data)
	        });
		}

	})
}

