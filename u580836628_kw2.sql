-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 25-Nov-2018 às 18:11
-- Versão do servidor: 10.2.17-MariaDB
-- PHP Version: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u580836628_kw2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `MAM_City`
--

CREATE TABLE `MAM_City` (
  `CT_ID` int(11) NOT NULL,
  `CT_NOME` varchar(120) DEFAULT NULL,
  `CT_UF` int(2) DEFAULT NULL,
  `CT_IBGE` int(7) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Municipios das Unidades Federativas';

--
-- Extraindo dados da tabela `MAM_City`
--

INSERT INTO `MAM_City` (`CT_ID`, `CT_NOME`, `CT_UF`, `CT_IBGE`) VALUES
(1, 'Afonso Cláudio', 8, 3200102),
(2, 'Água Doce do Norte', 8, 3200169),
(3, 'Águia Branca', 8, 3200136),
(4, 'Alegre', 8, 3200201),
(5, 'Alfredo Chaves', 8, 3200300),
(6, 'Alto Rio Novo', 8, 3200359),
(7, 'Anchieta', 8, 3200409),
(8, 'Apiacá', 8, 3200508),
(9, 'Aracruz', 8, 3200607),
(10, 'Atilio Vivacqua', 8, 3200706),
(11, 'Baixo Guandu', 8, 3200805),
(12, 'Barra de São Francisco', 8, 3200904),
(13, 'Boa Esperança', 8, 3201001),
(14, 'Bom Jesus do Norte', 8, 3201100),
(15, 'Brejetuba', 8, 3201159),
(16, 'Cachoeiro de Itapemirim', 8, 3201209),
(17, 'Cariacica', 8, 3201308),
(18, 'Castelo', 8, 3201407),
(19, 'Colatina', 8, 3201506),
(20, 'Conceição da Barra', 8, 3201605),
(21, 'Conceição do Castelo', 8, 3201704),
(22, 'Divino de São Lourenço', 8, 3201803),
(23, 'Domingos Martins', 8, 3201902),
(24, 'Dores do Rio Preto', 8, 3202009),
(25, 'Ecoporanga', 8, 3202108),
(26, 'Fundão', 8, 3202207),
(27, 'Governador Lindenberg', 8, 3202256),
(28, 'Guaçuí', 8, 3202306),
(29, 'Guarapari', 8, 3202405),
(30, 'Ibatiba', 8, 3202454),
(31, 'Ibiraçu', 8, 3202504),
(32, 'Ibitirama', 8, 3202553),
(33, 'Iconha', 8, 3202603),
(34, 'Irupi', 8, 3202652),
(35, 'Itaguaçu', 8, 3202702),
(36, 'Itapemirim', 8, 3202801),
(37, 'Itarana', 8, 3202900),
(38, 'Iúna', 8, 3203007),
(39, 'Jaguaré', 8, 3203056),
(40, 'Jerônimo Monteiro', 8, 3203106),
(41, 'João Neiva', 8, 3203130),
(42, 'Laranja da Terra', 8, 3203163),
(43, 'Linhares', 8, 3203205),
(44, 'Mantenópolis', 8, 3203304),
(45, 'Marataízes', 8, 3203320),
(46, 'Marechal Floriano', 8, 3203346),
(47, 'Marilândia', 8, 3203353),
(48, 'Mimoso do Sul', 8, 3203403),
(49, 'Montanha', 8, 3203502),
(50, 'Mucurici', 8, 3203601),
(51, 'Muniz Freire', 8, 3203700),
(52, 'Muqui', 8, 3203809),
(53, 'Nova Venécia', 8, 3203908),
(54, 'Pancas', 8, 3204005),
(55, 'Pedro Canário', 8, 3204054),
(56, 'Pinheiros', 8, 3204104),
(57, 'Piúma', 8, 3204203),
(58, 'Ponto Belo', 8, 3204252),
(59, 'Presidente Kennedy', 8, 3204302),
(60, 'Rio Bananal', 8, 3204351),
(61, 'Rio Novo do Sul', 8, 3204401),
(62, 'Santa Leopoldina', 8, 3204500),
(63, 'Santa Maria de Jetibá', 8, 3204559),
(64, 'Santa Teresa', 8, 3204609),
(65, 'São Domingos do Norte', 8, 3204658),
(66, 'São Gabriel da Palha', 8, 3204708),
(67, 'São José do Calçado', 8, 3204807),
(68, 'São Mateus', 8, 3204906),
(69, 'São Roque do Canaã', 8, 3204955),
(70, 'Serra', 8, 3205002),
(71, 'Sooretama', 8, 3205010),
(72, 'Vargem Alta', 8, 3205036),
(73, 'Venda Nova do Imigrante', 8, 3205069),
(74, 'Viana', 8, 3205101),
(75, 'Vila Pavão', 8, 3205150),
(76, 'Vila Valério', 8, 3205176),
(77, 'Vila Velha', 8, 3205200),
(78, 'Vitória', 8, 3205309),
(79, 'Acrelândia', 1, 1200013),
(80, 'Assis Brasil', 1, 1200054),
(81, 'Brasiléia', 1, 1200104),
(82, 'Bujari', 1, 1200138),
(83, 'Capixaba', 1, 1200179),
(84, 'Cruzeiro do Sul', 1, 1200203),
(85, 'Epitaciolândia', 1, 1200252),
(86, 'Feijó', 1, 1200302),
(87, 'Jordão', 1, 1200328),
(88, 'Mâncio Lima', 1, 1200336),
(89, 'Manoel Urbano', 1, 1200344),
(90, 'Marechal Thaumaturgo', 1, 1200351),
(91, 'Plácido de Castro', 1, 1200385),
(92, 'Porto Acre', 1, 1200807),
(93, 'Porto Walter', 1, 1200393),
(94, 'Rio Branco', 1, 1200401),
(95, 'Rodrigues Alves', 1, 1200427),
(96, 'Santa Rosa do Purus', 1, 1200435),
(97, 'Sena Madureira', 1, 1200500),
(98, 'Senador Guiomard', 1, 1200450),
(99, 'Tarauacá', 1, 1200609),
(100, 'Xapuri', 1, 1200708),
(101, 'Água Branca', 2, 2700102),
(102, 'Anadia', 2, 2700201),
(103, 'Arapiraca', 2, 2700300),
(104, 'Atalaia', 2, 2700409),
(105, 'Barra de Santo Antônio', 2, 2700508),
(106, 'Barra de São Miguel', 2, 2700607),
(107, 'Batalha', 2, 2700706),
(108, 'Belém', 2, 2700805),
(109, 'Belo Monte', 2, 2700904),
(110, 'Boca da Mata', 2, 2701001),
(111, 'Branquinha', 2, 2701100),
(112, 'Cacimbinhas', 2, 2701209),
(113, 'Cajueiro', 2, 2701308),
(114, 'Campestre', 2, 2701357),
(115, 'Campo Alegre', 2, 2701407),
(116, 'Campo Grande', 2, 2701506),
(117, 'Canapi', 2, 2701605),
(118, 'Capela', 2, 2701704),
(119, 'Carneiros', 2, 2701803),
(120, 'Chã Preta', 2, 2701902),
(121, 'Coité do Nóia', 2, 2702009),
(122, 'Colônia Leopoldina', 2, 2702108),
(123, 'Coqueiro Seco', 2, 2702207),
(124, 'Coruripe', 2, 2702306),
(125, 'Craíbas', 2, 2702355),
(126, 'Delmiro Gouveia', 2, 2702405),
(127, 'Dois Riachos', 2, 2702504),
(128, 'Estrela de Alagoas', 2, 2702553),
(129, 'Feira Grande', 2, 2702603),
(130, 'Feliz Deserto', 2, 2702702),
(131, 'Flexeiras', 2, 2702801),
(132, 'Girau do Ponciano', 2, 2702900),
(133, 'Ibateguara', 2, 2703007),
(134, 'Igaci', 2, 2703106),
(135, 'Igreja Nova', 2, 2703205),
(136, 'Inhapi', 2, 2703304),
(137, 'Jacaré dos Homens', 2, 2703403),
(138, 'Jacuípe', 2, 2703502),
(139, 'Japaratinga', 2, 2703601),
(140, 'Jaramataia', 2, 2703700),
(141, 'Jequiá da Praia', 2, 2703759),
(142, 'Joaquim Gomes', 2, 2703809),
(143, 'Jundiá', 2, 2703908),
(144, 'Junqueiro', 2, 2704005),
(145, 'Lagoa da Canoa', 2, 2704104),
(146, 'Limoeiro de Anadia', 2, 2704203),
(147, 'Maceió', 2, 2704302),
(148, 'Major Isidoro', 2, 2704401),
(149, 'Mar Vermelho', 2, 2704906),
(150, 'Maragogi', 2, 2704500),
(151, 'Maravilha', 2, 2704609),
(152, 'Marechal Deodoro', 2, 2704708),
(153, 'Maribondo', 2, 2704807),
(154, 'Mata Grande', 2, 2705002),
(155, 'Matriz de Camaragibe', 2, 2705101),
(156, 'Messias', 2, 2705200),
(157, 'Minador do Negrão', 2, 2705309),
(158, 'Monteirópolis', 2, 2705408),
(159, 'Murici', 2, 2705507),
(160, 'Novo Lino', 2, 2705606),
(161, 'Olho d`Água das Flores', 2, 2705705),
(162, 'Olho d`Água do Casado', 2, 2705804),
(163, 'Olho d`Água Grande', 2, 2705903),
(164, 'Olivença', 2, 2706000),
(165, 'Ouro Branco', 2, 2706109),
(166, 'Palestina', 2, 2706208),
(167, 'Palmeira dos Índios', 2, 2706307),
(168, 'Pão de Açúcar', 2, 2706406),
(169, 'Pariconha', 2, 2706422),
(170, 'Paripueira', 2, 2706448),
(171, 'Passo de Camaragibe', 2, 2706505),
(172, 'Paulo Jacinto', 2, 2706604),
(173, 'Penedo', 2, 2706703),
(174, 'Piaçabuçu', 2, 2706802),
(175, 'Pilar', 2, 2706901),
(176, 'Pindoba', 2, 2707008),
(177, 'Piranhas', 2, 2707107),
(178, 'Poço das Trincheiras', 2, 2707206),
(179, 'Porto Calvo', 2, 2707305),
(180, 'Porto de Pedras', 2, 2707404),
(181, 'Porto Real do Colégio', 2, 2707503),
(182, 'Quebrangulo', 2, 2707602),
(183, 'Rio Largo', 2, 2707701),
(184, 'Roteiro', 2, 2707800),
(185, 'Santa Luzia do Norte', 2, 2707909),
(186, 'Santana do Ipanema', 2, 2708006),
(187, 'Santana do Mundaú', 2, 2708105),
(188, 'São Brás', 2, 2708204),
(189, 'São José da Laje', 2, 2708303),
(190, 'São José da Tapera', 2, 2708402),
(191, 'São Luís do Quitunde', 2, 2708501),
(192, 'São Miguel dos Campos', 2, 2708600),
(193, 'São Miguel dos Milagres', 2, 2708709),
(194, 'São Sebastião', 2, 2708808),
(195, 'Satuba', 2, 2708907),
(196, 'Senador Rui Palmeira', 2, 2708956),
(197, 'Tanque d`Arca', 2, 2709004),
(198, 'Taquarana', 2, 2709103),
(199, 'Teotônio Vilela', 2, 2709152),
(200, 'Traipu', 2, 2709202),
(201, 'União dos Palmares', 2, 2709301),
(202, 'Viçosa', 2, 2709400),
(203, 'Amapá', 4, 1600105),
(204, 'Calçoene', 4, 1600204),
(205, 'Cutias', 4, 1600212),
(206, 'Ferreira Gomes', 4, 1600238),
(207, 'Itaubal', 4, 1600253),
(208, 'Laranjal do Jari', 4, 1600279),
(209, 'Macapá', 4, 1600303),
(210, 'Mazagão', 4, 1600402),
(211, 'Oiapoque', 4, 1600501),
(212, 'Pedra Branca do Amaparí', 4, 1600154),
(213, 'Porto Grande', 4, 1600535),
(214, 'Pracuúba', 4, 1600550),
(215, 'Santana', 4, 1600600),
(216, 'Serra do Navio', 4, 1600055),
(217, 'Tartarugalzinho', 4, 1600709),
(218, 'Vitória do Jari', 4, 1600808),
(219, 'Alvarães', 3, 1300029),
(220, 'Amaturá', 3, 1300060),
(221, 'Anamã', 3, 1300086),
(222, 'Anori', 3, 1300102),
(223, 'Apuí', 3, 1300144),
(224, 'Atalaia do Norte', 3, 1300201),
(225, 'Autazes', 3, 1300300),
(226, 'Barcelos', 3, 1300409),
(227, 'Barreirinha', 3, 1300508),
(228, 'Benjamin Constant', 3, 1300607),
(229, 'Beruri', 3, 1300631),
(230, 'Boa Vista do Ramos', 3, 1300680),
(231, 'Boca do Acre', 3, 1300706),
(232, 'Borba', 3, 1300805),
(233, 'Caapiranga', 3, 1300839),
(234, 'Canutama', 3, 1300904),
(235, 'Carauari', 3, 1301001),
(236, 'Careiro', 3, 1301100),
(237, 'Careiro da Várzea', 3, 1301159),
(238, 'Coari', 3, 1301209),
(239, 'Codajás', 3, 1301308),
(240, 'Eirunepé', 3, 1301407),
(241, 'Envira', 3, 1301506),
(242, 'Fonte Boa', 3, 1301605),
(243, 'Guajará', 3, 1301654),
(244, 'Humaitá', 3, 1301704),
(245, 'Ipixuna', 3, 1301803),
(246, 'Iranduba', 3, 1301852),
(247, 'Itacoatiara', 3, 1301902),
(248, 'Itamarati', 3, 1301951),
(249, 'Itapiranga', 3, 1302009),
(250, 'Japurá', 3, 1302108),
(251, 'Juruá', 3, 1302207),
(252, 'Jutaí', 3, 1302306),
(253, 'Lábrea', 3, 1302405),
(254, 'Manacapuru', 3, 1302504),
(255, 'Manaquiri', 3, 1302553),
(256, 'Manaus', 3, 1302603),
(257, 'Manicoré', 3, 1302702),
(258, 'Maraã', 3, 1302801),
(259, 'Maués', 3, 1302900),
(260, 'Nhamundá', 3, 1303007),
(261, 'Nova Olinda do Norte', 3, 1303106),
(262, 'Novo Airão', 3, 1303205),
(263, 'Novo Aripuanã', 3, 1303304),
(264, 'Parintins', 3, 1303403),
(265, 'Pauini', 3, 1303502),
(266, 'Presidente Figueiredo', 3, 1303536),
(267, 'Rio Preto da Eva', 3, 1303569),
(268, 'Santa Isabel do Rio Negro', 3, 1303601),
(269, 'Santo Antônio do Içá', 3, 1303700),
(270, 'São Gabriel da Cachoeira', 3, 1303809),
(271, 'São Paulo de Olivença', 3, 1303908),
(272, 'São Sebastião do Uatumã', 3, 1303957),
(273, 'Silves', 3, 1304005),
(274, 'Tabatinga', 3, 1304062),
(275, 'Tapauá', 3, 1304104),
(276, 'Tefé', 3, 1304203),
(277, 'Tonantins', 3, 1304237),
(278, 'Uarini', 3, 1304260),
(279, 'Urucará', 3, 1304302),
(280, 'Urucurituba', 3, 1304401),
(281, 'Abaíra', 5, 2900108),
(282, 'Abaré', 5, 2900207),
(283, 'Acajutiba', 5, 2900306),
(284, 'Adustina', 5, 2900355),
(285, 'Água Fria', 5, 2900405),
(286, 'Aiquara', 5, 2900603),
(287, 'Alagoinhas', 5, 2900702),
(288, 'Alcobaça', 5, 2900801),
(289, 'Almadina', 5, 2900900),
(290, 'Amargosa', 5, 2901007),
(291, 'Amélia Rodrigues', 5, 2901106),
(292, 'América Dourada', 5, 2901155),
(293, 'Anagé', 5, 2901205),
(294, 'Andaraí', 5, 2901304),
(295, 'Andorinha', 5, 2901353),
(296, 'Angical', 5, 2901403),
(297, 'Anguera', 5, 2901502),
(298, 'Antas', 5, 2901601),
(299, 'Antônio Cardoso', 5, 2901700),
(300, 'Antônio Gonçalves', 5, 2901809),
(301, 'Aporá', 5, 2901908),
(302, 'Apuarema', 5, 2901957),
(303, 'Araças', 5, 2902054),
(304, 'Aracatu', 5, 2902005),
(305, 'Araci', 5, 2902104),
(306, 'Aramari', 5, 2902203),
(307, 'Arataca', 5, 2902252),
(308, 'Aratuípe', 5, 2902302),
(309, 'Aurelino Leal', 5, 2902401),
(310, 'Baianópolis', 5, 2902500),
(311, 'Baixa Grande', 5, 2902609),
(312, 'Banzaê', 5, 2902658),
(313, 'Barra', 5, 2902708),
(314, 'Barra da Estiva', 5, 2902807),
(315, 'Barra do Choça', 5, 2902906),
(316, 'Barra do Mendes', 5, 2903003),
(317, 'Barra do Rocha', 5, 2903102),
(318, 'Barreiras', 5, 2903201),
(319, 'Barro Alto', 5, 2903235),
(320, 'Barro Preto (antigo Gov. Lomanto Jr.)', 5, 2903300),
(321, 'Barrocas', 5, 2903276),
(322, 'Belmonte', 5, 2903409),
(323, 'Belo Campo', 5, 2903508),
(324, 'Biritinga', 5, 2903607),
(325, 'Boa Nova', 5, 2903706),
(326, 'Boa Vista do Tupim', 5, 2903805),
(327, 'Bom Jesus da Lapa', 5, 2903904),
(328, 'Bom Jesus da Serra', 5, 2903953),
(329, 'Boninal', 5, 2904001),
(330, 'Bonito', 5, 2904050),
(331, 'Boquira', 5, 2904100),
(332, 'Botuporã', 5, 2904209),
(333, 'Brejões', 5, 2904308),
(334, 'Brejolândia', 5, 2904407),
(335, 'Brotas de Macaúbas', 5, 2904506),
(336, 'Brumado', 5, 2904605),
(337, 'Buerarema', 5, 2904704),
(338, 'Buritirama', 5, 2904753),
(339, 'Caatiba', 5, 2904803),
(340, 'Cabaceiras do Paraguaçu', 5, 2904852),
(341, 'Cachoeira', 5, 2904902),
(342, 'Caculé', 5, 2905008),
(343, 'Caém', 5, 2905107),
(344, 'Caetanos', 5, 2905156),
(345, 'Caetité', 5, 2905206),
(346, 'Cafarnaum', 5, 2905305),
(347, 'Cairu', 5, 2905404),
(348, 'Caldeirão Grande', 5, 2905503),
(349, 'Camacan', 5, 2905602),
(350, 'Camaçari', 5, 2905701),
(351, 'Camamu', 5, 2905800),
(352, 'Campo Alegre de Lourdes', 5, 2905909),
(353, 'Campo Formoso', 5, 2906006),
(354, 'Canápolis', 5, 2906105),
(355, 'Canarana', 5, 2906204),
(356, 'Canavieiras', 5, 2906303),
(357, 'Candeal', 5, 2906402),
(358, 'Candeias', 5, 2906501),
(359, 'Candiba', 5, 2906600),
(360, 'Cândido Sales', 5, 2906709),
(361, 'Cansanção', 5, 2906808),
(362, 'Canudos', 5, 2906824),
(363, 'Capela do Alto Alegre', 5, 2906857),
(364, 'Capim Grosso', 5, 2906873),
(365, 'Caraíbas', 5, 2906899),
(366, 'Caravelas', 5, 2906907),
(367, 'Cardeal da Silva', 5, 2907004),
(368, 'Carinhanha', 5, 2907103),
(369, 'Casa Nova', 5, 2907202),
(370, 'Castro Alves', 5, 2907301),
(371, 'Catolândia', 5, 2907400),
(372, 'Catu', 5, 2907509),
(373, 'Caturama', 5, 2907558),
(374, 'Central', 5, 2907608),
(375, 'Chorrochó', 5, 2907707),
(376, 'Cícero Dantas', 5, 2907806),
(377, 'Cipó', 5, 2907905),
(378, 'Coaraci', 5, 2908002),
(379, 'Cocos', 5, 2908101),
(380, 'Conceição da Feira', 5, 2908200),
(381, 'Conceição do Almeida', 5, 2908309),
(382, 'Conceição do Coité', 5, 2908408),
(383, 'Conceição do Jacuípe', 5, 2908507),
(384, 'Conde', 5, 2908606),
(385, 'Condeúba', 5, 2908705),
(386, 'Contendas do Sincorá', 5, 2908804),
(387, 'Coração de Maria', 5, 2908903),
(388, 'Cordeiros', 5, 2909000),
(389, 'Coribe', 5, 2909109),
(390, 'Coronel João Sá', 5, 2909208),
(391, 'Correntina', 5, 2909307),
(392, 'Cotegipe', 5, 2909406),
(393, 'Cravolândia', 5, 2909505),
(394, 'Crisópolis', 5, 2909604),
(395, 'Cristópolis', 5, 2909703),
(396, 'Cruz das Almas', 5, 2909802),
(397, 'Curaçá', 5, 2909901),
(398, 'Dário Meira', 5, 2910008),
(399, 'Dias d`Ávila', 5, 2910057),
(400, 'Dom Basílio', 5, 2910107),
(401, 'Dom Macedo Costa', 5, 2910206),
(402, 'Elísio Medrado', 5, 2910305),
(403, 'Encruzilhada', 5, 2910404),
(404, 'Entre Rios', 5, 2910503),
(405, 'Érico Cardoso', 5, 2900504),
(406, 'Esplanada', 5, 2910602),
(407, 'Euclides da Cunha', 5, 2910701),
(408, 'Eunápolis', 5, 2910727),
(409, 'Fátima', 5, 2910750),
(410, 'Feira da Mata', 5, 2910776),
(411, 'Feira de Santana', 5, 2910800),
(412, 'Filadélfia', 5, 2910859),
(413, 'Firmino Alves', 5, 2910909),
(414, 'Floresta Azul', 5, 2911006),
(415, 'Formosa do Rio Preto', 5, 2911105),
(416, 'Gandu', 5, 2911204),
(417, 'Gavião', 5, 2911253),
(418, 'Gentio do Ouro', 5, 2911303),
(419, 'Glória', 5, 2911402),
(420, 'Gongogi', 5, 2911501),
(421, 'Governador Mangabeira', 5, 2911600),
(422, 'Guajeru', 5, 2911659),
(423, 'Guanambi', 5, 2911709),
(424, 'Guaratinga', 5, 2911808),
(425, 'Heliópolis', 5, 2911857),
(426, 'Iaçu', 5, 2911907),
(427, 'Ibiassucê', 5, 2912004),
(428, 'Ibicaraí', 5, 2912103),
(429, 'Ibicoara', 5, 2912202),
(430, 'Ibicuí', 5, 2912301),
(431, 'Ibipeba', 5, 2912400),
(432, 'Ibipitanga', 5, 2912509),
(433, 'Ibiquera', 5, 2912608),
(434, 'Ibirapitanga', 5, 2912707),
(435, 'Ibirapuã', 5, 2912806),
(436, 'Ibirataia', 5, 2912905),
(437, 'Ibitiara', 5, 2913002),
(438, 'Ibititá', 5, 2913101),
(439, 'Ibotirama', 5, 2913200),
(440, 'Ichu', 5, 2913309),
(441, 'Igaporã', 5, 2913408),
(442, 'Igrapiúna', 5, 2913457),
(443, 'Iguaí', 5, 2913507),
(444, 'Ilhéus', 5, 2913606),
(445, 'Inhambupe', 5, 2913705),
(446, 'Ipecaetá', 5, 2913804),
(447, 'Ipiaú', 5, 2913903),
(448, 'Ipirá', 5, 2914000),
(449, 'Ipupiara', 5, 2914109),
(450, 'Irajuba', 5, 2914208),
(451, 'Iramaia', 5, 2914307),
(452, 'Iraquara', 5, 2914406),
(453, 'Irará', 5, 2914505),
(454, 'Irecê', 5, 2914604),
(455, 'Itabela', 5, 2914653),
(456, 'Itaberaba', 5, 2914703),
(457, 'Itabuna', 5, 2914802),
(458, 'Itacaré', 5, 2914901),
(459, 'Itaeté', 5, 2915007),
(460, 'Itagi', 5, 2915106),
(461, 'Itagibá', 5, 2915205),
(462, 'Itagimirim', 5, 2915304),
(463, 'Itaguaçu da Bahia', 5, 2915353),
(464, 'Itaju do Colônia', 5, 2915403),
(465, 'Itajuípe', 5, 2915502),
(466, 'Itamaraju', 5, 2915601),
(467, 'Itamari', 5, 2915700),
(468, 'Itambé', 5, 2915809),
(469, 'Itanagra', 5, 2915908),
(470, 'Itanhém', 5, 2916005),
(471, 'Itaparica', 5, 2916104),
(472, 'Itapé', 5, 2916203),
(473, 'Itapebi', 5, 2916302),
(474, 'Itapetinga', 5, 2916401),
(475, 'Itapicuru', 5, 2916500),
(476, 'Itapitanga', 5, 2916609),
(477, 'Itaquara', 5, 2916708),
(478, 'Itarantim', 5, 2916807),
(479, 'Itatim', 5, 2916856),
(480, 'Itiruçu', 5, 2916906),
(481, 'Itiúba', 5, 2917003),
(482, 'Itororó', 5, 2917102),
(483, 'Ituaçu', 5, 2917201),
(484, 'Ituberá', 5, 2917300),
(485, 'Iuiú', 5, 2917334),
(486, 'Jaborandi', 5, 2917359),
(487, 'Jacaraci', 5, 2917409),
(488, 'Jacobina', 5, 2917508),
(489, 'Jaguaquara', 5, 2917607),
(490, 'Jaguarari', 5, 2917706),
(491, 'Jaguaripe', 5, 2917805),
(492, 'Jandaíra', 5, 2917904),
(493, 'Jequié', 5, 2918001),
(494, 'Jeremoabo', 5, 2918100),
(495, 'Jiquiriçá', 5, 2918209),
(496, 'Jitaúna', 5, 2918308),
(497, 'João Dourado', 5, 2918357),
(498, 'Juazeiro', 5, 2918407),
(499, 'Jucuruçu', 5, 2918456),
(500, 'Jussara', 5, 2918506),
(501, 'Jussari', 5, 2918555),
(502, 'Jussiape', 5, 2918605),
(503, 'Lafaiete Coutinho', 5, 2918704),
(504, 'Lagoa Real', 5, 2918753),
(505, 'Laje', 5, 2918803),
(506, 'Lajedão', 5, 2918902),
(507, 'Lajedinho', 5, 2919009),
(508, 'Lajedo do Tabocal', 5, 2919058),
(509, 'Lamarão', 5, 2919108),
(510, 'Lapão', 5, 2919157),
(511, 'Lauro de Freitas', 5, 2919207),
(512, 'Lençóis', 5, 2919306),
(513, 'Licínio de Almeida', 5, 2919405),
(514, 'Livramento de Nossa Senhora', 5, 2919504),
(515, 'Luís Eduardo Magalhães', 5, 2919553),
(516, 'Macajuba', 5, 2919603),
(517, 'Macarani', 5, 2919702),
(518, 'Macaúbas', 5, 2919801),
(519, 'Macururé', 5, 2919900),
(520, 'Madre de Deus', 5, 2919926),
(521, 'Maetinga', 5, 2919959),
(522, 'Maiquinique', 5, 2920007),
(523, 'Mairi', 5, 2920106),
(524, 'Malhada', 5, 2920205),
(525, 'Malhada de Pedras', 5, 2920304),
(526, 'Manoel Vitorino', 5, 2920403),
(527, 'Mansidão', 5, 2920452),
(528, 'Maracás', 5, 2920502),
(529, 'Maragogipe', 5, 2920601),
(530, 'Maraú', 5, 2920700),
(531, 'Marcionílio Souza', 5, 2920809),
(532, 'Mascote', 5, 2920908),
(533, 'Mata de São João', 5, 2921005),
(534, 'Matina', 5, 2921054),
(535, 'Medeiros Neto', 5, 2921104),
(536, 'Miguel Calmon', 5, 2921203),
(537, 'Milagres', 5, 2921302),
(538, 'Mirangaba', 5, 2921401),
(539, 'Mirante', 5, 2921450),
(540, 'Monte Santo', 5, 2921500),
(541, 'Morpará', 5, 2921609),
(542, 'Morro do Chapéu', 5, 2921708),
(543, 'Mortugaba', 5, 2921807),
(544, 'Mucugê', 5, 2921906),
(545, 'Mucuri', 5, 2922003),
(546, 'Mulungu do Morro', 5, 2922052),
(547, 'Mundo Novo', 5, 2922102),
(548, 'Muniz Ferreira', 5, 2922201),
(549, 'Muquém de São Francisco', 5, 2922250),
(550, 'Muritiba', 5, 2922300),
(551, 'Mutuípe', 5, 2922409),
(552, 'Nazaré', 5, 2922508),
(553, 'Nilo Peçanha', 5, 2922607),
(554, 'Nordestina', 5, 2922656),
(555, 'Nova Canaã', 5, 2922706),
(556, 'Nova Fátima', 5, 2922730),
(557, 'Nova Ibiá', 5, 2922755),
(558, 'Nova Itarana', 5, 2922805),
(559, 'Nova Redenção', 5, 2922854),
(560, 'Nova Soure', 5, 2922904),
(561, 'Nova Viçosa', 5, 2923001),
(562, 'Novo Horizonte', 5, 2923035),
(563, 'Novo Triunfo', 5, 2923050),
(564, 'Olindina', 5, 2923100),
(565, 'Oliveira dos Brejinhos', 5, 2923209),
(566, 'Ouriçangas', 5, 2923308),
(567, 'Ourolândia', 5, 2923357),
(568, 'Palmas de Monte Alto', 5, 2923407),
(569, 'Palmeiras', 5, 2923506),
(570, 'Paramirim', 5, 2923605),
(571, 'Paratinga', 5, 2923704),
(572, 'Paripiranga', 5, 2923803),
(573, 'Pau Brasil', 5, 2923902),
(574, 'Paulo Afonso', 5, 2924009),
(575, 'Pé de Serra', 5, 2924058),
(576, 'Pedrão', 5, 2924108),
(577, 'Pedro Alexandre', 5, 2924207),
(578, 'Piatã', 5, 2924306),
(579, 'Pilão Arcado', 5, 2924405),
(580, 'Pindaí', 5, 2924504),
(581, 'Pindobaçu', 5, 2924603),
(582, 'Pintadas', 5, 2924652),
(583, 'Piraí do Norte', 5, 2924678),
(584, 'Piripá', 5, 2924702),
(585, 'Piritiba', 5, 2924801),
(586, 'Planaltino', 5, 2924900),
(587, 'Planalto', 5, 2925006),
(588, 'Poções', 5, 2925105),
(589, 'Pojuca', 5, 2925204),
(590, 'Ponto Novo', 5, 2925253),
(591, 'Porto Seguro', 5, 2925303),
(592, 'Potiraguá', 5, 2925402),
(593, 'Prado', 5, 2925501),
(594, 'Presidente Dutra', 5, 2925600),
(595, 'Presidente Jânio Quadros', 5, 2925709),
(596, 'Presidente Tancredo Neves', 5, 2925758),
(597, 'Queimadas', 5, 2925808),
(598, 'Quijingue', 5, 2925907),
(599, 'Quixabeira', 5, 2925931),
(600, 'Rafael Jambeiro', 5, 2925956),
(601, 'Remanso', 5, 2926004),
(602, 'Retirolândia', 5, 2926103),
(603, 'Riachão das Neves', 5, 2926202),
(604, 'Riachão do Jacuípe', 5, 2926301),
(605, 'Riacho de Santana', 5, 2926400),
(606, 'Ribeira do Amparo', 5, 2926509),
(607, 'Ribeira do Pombal', 5, 2926608),
(608, 'Ribeirão do Largo', 5, 2926657),
(609, 'Rio de Contas', 5, 2926707),
(610, 'Rio do Antônio', 5, 2926806),
(611, 'Rio do Pires', 5, 2926905),
(612, 'Rio Real', 5, 2927002),
(613, 'Rodelas', 5, 2927101),
(614, 'Ruy Barbosa', 5, 2927200),
(615, 'Salinas da Margarida', 5, 2927309),
(616, 'Salvador', 5, 2927408),
(617, 'Santa Bárbara', 5, 2927507),
(618, 'Santa Brígida', 5, 2927606),
(619, 'Santa Cruz Cabrália', 5, 2927705),
(620, 'Santa Cruz da Vitória', 5, 2927804),
(621, 'Santa Inês', 5, 2927903),
(622, 'Santa Luzia', 5, 2928059),
(623, 'Santa Maria da Vitória', 5, 2928109),
(624, 'Santa Rita de Cássia', 5, 2928406),
(625, 'Santa Teresinha', 5, 2928505),
(626, 'Santaluz', 5, 2928000),
(627, 'Santana', 5, 2928208),
(628, 'Santanópolis', 5, 2928307),
(629, 'Santo Amaro', 5, 2928604),
(630, 'Santo Antônio de Jesus', 5, 2928703),
(631, 'Santo Estêvão', 5, 2928802),
(632, 'São Desidério', 5, 2928901),
(633, 'São Domingos', 5, 2928950),
(634, 'São Felipe', 5, 2929107),
(635, 'São Félix', 5, 2929008),
(636, 'São Félix do Coribe', 5, 2929057),
(637, 'São Francisco do Conde', 5, 2929206),
(638, 'São Gabriel', 5, 2929255),
(639, 'São Gonçalo dos Campos', 5, 2929305),
(640, 'São José da Vitória', 5, 2929354),
(641, 'São José do Jacuípe', 5, 2929370),
(642, 'São Miguel das Matas', 5, 2929404),
(643, 'São Sebastião do Passé', 5, 2929503),
(644, 'Sapeaçu', 5, 2929602),
(645, 'Sátiro Dias', 5, 2929701),
(646, 'Saubara', 5, 2929750),
(647, 'Saúde', 5, 2929800),
(648, 'Seabra', 5, 2929909),
(649, 'Sebastião Laranjeiras', 5, 2930006),
(650, 'Senhor do Bonfim', 5, 2930105),
(651, 'Sento Sé', 5, 2930204),
(652, 'Serra do Ramalho', 5, 2930154),
(653, 'Serra Dourada', 5, 2930303),
(654, 'Serra Preta', 5, 2930402),
(655, 'Serrinha', 5, 2930501),
(656, 'Serrolândia', 5, 2930600),
(657, 'Simões Filho', 5, 2930709),
(658, 'Sítio do Mato', 5, 2930758),
(659, 'Sítio do Quinto', 5, 2930766),
(660, 'Sobradinho', 5, 2930774),
(661, 'Souto Soares', 5, 2930808),
(662, 'Tabocas do Brejo Velho', 5, 2930907),
(663, 'Tanhaçu', 5, 2931004),
(664, 'Tanque Novo', 5, 2931053),
(665, 'Tanquinho', 5, 2931103),
(666, 'Taperoá', 5, 2931202),
(667, 'Tapiramutá', 5, 2931301),
(668, 'Teixeira de Freitas', 5, 2931350),
(669, 'Teodoro Sampaio', 5, 2931400),
(670, 'Teofilândia', 5, 2931509),
(671, 'Teolândia', 5, 2931608),
(672, 'Terra Nova', 5, 2931707),
(673, 'Tremedal', 5, 2931806),
(674, 'Tucano', 5, 2931905),
(675, 'Uauá', 5, 2932002),
(676, 'Ubaíra', 5, 2932101),
(677, 'Ubaitaba', 5, 2932200),
(678, 'Ubatã', 5, 2932309),
(679, 'Uibaí', 5, 2932408),
(680, 'Umburanas', 5, 2932457),
(681, 'Una', 5, 2932507),
(682, 'Urandi', 5, 2932606),
(683, 'Uruçuca', 5, 2932705),
(684, 'Utinga', 5, 2932804),
(685, 'Valença', 5, 2932903),
(686, 'Valente', 5, 2933000),
(687, 'Várzea da Roça', 5, 2933059),
(688, 'Várzea do Poço', 5, 2933109),
(689, 'Várzea Nova', 5, 2933158),
(690, 'Varzedo', 5, 2933174),
(691, 'Vera Cruz', 5, 2933208),
(692, 'Vereda', 5, 2933257),
(693, 'Vitória da Conquista', 5, 2933307),
(694, 'Wagner', 5, 2933406),
(695, 'Wanderley', 5, 2933455),
(696, 'Wenceslau Guimarães', 5, 2933505),
(697, 'Xique-Xique', 5, 2933604),
(698, 'Abaiara', 6, 2300101),
(699, 'Acarape', 6, 2300150),
(700, 'Acaraú', 6, 2300200),
(701, 'Acopiara', 6, 2300309),
(702, 'Aiuaba', 6, 2300408),
(703, 'Alcântaras', 6, 2300507),
(704, 'Altaneira', 6, 2300606),
(705, 'Alto Santo', 6, 2300705),
(706, 'Amontada', 6, 2300754),
(707, 'Antonina do Norte', 6, 2300804),
(708, 'Apuiarés', 6, 2300903),
(709, 'Aquiraz', 6, 2301000),
(710, 'Aracati', 6, 2301109),
(711, 'Aracoiaba', 6, 2301208),
(712, 'Ararendá', 6, 2301257),
(713, 'Araripe', 6, 2301307),
(714, 'Aratuba', 6, 2301406),
(715, 'Arneiroz', 6, 2301505),
(716, 'Assaré', 6, 2301604),
(717, 'Aurora', 6, 2301703),
(718, 'Baixio', 6, 2301802),
(719, 'Banabuiú', 6, 2301851),
(720, 'Barbalha', 6, 2301901),
(721, 'Barreira', 6, 2301950),
(722, 'Barro', 6, 2302008),
(723, 'Barroquinha', 6, 2302057),
(724, 'Baturité', 6, 2302107),
(725, 'Beberibe', 6, 2302206),
(726, 'Bela Cruz', 6, 2302305),
(727, 'Boa Viagem', 6, 2302404),
(728, 'Brejo Santo', 6, 2302503),
(729, 'Camocim', 6, 2302602),
(730, 'Campos Sales', 6, 2302701),
(731, 'Canindé', 6, 2302800),
(732, 'Capistrano', 6, 2302909),
(733, 'Caridade', 6, 2303006),
(734, 'Cariré', 6, 2303105),
(735, 'Caririaçu', 6, 2303204),
(736, 'Cariús', 6, 2303303),
(737, 'Carnaubal', 6, 2303402),
(738, 'Cascavel', 6, 2303501),
(739, 'Catarina', 6, 2303600),
(740, 'Catunda', 6, 2303659),
(741, 'Caucaia', 6, 2303709),
(742, 'Cedro', 6, 2303808),
(743, 'Chaval', 6, 2303907),
(744, 'Choró', 6, 2303931),
(745, 'Chorozinho', 6, 2303956),
(746, 'Coreaú', 6, 2304004),
(747, 'Crateús', 6, 2304103),
(748, 'Crato', 6, 2304202),
(749, 'Croatá', 6, 2304236),
(750, 'Cruz', 6, 2304251),
(751, 'Deputado Irapuan Pinheiro', 6, 2304269),
(752, 'Ererê', 6, 2304277),
(753, 'Eusébio', 6, 2304285),
(754, 'Farias Brito', 6, 2304301),
(755, 'Forquilha', 6, 2304350),
(756, 'Fortaleza', 6, 2304400),
(757, 'Fortim', 6, 2304459),
(758, 'Frecheirinha', 6, 2304509),
(759, 'General Sampaio', 6, 2304608),
(760, 'Graça', 6, 2304657),
(761, 'Granja', 6, 2304707),
(762, 'Granjeiro', 6, 2304806),
(763, 'Groaíras', 6, 2304905),
(764, 'Guaiúba', 6, 2304954),
(765, 'Guaraciaba do Norte', 6, 2305001),
(766, 'Guaramiranga', 6, 2305100),
(767, 'Hidrolândia', 6, 2305209),
(768, 'Horizonte', 6, 2305233),
(769, 'Ibaretama', 6, 2305266),
(770, 'Ibiapina', 6, 2305308),
(771, 'Ibicuitinga', 6, 2305332),
(772, 'Icapuí', 6, 2305357),
(773, 'Icó', 6, 2305407),
(774, 'Iguatu', 6, 2305506),
(775, 'Independência', 6, 2305605),
(776, 'Ipaporanga', 6, 2305654),
(777, 'Ipaumirim', 6, 2305704),
(778, 'Ipu', 6, 2305803),
(779, 'Ipueiras', 6, 2305902),
(780, 'Iracema', 6, 2306009),
(781, 'Irauçuba', 6, 2306108),
(782, 'Itaiçaba', 6, 2306207),
(783, 'Itaitinga', 6, 2306256),
(784, 'Itapagé', 6, 2306306),
(785, 'Itapipoca', 6, 2306405),
(786, 'Itapiúna', 6, 2306504),
(787, 'Itarema', 6, 2306553),
(788, 'Itatira', 6, 2306603),
(789, 'Jaguaretama', 6, 2306702),
(790, 'Jaguaribara', 6, 2306801),
(791, 'Jaguaribe', 6, 2306900),
(792, 'Jaguaruana', 6, 2307007),
(793, 'Jardim', 6, 2307106),
(794, 'Jati', 6, 2307205),
(795, 'Jijoca de Jericoacoara', 6, 2307254),
(796, 'Juazeiro do Norte', 6, 2307304),
(797, 'Jucás', 6, 2307403),
(798, 'Lavras da Mangabeira', 6, 2307502),
(799, 'Limoeiro do Norte', 6, 2307601),
(800, 'Madalena', 6, 2307635),
(801, 'Maracanaú', 6, 2307650),
(802, 'Maranguape', 6, 2307700),
(803, 'Marco', 6, 2307809),
(804, 'Martinópole', 6, 2307908),
(805, 'Massapê', 6, 2308005),
(806, 'Mauriti', 6, 2308104),
(807, 'Meruoca', 6, 2308203),
(808, 'Milagres', 6, 2308302),
(809, 'Milhã', 6, 2308351),
(810, 'Miraíma', 6, 2308377),
(811, 'Missão Velha', 6, 2308401),
(812, 'Mombaça', 6, 2308500),
(813, 'Monsenhor Tabosa', 6, 2308609),
(814, 'Morada Nova', 6, 2308708),
(815, 'Moraújo', 6, 2308807),
(816, 'Morrinhos', 6, 2308906),
(817, 'Mucambo', 6, 2309003),
(818, 'Mulungu', 6, 2309102),
(819, 'Nova Olinda', 6, 2309201),
(820, 'Nova Russas', 6, 2309300),
(821, 'Novo Oriente', 6, 2309409),
(822, 'Ocara', 6, 2309458),
(823, 'Orós', 6, 2309508),
(824, 'Pacajus', 6, 2309607),
(825, 'Pacatuba', 6, 2309706),
(826, 'Pacoti', 6, 2309805),
(827, 'Pacujá', 6, 2309904),
(828, 'Palhano', 6, 2310001),
(829, 'Palmácia', 6, 2310100),
(830, 'Paracuru', 6, 2310209),
(831, 'Paraipaba', 6, 2310258),
(832, 'Parambu', 6, 2310308),
(833, 'Paramoti', 6, 2310407),
(834, 'Pedra Branca', 6, 2310506),
(835, 'Penaforte', 6, 2310605),
(836, 'Pentecoste', 6, 2310704),
(837, 'Pereiro', 6, 2310803),
(838, 'Pindoretama', 6, 2310852),
(839, 'Piquet Carneiro', 6, 2310902),
(840, 'Pires Ferreira', 6, 2310951),
(841, 'Poranga', 6, 2311009),
(842, 'Porteiras', 6, 2311108),
(843, 'Potengi', 6, 2311207),
(844, 'Potiretama', 6, 2311231),
(845, 'Quiterianópolis', 6, 2311264),
(846, 'Quixadá', 6, 2311306),
(847, 'Quixelô', 6, 2311355),
(848, 'Quixeramobim', 6, 2311405),
(849, 'Quixeré', 6, 2311504),
(850, 'Redenção', 6, 2311603),
(851, 'Reriutaba', 6, 2311702),
(852, 'Russas', 6, 2311801),
(853, 'Saboeiro', 6, 2311900),
(854, 'Salitre', 6, 2311959),
(855, 'Santa Quitéria', 6, 2312205),
(856, 'Santana do Acaraú', 6, 2312007),
(857, 'Santana do Cariri', 6, 2312106),
(858, 'São Benedito', 6, 2312304),
(859, 'São Gonçalo do Amarante', 6, 2312403),
(860, 'São João do Jaguaribe', 6, 2312502),
(861, 'São Luís do Curu', 6, 2312601),
(862, 'Senador Pompeu', 6, 2312700),
(863, 'Senador Sá', 6, 2312809),
(864, 'Sobral', 6, 2312908),
(865, 'Solonópole', 6, 2313005),
(866, 'Tabuleiro do Norte', 6, 2313104),
(867, 'Tamboril', 6, 2313203),
(868, 'Tarrafas', 6, 2313252),
(869, 'Tauá', 6, 2313302),
(870, 'Tejuçuoca', 6, 2313351),
(871, 'Tianguá', 6, 2313401),
(872, 'Trairi', 6, 2313500),
(873, 'Tururu', 6, 2313559),
(874, 'Ubajara', 6, 2313609),
(875, 'Umari', 6, 2313708),
(876, 'Umirim', 6, 2313757),
(877, 'Uruburetama', 6, 2313807),
(878, 'Uruoca', 6, 2313906),
(879, 'Varjota', 6, 2313955),
(880, 'Várzea Alegre', 6, 2314003),
(881, 'Viçosa do Ceará', 6, 2314102),
(882, 'Brasília', 7, 5300108),
(883, 'Abadia de Goiás', 9, 5200050),
(884, 'Abadiânia', 9, 5200100),
(885, 'Acreúna', 9, 5200134),
(886, 'Adelândia', 9, 5200159),
(887, 'Água Fria de Goiás', 9, 5200175),
(888, 'Água Limpa', 9, 5200209),
(889, 'Águas Lindas de Goiás', 9, 5200258),
(890, 'Alexânia', 9, 5200308),
(891, 'Aloândia', 9, 5200506),
(892, 'Alto Horizonte', 9, 5200555),
(893, 'Alto Paraíso de Goiás', 9, 5200605),
(894, 'Alvorada do Norte', 9, 5200803),
(895, 'Amaralina', 9, 5200829),
(896, 'Americano do Brasil', 9, 5200852),
(897, 'Amorinópolis', 9, 5200902),
(898, 'Anápolis', 9, 5201108),
(899, 'Anhanguera', 9, 5201207),
(900, 'Anicuns', 9, 5201306),
(901, 'Aparecida de Goiânia', 9, 5201405),
(902, 'Aparecida do Rio Doce', 9, 5201454),
(903, 'Aporé', 9, 5201504),
(904, 'Araçu', 9, 5201603),
(905, 'Aragarças', 9, 5201702),
(906, 'Aragoiânia', 9, 5201801),
(907, 'Araguapaz', 9, 5202155),
(908, 'Arenópolis', 9, 5202353),
(909, 'Aruanã', 9, 5202502),
(910, 'Aurilândia', 9, 5202601),
(911, 'Avelinópolis', 9, 5202809),
(912, 'Baliza', 9, 5203104),
(913, 'Barro Alto', 9, 5203203),
(914, 'Bela Vista de Goiás', 9, 5203302),
(915, 'Bom Jardim de Goiás', 9, 5203401),
(916, 'Bom Jesus de Goiás', 9, 5203500),
(917, 'Bonfinópolis', 9, 5203559),
(918, 'Bonópolis', 9, 5203575),
(919, 'Brazabrantes', 9, 5203609),
(920, 'Britânia', 9, 5203807),
(921, 'Buriti Alegre', 9, 5203906),
(922, 'Buriti de Goiás', 9, 5203939),
(923, 'Buritinópolis', 9, 5203962),
(924, 'Cabeceiras', 9, 5204003),
(925, 'Cachoeira Alta', 9, 5204102),
(926, 'Cachoeira de Goiás', 9, 5204201),
(927, 'Cachoeira Dourada', 9, 5204250),
(928, 'Caçu', 9, 5204300),
(929, 'Caiapônia', 9, 5204409),
(930, 'Caldas Novas', 9, 5204508),
(931, 'Caldazinha', 9, 5204557),
(932, 'Campestre de Goiás', 9, 5204607),
(933, 'Campinaçu', 9, 5204656),
(934, 'Campinorte', 9, 5204706),
(935, 'Campo Alegre de Goiás', 9, 5204805),
(936, 'Campo Limpo de Goiás', 9, 5204854),
(937, 'Campos Belos', 9, 5204904),
(938, 'Campos Verdes', 9, 5204953),
(939, 'Carmo do Rio Verde', 9, 5205000),
(940, 'Castelândia', 9, 5205059),
(941, 'Catalão', 9, 5205109),
(942, 'Caturaí', 9, 5205208),
(943, 'Cavalcante', 9, 5205307),
(944, 'Ceres', 9, 5205406),
(945, 'Cezarina', 9, 5205455),
(946, 'Chapadão do Céu', 9, 5205471),
(947, 'Cidade Ocidental', 9, 5205497),
(948, 'Cocalzinho de Goiás', 9, 5205513),
(949, 'Colinas do Sul', 9, 5205521),
(950, 'Córrego do Ouro', 9, 5205703),
(951, 'Corumbá de Goiás', 9, 5205802),
(952, 'Corumbaíba', 9, 5205901),
(953, 'Cristalina', 9, 5206206),
(954, 'Cristianópolis', 9, 5206305),
(955, 'Crixás', 9, 5206404),
(956, 'Cromínia', 9, 5206503),
(957, 'Cumari', 9, 5206602),
(958, 'Damianópolis', 9, 5206701),
(959, 'Damolândia', 9, 5206800),
(960, 'Davinópolis', 9, 5206909),
(961, 'Diorama', 9, 5207105),
(962, 'Divinópolis de Goiás', 9, 5208301),
(963, 'Doverlândia', 9, 5207253),
(964, 'Edealina', 9, 5207352),
(965, 'Edéia', 9, 5207402),
(966, 'Estrela do Norte', 9, 5207501),
(967, 'Faina', 9, 5207535),
(968, 'Fazenda Nova', 9, 5207600),
(969, 'Firminópolis', 9, 5207808),
(970, 'Flores de Goiás', 9, 5207907),
(971, 'Formosa', 9, 5208004),
(972, 'Formoso', 9, 5208103),
(973, 'Gameleira de Goiás', 9, 5208152),
(974, 'Goianápolis', 9, 5208400),
(975, 'Goiandira', 9, 5208509),
(976, 'Goianésia', 9, 5208608),
(977, 'Goiânia', 9, 5208707),
(978, 'Goianira', 9, 5208806),
(979, 'Goiás', 9, 5208905),
(980, 'Goiatuba', 9, 5209101),
(981, 'Gouvelândia', 9, 5209150),
(982, 'Guapó', 9, 5209200),
(983, 'Guaraíta', 9, 5209291),
(984, 'Guarani de Goiás', 9, 5209408),
(985, 'Guarinos', 9, 5209457),
(986, 'Heitoraí', 9, 5209606),
(987, 'Hidrolândia', 9, 5209705),
(988, 'Hidrolina', 9, 5209804),
(989, 'Iaciara', 9, 5209903),
(990, 'Inaciolândia', 9, 5209937),
(991, 'Indiara', 9, 5209952),
(992, 'Inhumas', 9, 5210000),
(993, 'Ipameri', 9, 5210109),
(994, 'Ipiranga de Goiás', 9, 5210158),
(995, 'Iporá', 9, 5210208),
(996, 'Israelândia', 9, 5210307),
(997, 'Itaberaí', 9, 5210406),
(998, 'Itaguari', 9, 5210562),
(999, 'Itaguaru', 9, 5210604),
(1000, 'Itajá', 9, 5210802),
(1001, 'Itapaci', 9, 5210901),
(1002, 'Itapirapuã', 9, 5211008),
(1003, 'Itapuranga', 9, 5211206),
(1004, 'Itarumã', 9, 5211305),
(1005, 'Itauçu', 9, 5211404),
(1006, 'Itumbiara', 9, 5211503),
(1007, 'Ivolândia', 9, 5211602),
(1008, 'Jandaia', 9, 5211701),
(1009, 'Jaraguá', 9, 5211800),
(1010, 'Jataí', 9, 5211909),
(1011, 'Jaupaci', 9, 5212006),
(1012, 'Jesúpolis', 9, 5212055),
(1013, 'Joviânia', 9, 5212105),
(1014, 'Jussara', 9, 5212204),
(1015, 'Lagoa Santa', 9, 5212253),
(1016, 'Leopoldo de Bulhões', 9, 5212303),
(1017, 'Luziânia', 9, 5212501),
(1018, 'Mairipotaba', 9, 5212600),
(1019, 'Mambaí', 9, 5212709),
(1020, 'Mara Rosa', 9, 5212808),
(1021, 'Marzagão', 9, 5212907),
(1022, 'Matrinchã', 9, 5212956),
(1023, 'Maurilândia', 9, 5213004),
(1024, 'Mimoso de Goiás', 9, 5213053),
(1025, 'Minaçu', 9, 5213087),
(1026, 'Mineiros', 9, 5213103),
(1027, 'Moiporá', 9, 5213400),
(1028, 'Monte Alegre de Goiás', 9, 5213509),
(1029, 'Montes Claros de Goiás', 9, 5213707),
(1030, 'Montividiu', 9, 5213756),
(1031, 'Montividiu do Norte', 9, 5213772),
(1032, 'Morrinhos', 9, 5213806),
(1033, 'Morro Agudo de Goiás', 9, 5213855),
(1034, 'Mossâmedes', 9, 5213905),
(1035, 'Mozarlândia', 9, 5214002),
(1036, 'Mundo Novo', 9, 5214051),
(1037, 'Mutunópolis', 9, 5214101),
(1038, 'Nazário', 9, 5214408),
(1039, 'Nerópolis', 9, 5214507),
(1040, 'Niquelândia', 9, 5214606),
(1041, 'Nova América', 9, 5214705),
(1042, 'Nova Aurora', 9, 5214804),
(1043, 'Nova Crixás', 9, 5214838),
(1044, 'Nova Glória', 9, 5214861),
(1045, 'Nova Iguaçu de Goiás', 9, 5214879),
(1046, 'Nova Roma', 9, 5214903),
(1047, 'Nova Veneza', 9, 5215009),
(1048, 'Novo Brasil', 9, 5215207),
(1049, 'Novo Gama', 9, 5215231),
(1050, 'Novo Planalto', 9, 5215256),
(1051, 'Orizona', 9, 5215306),
(1052, 'Ouro Verde de Goiás', 9, 5215405),
(1053, 'Ouvidor', 9, 5215504),
(1054, 'Padre Bernardo', 9, 5215603),
(1055, 'Palestina de Goiás', 9, 5215652),
(1056, 'Palmeiras de Goiás', 9, 5215702),
(1057, 'Palmelo', 9, 5215801),
(1058, 'Palminópolis', 9, 5215900),
(1059, 'Panamá', 9, 5216007),
(1060, 'Paranaiguara', 9, 5216304),
(1061, 'Paraúna', 9, 5216403),
(1062, 'Perolândia', 9, 5216452),
(1063, 'Petrolina de Goiás', 9, 5216809),
(1064, 'Pilar de Goiás', 9, 5216908),
(1065, 'Piracanjuba', 9, 5217104),
(1066, 'Piranhas', 9, 5217203),
(1067, 'Pirenópolis', 9, 5217302),
(1068, 'Pires do Rio', 9, 5217401),
(1069, 'Planaltina', 9, 5217609),
(1070, 'Pontalina', 9, 5217708),
(1071, 'Porangatu', 9, 5218003),
(1072, 'Porteirão', 9, 5218052),
(1073, 'Portelândia', 9, 5218102),
(1074, 'Posse', 9, 5218300),
(1075, 'Professor Jamil', 9, 5218391),
(1076, 'Quirinópolis', 9, 5218508),
(1077, 'Rialma', 9, 5218607),
(1078, 'Rianápolis', 9, 5218706),
(1079, 'Rio Quente', 9, 5218789),
(1080, 'Rio Verde', 9, 5218805),
(1081, 'Rubiataba', 9, 5218904),
(1082, 'Sanclerlândia', 9, 5219001),
(1083, 'Santa Bárbara de Goiás', 9, 5219100),
(1084, 'Santa Cruz de Goiás', 9, 5219209),
(1085, 'Santa Fé de Goiás', 9, 5219258),
(1086, 'Santa Helena de Goiás', 9, 5219308),
(1087, 'Santa Isabel', 9, 5219357),
(1088, 'Santa Rita do Araguaia', 9, 5219407),
(1089, 'Santa Rita do Novo Destino', 9, 5219456),
(1090, 'Santa Rosa de Goiás', 9, 5219506),
(1091, 'Santa Tereza de Goiás', 9, 5219605),
(1092, 'Santa Terezinha de Goiás', 9, 5219704),
(1093, 'Santo Antônio da Barra', 9, 5219712),
(1094, 'Santo Antônio de Goiás', 9, 5219738),
(1095, 'Santo Antônio do Descoberto', 9, 5219753),
(1096, 'São Domingos', 9, 5219803),
(1097, 'São Francisco de Goiás', 9, 5219902),
(1098, 'São João d`Aliança', 9, 5220009),
(1099, 'São João da Paraúna', 9, 5220058),
(1100, 'São Luís de Montes Belos', 9, 5220108),
(1101, 'São Luíz do Norte', 9, 5220157),
(1102, 'São Miguel do Araguaia', 9, 5220207),
(1103, 'São Miguel do Passa Quatro', 9, 5220264),
(1104, 'São Patrício', 9, 5220280),
(1105, 'São Simão', 9, 5220405),
(1106, 'Senador Canedo', 9, 5220454),
(1107, 'Serranópolis', 9, 5220504),
(1108, 'Silvânia', 9, 5220603),
(1109, 'Simolândia', 9, 5220686),
(1110, 'Sítio d`Abadia', 9, 5220702),
(1111, 'Taquaral de Goiás', 9, 5221007),
(1112, 'Teresina de Goiás', 9, 5221080),
(1113, 'Terezópolis de Goiás', 9, 5221197),
(1114, 'Três Ranchos', 9, 5221304),
(1115, 'Trindade', 9, 5221403),
(1116, 'Trombas', 9, 5221452),
(1117, 'Turvânia', 9, 5221502),
(1118, 'Turvelândia', 9, 5221551),
(1119, 'Uirapuru', 9, 5221577),
(1120, 'Uruaçu', 9, 5221601),
(1121, 'Uruana', 9, 5221700),
(1122, 'Urutaí', 9, 5221809),
(1123, 'Valparaíso de Goiás', 9, 5221858),
(1124, 'Varjão', 9, 5221908),
(1125, 'Vianópolis', 9, 5222005),
(1126, 'Vicentinópolis', 9, 5222054),
(1127, 'Vila Boa', 9, 5222203),
(1128, 'Vila Propício', 9, 5222302),
(1129, 'Açailândia', 10, 2100055),
(1130, 'Afonso Cunha', 10, 2100105),
(1131, 'Água Doce do Maranhão', 10, 2100154),
(1132, 'Alcântara', 10, 2100204),
(1133, 'Aldeias Altas', 10, 2100303),
(1134, 'Altamira do Maranhão', 10, 2100402),
(1135, 'Alto Alegre do Maranhão', 10, 2100436),
(1136, 'Alto Alegre do Pindaré', 10, 2100477),
(1137, 'Alto Parnaíba', 10, 2100501),
(1138, 'Amapá do Maranhão', 10, 2100550),
(1139, 'Amarante do Maranhão', 10, 2100600),
(1140, 'Anajatuba', 10, 2100709),
(1141, 'Anapurus', 10, 2100808),
(1142, 'Apicum-Açu', 10, 2100832),
(1143, 'Araguanã', 10, 2100873),
(1144, 'Araioses', 10, 2100907),
(1145, 'Arame', 10, 2100956),
(1146, 'Arari', 10, 2101004),
(1147, 'Axixá', 10, 2101103),
(1148, 'Bacabal', 10, 2101202),
(1149, 'Bacabeira', 10, 2101251),
(1150, 'Bacuri', 10, 2101301),
(1151, 'Bacurituba', 10, 2101350),
(1152, 'Balsas', 10, 2101400),
(1153, 'Barão de Grajaú', 10, 2101509),
(1154, 'Barra do Corda', 10, 2101608),
(1155, 'Barreirinhas', 10, 2101707),
(1156, 'Bela Vista do Maranhão', 10, 2101772),
(1157, 'Belágua', 10, 2101731),
(1158, 'Benedito Leite', 10, 2101806),
(1159, 'Bequimão', 10, 2101905),
(1160, 'Bernardo do Mearim', 10, 2101939),
(1161, 'Boa Vista do Gurupi', 10, 2101970),
(1162, 'Bom Jardim', 10, 2102002),
(1163, 'Bom Jesus das Selvas', 10, 2102036),
(1164, 'Bom Lugar', 10, 2102077),
(1165, 'Brejo', 10, 2102101),
(1166, 'Brejo de Areia', 10, 2102150),
(1167, 'Buriti', 10, 2102200),
(1168, 'Buriti Bravo', 10, 2102309),
(1169, 'Buriticupu', 10, 2102325),
(1170, 'Buritirana', 10, 2102358),
(1171, 'Cachoeira Grande', 10, 2102374),
(1172, 'Cajapió', 10, 2102408),
(1173, 'Cajari', 10, 2102507),
(1174, 'Campestre do Maranhão', 10, 2102556),
(1175, 'Cândido Mendes', 10, 2102606),
(1176, 'Cantanhede', 10, 2102705),
(1177, 'Capinzal do Norte', 10, 2102754),
(1178, 'Carolina', 10, 2102804),
(1179, 'Carutapera', 10, 2102903),
(1180, 'Caxias', 10, 2103000),
(1181, 'Cedral', 10, 2103109),
(1182, 'Central do Maranhão', 10, 2103125),
(1183, 'Centro do Guilherme', 10, 2103158),
(1184, 'Centro Novo do Maranhão', 10, 2103174),
(1185, 'Chapadinha', 10, 2103208),
(1186, 'Cidelândia', 10, 2103257),
(1187, 'Codó', 10, 2103307),
(1188, 'Coelho Neto', 10, 2103406),
(1189, 'Colinas', 10, 2103505),
(1190, 'Conceição do Lago-Açu', 10, 2103554),
(1191, 'Coroatá', 10, 2103604),
(1192, 'Cururupu', 10, 2103703),
(1193, 'Davinópolis', 10, 2103752),
(1194, 'Dom Pedro', 10, 2103802),
(1195, 'Duque Bacelar', 10, 2103901),
(1196, 'Esperantinópolis', 10, 2104008),
(1197, 'Estreito', 10, 2104057),
(1198, 'Feira Nova do Maranhão', 10, 2104073),
(1199, 'Fernando Falcão', 10, 2104081),
(1200, 'Formosa da Serra Negra', 10, 2104099),
(1201, 'Fortaleza dos Nogueiras', 10, 2104107),
(1202, 'Fortuna', 10, 2104206),
(1203, 'Godofredo Viana', 10, 2104305),
(1204, 'Gonçalves Dias', 10, 2104404),
(1205, 'Governador Archer', 10, 2104503),
(1206, 'Governador Edison Lobão', 10, 2104552),
(1207, 'Governador Eugênio Barros', 10, 2104602),
(1208, 'Governador Luiz Rocha', 10, 2104628),
(1209, 'Governador Newton Bello', 10, 2104651),
(1210, 'Governador Nunes Freire', 10, 2104677),
(1211, 'Graça Aranha', 10, 2104701),
(1212, 'Grajaú', 10, 2104800),
(1213, 'Guimarães', 10, 2104909),
(1214, 'Humberto de Campos', 10, 2105005),
(1215, 'Icatu', 10, 2105104),
(1216, 'Igarapé do Meio', 10, 2105153),
(1217, 'Igarapé Grande', 10, 2105203),
(1218, 'Imperatriz', 10, 2105302),
(1219, 'Itaipava do Grajaú', 10, 2105351),
(1220, 'Itapecuru Mirim', 10, 2105401),
(1221, 'Itinga do Maranhão', 10, 2105427),
(1222, 'Jatobá', 10, 2105450),
(1223, 'Jenipapo dos Vieiras', 10, 2105476),
(1224, 'João Lisboa', 10, 2105500),
(1225, 'Joselândia', 10, 2105609),
(1226, 'Junco do Maranhão', 10, 2105658),
(1227, 'Lago da Pedra', 10, 2105708),
(1228, 'Lago do Junco', 10, 2105807),
(1229, 'Lago dos Rodrigues', 10, 2105948),
(1230, 'Lago Verde', 10, 2105906),
(1231, 'Lagoa do Mato', 10, 2105922),
(1232, 'Lagoa Grande do Maranhão', 10, 2105963),
(1233, 'Lajeado Novo', 10, 2105989),
(1234, 'Lima Campos', 10, 2106003),
(1235, 'Loreto', 10, 2106102),
(1236, 'Luís Domingues', 10, 2106201),
(1237, 'Magalhães de Almeida', 10, 2106300),
(1238, 'Maracaçumé', 10, 2106326),
(1239, 'Marajá do Sena', 10, 2106359),
(1240, 'Maranhãozinho', 10, 2106375),
(1241, 'Mata Roma', 10, 2106409),
(1242, 'Matinha', 10, 2106508),
(1243, 'Matões', 10, 2106607),
(1244, 'Matões do Norte', 10, 2106631),
(1245, 'Milagres do Maranhão', 10, 2106672),
(1246, 'Mirador', 10, 2106706),
(1247, 'Miranda do Norte', 10, 2106755),
(1248, 'Mirinzal', 10, 2106805),
(1249, 'Monção', 10, 2106904),
(1250, 'Montes Altos', 10, 2107001),
(1251, 'Morros', 10, 2107100),
(1252, 'Nina Rodrigues', 10, 2107209),
(1253, 'Nova Colinas', 10, 2107258),
(1254, 'Nova Iorque', 10, 2107308),
(1255, 'Nova Olinda do Maranhão', 10, 2107357),
(1256, 'Olho d`Água das Cunhãs', 10, 2107407),
(1257, 'Olinda Nova do Maranhão', 10, 2107456),
(1258, 'Paço do Lumiar', 10, 2107506),
(1259, 'Palmeirândia', 10, 2107605),
(1260, 'Paraibano', 10, 2107704),
(1261, 'Parnarama', 10, 2107803),
(1262, 'Passagem Franca', 10, 2107902),
(1263, 'Pastos Bons', 10, 2108009),
(1264, 'Paulino Neves', 10, 2108058),
(1265, 'Paulo Ramos', 10, 2108108),
(1266, 'Pedreiras', 10, 2108207),
(1267, 'Pedro do Rosário', 10, 2108256),
(1268, 'Penalva', 10, 2108306),
(1269, 'Peri Mirim', 10, 2108405),
(1270, 'Peritoró', 10, 2108454),
(1271, 'Pindaré-Mirim', 10, 2108504),
(1272, 'Pinheiro', 10, 2108603),
(1273, 'Pio XII', 10, 2108702),
(1274, 'Pirapemas', 10, 2108801),
(1275, 'Poção de Pedras', 10, 2108900),
(1276, 'Porto Franco', 10, 2109007),
(1277, 'Porto Rico do Maranhão', 10, 2109056),
(1278, 'Presidente Dutra', 10, 2109106),
(1279, 'Presidente Juscelino', 10, 2109205),
(1280, 'Presidente Médici', 10, 2109239),
(1281, 'Presidente Sarney', 10, 2109270),
(1282, 'Presidente Vargas', 10, 2109304),
(1283, 'Primeira Cruz', 10, 2109403),
(1284, 'Raposa', 10, 2109452),
(1285, 'Riachão', 10, 2109502),
(1286, 'Ribamar Fiquene', 10, 2109551),
(1287, 'Rosário', 10, 2109601),
(1288, 'Sambaíba', 10, 2109700),
(1289, 'Santa Filomena do Maranhão', 10, 2109759),
(1290, 'Santa Helena', 10, 2109809),
(1291, 'Santa Inês', 10, 2109908),
(1292, 'Santa Luzia', 10, 2110005),
(1293, 'Santa Luzia do Paruá', 10, 2110039),
(1294, 'Santa Quitéria do Maranhão', 10, 2110104),
(1295, 'Santa Rita', 10, 2110203),
(1296, 'Santana do Maranhão', 10, 2110237),
(1297, 'Santo Amaro do Maranhão', 10, 2110278),
(1298, 'Santo Antônio dos Lopes', 10, 2110302),
(1299, 'São Benedito do Rio Preto', 10, 2110401),
(1300, 'São Bento', 10, 2110500),
(1301, 'São Bernardo', 10, 2110609),
(1302, 'São Domingos do Azeitão', 10, 2110658),
(1303, 'São Domingos do Maranhão', 10, 2110708),
(1304, 'São Félix de Balsas', 10, 2110807),
(1305, 'São Francisco do Brejão', 10, 2110856),
(1306, 'São Francisco do Maranhão', 10, 2110906),
(1307, 'São João Batista', 10, 2111003),
(1308, 'São João do Carú', 10, 2111029),
(1309, 'São João do Paraíso', 10, 2111052),
(1310, 'São João do Soter', 10, 2111078),
(1311, 'São João dos Patos', 10, 2111102),
(1312, 'São José de Ribamar', 10, 2111201),
(1313, 'São José dos Basílios', 10, 2111250),
(1314, 'São Luís', 10, 2111300),
(1315, 'São Luís Gonzaga do Maranhão', 10, 2111409),
(1316, 'São Mateus do Maranhão', 10, 2111508),
(1317, 'São Pedro da Água Branca', 10, 2111532),
(1318, 'São Pedro dos Crentes', 10, 2111573),
(1319, 'São Raimundo das Mangabeiras', 10, 2111607),
(1320, 'São Raimundo do Doca Bezerra', 10, 2111631),
(1321, 'São Roberto', 10, 2111672),
(1322, 'São Vicente Ferrer', 10, 2111706),
(1323, 'Satubinha', 10, 2111722),
(1324, 'Senador Alexandre Costa', 10, 2111748),
(1325, 'Senador La Rocque', 10, 2111763),
(1326, 'Serrano do Maranhão', 10, 2111789),
(1327, 'Sítio Novo', 10, 2111805),
(1328, 'Sucupira do Norte', 10, 2111904),
(1329, 'Sucupira do Riachão', 10, 2111953),
(1330, 'Tasso Fragoso', 10, 2112001),
(1331, 'Timbiras', 10, 2112100),
(1332, 'Timon', 10, 2112209),
(1333, 'Trizidela do Vale', 10, 2112233),
(1334, 'Tufilândia', 10, 2112274),
(1335, 'Tuntum', 10, 2112308),
(1336, 'Turiaçu', 10, 2112407),
(1337, 'Turilândia', 10, 2112456),
(1338, 'Tutóia', 10, 2112506),
(1339, 'Urbano Santos', 10, 2112605),
(1340, 'Vargem Grande', 10, 2112704),
(1341, 'Viana', 10, 2112803),
(1342, 'Vila Nova dos Martírios', 10, 2112852),
(1343, 'Vitória do Mearim', 10, 2112902),
(1344, 'Vitorino Freire', 10, 2113009),
(1345, 'Zé Doca', 10, 2114007),
(1346, 'Acorizal', 13, 5100102),
(1347, 'Água Boa', 13, 5100201),
(1348, 'Alta Floresta', 13, 5100250),
(1349, 'Alto Araguaia', 13, 5100300),
(1350, 'Alto Boa Vista', 13, 5100359),
(1351, 'Alto Garças', 13, 5100409),
(1352, 'Alto Paraguai', 13, 5100508),
(1353, 'Alto Taquari', 13, 5100607),
(1354, 'Apiacás', 13, 5100805),
(1355, 'Araguaiana', 13, 5101001),
(1356, 'Araguainha', 13, 5101209),
(1357, 'Araputanga', 13, 5101258),
(1358, 'Arenápolis', 13, 5101308),
(1359, 'Aripuanã', 13, 5101407),
(1360, 'Barão de Melgaço', 13, 5101605),
(1361, 'Barra do Bugres', 13, 5101704),
(1362, 'Barra do Garças', 13, 5101803),
(1363, 'Bom Jesus do Araguaia', 13, 5101852),
(1364, 'Brasnorte', 13, 5101902),
(1365, 'Cáceres', 13, 5102504),
(1366, 'Campinápolis', 13, 5102603),
(1367, 'Campo Novo do Parecis', 13, 5102637),
(1368, 'Campo Verde', 13, 5102678),
(1369, 'Campos de Júlio', 13, 5102686),
(1370, 'Canabrava do Norte', 13, 5102694),
(1371, 'Canarana', 13, 5102702),
(1372, 'Carlinda', 13, 5102793),
(1373, 'Castanheira', 13, 5102850),
(1374, 'Chapada dos Guimarães', 13, 5103007),
(1375, 'Cláudia', 13, 5103056),
(1376, 'Cocalinho', 13, 5103106),
(1377, 'Colíder', 13, 5103205),
(1378, 'Colniza', 13, 5103254),
(1379, 'Comodoro', 13, 5103304),
(1380, 'Confresa', 13, 5103353),
(1381, 'Conquista d`Oeste', 13, 5103361),
(1382, 'Cotriguaçu', 13, 5103379),
(1383, 'Cuiabá', 13, 5103403),
(1384, 'Curvelândia', 13, 5103437),
(1386, 'Denise', 13, 5103452),
(1387, 'Diamantino', 13, 5103502),
(1388, 'Dom Aquino', 13, 5103601),
(1389, 'Feliz Natal', 13, 5103700),
(1390, 'Figueirópolis d`Oeste', 13, 5103809),
(1391, 'Gaúcha do Norte', 13, 5103858),
(1392, 'General Carneiro', 13, 5103908),
(1393, 'Glória d`Oeste', 13, 5103957),
(1394, 'Guarantã do Norte', 13, 5104104),
(1395, 'Guiratinga', 13, 5104203),
(1396, 'Indiavaí', 13, 5104500),
(1397, 'Ipiranga do Norte', 13, 5104526),
(1398, 'Itanhangá', 13, 5104542),
(1399, 'Itaúba', 13, 5104559),
(1400, 'Itiquira', 13, 5104609),
(1401, 'Jaciara', 13, 5104807),
(1402, 'Jangada', 13, 5104906),
(1403, 'Jauru', 13, 5105002),
(1404, 'Juara', 13, 5105101),
(1405, 'Juína', 13, 5105150),
(1406, 'Juruena', 13, 5105176),
(1407, 'Juscimeira', 13, 5105200),
(1408, 'Lambari d`Oeste', 13, 5105234),
(1409, 'Lucas do Rio Verde', 13, 5105259),
(1410, 'Luciára', 13, 5105309),
(1411, 'Marcelândia', 13, 5105580),
(1412, 'Matupá', 13, 5105606),
(1413, 'Mirassol d`Oeste', 13, 5105622),
(1414, 'Nobres', 13, 5105903),
(1415, 'Nortelândia', 13, 5106000),
(1416, 'Nossa Senhora do Livramento', 13, 5106109),
(1417, 'Nova Bandeirantes', 13, 5106158),
(1418, 'Nova Brasilândia', 13, 5106208),
(1419, 'Nova Canaã do Norte', 13, 5106216),
(1420, 'Nova Guarita', 13, 5108808),
(1421, 'Nova Lacerda', 13, 5106182),
(1422, 'Nova Marilândia', 13, 5108857),
(1423, 'Nova Maringá', 13, 5108907),
(1424, 'Nova Monte verde', 13, 5108956),
(1425, 'Nova Mutum', 13, 5106224),
(1426, 'Nova Olímpia', 13, 5106232),
(1427, 'Nova Santa Helena', 13, 5106190),
(1428, 'Nova Ubiratã', 13, 5106240),
(1429, 'Nova Xavantina', 13, 5106257),
(1430, 'Novo Horizonte do Norte', 13, 5106273),
(1431, 'Novo Mundo', 13, 5106265),
(1432, 'Novo Santo Antônio', 13, 5106315),
(1433, 'Novo São Joaquim', 13, 5106281),
(1434, 'Paranaíta', 13, 5106299),
(1435, 'Paranatinga', 13, 5106307),
(1436, 'Pedra Preta', 13, 5106372),
(1437, 'Peixoto de Azevedo', 13, 5106422),
(1438, 'Planalto da Serra', 13, 5106455),
(1439, 'Poconé', 13, 5106505),
(1440, 'Pontal do Araguaia', 13, 5106653),
(1441, 'Ponte Branca', 13, 5106703),
(1442, 'Pontes e Lacerda', 13, 5106752),
(1443, 'Porto Alegre do Norte', 13, 5106778),
(1444, 'Porto dos Gaúchos', 13, 5106802),
(1445, 'Porto Esperidião', 13, 5106828),
(1446, 'Porto Estrela', 13, 5106851),
(1447, 'Poxoréo', 13, 5107008),
(1448, 'Primavera do Leste', 13, 5107040),
(1449, 'Querência', 13, 5107065),
(1450, 'Reserva do Cabaçal', 13, 5107156),
(1451, 'Ribeirão Cascalheira', 13, 5107180),
(1452, 'Ribeirãozinho', 13, 5107198),
(1453, 'Rio Branco', 13, 5107206),
(1454, 'Rondolândia', 13, 5107578),
(1455, 'Rondonópolis', 13, 5107602),
(1456, 'Rosário Oeste', 13, 5107701),
(1457, 'Salto do Céu', 13, 5107750),
(1458, 'Santa Carmem', 13, 5107248),
(1459, 'Santa Cruz do Xingu', 13, 5107743),
(1460, 'Santa Rita do Trivelato', 13, 5107768),
(1461, 'Santa Terezinha', 13, 5107776),
(1462, 'Santo Afonso', 13, 5107263),
(1463, 'Santo Antônio do Leste', 13, 5107792),
(1464, 'Santo Antônio do Leverger', 13, 5107800),
(1465, 'São Félix do Araguaia', 13, 5107859),
(1466, 'São José do Povo', 13, 5107297),
(1467, 'São José do Rio Claro', 13, 5107305),
(1468, 'São José do Xingu', 13, 5107354),
(1469, 'São José dos Quatro Marcos', 13, 5107107),
(1470, 'São Pedro da Cipa', 13, 5107404),
(1471, 'Sapezal', 13, 5107875),
(1472, 'Serra Nova Dourada', 13, 5107883),
(1473, 'Sinop', 13, 5107909),
(1474, 'Sorriso', 13, 5107925),
(1475, 'Tabaporã', 13, 5107941),
(1476, 'Tangará da Serra', 13, 5107958),
(1477, 'Tapurah', 13, 5108006),
(1478, 'Terra Nova do Norte', 13, 5108055),
(1479, 'Tesouro', 13, 5108105),
(1480, 'Torixoréu', 13, 5108204),
(1481, 'União do Sul', 13, 5108303),
(1482, 'Vale de São Domingos', 13, 5108352),
(1483, 'Várzea Grande', 13, 5108402),
(1484, 'Vera', 13, 5108501),
(1485, 'Vila Bela da Santíssima Trindade', 13, 5105507),
(1486, 'Vila Rica', 13, 5108600),
(1487, 'Água Clara', 12, 5000203),
(1488, 'Alcinópolis', 12, 5000252),
(1489, 'Amambaí', 12, 5000609),
(1490, 'Anastácio', 12, 5000708),
(1491, 'Anaurilândia', 12, 5000807),
(1492, 'Angélica', 12, 5000856),
(1493, 'Antônio João', 12, 5000906),
(1494, 'Aparecida do Taboado', 12, 5001003),
(1495, 'Aquidauana', 12, 5001102),
(1496, 'Aral Moreira', 12, 5001243),
(1497, 'Bandeirantes', 12, 5001508),
(1498, 'Bataguassu', 12, 5001904),
(1500, 'Bela Vista', 12, 5002100),
(1501, 'Bodoquena', 12, 5002159),
(1502, 'Bonito', 12, 5002209),
(1503, 'Brasilândia', 12, 5002308),
(1504, 'Caarapó', 12, 5002407),
(1505, 'Camapuã', 12, 5002605),
(1506, 'Campo Grande', 12, 5002704),
(1507, 'Caracol', 12, 5002803),
(1508, 'Cassilândia', 12, 5002902),
(1509, 'Chapadão do Sul', 12, 5002951),
(1510, 'Corguinho', 12, 5003108),
(1511, 'Coronel Sapucaia', 12, 5003157),
(1512, 'Corumbá', 12, 5003207),
(1513, 'Costa Rica', 12, 5003256),
(1514, 'Coxim', 12, 5003306),
(1515, 'Deodápolis', 12, 5003454),
(1516, 'Dois Irmãos do Buriti', 12, 5003488),
(1517, 'Douradina', 12, 5003504),
(1518, 'Dourados', 12, 5003702),
(1519, 'Eldorado', 12, 5003751);
INSERT INTO `MAM_City` (`CT_ID`, `CT_NOME`, `CT_UF`, `CT_IBGE`) VALUES
(1520, 'Fátima do Sul', 12, 5003801),
(1521, 'Figueirão', 12, 5003900),
(1522, 'Glória de Dourados', 12, 5004007),
(1523, 'Guia Lopes da Laguna', 12, 5004106),
(1524, 'Iguatemi', 12, 5004304),
(1525, 'Inocência', 12, 5004403),
(1526, 'Itaporã', 12, 5004502),
(1527, 'Itaquiraí', 12, 5004601),
(1528, 'Ivinhema', 12, 5004700),
(1529, 'Japorã', 12, 5004809),
(1530, 'Jaraguari', 12, 5004908),
(1531, 'Jardim', 12, 5005004),
(1532, 'Jateí', 12, 5005103),
(1533, 'Juti', 12, 5005152),
(1534, 'Ladário', 12, 5005202),
(1535, 'Laguna Carapã', 12, 5005251),
(1536, 'Maracaju', 12, 5005400),
(1537, 'Miranda', 12, 5005608),
(1538, 'Mundo Novo', 12, 5005681),
(1539, 'Naviraí', 12, 5005707),
(1540, 'Nioaque', 12, 5005806),
(1541, 'Nova Alvorada do Sul', 12, 5006002),
(1542, 'Nova Andradina', 12, 5006200),
(1543, 'Novo Horizonte do Sul', 12, 5006259),
(1544, 'Paranaíba', 12, 5006309),
(1545, 'Paranhos', 12, 5006358),
(1546, 'Pedro Gomes', 12, 5006408),
(1547, 'Ponta Porã', 12, 5006606),
(1548, 'Porto Murtinho', 12, 5006903),
(1549, 'Ribas do Rio Pardo', 12, 5007109),
(1550, 'Rio Brilhante', 12, 5007208),
(1551, 'Rio Negro', 12, 5007307),
(1552, 'Rio Verde de Mato Grosso', 12, 5007406),
(1553, 'Rochedo', 12, 5007505),
(1554, 'Santa Rita do Pardo', 12, 5007554),
(1555, 'São Gabriel do Oeste', 12, 5007695),
(1556, 'Selvíria', 12, 5007802),
(1557, 'Sete Quedas', 12, 5007703),
(1558, 'Sidrolândia', 12, 5007901),
(1559, 'Sonora', 12, 5007935),
(1560, 'Tacuru', 12, 5007950),
(1561, 'Taquarussu', 12, 5007976),
(1562, 'Terenos', 12, 5008008),
(1563, 'Três Lagoas', 12, 5008305),
(1564, 'Vicentina', 12, 5008404),
(1565, 'Abadia dos Dourados', 11, 3100104),
(1566, 'Abaeté', 11, 3100203),
(1567, 'Abre Campo', 11, 3100302),
(1568, 'Acaiaca', 11, 3100401),
(1569, 'Açucena', 11, 3100500),
(1570, 'Água Boa', 11, 3100609),
(1571, 'Água Comprida', 11, 3100708),
(1572, 'Aguanil', 11, 3100807),
(1573, 'Águas Formosas', 11, 3100906),
(1574, 'Águas Vermelhas', 11, 3101003),
(1575, 'Aimorés', 11, 3101102),
(1576, 'Aiuruoca', 11, 3101201),
(1577, 'Alagoa', 11, 3101300),
(1578, 'Albertina', 11, 3101409),
(1579, 'Além Paraíba', 11, 3101508),
(1580, 'Alfenas', 11, 3101607),
(1581, 'Alfredo Vasconcelos', 11, 3101631),
(1582, 'Almenara', 11, 3101706),
(1583, 'Alpercata', 11, 3101805),
(1584, 'Alpinópolis', 11, 3101904),
(1585, 'Alterosa', 11, 3102001),
(1586, 'Alto Caparaó', 11, 3102050),
(1587, 'Alto Jequitibá', 11, 3153509),
(1588, 'Alto Rio Doce', 11, 3102100),
(1589, 'Alvarenga', 11, 3102209),
(1590, 'Alvinópolis', 11, 3102308),
(1591, 'Alvorada de Minas', 11, 3102407),
(1592, 'Amparo do Serra', 11, 3102506),
(1593, 'Andradas', 11, 3102605),
(1594, 'Andrelândia', 11, 3102803),
(1595, 'Angelândia', 11, 3102852),
(1596, 'Antônio Carlos', 11, 3102902),
(1597, 'Antônio Dias', 11, 3103009),
(1598, 'Antônio Prado de Minas', 11, 3103108),
(1599, 'Araçaí', 11, 3103207),
(1600, 'Aracitaba', 11, 3103306),
(1601, 'Araçuaí', 11, 3103405),
(1602, 'Araguari', 11, 3103504),
(1603, 'Arantina', 11, 3103603),
(1604, 'Araponga', 11, 3103702),
(1605, 'Araporã', 11, 3103751),
(1606, 'Arapuá', 11, 3103801),
(1607, 'Araújos', 11, 3103900),
(1608, 'Araxá', 11, 3104007),
(1609, 'Arceburgo', 11, 3104106),
(1610, 'Arcos', 11, 3104205),
(1611, 'Areado', 11, 3104304),
(1612, 'Argirita', 11, 3104403),
(1613, 'Aricanduva', 11, 3104452),
(1614, 'Arinos', 11, 3104502),
(1615, 'Astolfo Dutra', 11, 3104601),
(1616, 'Ataléia', 11, 3104700),
(1617, 'Augusto de Lima', 11, 3104809),
(1618, 'Baependi', 11, 3104908),
(1619, 'Baldim', 11, 3105004),
(1620, 'Bambuí', 11, 3105103),
(1621, 'Bandeira', 11, 3105202),
(1622, 'Bandeira do Sul', 11, 3105301),
(1623, 'Barão de Cocais', 11, 3105400),
(1624, 'Barão de Monte Alto', 11, 3105509),
(1625, 'Barbacena', 11, 3105608),
(1626, 'Barra Longa', 11, 3105707),
(1627, 'Barroso', 11, 3105905),
(1628, 'Bela Vista de Minas', 11, 3106002),
(1629, 'Belmiro Braga', 11, 3106101),
(1630, 'Belo Horizonte', 11, 3106200),
(1631, 'Belo Oriente', 11, 3106309),
(1632, 'Belo Vale', 11, 3106408),
(1633, 'Berilo', 11, 3106507),
(1634, 'Berizal', 11, 3106655),
(1635, 'Bertópolis', 11, 3106606),
(1636, 'Betim', 11, 3106705),
(1637, 'Bias Fortes', 11, 3106804),
(1638, 'Bicas', 11, 3106903),
(1639, 'Biquinhas', 11, 3107000),
(1640, 'Boa Esperança', 11, 3107109),
(1641, 'Bocaina de Minas', 11, 3107208),
(1642, 'Bocaiúva', 11, 3107307),
(1643, 'Bom Despacho', 11, 3107406),
(1644, 'Bom Jardim de Minas', 11, 3107505),
(1645, 'Bom Jesus da Penha', 11, 3107604),
(1646, 'Bom Jesus do Amparo', 11, 3107703),
(1647, 'Bom Jesus do Galho', 11, 3107802),
(1648, 'Bom Repouso', 11, 3107901),
(1649, 'Bom Sucesso', 11, 3108008),
(1650, 'Bonfim', 11, 3108107),
(1651, 'Bonfinópolis de Minas', 11, 3108206),
(1652, 'Bonito de Minas', 11, 3108255),
(1653, 'Borda da Mata', 11, 3108305),
(1654, 'Botelhos', 11, 3108404),
(1655, 'Botumirim', 11, 3108503),
(1656, 'Brás Pires', 11, 3108701),
(1657, 'Brasilândia de Minas', 11, 3108552),
(1658, 'Brasília de Minas', 11, 3108602),
(1659, 'Brasópolis', 11, 3108909),
(1660, 'Braúnas', 11, 3108800),
(1661, 'Brumadinho', 11, 3109006),
(1662, 'Bueno Brandão', 11, 3109105),
(1663, 'Buenópolis', 11, 3109204),
(1664, 'Bugre', 11, 3109253),
(1665, 'Buritis', 11, 3109303),
(1666, 'Buritizeiro', 11, 3109402),
(1667, 'Cabeceira Grande', 11, 3109451),
(1668, 'Cabo Verde', 11, 3109501),
(1669, 'Cachoeira da Prata', 11, 3109600),
(1670, 'Cachoeira de Minas', 11, 3109709),
(1671, 'Cachoeira de Pajeú', 11, 3102704),
(1672, 'Cachoeira Dourada', 11, 3109808),
(1673, 'Caetanópolis', 11, 3109907),
(1674, 'Caeté', 11, 3110004),
(1675, 'Caiana', 11, 3110103),
(1676, 'Cajuri', 11, 3110202),
(1677, 'Caldas', 11, 3110301),
(1678, 'Camacho', 11, 3110400),
(1679, 'Camanducaia', 11, 3110509),
(1680, 'Cambuí', 11, 3110608),
(1681, 'Cambuquira', 11, 3110707),
(1682, 'Campanário', 11, 3110806),
(1683, 'Campanha', 11, 3110905),
(1684, 'Campestre', 11, 3111002),
(1685, 'Campina Verde', 11, 3111101),
(1686, 'Campo Azul', 11, 3111150),
(1687, 'Campo Belo', 11, 3111200),
(1688, 'Campo do Meio', 11, 3111309),
(1689, 'Campo Florido', 11, 3111408),
(1690, 'Campos Altos', 11, 3111507),
(1691, 'Campos Gerais', 11, 3111606),
(1692, 'Cana Verde', 11, 3111903),
(1693, 'Canaã', 11, 3111705),
(1694, 'Canápolis', 11, 3111804),
(1695, 'Candeias', 11, 3112000),
(1696, 'Cantagalo', 11, 3112059),
(1697, 'Caparaó', 11, 3112109),
(1698, 'Capela Nova', 11, 3112208),
(1699, 'Capelinha', 11, 3112307),
(1700, 'Capetinga', 11, 3112406),
(1701, 'Capim Branco', 11, 3112505),
(1702, 'Capinópolis', 11, 3112604),
(1703, 'Capitão Andrade', 11, 3112653),
(1704, 'Capitão Enéas', 11, 3112703),
(1705, 'Capitólio', 11, 3112802),
(1706, 'Caputira', 11, 3112901),
(1707, 'Caraí', 11, 3113008),
(1708, 'Caranaíba', 11, 3113107),
(1709, 'Carandaí', 11, 3113206),
(1710, 'Carangola', 11, 3113305),
(1711, 'Caratinga', 11, 3113404),
(1712, 'Carbonita', 11, 3113503),
(1713, 'Careaçu', 11, 3113602),
(1714, 'Carlos Chagas', 11, 3113701),
(1715, 'Carmésia', 11, 3113800),
(1716, 'Carmo da Cachoeira', 11, 3113909),
(1717, 'Carmo da Mata', 11, 3114006),
(1718, 'Carmo de Minas', 11, 3114105),
(1719, 'Carmo do Cajuru', 11, 3114204),
(1720, 'Carmo do Paranaíba', 11, 3114303),
(1721, 'Carmo do Rio Claro', 11, 3114402),
(1722, 'Carmópolis de Minas', 11, 3114501),
(1723, 'Carneirinho', 11, 3114550),
(1724, 'Carrancas', 11, 3114600),
(1725, 'Carvalhópolis', 11, 3114709),
(1726, 'Carvalhos', 11, 3114808),
(1727, 'Casa Grande', 11, 3114907),
(1728, 'Cascalho Rico', 11, 3115003),
(1729, 'Cássia', 11, 3115102),
(1730, 'Cataguases', 11, 3115300),
(1731, 'Catas Altas', 11, 3115359),
(1732, 'Catas Altas da Noruega', 11, 3115409),
(1733, 'Catuji', 11, 3115458),
(1734, 'Catuti', 11, 3115474),
(1735, 'Caxambu', 11, 3115508),
(1736, 'Cedro do Abaeté', 11, 3115607),
(1737, 'Central de Minas', 11, 3115706),
(1738, 'Centralina', 11, 3115805),
(1739, 'Chácara', 11, 3115904),
(1740, 'Chalé', 11, 3116001),
(1741, 'Chapada do Norte', 11, 3116100),
(1742, 'Chapada Gaúcha', 11, 3116159),
(1743, 'Chiador', 11, 3116209),
(1744, 'Cipotânea', 11, 3116308),
(1745, 'Claraval', 11, 3116407),
(1746, 'Claro dos Poções', 11, 3116506),
(1747, 'Cláudio', 11, 3116605),
(1748, 'Coimbra', 11, 3116704),
(1749, 'Coluna', 11, 3116803),
(1750, 'Comendador Gomes', 11, 3116902),
(1751, 'Comercinho', 11, 3117009),
(1752, 'Conceição da Aparecida', 11, 3117108),
(1753, 'Conceição da Barra de Minas', 11, 3115201),
(1754, 'Conceição das Alagoas', 11, 3117306),
(1755, 'Conceição das Pedras', 11, 3117207),
(1756, 'Conceição de Ipanema', 11, 3117405),
(1757, 'Conceição do Mato Dentro', 11, 3117504),
(1758, 'Conceição do Pará', 11, 3117603),
(1759, 'Conceição do Rio Verde', 11, 3117702),
(1760, 'Conceição dos Ouros', 11, 3117801),
(1761, 'Cônego Marinho', 11, 3117836),
(1762, 'Confins', 11, 3117876),
(1763, 'Congonhal', 11, 3117900),
(1764, 'Congonhas', 11, 3118007),
(1765, 'Congonhas do Norte', 11, 3118106),
(1766, 'Conquista', 11, 3118205),
(1767, 'Conselheiro Lafaiete', 11, 3118304),
(1768, 'Conselheiro Pena', 11, 3118403),
(1769, 'Consolação', 11, 3118502),
(1770, 'Contagem', 11, 3118601),
(1771, 'Coqueiral', 11, 3118700),
(1772, 'Coração de Jesus', 11, 3118809),
(1773, 'Cordisburgo', 11, 3118908),
(1774, 'Cordislândia', 11, 3119005),
(1775, 'Corinto', 11, 3119104),
(1776, 'Coroaci', 11, 3119203),
(1777, 'Coromandel', 11, 3119302),
(1778, 'Coronel Fabriciano', 11, 3119401),
(1779, 'Coronel Murta', 11, 3119500),
(1780, 'Coronel Pacheco', 11, 3119609),
(1781, 'Coronel Xavier Chaves', 11, 3119708),
(1782, 'Córrego Danta', 11, 3119807),
(1783, 'Córrego do Bom Jesus', 11, 3119906),
(1784, 'Córrego Fundo', 11, 3119955),
(1785, 'Córrego Novo', 11, 3120003),
(1786, 'Couto de Magalhães de Minas', 11, 3120102),
(1787, 'Crisólita', 11, 3120151),
(1788, 'Cristais', 11, 3120201),
(1789, 'Cristália', 11, 3120300),
(1790, 'Cristiano Otoni', 11, 3120409),
(1791, 'Cristina', 11, 3120508),
(1792, 'Crucilândia', 11, 3120607),
(1793, 'Cruzeiro da Fortaleza', 11, 3120706),
(1794, 'Cruzília', 11, 3120805),
(1795, 'Cuparaque', 11, 3120839),
(1796, 'Curral de Dentro', 11, 3120870),
(1797, 'Curvelo', 11, 3120904),
(1798, 'Datas', 11, 3121001),
(1799, 'Delfim Moreira', 11, 3121100),
(1800, 'Delfinópolis', 11, 3121209),
(1801, 'Delta', 11, 3121258),
(1802, 'Descoberto', 11, 3121308),
(1803, 'Desterro de Entre Rios', 11, 3121407),
(1804, 'Desterro do Melo', 11, 3121506),
(1805, 'Diamantina', 11, 3121605),
(1806, 'Diogo de Vasconcelos', 11, 3121704),
(1807, 'Dionísio', 11, 3121803),
(1808, 'Divinésia', 11, 3121902),
(1809, 'Divino', 11, 3122009),
(1810, 'Divino das Laranjeiras', 11, 3122108),
(1811, 'Divinolândia de Minas', 11, 3122207),
(1812, 'Divinópolis', 11, 3122306),
(1813, 'Divisa Alegre', 11, 3122355),
(1814, 'Divisa Nova', 11, 3122405),
(1815, 'Divisópolis', 11, 3122454),
(1816, 'Dom Bosco', 11, 3122470),
(1817, 'Dom Cavati', 11, 3122504),
(1818, 'Dom Joaquim', 11, 3122603),
(1819, 'Dom Silvério', 11, 3122702),
(1820, 'Dom Viçoso', 11, 3122801),
(1821, 'Dona Eusébia', 11, 3122900),
(1822, 'Dores de Campos', 11, 3123007),
(1823, 'Dores de Guanhães', 11, 3123106),
(1824, 'Dores do Indaiá', 11, 3123205),
(1825, 'Dores do Turvo', 11, 3123304),
(1826, 'Doresópolis', 11, 3123403),
(1827, 'Douradoquara', 11, 3123502),
(1828, 'Durandé', 11, 3123528),
(1829, 'Elói Mendes', 11, 3123601),
(1830, 'Engenheiro Caldas', 11, 3123700),
(1831, 'Engenheiro Navarro', 11, 3123809),
(1832, 'Entre Folhas', 11, 3123858),
(1833, 'Entre Rios de Minas', 11, 3123908),
(1834, 'Ervália', 11, 3124005),
(1835, 'Esmeraldas', 11, 3124104),
(1836, 'Espera Feliz', 11, 3124203),
(1837, 'Espinosa', 11, 3124302),
(1838, 'Espírito Santo do Dourado', 11, 3124401),
(1839, 'Estiva', 11, 3124500),
(1840, 'Estrela Dalva', 11, 3124609),
(1841, 'Estrela do Indaiá', 11, 3124708),
(1842, 'Estrela do Sul', 11, 3124807),
(1843, 'Eugenópolis', 11, 3124906),
(1844, 'Ewbank da Câmara', 11, 3125002),
(1845, 'Extrema', 11, 3125101),
(1846, 'Fama', 11, 3125200),
(1847, 'Faria Lemos', 11, 3125309),
(1848, 'Felício dos Santos', 11, 3125408),
(1849, 'Felisburgo', 11, 3125606),
(1850, 'Felixlândia', 11, 3125705),
(1851, 'Fernandes Tourinho', 11, 3125804),
(1852, 'Ferros', 11, 3125903),
(1853, 'Fervedouro', 11, 3125952),
(1854, 'Florestal', 11, 3126000),
(1855, 'Formiga', 11, 3126109),
(1856, 'Formoso', 11, 3126208),
(1857, 'Fortaleza de Minas', 11, 3126307),
(1858, 'Fortuna de Minas', 11, 3126406),
(1859, 'Francisco Badaró', 11, 3126505),
(1860, 'Francisco Dumont', 11, 3126604),
(1861, 'Francisco Sá', 11, 3126703),
(1862, 'Franciscópolis', 11, 3126752),
(1863, 'Frei Gaspar', 11, 3126802),
(1864, 'Frei Inocêncio', 11, 3126901),
(1865, 'Frei Lagonegro', 11, 3126950),
(1866, 'Fronteira', 11, 3127008),
(1867, 'Fronteira dos Vales', 11, 3127057),
(1868, 'Fruta de Leite', 11, 3127073),
(1869, 'Frutal', 11, 3127107),
(1870, 'Funilândia', 11, 3127206),
(1871, 'Galiléia', 11, 3127305),
(1872, 'Gameleiras', 11, 3127339),
(1873, 'Glaucilândia', 11, 3127354),
(1874, 'Goiabeira', 11, 3127370),
(1875, 'Goianá', 11, 3127388),
(1876, 'Gonçalves', 11, 3127404),
(1877, 'Gonzaga', 11, 3127503),
(1878, 'Gouveia', 11, 3127602),
(1879, 'Governador Valadares', 11, 3127701),
(1880, 'Grão Mogol', 11, 3127800),
(1881, 'Grupiara', 11, 3127909),
(1882, 'Guanhães', 11, 3128006),
(1883, 'Guapé', 11, 3128105),
(1884, 'Guaraciaba', 11, 3128204),
(1885, 'Guaraciama', 11, 3128253),
(1886, 'Guaranésia', 11, 3128303),
(1887, 'Guarani', 11, 3128402),
(1888, 'Guarará', 11, 3128501),
(1889, 'Guarda-Mor', 11, 3128600),
(1890, 'Guaxupé', 11, 3128709),
(1891, 'Guidoval', 11, 3128808),
(1892, 'Guimarânia', 11, 3128907),
(1893, 'Guiricema', 11, 3129004),
(1894, 'Gurinhatã', 11, 3129103),
(1895, 'Heliodora', 11, 3129202),
(1896, 'Iapu', 11, 3129301),
(1897, 'Ibertioga', 11, 3129400),
(1898, 'Ibiá', 11, 3129509),
(1899, 'Ibiaí', 11, 3129608),
(1900, 'Ibiracatu', 11, 3129657),
(1901, 'Ibiraci', 11, 3129707),
(1902, 'Ibirité', 11, 3129806),
(1903, 'Ibitiúra de Minas', 11, 3129905),
(1904, 'Ibituruna', 11, 3130002),
(1905, 'Icaraí de Minas', 11, 3130051),
(1906, 'Igarapé', 11, 3130101),
(1907, 'Igaratinga', 11, 3130200),
(1908, 'Iguatama', 11, 3130309),
(1909, 'Ijaci', 11, 3130408),
(1910, 'Ilicínea', 11, 3130507),
(1911, 'Imbé de Minas', 11, 3130556),
(1912, 'Inconfidentes', 11, 3130606),
(1913, 'Indaiabira', 11, 3130655),
(1914, 'Indianópolis', 11, 3130705),
(1915, 'Ingaí', 11, 3130804),
(1916, 'Inhapim', 11, 3130903),
(1917, 'Inhaúma', 11, 3131000),
(1918, 'Inimutaba', 11, 3131109),
(1919, 'Ipaba', 11, 3131158),
(1920, 'Ipanema', 11, 3131208),
(1921, 'Ipatinga', 11, 3131307),
(1922, 'Ipiaçu', 11, 3131406),
(1923, 'Ipuiúna', 11, 3131505),
(1924, 'Iraí de Minas', 11, 3131604),
(1925, 'Itabira', 11, 3131703),
(1927, 'Itabirito', 11, 3131901),
(1928, 'Itacambira', 11, 3132008),
(1929, 'Itacarambi', 11, 3132107),
(1930, 'Itaguara', 11, 3132206),
(1931, 'Itaipé', 11, 3132305),
(1932, 'Itajubá', 11, 3132404),
(1933, 'Itamarandiba', 11, 3132503),
(1934, 'Itamarati de Minas', 11, 3132602),
(1935, 'Itambacuri', 11, 3132701),
(1936, 'Itambé do Mato Dentro', 11, 3132800),
(1937, 'Itamogi', 11, 3132909),
(1938, 'Itamonte', 11, 3133006),
(1939, 'Itanhandu', 11, 3133105),
(1940, 'Itanhomi', 11, 3133204),
(1941, 'Itaobim', 11, 3133303),
(1942, 'Itapagipe', 11, 3133402),
(1943, 'Itapecerica', 11, 3133501),
(1944, 'Itapeva', 11, 3133600),
(1945, 'Itatiaiuçu', 11, 3133709),
(1946, 'Itaú de Minas', 11, 3133758),
(1947, 'Itaúna', 11, 3133808),
(1948, 'Itaverava', 11, 3133907),
(1949, 'Itinga', 11, 3134004),
(1950, 'Itueta', 11, 3134103),
(1951, 'Ituiutaba', 11, 3134202),
(1952, 'Itumirim', 11, 3134301),
(1953, 'Iturama', 11, 3134400),
(1954, 'Itutinga', 11, 3134509),
(1955, 'Jaboticatubas', 11, 3134608),
(1956, 'Jacinto', 11, 3134707),
(1957, 'Jacuí', 11, 3134806),
(1958, 'Jacutinga', 11, 3134905),
(1959, 'Jaguaraçu', 11, 3135001),
(1960, 'Jaíba', 11, 3135050),
(1961, 'Jampruca', 11, 3135076),
(1962, 'Janaúba', 11, 3135100),
(1963, 'Januária', 11, 3135209),
(1964, 'Japaraíba', 11, 3135308),
(1965, 'Japonvar', 11, 3135357),
(1966, 'Jeceaba', 11, 3135407),
(1967, 'Jenipapo de Minas', 11, 3135456),
(1968, 'Jequeri', 11, 3135506),
(1969, 'Jequitaí', 11, 3135605),
(1970, 'Jequitibá', 11, 3135704),
(1971, 'Jequitinhonha', 11, 3135803),
(1972, 'Jesuânia', 11, 3135902),
(1973, 'Joaíma', 11, 3136009),
(1974, 'Joanésia', 11, 3136108),
(1975, 'João Monlevade', 11, 3136207),
(1976, 'João Pinheiro', 11, 3136306),
(1977, 'Joaquim Felício', 11, 3136405),
(1978, 'Jordânia', 11, 3136504),
(1979, 'José Gonçalves de Minas', 11, 3136520),
(1980, 'José Raydan', 11, 3136553),
(1981, 'Josenópolis', 11, 3136579),
(1982, 'Juatuba', 11, 3136652),
(1983, 'Juiz de Fora', 11, 3136702),
(1984, 'Juramento', 11, 3136801),
(1985, 'Juruaia', 11, 3136900),
(1986, 'Juvenília', 11, 3136959),
(1987, 'Ladainha', 11, 3137007),
(1988, 'Lagamar', 11, 3137106),
(1989, 'Lagoa da Prata', 11, 3137205),
(1990, 'Lagoa dos Patos', 11, 3137304),
(1991, 'Lagoa Dourada', 11, 3137403),
(1992, 'Lagoa Formosa', 11, 3137502),
(1993, 'Lagoa Grande', 11, 3137536),
(1994, 'Lagoa Santa', 11, 3137601),
(1995, 'Lajinha', 11, 3137700),
(1996, 'Lambari', 11, 3137809),
(1997, 'Lamim', 11, 3137908),
(1998, 'Laranjal', 11, 3138005),
(1999, 'Lassance', 11, 3138104),
(2000, 'Lavras', 11, 3138203),
(2001, 'Leandro Ferreira', 11, 3138302),
(2002, 'Leme do Prado', 11, 3138351),
(2003, 'Leopoldina', 11, 3138401),
(2004, 'Liberdade', 11, 3138500),
(2005, 'Lima Duarte', 11, 3138609),
(2006, 'Limeira do Oeste', 11, 3138625),
(2007, 'Lontra', 11, 3138658),
(2008, 'Luisburgo', 11, 3138674),
(2009, 'Luislândia', 11, 3138682),
(2010, 'Luminárias', 11, 3138708),
(2011, 'Luz', 11, 3138807),
(2012, 'Machacalis', 11, 3138906),
(2013, 'Machado', 11, 3139003),
(2014, 'Madre de Deus de Minas', 11, 3139102),
(2015, 'Malacacheta', 11, 3139201),
(2016, 'Mamonas', 11, 3139250),
(2017, 'Manga', 11, 3139300),
(2018, 'Manhuaçu', 11, 3139409),
(2019, 'Manhumirim', 11, 3139508),
(2020, 'Mantena', 11, 3139607),
(2021, 'Mar de Espanha', 11, 3139805),
(2022, 'Maravilhas', 11, 3139706),
(2023, 'Maria da Fé', 11, 3139904),
(2024, 'Mariana', 11, 3140001),
(2025, 'Marilac', 11, 3140100),
(2026, 'Mário Campos', 11, 3140159),
(2027, 'Maripá de Minas', 11, 3140209),
(2028, 'Marliéria', 11, 3140308),
(2029, 'Marmelópolis', 11, 3140407),
(2030, 'Martinho Campos', 11, 3140506),
(2031, 'Martins Soares', 11, 3140530),
(2032, 'Mata Verde', 11, 3140555),
(2033, 'Materlândia', 11, 3140605),
(2034, 'Mateus Leme', 11, 3140704),
(2035, 'Mathias Lobato', 11, 3171501),
(2036, 'Matias Barbosa', 11, 3140803),
(2037, 'Matias Cardoso', 11, 3140852),
(2038, 'Matipó', 11, 3140902),
(2039, 'Mato Verde', 11, 3141009),
(2040, 'Matozinhos', 11, 3141108),
(2041, 'Matutina', 11, 3141207),
(2042, 'Medeiros', 11, 3141306),
(2043, 'Medina', 11, 3141405),
(2044, 'Mendes Pimentel', 11, 3141504),
(2045, 'Mercês', 11, 3141603),
(2046, 'Mesquita', 11, 3141702),
(2047, 'Minas Novas', 11, 3141801),
(2048, 'Minduri', 11, 3141900),
(2049, 'Mirabela', 11, 3142007),
(2050, 'Miradouro', 11, 3142106),
(2051, 'Miraí', 11, 3142205),
(2052, 'Miravânia', 11, 3142254),
(2053, 'Moeda', 11, 3142304),
(2054, 'Moema', 11, 3142403),
(2055, 'Monjolos', 11, 3142502),
(2056, 'Monsenhor Paulo', 11, 3142601),
(2057, 'Montalvânia', 11, 3142700),
(2058, 'Monte Alegre de Minas', 11, 3142809),
(2059, 'Monte Azul', 11, 3142908),
(2060, 'Monte Belo', 11, 3143005),
(2061, 'Monte Carmelo', 11, 3143104),
(2062, 'Monte Formoso', 11, 3143153),
(2063, 'Monte Santo de Minas', 11, 3143203),
(2064, 'Monte Sião', 11, 3143401),
(2065, 'Montes Claros', 11, 3143302),
(2066, 'Montezuma', 11, 3143450),
(2067, 'Morada Nova de Minas', 11, 3143500),
(2068, 'Morro da Garça', 11, 3143609),
(2069, 'Morro do Pilar', 11, 3143708),
(2070, 'Munhoz', 11, 3143807),
(2071, 'Muriaé', 11, 3143906),
(2072, 'Mutum', 11, 3144003),
(2073, 'Muzambinho', 11, 3144102),
(2074, 'Nacip Raydan', 11, 3144201),
(2075, 'Nanuque', 11, 3144300),
(2076, 'Naque', 11, 3144359),
(2077, 'Natalândia', 11, 3144375),
(2078, 'Natércia', 11, 3144409),
(2079, 'Nazareno', 11, 3144508),
(2080, 'Nepomuceno', 11, 3144607),
(2081, 'Ninheira', 11, 3144656),
(2082, 'Nova Belém', 11, 3144672),
(2083, 'Nova Era', 11, 3144706),
(2084, 'Nova Lima', 11, 3144805),
(2085, 'Nova Módica', 11, 3144904),
(2086, 'Nova Ponte', 11, 3145000),
(2087, 'Nova Porteirinha', 11, 3145059),
(2088, 'Nova Resende', 11, 3145109),
(2089, 'Nova Serrana', 11, 3145208),
(2090, 'Nova União', 11, 3136603),
(2091, 'Novo Cruzeiro', 11, 3145307),
(2092, 'Novo Oriente de Minas', 11, 3145356),
(2093, 'Novorizonte', 11, 3145372),
(2094, 'Olaria', 11, 3145406),
(2095, 'Olhos-d`Água', 11, 3145455),
(2096, 'Olímpio Noronha', 11, 3145505),
(2097, 'Oliveira', 11, 3145604),
(2098, 'Oliveira Fortes', 11, 3145703),
(2099, 'Onça de Pitangui', 11, 3145802),
(2100, 'Oratórios', 11, 3145851),
(2101, 'Orizânia', 11, 3145877),
(2102, 'Ouro Branco', 11, 3145901),
(2103, 'Ouro Fino', 11, 3146008),
(2104, 'Ouro Preto', 11, 3146107),
(2105, 'Ouro Verde de Minas', 11, 3146206),
(2106, 'Padre Carvalho', 11, 3146255),
(2107, 'Padre Paraíso', 11, 3146305),
(2108, 'Pai Pedro', 11, 3146552),
(2109, 'Paineiras', 11, 3146404),
(2110, 'Pains', 11, 3146503),
(2111, 'Paiva', 11, 3146602),
(2112, 'Palma', 11, 3146701),
(2113, 'Palmópolis', 11, 3146750),
(2114, 'Papagaios', 11, 3146909),
(2115, 'Pará de Minas', 11, 3147105),
(2116, 'Paracatu', 11, 3147006),
(2117, 'Paraguaçu', 11, 3147204),
(2118, 'Paraisópolis', 11, 3147303),
(2119, 'Paraopeba', 11, 3147402),
(2120, 'Passa Quatro', 11, 3147600),
(2121, 'Passa Tempo', 11, 3147709),
(2122, 'Passabém', 11, 3147501),
(2123, 'Passa-Vinte', 11, 3147808),
(2124, 'Passos', 11, 3147907),
(2125, 'Patis', 11, 3147956),
(2126, 'Patos de Minas', 11, 3148004),
(2127, 'Patrocínio', 11, 3148103),
(2128, 'Patrocínio do Muriaé', 11, 3148202),
(2129, 'Paula Cândido', 11, 3148301),
(2130, 'Paulistas', 11, 3148400),
(2131, 'Pavão', 11, 3148509),
(2132, 'Peçanha', 11, 3148608),
(2133, 'Pedra Azul', 11, 3148707),
(2134, 'Pedra Bonita', 11, 3148756),
(2135, 'Pedra do Anta', 11, 3148806),
(2136, 'Pedra do Indaiá', 11, 3148905),
(2137, 'Pedra Dourada', 11, 3149002),
(2138, 'Pedralva', 11, 3149101),
(2139, 'Pedras de Maria da Cruz', 11, 3149150),
(2140, 'Pedrinópolis', 11, 3149200),
(2141, 'Pedro Leopoldo', 11, 3149309),
(2142, 'Pedro Teixeira', 11, 3149408),
(2143, 'Pequeri', 11, 3149507),
(2144, 'Pequi', 11, 3149606),
(2145, 'Perdigão', 11, 3149705),
(2146, 'Perdizes', 11, 3149804),
(2147, 'Perdões', 11, 3149903),
(2148, 'Periquito', 11, 3149952),
(2149, 'Pescador', 11, 3150000),
(2150, 'Piau', 11, 3150109),
(2151, 'Piedade de Caratinga', 11, 3150158),
(2152, 'Piedade de Ponte Nova', 11, 3150208),
(2153, 'Piedade do Rio Grande', 11, 3150307),
(2154, 'Piedade dos Gerais', 11, 3150406),
(2155, 'Pimenta', 11, 3150505),
(2156, 'Pingo-d`Água', 11, 3150539),
(2157, 'Pintópolis', 11, 3150570),
(2158, 'Piracema', 11, 3150604),
(2159, 'Pirajuba', 11, 3150703),
(2160, 'Piranga', 11, 3150802),
(2161, 'Piranguçu', 11, 3150901),
(2162, 'Piranguinho', 11, 3151008),
(2163, 'Pirapetinga', 11, 3151107),
(2164, 'Pirapora', 11, 3151206),
(2165, 'Piraúba', 11, 3151305),
(2166, 'Pitangui', 11, 3151404),
(2167, 'Piumhi', 11, 3151503),
(2168, 'Planura', 11, 3151602),
(2169, 'Poço Fundo', 11, 3151701),
(2170, 'Poços de Caldas', 11, 3151800),
(2171, 'Pocrane', 11, 3151909),
(2172, 'Pompéu', 11, 3152006),
(2173, 'Ponte Nova', 11, 3152105),
(2174, 'Ponto Chique', 11, 3152131),
(2175, 'Ponto dos Volantes', 11, 3152170),
(2176, 'Porteirinha', 11, 3152204),
(2177, 'Porto Firme', 11, 3152303),
(2178, 'Poté', 11, 3152402),
(2179, 'Pouso Alegre', 11, 3152501),
(2180, 'Pouso Alto', 11, 3152600),
(2181, 'Prados', 11, 3152709),
(2182, 'Prata', 11, 3152808),
(2183, 'Pratápolis', 11, 3152907),
(2184, 'Pratinha', 11, 3153004),
(2185, 'Presidente Bernardes', 11, 3153103),
(2186, 'Presidente Juscelino', 11, 3153202),
(2187, 'Presidente Kubitschek', 11, 3153301),
(2188, 'Presidente Olegário', 11, 3153400),
(2189, 'Prudente de Morais', 11, 3153608),
(2190, 'Quartel Geral', 11, 3153707),
(2191, 'Queluzito', 11, 3153806),
(2192, 'Raposos', 11, 3153905),
(2193, 'Raul Soares', 11, 3154002),
(2194, 'Recreio', 11, 3154101),
(2195, 'Reduto', 11, 3154150),
(2196, 'Resende Costa', 11, 3154200),
(2197, 'Resplendor', 11, 3154309),
(2198, 'Ressaquinha', 11, 3154408),
(2199, 'Riachinho', 11, 3154457),
(2200, 'Riacho dos Machados', 11, 3154507),
(2201, 'Ribeirão das Neves', 11, 3154606),
(2202, 'Ribeirão Vermelho', 11, 3154705),
(2203, 'Rio Acima', 11, 3154804),
(2204, 'Rio Casca', 11, 3154903),
(2205, 'Rio do Prado', 11, 3155108),
(2206, 'Rio Doce', 11, 3155009),
(2207, 'Rio Espera', 11, 3155207),
(2208, 'Rio Manso', 11, 3155306),
(2209, 'Rio Novo', 11, 3155405),
(2210, 'Rio Paranaíba', 11, 3155504),
(2211, 'Rio Pardo de Minas', 11, 3155603),
(2212, 'Rio Piracicaba', 11, 3155702),
(2213, 'Rio Pomba', 11, 3155801),
(2214, 'Rio Preto', 11, 3155900),
(2215, 'Rio Vermelho', 11, 3156007),
(2216, 'Ritápolis', 11, 3156106),
(2217, 'Rochedo de Minas', 11, 3156205),
(2218, 'Rodeiro', 11, 3156304),
(2219, 'Romaria', 11, 3156403),
(2220, 'Rosário da Limeira', 11, 3156452),
(2221, 'Rubelita', 11, 3156502),
(2222, 'Rubim', 11, 3156601),
(2223, 'Sabará', 11, 3156700),
(2224, 'Sabinópolis', 11, 3156809),
(2225, 'Sacramento', 11, 3156908),
(2226, 'Salinas', 11, 3157005),
(2227, 'Salto da Divisa', 11, 3157104),
(2228, 'Santa Bárbara', 11, 3157203),
(2229, 'Santa Bárbara do Leste', 11, 3157252),
(2230, 'Santa Bárbara do Monte Verde', 11, 3157278),
(2231, 'Santa Bárbara do Tugúrio', 11, 3157302),
(2232, 'Santa Cruz de Minas', 11, 3157336),
(2233, 'Santa Cruz de Salinas', 11, 3157377),
(2234, 'Santa Cruz do Escalvado', 11, 3157401),
(2235, 'Santa Efigênia de Minas', 11, 3157500),
(2236, 'Santa Fé de Minas', 11, 3157609),
(2237, 'Santa Helena de Minas', 11, 3157658),
(2238, 'Santa Juliana', 11, 3157708),
(2239, 'Santa Luzia', 11, 3157807),
(2240, 'Santa Margarida', 11, 3157906),
(2241, 'Santa Maria de Itabira', 11, 3158003),
(2242, 'Santa Maria do Salto', 11, 3158102),
(2243, 'Santa Maria do Suaçuí', 11, 3158201),
(2244, 'Santa Rita de Caldas', 11, 3159209),
(2245, 'Santa Rita de Ibitipoca', 11, 3159407),
(2246, 'Santa Rita de Jacutinga', 11, 3159308),
(2247, 'Santa Rita de Minas', 11, 3159357),
(2248, 'Santa Rita do Itueto', 11, 3159506),
(2249, 'Santa Rita do Sapucaí', 11, 3159605),
(2250, 'Santa Rosa da Serra', 11, 3159704),
(2251, 'Santa Vitória', 11, 3159803),
(2252, 'Santana da Vargem', 11, 3158300),
(2253, 'Santana de Cataguases', 11, 3158409),
(2254, 'Santana de Pirapama', 11, 3158508),
(2255, 'Santana do Deserto', 11, 3158607),
(2256, 'Santana do Garambéu', 11, 3158706),
(2257, 'Santana do Jacaré', 11, 3158805),
(2258, 'Santana do Manhuaçu', 11, 3158904),
(2259, 'Santana do Paraíso', 11, 3158953),
(2260, 'Santana do Riacho', 11, 3159001),
(2261, 'Santana dos Montes', 11, 3159100),
(2262, 'Santo Antônio do Amparo', 11, 3159902),
(2263, 'Santo Antônio do Aventureiro', 11, 3160009),
(2264, 'Santo Antônio do Grama', 11, 3160108),
(2265, 'Santo Antônio do Itambé', 11, 3160207),
(2266, 'Santo Antônio do Jacinto', 11, 3160306),
(2267, 'Santo Antônio do Monte', 11, 3160405),
(2268, 'Santo Antônio do Retiro', 11, 3160454),
(2269, 'Santo Antônio do Rio Abaixo', 11, 3160504),
(2270, 'Santo Hipólito', 11, 3160603),
(2271, 'Santos Dumont', 11, 3160702),
(2272, 'São Bento Abade', 11, 3160801),
(2273, 'São Brás do Suaçuí', 11, 3160900),
(2274, 'São Domingos das Dores', 11, 3160959),
(2275, 'São Domingos do Prata', 11, 3161007),
(2276, 'São Félix de Minas', 11, 3161056),
(2277, 'São Francisco', 11, 3161106),
(2278, 'São Francisco de Paula', 11, 3161205),
(2279, 'São Francisco de Sales', 11, 3161304),
(2280, 'São Francisco do Glória', 11, 3161403),
(2281, 'São Geraldo', 11, 3161502),
(2282, 'São Geraldo da Piedade', 11, 3161601),
(2283, 'São Geraldo do Baixio', 11, 3161650),
(2284, 'São Gonçalo do Abaeté', 11, 3161700),
(2285, 'São Gonçalo do Pará', 11, 3161809),
(2286, 'São Gonçalo do Rio Abaixo', 11, 3161908),
(2287, 'São Gonçalo do Rio Preto', 11, 3125507),
(2288, 'São Gonçalo do Sapucaí', 11, 3162005),
(2289, 'São Gotardo', 11, 3162104),
(2290, 'São João Batista do Glória', 11, 3162203),
(2291, 'São João da Lagoa', 11, 3162252),
(2292, 'São João da Mata', 11, 3162302),
(2293, 'São João da Ponte', 11, 3162401),
(2294, 'São João das Missões', 11, 3162450),
(2295, 'São João del Rei', 11, 3162500),
(2296, 'São João do Manhuaçu', 11, 3162559),
(2297, 'São João do Manteninha', 11, 3162575),
(2298, 'São João do Oriente', 11, 3162609),
(2299, 'São João do Pacuí', 11, 3162658),
(2300, 'São João do Paraíso', 11, 3162708),
(2301, 'São João Evangelista', 11, 3162807),
(2302, 'São João Nepomuceno', 11, 3162906),
(2303, 'São Joaquim de Bicas', 11, 3162922),
(2304, 'São José da Barra', 11, 3162948),
(2305, 'São José da Lapa', 11, 3162955),
(2306, 'São José da Safira', 11, 3163003),
(2307, 'São José da Varginha', 11, 3163102),
(2308, 'São José do Alegre', 11, 3163201),
(2309, 'São José do Divino', 11, 3163300),
(2310, 'São José do Goiabal', 11, 3163409),
(2311, 'São José do Jacuri', 11, 3163508),
(2312, 'São José do Mantimento', 11, 3163607),
(2313, 'São Lourenço', 11, 3163706),
(2314, 'São Miguel do Anta', 11, 3163805),
(2315, 'São Pedro da União', 11, 3163904),
(2316, 'São Pedro do Suaçuí', 11, 3164100),
(2317, 'São Pedro dos Ferros', 11, 3164001),
(2318, 'São Romão', 11, 3164209),
(2319, 'São Roque de Minas', 11, 3164308),
(2320, 'São Sebastião da Bela Vista', 11, 3164407),
(2321, 'São Sebastião da Vargem Alegre', 11, 3164431),
(2322, 'São Sebastião do Anta', 11, 3164472),
(2323, 'São Sebastião do Maranhão', 11, 3164506),
(2324, 'São Sebastião do Oeste', 11, 3164605),
(2325, 'São Sebastião do Paraíso', 11, 3164704),
(2326, 'São Sebastião do Rio Preto', 11, 3164803),
(2327, 'São Sebastião do Rio Verde', 11, 3164902),
(2328, 'São Thomé das Letras', 11, 3165206),
(2329, 'São Tiago', 11, 3165008),
(2330, 'São Tomás de Aquino', 11, 3165107),
(2331, 'São Vicente de Minas', 11, 3165305),
(2332, 'Sapucaí-Mirim', 11, 3165404),
(2333, 'Sardoá', 11, 3165503),
(2334, 'Sarzedo', 11, 3165537),
(2335, 'Sem-Peixe', 11, 3165560),
(2336, 'Senador Amaral', 11, 3165578),
(2337, 'Senador Cortes', 11, 3165602),
(2338, 'Senador Firmino', 11, 3165701),
(2339, 'Senador José Bento', 11, 3165800),
(2340, 'Senador Modestino Gonçalves', 11, 3165909),
(2341, 'Senhora de Oliveira', 11, 3166006),
(2342, 'Senhora do Porto', 11, 3166105),
(2343, 'Senhora dos Remédios', 11, 3166204),
(2344, 'Sericita', 11, 3166303),
(2345, 'Seritinga', 11, 3166402),
(2346, 'Serra Azul de Minas', 11, 3166501),
(2347, 'Serra da Saudade', 11, 3166600),
(2348, 'Serra do Salitre', 11, 3166808),
(2349, 'Serra dos Aimorés', 11, 3166709),
(2350, 'Serrania', 11, 3166907),
(2351, 'Serranópolis de Minas', 11, 3166956),
(2352, 'Serranos', 11, 3167004),
(2353, 'Serro', 11, 3167103),
(2354, 'Sete Lagoas', 11, 3167202),
(2355, 'Setubinha', 11, 3165552),
(2356, 'Silveirânia', 11, 3167301),
(2357, 'Silvianópolis', 11, 3167400),
(2358, 'Simão Pereira', 11, 3167509),
(2359, 'Simonésia', 11, 3167608),
(2360, 'Sobrália', 11, 3167707),
(2361, 'Soledade de Minas', 11, 3167806),
(2362, 'Tabuleiro', 11, 3167905),
(2363, 'Taiobeiras', 11, 3168002),
(2364, 'Taparuba', 11, 3168051),
(2365, 'Tapira', 11, 3168101),
(2366, 'Tapiraí', 11, 3168200),
(2367, 'Taquaraçu de Minas', 11, 3168309),
(2368, 'Tarumirim', 11, 3168408),
(2369, 'Teixeiras', 11, 3168507),
(2370, 'Teófilo Otoni', 11, 3168606),
(2371, 'Timóteo', 11, 3168705),
(2372, 'Tiradentes', 11, 3168804),
(2373, 'Tiros', 11, 3168903),
(2374, 'Tocantins', 11, 3169000),
(2375, 'Tocos do Moji', 11, 3169059),
(2376, 'Toledo', 11, 3169109),
(2377, 'Tombos', 11, 3169208),
(2378, 'Três Corações', 11, 3169307),
(2379, 'Três Marias', 11, 3169356),
(2380, 'Três Pontas', 11, 3169406),
(2381, 'Tumiritinga', 11, 3169505),
(2382, 'Tupaciguara', 11, 3169604),
(2383, 'Turmalina', 11, 3169703),
(2384, 'Turvolândia', 11, 3169802),
(2385, 'Ubá', 11, 3169901),
(2386, 'Ubaí', 11, 3170008),
(2387, 'Ubaporanga', 11, 3170057),
(2388, 'Uberaba', 11, 3170107),
(2389, 'Uberlândia', 11, 3170206),
(2390, 'Umburatiba', 11, 3170305),
(2391, 'Unaí', 11, 3170404),
(2392, 'União de Minas', 11, 3170438),
(2393, 'Uruana de Minas', 11, 3170479),
(2394, 'Urucânia', 11, 3170503),
(2395, 'Urucuia', 11, 3170529),
(2396, 'Vargem Alegre', 11, 3170578),
(2397, 'Vargem Bonita', 11, 3170602),
(2398, 'Vargem Grande do Rio Pardo', 11, 3170651),
(2399, 'Varginha', 11, 3170701),
(2400, 'Varjão de Minas', 11, 3170750),
(2401, 'Várzea da Palma', 11, 3170800),
(2402, 'Varzelândia', 11, 3170909),
(2403, 'Vazante', 11, 3171006),
(2404, 'Verdelândia', 11, 3171030),
(2405, 'Veredinha', 11, 3171071),
(2406, 'Veríssimo', 11, 3171105),
(2407, 'Vermelho Novo', 11, 3171154),
(2408, 'Vespasiano', 11, 3171204),
(2409, 'Viçosa', 11, 3171303),
(2410, 'Vieiras', 11, 3171402),
(2411, 'Virgem da Lapa', 11, 3171600),
(2412, 'Virgínia', 11, 3171709),
(2413, 'Virginópolis', 11, 3171808),
(2414, 'Virgolândia', 11, 3171907),
(2415, 'Visconde do Rio Branco', 11, 3172004),
(2416, 'Volta Grande', 11, 3172103),
(2417, 'Wenceslau Braz', 11, 3172202),
(2418, 'Abaetetuba', 14, 1500107),
(2419, 'Abel Figueiredo', 14, 1500131),
(2420, 'Acará', 14, 1500206),
(2421, 'Afuá', 14, 1500305),
(2422, 'Água Azul do Norte', 14, 1500347),
(2423, 'Alenquer', 14, 1500404),
(2424, 'Almeirim', 14, 1500503),
(2425, 'Altamira', 14, 1500602),
(2426, 'Anajás', 14, 1500701),
(2427, 'Ananindeua', 14, 1500800),
(2428, 'Anapu', 14, 1500859),
(2429, 'Augusto Corrêa', 14, 1500909),
(2430, 'Aurora do Pará', 14, 1500958),
(2431, 'Aveiro', 14, 1501006),
(2432, 'Bagre', 14, 1501105),
(2433, 'Baião', 14, 1501204),
(2434, 'Bannach', 14, 1501253),
(2435, 'Barcarena', 14, 1501303),
(2436, 'Belém', 14, 1501402),
(2437, 'Belterra', 14, 1501451),
(2438, 'Benevides', 14, 1501501),
(2439, 'Bom Jesus do Tocantins', 14, 1501576),
(2440, 'Bonito', 14, 1501600),
(2441, 'Bragança', 14, 1501709),
(2442, 'Brasil Novo', 14, 1501725),
(2443, 'Brejo Grande do Araguaia', 14, 1501758),
(2444, 'Breu Branco', 14, 1501782),
(2445, 'Breves', 14, 1501808),
(2446, 'Bujaru', 14, 1501907),
(2447, 'Cachoeira do Arari', 14, 1502004),
(2448, 'Cachoeira do Piriá', 14, 1501956),
(2449, 'Cametá', 14, 1502103),
(2450, 'Canaã dos Carajás', 14, 1502152),
(2451, 'Capanema', 14, 1502202),
(2452, 'Capitão Poço', 14, 1502301),
(2453, 'Castanhal', 14, 1502400),
(2454, 'Chaves', 14, 1502509),
(2455, 'Colares', 14, 1502608),
(2456, 'Conceição do Araguaia', 14, 1502707),
(2457, 'Concórdia do Pará', 14, 1502756),
(2458, 'Cumaru do Norte', 14, 1502764),
(2459, 'Curionópolis', 14, 1502772),
(2460, 'Curralinho', 14, 1502806),
(2461, 'Curuá', 14, 1502855),
(2462, 'Curuçá', 14, 1502905),
(2463, 'Dom Eliseu', 14, 1502939),
(2464, 'Eldorado dos Carajás', 14, 1502954),
(2465, 'Faro', 14, 1503002),
(2466, 'Floresta do Araguaia', 14, 1503044),
(2467, 'Garrafão do Norte', 14, 1503077),
(2468, 'Goianésia do Pará', 14, 1503093),
(2469, 'Gurupá', 14, 1503101),
(2470, 'Igarapé-Açu', 14, 1503200),
(2471, 'Igarapé-Miri', 14, 1503309),
(2472, 'Inhangapi', 14, 1503408),
(2473, 'Ipixuna do Pará', 14, 1503457),
(2474, 'Irituia', 14, 1503507),
(2475, 'Itaituba', 14, 1503606),
(2476, 'Itupiranga', 14, 1503705),
(2477, 'Jacareacanga', 14, 1503754),
(2478, 'Jacundá', 14, 1503804),
(2479, 'Juruti', 14, 1503903),
(2480, 'Limoeiro do Ajuru', 14, 1504000),
(2481, 'Mãe do Rio', 14, 1504059),
(2482, 'Magalhães Barata', 14, 1504109),
(2483, 'Marabá', 14, 1504208),
(2484, 'Maracanã', 14, 1504307),
(2485, 'Marapanim', 14, 1504406),
(2486, 'Marituba', 14, 1504422),
(2487, 'Medicilândia', 14, 1504455),
(2488, 'Melgaço', 14, 1504505),
(2489, 'Mocajuba', 14, 1504604),
(2490, 'Moju', 14, 1504703),
(2491, 'Monte Alegre', 14, 1504802),
(2492, 'Muaná', 14, 1504901),
(2493, 'Nova Esperança do Piriá', 14, 1504950),
(2494, 'Nova Ipixuna', 14, 1504976),
(2495, 'Nova Timboteua', 14, 1505007),
(2496, 'Novo Progresso', 14, 1505031),
(2497, 'Novo Repartimento', 14, 1505064),
(2498, 'Óbidos', 14, 1505106),
(2499, 'Oeiras do Pará', 14, 1505205),
(2500, 'Oriximiná', 14, 1505304),
(2501, 'Ourém', 14, 1505403),
(2502, 'Ourilândia do Norte', 14, 1505437),
(2503, 'Pacajá', 14, 1505486),
(2504, 'Palestina do Pará', 14, 1505494),
(2505, 'Paragominas', 14, 1505502),
(2506, 'Parauapebas', 14, 1505536),
(2507, 'Pau d`Arco', 14, 1505551),
(2508, 'Peixe-Boi', 14, 1505601),
(2509, 'Piçarra', 14, 1505635),
(2510, 'Placas', 14, 1505650),
(2511, 'Ponta de Pedras', 14, 1505700),
(2512, 'Portel', 14, 1505809),
(2513, 'Porto de Moz', 14, 1505908),
(2514, 'Prainha', 14, 1506005),
(2515, 'Primavera', 14, 1506104),
(2516, 'Quatipuru', 14, 1506112),
(2517, 'Redenção', 14, 1506138),
(2518, 'Rio Maria', 14, 1506161),
(2519, 'Rondon do Pará', 14, 1506187),
(2520, 'Rurópolis', 14, 1506195),
(2521, 'Salinópolis', 14, 1506203),
(2522, 'Salvaterra', 14, 1506302),
(2523, 'Santa Bárbara do Pará', 14, 1506351),
(2524, 'Santa Cruz do Arari', 14, 1506401),
(2525, 'Santa Isabel do Pará', 14, 1506500),
(2526, 'Santa Luzia do Pará', 14, 1506559),
(2527, 'Santa Maria das Barreiras', 14, 1506583),
(2528, 'Santa Maria do Pará', 14, 1506609),
(2529, 'Santana do Araguaia', 14, 1506708),
(2530, 'Santarém', 14, 1506807),
(2531, 'Santarém Novo', 14, 1506906),
(2532, 'Santo Antônio do Tauá', 14, 1507003),
(2533, 'São Caetano de Odivelas', 14, 1507102),
(2534, 'São Domingos do Araguaia', 14, 1507151),
(2535, 'São Domingos do Capim', 14, 1507201),
(2536, 'São Félix do Xingu', 14, 1507300),
(2537, 'São Francisco do Pará', 14, 1507409),
(2538, 'São Geraldo do Araguaia', 14, 1507458),
(2539, 'São João da Ponta', 14, 1507466),
(2540, 'São João de Pirabas', 14, 1507474),
(2541, 'São João do Araguaia', 14, 1507508),
(2542, 'São Miguel do Guamá', 14, 1507607),
(2543, 'São Sebastião da Boa Vista', 14, 1507706),
(2544, 'Sapucaia', 14, 1507755),
(2545, 'Senador José Porfírio', 14, 1507805),
(2546, 'Soure', 14, 1507904),
(2547, 'Tailândia', 14, 1507953),
(2548, 'Terra Alta', 14, 1507961),
(2549, 'Terra Santa', 14, 1507979),
(2550, 'Tomé-Açu', 14, 1508001),
(2551, 'Tracuateua', 14, 1508035),
(2552, 'Trairão', 14, 1508050),
(2553, 'Tucumã', 14, 1508084),
(2554, 'Tucuruí', 14, 1508100),
(2555, 'Ulianópolis', 14, 1508126),
(2556, 'Uruará', 14, 1508159),
(2557, 'Vigia', 14, 1508209),
(2558, 'Viseu', 14, 1508308),
(2559, 'Vitória do Xingu', 14, 1508357),
(2560, 'Xinguara', 14, 1508407),
(2561, 'Água Branca', 15, 2500106),
(2562, 'Aguiar', 15, 2500205),
(2563, 'Alagoa Grande', 15, 2500304),
(2564, 'Alagoa Nova', 15, 2500403),
(2565, 'Alagoinha', 15, 2500502),
(2566, 'Alcantil', 15, 2500536),
(2567, 'Algodão de Jandaíra', 15, 2500577),
(2568, 'Alhandra', 15, 2500601),
(2569, 'Amparo', 15, 2500734),
(2570, 'Aparecida', 15, 2500775),
(2571, 'Araçagi', 15, 2500809),
(2572, 'Arara', 15, 2500908),
(2573, 'Araruna', 15, 2501005),
(2574, 'Areia', 15, 2501104),
(2575, 'Areia de Baraúnas', 15, 2501153),
(2576, 'Areial', 15, 2501203),
(2577, 'Aroeiras', 15, 2501302),
(2578, 'Assunção', 15, 2501351),
(2579, 'Baía da Traição', 15, 2501401),
(2580, 'Bananeiras', 15, 2501500),
(2581, 'Baraúna', 15, 2501534),
(2582, 'Barra de Santa Rosa', 15, 2501609),
(2583, 'Barra de Santana', 15, 2501575),
(2584, 'Barra de São Miguel', 15, 2501708),
(2585, 'Bayeux', 15, 2501807),
(2586, 'Belém', 15, 2501906),
(2587, 'Belém do Brejo do Cruz', 15, 2502003),
(2588, 'Bernardino Batista', 15, 2502052),
(2589, 'Boa Ventura', 15, 2502102),
(2590, 'Boa Vista', 15, 2502151),
(2591, 'Bom Jesus', 15, 2502201),
(2592, 'Bom Sucesso', 15, 2502300),
(2593, 'Bonito de Santa Fé', 15, 2502409),
(2594, 'Boqueirão', 15, 2502508),
(2595, 'Borborema', 15, 2502706),
(2596, 'Brejo do Cruz', 15, 2502805),
(2597, 'Brejo dos Santos', 15, 2502904),
(2598, 'Caaporã', 15, 2503001),
(2599, 'Cabaceiras', 15, 2503100),
(2600, 'Cabedelo', 15, 2503209),
(2601, 'Cachoeira dos Índios', 15, 2503308),
(2602, 'Cacimba de Areia', 15, 2503407),
(2603, 'Cacimba de Dentro', 15, 2503506),
(2604, 'Cacimbas', 15, 2503555),
(2605, 'Caiçara', 15, 2503605),
(2606, 'Cajazeiras', 15, 2503704),
(2607, 'Cajazeirinhas', 15, 2503753),
(2608, 'Caldas Brandão', 15, 2503803),
(2609, 'Camalaú', 15, 2503902),
(2610, 'Campina Grande', 15, 2504009),
(2611, 'Campo de Santana', 15, 2516409),
(2612, 'Capim', 15, 2504033),
(2613, 'Caraúbas', 15, 2504074),
(2614, 'Carrapateira', 15, 2504108),
(2615, 'Casserengue', 15, 2504157),
(2616, 'Catingueira', 15, 2504207),
(2617, 'Catolé do Rocha', 15, 2504306),
(2618, 'Caturité', 15, 2504355),
(2619, 'Conceição', 15, 2504405),
(2620, 'Condado', 15, 2504504),
(2621, 'Conde', 15, 2504603),
(2622, 'Congo', 15, 2504702),
(2623, 'Coremas', 15, 2504801),
(2624, 'Coxixola', 15, 2504850),
(2625, 'Cruz do Espírito Santo', 15, 2504900),
(2626, 'Cubati', 15, 2505006),
(2627, 'Cuité', 15, 2505105),
(2628, 'Cuité de Mamanguape', 15, 2505238),
(2629, 'Cuitegi', 15, 2505204),
(2630, 'Curral de Cima', 15, 2505279),
(2631, 'Curral Velho', 15, 2505303),
(2632, 'Damião', 15, 2505352),
(2633, 'Desterro', 15, 2505402),
(2634, 'Diamante', 15, 2505600),
(2635, 'Dona Inês', 15, 2505709),
(2636, 'Duas Estradas', 15, 2505808),
(2637, 'Emas', 15, 2505907),
(2638, 'Esperança', 15, 2506004),
(2639, 'Fagundes', 15, 2506103),
(2640, 'Frei Martinho', 15, 2506202),
(2641, 'Gado Bravo', 15, 2506251),
(2642, 'Guarabira', 15, 2506301),
(2643, 'Gurinhém', 15, 2506400),
(2644, 'Gurjão', 15, 2506509),
(2645, 'Ibiara', 15, 2506608),
(2646, 'Igaracy', 15, 2502607),
(2647, 'Imaculada', 15, 2506707),
(2648, 'Ingá', 15, 2506806),
(2649, 'Itabaiana', 15, 2506905),
(2650, 'Itaporanga', 15, 2507002),
(2651, 'Itapororoca', 15, 2507101),
(2652, 'Itatuba', 15, 2507200),
(2653, 'Jacaraú', 15, 2507309),
(2654, 'Jericó', 15, 2507408),
(2655, 'João Pessoa', 15, 2507507),
(2656, 'Juarez Távora', 15, 2507606),
(2657, 'Juazeirinho', 15, 2507705),
(2658, 'Junco do Seridó', 15, 2507804),
(2659, 'Juripiranga', 15, 2507903),
(2660, 'Juru', 15, 2508000),
(2661, 'Lagoa', 15, 2508109),
(2662, 'Lagoa de Dentro', 15, 2508208),
(2663, 'Lagoa Seca', 15, 2508307),
(2664, 'Lastro', 15, 2508406),
(2665, 'Livramento', 15, 2508505),
(2666, 'Logradouro', 15, 2508554),
(2667, 'Lucena', 15, 2508604),
(2668, 'Mãe d`Água', 15, 2508703),
(2669, 'Malta', 15, 2508802),
(2670, 'Mamanguape', 15, 2508901),
(2671, 'Manaíra', 15, 2509008),
(2672, 'Marcação', 15, 2509057),
(2673, 'Mari', 15, 2509107),
(2674, 'Marizópolis', 15, 2509156),
(2675, 'Massaranduba', 15, 2509206),
(2676, 'Mataraca', 15, 2509305),
(2677, 'Matinhas', 15, 2509339),
(2678, 'Mato Grosso', 15, 2509370),
(2679, 'Maturéia', 15, 2509396),
(2680, 'Mogeiro', 15, 2509404),
(2681, 'Montadas', 15, 2509503),
(2682, 'Monte Horebe', 15, 2509602),
(2683, 'Monteiro', 15, 2509701),
(2684, 'Mulungu', 15, 2509800),
(2685, 'Natuba', 15, 2509909),
(2686, 'Nazarezinho', 15, 2510006),
(2687, 'Nova Floresta', 15, 2510105),
(2688, 'Nova Olinda', 15, 2510204),
(2689, 'Nova Palmeira', 15, 2510303),
(2690, 'Olho d`Água', 15, 2510402),
(2691, 'Olivedos', 15, 2510501),
(2692, 'Ouro Velho', 15, 2510600),
(2693, 'Parari', 15, 2510659),
(2694, 'Passagem', 15, 2510709),
(2695, 'Patos', 15, 2510808),
(2696, 'Paulista', 15, 2510907),
(2697, 'Pedra Branca', 15, 2511004),
(2698, 'Pedra Lavrada', 15, 2511103),
(2699, 'Pedras de Fogo', 15, 2511202),
(2700, 'Pedro Régis', 15, 2512721),
(2701, 'Piancó', 15, 2511301),
(2702, 'Picuí', 15, 2511400),
(2703, 'Pilar', 15, 2511509),
(2704, 'Pilões', 15, 2511608),
(2705, 'Pilõezinhos', 15, 2511707),
(2706, 'Pirpirituba', 15, 2511806),
(2707, 'Pitimbu', 15, 2511905),
(2708, 'Pocinhos', 15, 2512002),
(2709, 'Poço Dantas', 15, 2512036),
(2710, 'Poço de José de Moura', 15, 2512077),
(2711, 'Pombal', 15, 2512101),
(2712, 'Prata', 15, 2512200),
(2713, 'Princesa Isabel', 15, 2512309),
(2714, 'Puxinanã', 15, 2512408),
(2715, 'Queimadas', 15, 2512507),
(2716, 'Quixabá', 15, 2512606),
(2717, 'Remígio', 15, 2512705),
(2718, 'Riachão', 15, 2512747),
(2719, 'Riachão do Bacamarte', 15, 2512754),
(2720, 'Riachão do Poço', 15, 2512762),
(2721, 'Riacho de Santo Antônio', 15, 2512788),
(2722, 'Riacho dos Cavalos', 15, 2512804),
(2723, 'Rio Tinto', 15, 2512903),
(2724, 'Salgadinho', 15, 2513000),
(2725, 'Salgado de São Félix', 15, 2513109),
(2726, 'Santa Cecília', 15, 2513158),
(2727, 'Santa Cruz', 15, 2513208),
(2728, 'Santa Helena', 15, 2513307),
(2729, 'Santa Inês', 15, 2513356),
(2730, 'Santa Luzia', 15, 2513406),
(2731, 'Santa Rita', 15, 2513703),
(2732, 'Santa Teresinha', 15, 2513802),
(2733, 'Santana de Mangueira', 15, 2513505),
(2734, 'Santana dos Garrotes', 15, 2513604),
(2735, 'Santarém', 15, 2513653),
(2736, 'Santo André', 15, 2513851),
(2737, 'São Bentinho', 15, 2513927),
(2738, 'São Bento', 15, 2513901),
(2739, 'São Domingos de Pombal', 15, 2513968),
(2740, 'São Domingos do Cariri', 15, 2513943),
(2741, 'São Francisco', 15, 2513984),
(2742, 'São João do Cariri', 15, 2514008),
(2743, 'São João do Rio do Peixe', 15, 2500700),
(2744, 'São João do Tigre', 15, 2514107),
(2745, 'São José da Lagoa Tapada', 15, 2514206),
(2746, 'São José de Caiana', 15, 2514305),
(2747, 'São José de Espinharas', 15, 2514404),
(2748, 'São José de Piranhas', 15, 2514503),
(2749, 'São José de Princesa', 15, 2514552),
(2750, 'São José do Bonfim', 15, 2514602),
(2751, 'São José do Brejo do Cruz', 15, 2514651),
(2752, 'São José do Sabugi', 15, 2514701),
(2753, 'São José dos Cordeiros', 15, 2514800),
(2754, 'São José dos Ramos', 15, 2514453),
(2755, 'São Mamede', 15, 2514909),
(2756, 'São Miguel de Taipu', 15, 2515005),
(2757, 'São Sebastião de Lagoa de Roça', 15, 2515104),
(2758, 'São Sebastião do Umbuzeiro', 15, 2515203),
(2759, 'Sapé', 15, 2515302),
(2760, 'Seridó', 15, 2515401),
(2761, 'Serra Branca', 15, 2515500),
(2762, 'Serra da Raiz', 15, 2515609),
(2763, 'Serra Grande', 15, 2515708),
(2764, 'Serra Redonda', 15, 2515807),
(2765, 'Serraria', 15, 2515906),
(2766, 'Sertãozinho', 15, 2515930),
(2767, 'Sobrado', 15, 2515971),
(2768, 'Solânea', 15, 2516003),
(2769, 'Soledade', 15, 2516102),
(2770, 'Sossêgo', 15, 2516151),
(2771, 'Sousa', 15, 2516201),
(2772, 'Sumé', 15, 2516300),
(2773, 'Taperoá', 15, 2516508),
(2774, 'Tavares', 15, 2516607),
(2775, 'Teixeira', 15, 2516706),
(2776, 'Tenório', 15, 2516755),
(2777, 'Triunfo', 15, 2516805),
(2778, 'Uiraúna', 15, 2516904),
(2779, 'Umbuzeiro', 15, 2517001),
(2780, 'Várzea', 15, 2517100),
(2781, 'Vieirópolis', 15, 2517209),
(2782, 'Vista Serrana', 15, 2505501),
(2783, 'Zabelê', 15, 2517407),
(2784, 'Abatiá', 18, 4100103),
(2785, 'Adrianópolis', 18, 4100202),
(2786, 'Agudos do Sul', 18, 4100301),
(2787, 'Almirante Tamandaré', 18, 4100400),
(2788, 'Altamira do Paraná', 18, 4100459),
(2789, 'Alto Paraíso', 18, 4128625),
(2790, 'Alto Paraná', 18, 4100608),
(2791, 'Alto Piquiri', 18, 4100707),
(2792, 'Altônia', 18, 4100509),
(2793, 'Alvorada do Sul', 18, 4100806),
(2794, 'Amaporã', 18, 4100905),
(2795, 'Ampére', 18, 4101002),
(2796, 'Anahy', 18, 4101051),
(2797, 'Andirá', 18, 4101101),
(2798, 'Ângulo', 18, 4101150),
(2799, 'Antonina', 18, 4101200),
(2800, 'Antônio Olinto', 18, 4101309),
(2801, 'Apucarana', 18, 4101408),
(2802, 'Arapongas', 18, 4101507),
(2803, 'Arapoti', 18, 4101606),
(2804, 'Arapuã', 18, 4101655),
(2805, 'Araruna', 18, 4101705),
(2806, 'Araucária', 18, 4101804),
(2807, 'Ariranha do Ivaí', 18, 4101853),
(2808, 'Assaí', 18, 4101903),
(2809, 'Assis Chateaubriand', 18, 4102000),
(2810, 'Astorga', 18, 4102109),
(2811, 'Atalaia', 18, 4102208),
(2812, 'Balsa Nova', 18, 4102307),
(2813, 'Bandeirantes', 18, 4102406),
(2814, 'Barbosa Ferraz', 18, 4102505),
(2815, 'Barra do Jacaré', 18, 4102703),
(2816, 'Barracão', 18, 4102604),
(2817, 'Bela Vista da Caroba', 18, 4102752),
(2818, 'Bela Vista do Paraíso', 18, 4102802),
(2819, 'Bituruna', 18, 4102901),
(2820, 'Boa Esperança', 18, 4103008),
(2821, 'Boa Esperança do Iguaçu', 18, 4103024),
(2822, 'Boa Ventura de São Roque', 18, 4103040),
(2823, 'Boa Vista da Aparecida', 18, 4103057),
(2824, 'Bocaiúva do Sul', 18, 4103107),
(2825, 'Bom Jesus do Sul', 18, 4103156),
(2826, 'Bom Sucesso', 18, 4103206),
(2827, 'Bom Sucesso do Sul', 18, 4103222),
(2828, 'Borrazópolis', 18, 4103305),
(2829, 'Braganey', 18, 4103354),
(2830, 'Brasilândia do Sul', 18, 4103370),
(2831, 'Cafeara', 18, 4103404),
(2832, 'Cafelândia', 18, 4103453),
(2833, 'Cafezal do Sul', 18, 4103479),
(2834, 'Califórnia', 18, 4103503),
(2835, 'Cambará', 18, 4103602),
(2836, 'Cambé', 18, 4103701),
(2837, 'Cambira', 18, 4103800),
(2838, 'Campina da Lagoa', 18, 4103909),
(2839, 'Campina do Simão', 18, 4103958),
(2840, 'Campina Grande do Sul', 18, 4104006),
(2841, 'Campo Bonito', 18, 4104055),
(2842, 'Campo do Tenente', 18, 4104105),
(2843, 'Campo Largo', 18, 4104204),
(2844, 'Campo Magro', 18, 4104253),
(2845, 'Campo Mourão', 18, 4104303),
(2846, 'Cândido de Abreu', 18, 4104402),
(2847, 'Candói', 18, 4104428),
(2848, 'Cantagalo', 18, 4104451),
(2849, 'Capanema', 18, 4104501),
(2850, 'Capitão Leônidas Marques', 18, 4104600),
(2851, 'Carambeí', 18, 4104659),
(2852, 'Carlópolis', 18, 4104709),
(2853, 'Cascavel', 18, 4104808),
(2854, 'Castro', 18, 4104907),
(2855, 'Catanduvas', 18, 4105003),
(2856, 'Centenário do Sul', 18, 4105102),
(2857, 'Cerro Azul', 18, 4105201),
(2858, 'Céu Azul', 18, 4105300),
(2859, 'Chopinzinho', 18, 4105409),
(2860, 'Cianorte', 18, 4105508),
(2861, 'Cidade Gaúcha', 18, 4105607),
(2862, 'Clevelândia', 18, 4105706),
(2863, 'Colombo', 18, 4105805),
(2864, 'Colorado', 18, 4105904),
(2865, 'Congonhinhas', 18, 4106001),
(2866, 'Conselheiro Mairinck', 18, 4106100),
(2867, 'Contenda', 18, 4106209),
(2868, 'Corbélia', 18, 4106308),
(2869, 'Cornélio Procópio', 18, 4106407),
(2870, 'Coronel Domingos Soares', 18, 4106456),
(2871, 'Coronel Vivida', 18, 4106506),
(2872, 'Corumbataí do Sul', 18, 4106555),
(2873, 'Cruz Machado', 18, 4106803),
(2874, 'Cruzeiro do Iguaçu', 18, 4106571),
(2875, 'Cruzeiro do Oeste', 18, 4106605),
(2876, 'Cruzeiro do Sul', 18, 4106704),
(2877, 'Cruzmaltina', 18, 4106852),
(2878, 'Curitiba', 18, 4106902),
(2879, 'Curiúva', 18, 4107009),
(2880, 'Diamante d`Oeste', 18, 4107157),
(2881, 'Diamante do Norte', 18, 4107108),
(2882, 'Diamante do Sul', 18, 4107124),
(2883, 'Dois Vizinhos', 18, 4107207),
(2884, 'Douradina', 18, 4107256),
(2885, 'Doutor Camargo', 18, 4107306),
(2886, 'Doutor Ulysses', 18, 4128633),
(2887, 'Enéas Marques', 18, 4107405),
(2888, 'Engenheiro Beltrão', 18, 4107504),
(2889, 'Entre Rios do Oeste', 18, 4107538),
(2890, 'Esperança Nova', 18, 4107520),
(2891, 'Espigão Alto do Iguaçu', 18, 4107546),
(2892, 'Farol', 18, 4107553),
(2893, 'Faxinal', 18, 4107603),
(2894, 'Fazenda Rio Grande', 18, 4107652),
(2895, 'Fênix', 18, 4107702),
(2896, 'Fernandes Pinheiro', 18, 4107736),
(2897, 'Figueira', 18, 4107751),
(2898, 'Flor da Serra do Sul', 18, 4107850),
(2899, 'Floraí', 18, 4107801),
(2900, 'Floresta', 18, 4107900),
(2901, 'Florestópolis', 18, 4108007),
(2902, 'Flórida', 18, 4108106),
(2903, 'Formosa do Oeste', 18, 4108205),
(2904, 'Foz do Iguaçu', 18, 4108304),
(2905, 'Foz do Jordão', 18, 4108452),
(2906, 'Francisco Alves', 18, 4108320),
(2907, 'Francisco Beltrão', 18, 4108403),
(2908, 'General Carneiro', 18, 4108502),
(2909, 'Godoy Moreira', 18, 4108551),
(2910, 'Goioerê', 18, 4108601),
(2911, 'Goioxim', 18, 4108650),
(2912, 'Grandes Rios', 18, 4108700),
(2913, 'Guaíra', 18, 4108809),
(2914, 'Guairaçá', 18, 4108908),
(2915, 'Guamiranga', 18, 4108957),
(2916, 'Guapirama', 18, 4109005),
(2917, 'Guaporema', 18, 4109104),
(2918, 'Guaraci', 18, 4109203),
(2919, 'Guaraniaçu', 18, 4109302),
(2920, 'Guarapuava', 18, 4109401),
(2921, 'Guaraqueçaba', 18, 4109500),
(2922, 'Guaratuba', 18, 4109609),
(2923, 'Honório Serpa', 18, 4109658),
(2924, 'Ibaiti', 18, 4109708),
(2925, 'Ibema', 18, 4109757),
(2926, 'Ibiporã', 18, 4109807),
(2927, 'Icaraíma', 18, 4109906),
(2928, 'Iguaraçu', 18, 4110003),
(2929, 'Iguatu', 18, 4110052),
(2930, 'Imbaú', 18, 4110078),
(2931, 'Imbituva', 18, 4110102),
(2932, 'Inácio Martins', 18, 4110201),
(2933, 'Inajá', 18, 4110300),
(2934, 'Indianópolis', 18, 4110409),
(2935, 'Ipiranga', 18, 4110508),
(2936, 'Iporã', 18, 4110607),
(2937, 'Iracema do Oeste', 18, 4110656),
(2938, 'Irati', 18, 4110706),
(2939, 'Iretama', 18, 4110805),
(2940, 'Itaguajé', 18, 4110904),
(2941, 'Itaipulândia', 18, 4110953),
(2942, 'Itambaracá', 18, 4111001),
(2943, 'Itambé', 18, 4111100),
(2944, 'Itapejara d`Oeste', 18, 4111209),
(2945, 'Itaperuçu', 18, 4111258),
(2946, 'Itaúna do Sul', 18, 4111308),
(2947, 'Ivaí', 18, 4111407),
(2948, 'Ivaiporã', 18, 4111506),
(2949, 'Ivaté', 18, 4111555),
(2950, 'Ivatuba', 18, 4111605),
(2951, 'Jaboti', 18, 4111704),
(2952, 'Jacarezinho', 18, 4111803),
(2953, 'Jaguapitã', 18, 4111902),
(2954, 'Jaguariaíva', 18, 4112009),
(2955, 'Jandaia do Sul', 18, 4112108),
(2956, 'Janiópolis', 18, 4112207),
(2957, 'Japira', 18, 4112306),
(2958, 'Japurá', 18, 4112405),
(2959, 'Jardim Alegre', 18, 4112504),
(2960, 'Jardim Olinda', 18, 4112603),
(2961, 'Jataizinho', 18, 4112702);
INSERT INTO `MAM_City` (`CT_ID`, `CT_NOME`, `CT_UF`, `CT_IBGE`) VALUES
(2962, 'Jesuítas', 18, 4112751),
(2963, 'Joaquim Távora', 18, 4112801),
(2964, 'Jundiaí do Sul', 18, 4112900),
(2965, 'Juranda', 18, 4112959),
(2966, 'Jussara', 18, 4113007),
(2967, 'Kaloré', 18, 4113106),
(2968, 'Lapa', 18, 4113205),
(2969, 'Laranjal', 18, 4113254),
(2970, 'Laranjeiras do Sul', 18, 4113304),
(2971, 'Leópolis', 18, 4113403),
(2972, 'Lidianópolis', 18, 4113429),
(2973, 'Lindoeste', 18, 4113452),
(2974, 'Loanda', 18, 4113502),
(2975, 'Lobato', 18, 4113601),
(2976, 'Londrina', 18, 4113700),
(2977, 'Luiziana', 18, 4113734),
(2978, 'Lunardelli', 18, 4113759),
(2979, 'Lupionópolis', 18, 4113809),
(2980, 'Mallet', 18, 4113908),
(2981, 'Mamborê', 18, 4114005),
(2982, 'Mandaguaçu', 18, 4114104),
(2983, 'Mandaguari', 18, 4114203),
(2984, 'Mandirituba', 18, 4114302),
(2985, 'Manfrinópolis', 18, 4114351),
(2986, 'Mangueirinha', 18, 4114401),
(2987, 'Manoel Ribas', 18, 4114500),
(2988, 'Marechal Cândido Rondon', 18, 4114609),
(2989, 'Maria Helena', 18, 4114708),
(2990, 'Marialva', 18, 4114807),
(2991, 'Marilândia do Sul', 18, 4114906),
(2992, 'Marilena', 18, 4115002),
(2993, 'Mariluz', 18, 4115101),
(2994, 'Maringá', 18, 4115200),
(2995, 'Mariópolis', 18, 4115309),
(2996, 'Maripá', 18, 4115358),
(2997, 'Marmeleiro', 18, 4115408),
(2998, 'Marquinho', 18, 4115457),
(2999, 'Marumbi', 18, 4115507),
(3000, 'Matelândia', 18, 4115606),
(3001, 'Matinhos', 18, 4115705),
(3002, 'Mato Rico', 18, 4115739),
(3003, 'Mauá da Serra', 18, 4115754),
(3004, 'Medianeira', 18, 4115804),
(3005, 'Mercedes', 18, 4115853),
(3006, 'Mirador', 18, 4115903),
(3007, 'Miraselva', 18, 4116000),
(3008, 'Missal', 18, 4116059),
(3009, 'Moreira Sales', 18, 4116109),
(3010, 'Morretes', 18, 4116208),
(3011, 'Munhoz de Melo', 18, 4116307),
(3012, 'Nossa Senhora das Graças', 18, 4116406),
(3013, 'Nova Aliança do Ivaí', 18, 4116505),
(3014, 'Nova América da Colina', 18, 4116604),
(3015, 'Nova Aurora', 18, 4116703),
(3016, 'Nova Cantu', 18, 4116802),
(3017, 'Nova Esperança', 18, 4116901),
(3018, 'Nova Esperança do Sudoeste', 18, 4116950),
(3019, 'Nova Fátima', 18, 4117008),
(3020, 'Nova Laranjeiras', 18, 4117057),
(3021, 'Nova Londrina', 18, 4117107),
(3022, 'Nova Olímpia', 18, 4117206),
(3023, 'Nova Prata do Iguaçu', 18, 4117255),
(3024, 'Nova Santa Bárbara', 18, 4117214),
(3025, 'Nova Santa Rosa', 18, 4117222),
(3026, 'Nova Tebas', 18, 4117271),
(3027, 'Novo Itacolomi', 18, 4117297),
(3028, 'Ortigueira', 18, 4117305),
(3029, 'Ourizona', 18, 4117404),
(3030, 'Ouro Verde do Oeste', 18, 4117453),
(3031, 'Paiçandu', 18, 4117503),
(3032, 'Palmas', 18, 4117602),
(3033, 'Palmeira', 18, 4117701),
(3034, 'Palmital', 18, 4117800),
(3035, 'Palotina', 18, 4117909),
(3036, 'Paraíso do Norte', 18, 4118006),
(3037, 'Paranacity', 18, 4118105),
(3038, 'Paranaguá', 18, 4118204),
(3039, 'Paranapoema', 18, 4118303),
(3040, 'Paranavaí', 18, 4118402),
(3041, 'Pato Bragado', 18, 4118451),
(3042, 'Pato Branco', 18, 4118501),
(3043, 'Paula Freitas', 18, 4118600),
(3044, 'Paulo Frontin', 18, 4118709),
(3045, 'Peabiru', 18, 4118808),
(3046, 'Perobal', 18, 4118857),
(3047, 'Pérola', 18, 4118907),
(3048, 'Pérola d`Oeste', 18, 4119004),
(3049, 'Piên', 18, 4119103),
(3050, 'Pinhais', 18, 4119152),
(3051, 'Pinhal de São Bento', 18, 4119251),
(3052, 'Pinhalão', 18, 4119202),
(3053, 'Pinhão', 18, 4119301),
(3054, 'Piraí do Sul', 18, 4119400),
(3055, 'Piraquara', 18, 4119509),
(3056, 'Pitanga', 18, 4119608),
(3057, 'Pitangueiras', 18, 4119657),
(3058, 'Planaltina do Paraná', 18, 4119707),
(3059, 'Planalto', 18, 4119806),
(3060, 'Ponta Grossa', 18, 4119905),
(3061, 'Pontal do Paraná', 18, 4119954),
(3062, 'Porecatu', 18, 4120002),
(3063, 'Porto Amazonas', 18, 4120101),
(3064, 'Porto Barreiro', 18, 4120150),
(3065, 'Porto Rico', 18, 4120200),
(3066, 'Porto Vitória', 18, 4120309),
(3067, 'Prado Ferreira', 18, 4120333),
(3068, 'Pranchita', 18, 4120358),
(3069, 'Presidente Castelo Branco', 18, 4120408),
(3070, 'Primeiro de Maio', 18, 4120507),
(3071, 'Prudentópolis', 18, 4120606),
(3072, 'Quarto Centenário', 18, 4120655),
(3073, 'Quatiguá', 18, 4120705),
(3074, 'Quatro Barras', 18, 4120804),
(3075, 'Quatro Pontes', 18, 4120853),
(3076, 'Quedas do Iguaçu', 18, 4120903),
(3077, 'Querência do Norte', 18, 4121000),
(3078, 'Quinta do Sol', 18, 4121109),
(3079, 'Quitandinha', 18, 4121208),
(3080, 'Ramilândia', 18, 4121257),
(3081, 'Rancho Alegre', 18, 4121307),
(3082, 'Rancho Alegre d`Oeste', 18, 4121356),
(3083, 'Realeza', 18, 4121406),
(3084, 'Rebouças', 18, 4121505),
(3085, 'Renascença', 18, 4121604),
(3086, 'Reserva', 18, 4121703),
(3087, 'Reserva do Iguaçu', 18, 4121752),
(3088, 'Ribeirão Claro', 18, 4121802),
(3089, 'Ribeirão do Pinhal', 18, 4121901),
(3090, 'Rio Azul', 18, 4122008),
(3091, 'Rio Bom', 18, 4122107),
(3092, 'Rio Bonito do Iguaçu', 18, 4122156),
(3093, 'Rio Branco do Ivaí', 18, 4122172),
(3094, 'Rio Branco do Sul', 18, 4122206),
(3095, 'Rio Negro', 18, 4122305),
(3096, 'Rolândia', 18, 4122404),
(3097, 'Roncador', 18, 4122503),
(3098, 'Rondon', 18, 4122602),
(3099, 'Rosário do Ivaí', 18, 4122651),
(3100, 'Sabáudia', 18, 4122701),
(3101, 'Salgado Filho', 18, 4122800),
(3102, 'Salto do Itararé', 18, 4122909),
(3103, 'Salto do Lontra', 18, 4123006),
(3104, 'Santa Amélia', 18, 4123105),
(3105, 'Santa Cecília do Pavão', 18, 4123204),
(3106, 'Santa Cruz de Monte Castelo', 18, 4123303),
(3107, 'Santa Fé', 18, 4123402),
(3108, 'Santa Helena', 18, 4123501),
(3109, 'Santa Inês', 18, 4123600),
(3110, 'Santa Isabel do Ivaí', 18, 4123709),
(3111, 'Santa Izabel do Oeste', 18, 4123808),
(3112, 'Santa Lúcia', 18, 4123824),
(3113, 'Santa Maria do Oeste', 18, 4123857),
(3114, 'Santa Mariana', 18, 4123907),
(3115, 'Santa Mônica', 18, 4123956),
(3116, 'Santa Tereza do Oeste', 18, 4124020),
(3117, 'Santa Terezinha de Itaipu', 18, 4124053),
(3118, 'Santana do Itararé', 18, 4124004),
(3119, 'Santo Antônio da Platina', 18, 4124103),
(3120, 'Santo Antônio do Caiuá', 18, 4124202),
(3121, 'Santo Antônio do Paraíso', 18, 4124301),
(3122, 'Santo Antônio do Sudoeste', 18, 4124400),
(3123, 'Santo Inácio', 18, 4124509),
(3124, 'São Carlos do Ivaí', 18, 4124608),
(3125, 'São Jerônimo da Serra', 18, 4124707),
(3126, 'São João', 18, 4124806),
(3127, 'São João do Caiuá', 18, 4124905),
(3128, 'São João do Ivaí', 18, 4125001),
(3129, 'São João do Triunfo', 18, 4125100),
(3130, 'São Jorge d`Oeste', 18, 4125209),
(3131, 'São Jorge do Ivaí', 18, 4125308),
(3132, 'São Jorge do Patrocínio', 18, 4125357),
(3133, 'São José da Boa Vista', 18, 4125407),
(3134, 'São José das Palmeiras', 18, 4125456),
(3135, 'São José dos Pinhais', 18, 4125506),
(3136, 'São Manoel do Paraná', 18, 4125555),
(3137, 'São Mateus do Sul', 18, 4125605),
(3138, 'São Miguel do Iguaçu', 18, 4125704),
(3139, 'São Pedro do Iguaçu', 18, 4125753),
(3140, 'São Pedro do Ivaí', 18, 4125803),
(3141, 'São Pedro do Paraná', 18, 4125902),
(3142, 'São Sebastião da Amoreira', 18, 4126009),
(3143, 'São Tomé', 18, 4126108),
(3144, 'Sapopema', 18, 4126207),
(3145, 'Sarandi', 18, 4126256),
(3146, 'Saudade do Iguaçu', 18, 4126272),
(3147, 'Sengés', 18, 4126306),
(3148, 'Serranópolis do Iguaçu', 18, 4126355),
(3149, 'Sertaneja', 18, 4126405),
(3150, 'Sertanópolis', 18, 4126504),
(3151, 'Siqueira Campos', 18, 4126603),
(3152, 'Sulina', 18, 4126652),
(3153, 'Tamarana', 18, 4126678),
(3154, 'Tamboara', 18, 4126702),
(3155, 'Tapejara', 18, 4126801),
(3156, 'Tapira', 18, 4126900),
(3157, 'Teixeira Soares', 18, 4127007),
(3158, 'Telêmaco Borba', 18, 4127106),
(3159, 'Terra Boa', 18, 4127205),
(3160, 'Terra Rica', 18, 4127304),
(3161, 'Terra Roxa', 18, 4127403),
(3162, 'Tibagi', 18, 4127502),
(3163, 'Tijucas do Sul', 18, 4127601),
(3164, 'Toledo', 18, 4127700),
(3165, 'Tomazina', 18, 4127809),
(3166, 'Três Barras do Paraná', 18, 4127858),
(3167, 'Tunas do Paraná', 18, 4127882),
(3168, 'Tuneiras do Oeste', 18, 4127908),
(3169, 'Tupãssi', 18, 4127957),
(3170, 'Turvo', 18, 4127965),
(3171, 'Ubiratã', 18, 4128005),
(3172, 'Umuarama', 18, 4128104),
(3173, 'União da Vitória', 18, 4128203),
(3174, 'Uniflor', 18, 4128302),
(3175, 'Uraí', 18, 4128401),
(3176, 'Ventania', 18, 4128534),
(3177, 'Vera Cruz do Oeste', 18, 4128559),
(3178, 'Verê', 18, 4128609),
(3179, 'Virmond', 18, 4128658),
(3180, 'Vitorino', 18, 4128708),
(3181, 'Wenceslau Braz', 18, 4128500),
(3182, 'Xambrê', 18, 4128807),
(3183, 'Abreu e Lima', 16, 2600054),
(3184, 'Afogados da Ingazeira', 16, 2600104),
(3185, 'Afrânio', 16, 2600203),
(3186, 'Agrestina', 16, 2600302),
(3187, 'Água Preta', 16, 2600401),
(3188, 'Águas Belas', 16, 2600500),
(3189, 'Alagoinha', 16, 2600609),
(3190, 'Aliança', 16, 2600708),
(3191, 'Altinho', 16, 2600807),
(3192, 'Amaraji', 16, 2600906),
(3193, 'Angelim', 16, 2601003),
(3194, 'Araçoiaba', 16, 2601052),
(3195, 'Araripina', 16, 2601102),
(3196, 'Arcoverde', 16, 2601201),
(3197, 'Barra de Guabiraba', 16, 2601300),
(3198, 'Barreiros', 16, 2601409),
(3199, 'Belém de Maria', 16, 2601508),
(3200, 'Belém de São Francisco', 16, 2601607),
(3201, 'Belo Jardim', 16, 2601706),
(3202, 'Betânia', 16, 2601805),
(3203, 'Bezerros', 16, 2601904),
(3204, 'Bodocó', 16, 2602001),
(3205, 'Bom Conselho', 16, 2602100),
(3206, 'Bom Jardim', 16, 2602209),
(3207, 'Bonito', 16, 2602308),
(3208, 'Brejão', 16, 2602407),
(3209, 'Brejinho', 16, 2602506),
(3210, 'Brejo da Madre de Deus', 16, 2602605),
(3211, 'Buenos Aires', 16, 2602704),
(3212, 'Buíque', 16, 2602803),
(3213, 'Cabo de Santo Agostinho', 16, 2602902),
(3214, 'Cabrobó', 16, 2603009),
(3215, 'Cachoeirinha', 16, 2603108),
(3216, 'Caetés', 16, 2603207),
(3217, 'Calçado', 16, 2603306),
(3218, 'Calumbi', 16, 2603405),
(3219, 'Camaragibe', 16, 2603454),
(3220, 'Camocim de São Félix', 16, 2603504),
(3221, 'Camutanga', 16, 2603603),
(3222, 'Canhotinho', 16, 2603702),
(3223, 'Capoeiras', 16, 2603801),
(3224, 'Carnaíba', 16, 2603900),
(3225, 'Carnaubeira da Penha', 16, 2603926),
(3226, 'Carpina', 16, 2604007),
(3227, 'Caruaru', 16, 2604106),
(3228, 'Casinhas', 16, 2604155),
(3229, 'Catende', 16, 2604205),
(3230, 'Cedro', 16, 2604304),
(3231, 'Chã de Alegria', 16, 2604403),
(3232, 'Chã Grande', 16, 2604502),
(3233, 'Condado', 16, 2604601),
(3234, 'Correntes', 16, 2604700),
(3235, 'Cortês', 16, 2604809),
(3236, 'Cumaru', 16, 2604908),
(3237, 'Cupira', 16, 2605004),
(3238, 'Custódia', 16, 2605103),
(3239, 'Dormentes', 16, 2605152),
(3240, 'Escada', 16, 2605202),
(3241, 'Exu', 16, 2605301),
(3242, 'Feira Nova', 16, 2605400),
(3243, 'Fernando de Noronha', 16, 2605459),
(3244, 'Ferreiros', 16, 2605509),
(3245, 'Flores', 16, 2605608),
(3246, 'Floresta', 16, 2605707),
(3247, 'Frei Miguelinho', 16, 2605806),
(3248, 'Gameleira', 16, 2605905),
(3249, 'Garanhuns', 16, 2606002),
(3250, 'Glória do Goitá', 16, 2606101),
(3251, 'Goiana', 16, 2606200),
(3252, 'Granito', 16, 2606309),
(3253, 'Gravatá', 16, 2606408),
(3254, 'Iati', 16, 2606507),
(3255, 'Ibimirim', 16, 2606606),
(3256, 'Ibirajuba', 16, 2606705),
(3257, 'Igarassu', 16, 2606804),
(3258, 'Iguaraci', 16, 2606903),
(3259, 'Ilha de Itamaracá', 16, 2607604),
(3260, 'Inajá', 16, 2607000),
(3261, 'Ingazeira', 16, 2607109),
(3262, 'Ipojuca', 16, 2607208),
(3263, 'Ipubi', 16, 2607307),
(3264, 'Itacuruba', 16, 2607406),
(3265, 'Itaíba', 16, 2607505),
(3266, 'Itambé', 16, 2607653),
(3267, 'Itapetim', 16, 2607703),
(3268, 'Itapissuma', 16, 2607752),
(3269, 'Itaquitinga', 16, 2607802),
(3270, 'Jaboatão dos Guararapes', 16, 2607901),
(3271, 'Jaqueira', 16, 2607950),
(3272, 'Jataúba', 16, 2608008),
(3273, 'Jatobá', 16, 2608057),
(3274, 'João Alfredo', 16, 2608107),
(3275, 'Joaquim Nabuco', 16, 2608206),
(3276, 'Jucati', 16, 2608255),
(3277, 'Jupi', 16, 2608305),
(3278, 'Jurema', 16, 2608404),
(3279, 'Lagoa do Carro', 16, 2608453),
(3280, 'Lagoa do Itaenga', 16, 2608503),
(3281, 'Lagoa do Ouro', 16, 2608602),
(3282, 'Lagoa dos Gatos', 16, 2608701),
(3283, 'Lagoa Grande', 16, 2608750),
(3284, 'Lajedo', 16, 2608800),
(3285, 'Limoeiro', 16, 2608909),
(3286, 'Macaparana', 16, 2609006),
(3287, 'Machados', 16, 2609105),
(3288, 'Manari', 16, 2609154),
(3289, 'Maraial', 16, 2609204),
(3290, 'Mirandiba', 16, 2609303),
(3291, 'Moreilândia', 16, 2614303),
(3292, 'Moreno', 16, 2609402),
(3293, 'Nazaré da Mata', 16, 2609501),
(3294, 'Olinda', 16, 2609600),
(3295, 'Orobó', 16, 2609709),
(3296, 'Orocó', 16, 2609808),
(3297, 'Ouricuri', 16, 2609907),
(3298, 'Palmares', 16, 2610004),
(3299, 'Palmeirina', 16, 2610103),
(3300, 'Panelas', 16, 2610202),
(3301, 'Paranatama', 16, 2610301),
(3302, 'Parnamirim', 16, 2610400),
(3303, 'Passira', 16, 2610509),
(3304, 'Paudalho', 16, 2610608),
(3305, 'Paulista', 16, 2610707),
(3306, 'Pedra', 16, 2610806),
(3307, 'Pesqueira', 16, 2610905),
(3308, 'Petrolândia', 16, 2611002),
(3309, 'Petrolina', 16, 2611101),
(3310, 'Poção', 16, 2611200),
(3311, 'Pombos', 16, 2611309),
(3312, 'Primavera', 16, 2611408),
(3313, 'Quipapá', 16, 2611507),
(3314, 'Quixaba', 16, 2611533),
(3315, 'Recife', 16, 2611606),
(3316, 'Riacho das Almas', 16, 2611705),
(3317, 'Ribeirão', 16, 2611804),
(3318, 'Rio Formoso', 16, 2611903),
(3319, 'Sairé', 16, 2612000),
(3320, 'Salgadinho', 16, 2612109),
(3321, 'Salgueiro', 16, 2612208),
(3322, 'Saloá', 16, 2612307),
(3323, 'Sanharó', 16, 2612406),
(3324, 'Santa Cruz', 16, 2612455),
(3325, 'Santa Cruz da Baixa Verde', 16, 2612471),
(3326, 'Santa Cruz do Capibaribe', 16, 2612505),
(3327, 'Santa Filomena', 16, 2612554),
(3328, 'Santa Maria da Boa Vista', 16, 2612604),
(3329, 'Santa Maria do Cambucá', 16, 2612703),
(3330, 'Santa Terezinha', 16, 2612802),
(3331, 'São Benedito do Sul', 16, 2612901),
(3332, 'São Bento do Una', 16, 2613008),
(3333, 'São Caitano', 16, 2613107),
(3334, 'São João', 16, 2613206),
(3335, 'São Joaquim do Monte', 16, 2613305),
(3336, 'São José da Coroa Grande', 16, 2613404),
(3337, 'São José do Belmonte', 16, 2613503),
(3338, 'São José do Egito', 16, 2613602),
(3339, 'São Lourenço da Mata', 16, 2613701),
(3340, 'São Vicente Ferrer', 16, 2613800),
(3341, 'Serra Talhada', 16, 2613909),
(3342, 'Serrita', 16, 2614006),
(3343, 'Sertânia', 16, 2614105),
(3344, 'Sirinhaém', 16, 2614204),
(3345, 'Solidão', 16, 2614402),
(3346, 'Surubim', 16, 2614501),
(3347, 'Tabira', 16, 2614600),
(3348, 'Tacaimbó', 16, 2614709),
(3349, 'Tacaratu', 16, 2614808),
(3350, 'Tamandaré', 16, 2614857),
(3351, 'Taquaritinga do Norte', 16, 2615003),
(3352, 'Terezinha', 16, 2615102),
(3353, 'Terra Nova', 16, 2615201),
(3354, 'Timbaúba', 16, 2615300),
(3355, 'Toritama', 16, 2615409),
(3356, 'Tracunhaém', 16, 2615508),
(3357, 'Trindade', 16, 2615607),
(3358, 'Triunfo', 16, 2615706),
(3359, 'Tupanatinga', 16, 2615805),
(3360, 'Tuparetama', 16, 2615904),
(3361, 'Venturosa', 16, 2616001),
(3362, 'Verdejante', 16, 2616100),
(3363, 'Vertente do Lério', 16, 2616183),
(3364, 'Vertentes', 16, 2616209),
(3365, 'Vicência', 16, 2616308),
(3366, 'Vitória de Santo Antão', 16, 2616407),
(3367, 'Xexéu', 16, 2616506),
(3368, 'Acauã', 17, 2200053),
(3369, 'Agricolândia', 17, 2200103),
(3370, 'Água Branca', 17, 2200202),
(3371, 'Alagoinha do Piauí', 17, 2200251),
(3372, 'Alegrete do Piauí', 17, 2200277),
(3373, 'Alto Longá', 17, 2200301),
(3374, 'Altos', 17, 2200400),
(3375, 'Alvorada do Gurguéia', 17, 2200459),
(3376, 'Amarante', 17, 2200509),
(3377, 'Angical do Piauí', 17, 2200608),
(3378, 'Anísio de Abreu', 17, 2200707),
(3379, 'Antônio Almeida', 17, 2200806),
(3380, 'Aroazes', 17, 2200905),
(3381, 'Aroeiras do Itaim', 17, 2200954),
(3382, 'Arraial', 17, 2201002),
(3383, 'Assunção do Piauí', 17, 2201051),
(3384, 'Avelino Lopes', 17, 2201101),
(3385, 'Baixa Grande do Ribeiro', 17, 2201150),
(3386, 'Barra d`Alcântara', 17, 2201176),
(3387, 'Barras', 17, 2201200),
(3388, 'Barreiras do Piauí', 17, 2201309),
(3389, 'Barro Duro', 17, 2201408),
(3390, 'Batalha', 17, 2201507),
(3391, 'Bela Vista do Piauí', 17, 2201556),
(3392, 'Belém do Piauí', 17, 2201572),
(3393, 'Beneditinos', 17, 2201606),
(3394, 'Bertolínia', 17, 2201705),
(3395, 'Betânia do Piauí', 17, 2201739),
(3396, 'Boa Hora', 17, 2201770),
(3397, 'Bocaina', 17, 2201804),
(3398, 'Bom Jesus', 17, 2201903),
(3399, 'Bom Princípio do Piauí', 17, 2201919),
(3400, 'Bonfim do Piauí', 17, 2201929),
(3401, 'Boqueirão do Piauí', 17, 2201945),
(3402, 'Brasileira', 17, 2201960),
(3403, 'Brejo do Piauí', 17, 2201988),
(3404, 'Buriti dos Lopes', 17, 2202000),
(3405, 'Buriti dos Montes', 17, 2202026),
(3406, 'Cabeceiras do Piauí', 17, 2202059),
(3407, 'Cajazeiras do Piauí', 17, 2202075),
(3408, 'Cajueiro da Praia', 17, 2202083),
(3409, 'Caldeirão Grande do Piauí', 17, 2202091),
(3410, 'Campinas do Piauí', 17, 2202109),
(3411, 'Campo Alegre do Fidalgo', 17, 2202117),
(3412, 'Campo Grande do Piauí', 17, 2202133),
(3413, 'Campo Largo do Piauí', 17, 2202174),
(3414, 'Campo Maior', 17, 2202208),
(3415, 'Canavieira', 17, 2202251),
(3416, 'Canto do Buriti', 17, 2202307),
(3417, 'Capitão de Campos', 17, 2202406),
(3418, 'Capitão Gervásio Oliveira', 17, 2202455),
(3419, 'Caracol', 17, 2202505),
(3420, 'Caraúbas do Piauí', 17, 2202539),
(3421, 'Caridade do Piauí', 17, 2202554),
(3422, 'Castelo do Piauí', 17, 2202604),
(3423, 'Caxingó', 17, 2202653),
(3424, 'Cocal', 17, 2202703),
(3425, 'Cocal de Telha', 17, 2202711),
(3426, 'Cocal dos Alves', 17, 2202729),
(3427, 'Coivaras', 17, 2202737),
(3428, 'Colônia do Gurguéia', 17, 2202752),
(3429, 'Colônia do Piauí', 17, 2202778),
(3430, 'Conceição do Canindé', 17, 2202802),
(3431, 'Coronel José Dias', 17, 2202851),
(3432, 'Corrente', 17, 2202901),
(3433, 'Cristalândia do Piauí', 17, 2203008),
(3434, 'Cristino Castro', 17, 2203107),
(3435, 'Curimatá', 17, 2203206),
(3436, 'Currais', 17, 2203230),
(3437, 'Curral Novo do Piauí', 17, 2203271),
(3438, 'Curralinhos', 17, 2203255),
(3439, 'Demerval Lobão', 17, 2203305),
(3440, 'Dirceu Arcoverde', 17, 2203354),
(3441, 'Dom Expedito Lopes', 17, 2203404),
(3442, 'Dom Inocêncio', 17, 2203453),
(3443, 'Domingos Mourão', 17, 2203420),
(3444, 'Elesbão Veloso', 17, 2203503),
(3445, 'Eliseu Martins', 17, 2203602),
(3446, 'Esperantina', 17, 2203701),
(3447, 'Fartura do Piauí', 17, 2203750),
(3448, 'Flores do Piauí', 17, 2203800),
(3449, 'Floresta do Piauí', 17, 2203859),
(3450, 'Floriano', 17, 2203909),
(3451, 'Francinópolis', 17, 2204006),
(3452, 'Francisco Ayres', 17, 2204105),
(3453, 'Francisco Macedo', 17, 2204154),
(3454, 'Francisco Santos', 17, 2204204),
(3455, 'Fronteiras', 17, 2204303),
(3456, 'Geminiano', 17, 2204352),
(3457, 'Gilbués', 17, 2204402),
(3458, 'Guadalupe', 17, 2204501),
(3459, 'Guaribas', 17, 2204550),
(3460, 'Hugo Napoleão', 17, 2204600),
(3461, 'Ilha Grande', 17, 2204659),
(3462, 'Inhuma', 17, 2204709),
(3463, 'Ipiranga do Piauí', 17, 2204808),
(3464, 'Isaías Coelho', 17, 2204907),
(3465, 'Itainópolis', 17, 2205003),
(3466, 'Itaueira', 17, 2205102),
(3467, 'Jacobina do Piauí', 17, 2205151),
(3468, 'Jaicós', 17, 2205201),
(3469, 'Jardim do Mulato', 17, 2205250),
(3470, 'Jatobá do Piauí', 17, 2205276),
(3471, 'Jerumenha', 17, 2205300),
(3472, 'João Costa', 17, 2205359),
(3473, 'Joaquim Pires', 17, 2205409),
(3474, 'Joca Marques', 17, 2205458),
(3475, 'José de Freitas', 17, 2205508),
(3476, 'Juazeiro do Piauí', 17, 2205516),
(3477, 'Júlio Borges', 17, 2205524),
(3478, 'Jurema', 17, 2205532),
(3479, 'Lagoa Alegre', 17, 2205557),
(3480, 'Lagoa de São Francisco', 17, 2205573),
(3481, 'Lagoa do Barro do Piauí', 17, 2205565),
(3482, 'Lagoa do Piauí', 17, 2205581),
(3483, 'Lagoa do Sítio', 17, 2205599),
(3484, 'Lagoinha do Piauí', 17, 2205540),
(3485, 'Landri Sales', 17, 2205607),
(3486, 'Luís Correia', 17, 2205706),
(3487, 'Luzilândia', 17, 2205805),
(3488, 'Madeiro', 17, 2205854),
(3489, 'Manoel Emídio', 17, 2205904),
(3490, 'Marcolândia', 17, 2205953),
(3491, 'Marcos Parente', 17, 2206001),
(3492, 'Massapê do Piauí', 17, 2206050),
(3493, 'Matias Olímpio', 17, 2206100),
(3494, 'Miguel Alves', 17, 2206209),
(3495, 'Miguel Leão', 17, 2206308),
(3496, 'Milton Brandão', 17, 2206357),
(3497, 'Monsenhor Gil', 17, 2206407),
(3498, 'Monsenhor Hipólito', 17, 2206506),
(3499, 'Monte Alegre do Piauí', 17, 2206605),
(3500, 'Morro Cabeça no Tempo', 17, 2206654),
(3501, 'Morro do Chapéu do Piauí', 17, 2206670),
(3502, 'Murici dos Portelas', 17, 2206696),
(3503, 'Nazaré do Piauí', 17, 2206704),
(3504, 'Nossa Senhora de Nazaré', 17, 2206753),
(3505, 'Nossa Senhora dos Remédios', 17, 2206803),
(3506, 'Nova Santa Rita', 17, 2207959),
(3507, 'Novo Oriente do Piauí', 17, 2206902),
(3508, 'Novo Santo Antônio', 17, 2206951),
(3509, 'Oeiras', 17, 2207009),
(3510, 'Olho d`Água do Piauí', 17, 2207108),
(3511, 'Padre Marcos', 17, 2207207),
(3512, 'Paes Landim', 17, 2207306),
(3513, 'Pajeú do Piauí', 17, 2207355),
(3514, 'Palmeira do Piauí', 17, 2207405),
(3515, 'Palmeirais', 17, 2207504),
(3516, 'Paquetá', 17, 2207553),
(3517, 'Parnaguá', 17, 2207603),
(3518, 'Parnaíba', 17, 2207702),
(3519, 'Passagem Franca do Piauí', 17, 2207751),
(3520, 'Patos do Piauí', 17, 2207777),
(3521, 'Pau d`Arco do Piauí', 17, 2207793),
(3522, 'Paulistana', 17, 2207801),
(3523, 'Pavussu', 17, 2207850),
(3524, 'Pedro II', 17, 2207900),
(3525, 'Pedro Laurentino', 17, 2207934),
(3526, 'Picos', 17, 2208007),
(3527, 'Pimenteiras', 17, 2208106),
(3528, 'Pio IX', 17, 2208205),
(3529, 'Piracuruca', 17, 2208304),
(3530, 'Piripiri', 17, 2208403),
(3531, 'Porto', 17, 2208502),
(3532, 'Porto Alegre do Piauí', 17, 2208551),
(3533, 'Prata do Piauí', 17, 2208601),
(3534, 'Queimada Nova', 17, 2208650),
(3535, 'Redenção do Gurguéia', 17, 2208700),
(3536, 'Regeneração', 17, 2208809),
(3537, 'Riacho Frio', 17, 2208858),
(3538, 'Ribeira do Piauí', 17, 2208874),
(3539, 'Ribeiro Gonçalves', 17, 2208908),
(3540, 'Rio Grande do Piauí', 17, 2209005),
(3541, 'Santa Cruz do Piauí', 17, 2209104),
(3542, 'Santa Cruz dos Milagres', 17, 2209153),
(3543, 'Santa Filomena', 17, 2209203),
(3544, 'Santa Luz', 17, 2209302),
(3545, 'Santa Rosa do Piauí', 17, 2209377),
(3546, 'Santana do Piauí', 17, 2209351),
(3547, 'Santo Antônio de Lisboa', 17, 2209401),
(3548, 'Santo Antônio dos Milagres', 17, 2209450),
(3549, 'Santo Inácio do Piauí', 17, 2209500),
(3550, 'São Braz do Piauí', 17, 2209559),
(3551, 'São Félix do Piauí', 17, 2209609),
(3552, 'São Francisco de Assis do Piauí', 17, 2209658),
(3553, 'São Francisco do Piauí', 17, 2209708),
(3554, 'São Gonçalo do Gurguéia', 17, 2209757),
(3555, 'São Gonçalo do Piauí', 17, 2209807),
(3556, 'São João da Canabrava', 17, 2209856),
(3557, 'São João da Fronteira', 17, 2209872),
(3558, 'São João da Serra', 17, 2209906),
(3559, 'São João da Varjota', 17, 2209955),
(3560, 'São João do Arraial', 17, 2209971),
(3561, 'São João do Piauí', 17, 2210003),
(3562, 'São José do Divino', 17, 2210052),
(3563, 'São José do Peixe', 17, 2210102),
(3564, 'São José do Piauí', 17, 2210201),
(3565, 'São Julião', 17, 2210300),
(3566, 'São Lourenço do Piauí', 17, 2210359),
(3567, 'São Luis do Piauí', 17, 2210375),
(3568, 'São Miguel da Baixa Grande', 17, 2210383),
(3569, 'São Miguel do Fidalgo', 17, 2210391),
(3570, 'São Miguel do Tapuio', 17, 2210409),
(3571, 'São Pedro do Piauí', 17, 2210508),
(3572, 'São Raimundo Nonato', 17, 2210607),
(3573, 'Sebastião Barros', 17, 2210623),
(3574, 'Sebastião Leal', 17, 2210631),
(3575, 'Sigefredo Pacheco', 17, 2210656),
(3576, 'Simões', 17, 2210706),
(3577, 'Simplício Mendes', 17, 2210805),
(3578, 'Socorro do Piauí', 17, 2210904),
(3579, 'Sussuapara', 17, 2210938),
(3580, 'Tamboril do Piauí', 17, 2210953),
(3581, 'Tanque do Piauí', 17, 2210979),
(3582, 'Teresina', 17, 2211001),
(3583, 'União', 17, 2211100),
(3584, 'Uruçuí', 17, 2211209),
(3585, 'Valença do Piauí', 17, 2211308),
(3586, 'Várzea Branca', 17, 2211357),
(3587, 'Várzea Grande', 17, 2211407),
(3588, 'Vera Mendes', 17, 2211506),
(3589, 'Vila Nova do Piauí', 17, 2211605),
(3590, 'Wall Ferraz', 17, 2211704),
(3591, 'Angra dos Reis', 19, 3300100),
(3592, 'Aperibé', 19, 3300159),
(3593, 'Araruama', 19, 3300209),
(3594, 'Areal', 19, 3300225),
(3595, 'Armação dos Búzios', 19, 3300233),
(3596, 'Arraial do Cabo', 19, 3300258),
(3597, 'Barra do Piraí', 19, 3300308),
(3598, 'Barra Mansa', 19, 3300407),
(3599, 'Belford Roxo', 19, 3300456),
(3600, 'Bom Jardim', 19, 3300506),
(3601, 'Bom Jesus do Itabapoana', 19, 3300605),
(3602, 'Cabo Frio', 19, 3300704),
(3603, 'Cachoeiras de Macacu', 19, 3300803),
(3604, 'Cambuci', 19, 3300902),
(3605, 'Campos dos Goytacazes', 19, 3301009),
(3606, 'Cantagalo', 19, 3301108),
(3607, 'Carapebus', 19, 3300936),
(3608, 'Cardoso Moreira', 19, 3301157),
(3609, 'Carmo', 19, 3301207),
(3610, 'Casimiro de Abreu', 19, 3301306),
(3611, 'Comendador Levy Gasparian', 19, 3300951),
(3612, 'Conceição de Macabu', 19, 3301405),
(3613, 'Cordeiro', 19, 3301504),
(3614, 'Duas Barras', 19, 3301603),
(3615, 'Duque de Caxias', 19, 3301702),
(3616, 'Engenheiro Paulo de Frontin', 19, 3301801),
(3617, 'Guapimirim', 19, 3301850),
(3618, 'Iguaba Grande', 19, 3301876),
(3619, 'Itaboraí', 19, 3301900),
(3620, 'Itaguaí', 19, 3302007),
(3621, 'Italva', 19, 3302056),
(3622, 'Itaocara', 19, 3302106),
(3623, 'Itaperuna', 19, 3302205),
(3624, 'Itatiaia', 19, 3302254),
(3625, 'Japeri', 19, 3302270),
(3626, 'Laje do Muriaé', 19, 3302304),
(3627, 'Macaé', 19, 3302403),
(3628, 'Macuco', 19, 3302452),
(3629, 'Magé', 19, 3302502),
(3630, 'Mangaratiba', 19, 3302601),
(3631, 'Maricá', 19, 3302700),
(3632, 'Mendes', 19, 3302809),
(3633, 'Mesquita', 19, 3302858),
(3634, 'Miguel Pereira', 19, 3302908),
(3635, 'Miracema', 19, 3303005),
(3636, 'Natividade', 19, 3303104),
(3637, 'Nilópolis', 19, 3303203),
(3638, 'Niterói', 19, 3303302),
(3639, 'Nova Friburgo', 19, 3303401),
(3640, 'Nova Iguaçu', 19, 3303500),
(3641, 'Paracambi', 19, 3303609),
(3642, 'Paraíba do Sul', 19, 3303708),
(3643, 'Parati', 19, 3303807),
(3644, 'Paty do Alferes', 19, 3303856),
(3645, 'Petrópolis', 19, 3303906),
(3646, 'Pinheiral', 19, 3303955),
(3647, 'Piraí', 19, 3304003),
(3648, 'Porciúncula', 19, 3304102),
(3649, 'Porto Real', 19, 3304110),
(3650, 'Quatis', 19, 3304128),
(3651, 'Queimados', 19, 3304144),
(3652, 'Quissamã', 19, 3304151),
(3653, 'Resende', 19, 3304201),
(3654, 'Rio Bonito', 19, 3304300),
(3655, 'Rio Claro', 19, 3304409),
(3656, 'Rio das Flores', 19, 3304508),
(3657, 'Rio das Ostras', 19, 3304524),
(3658, 'Rio de Janeiro', 19, 3304557),
(3659, 'Santa Maria Madalena', 19, 3304607),
(3660, 'Santo Antônio de Pádua', 19, 3304706),
(3661, 'São Fidélis', 19, 3304805),
(3662, 'São Francisco de Itabapoana', 19, 3304755),
(3663, 'São Gonçalo', 19, 3304904),
(3664, 'São João da Barra', 19, 3305000),
(3665, 'São João de Meriti', 19, 3305109),
(3666, 'São José de Ubá', 19, 3305133),
(3667, 'São José do Vale do Rio Preto', 19, 3305158),
(3668, 'São Pedro da Aldeia', 19, 3305208),
(3669, 'São Sebastião do Alto', 19, 3305307),
(3670, 'Sapucaia', 19, 3305406),
(3671, 'Saquarema', 19, 3305505),
(3672, 'Seropédica', 19, 3305554),
(3673, 'Silva Jardim', 19, 3305604),
(3674, 'Sumidouro', 19, 3305703),
(3675, 'Tanguá', 19, 3305752),
(3676, 'Teresópolis', 19, 3305802),
(3677, 'Trajano de Morais', 19, 3305901),
(3678, 'Três Rios', 19, 3306008),
(3679, 'Valença', 19, 3306107),
(3680, 'Varre-Sai', 19, 3306156),
(3681, 'Vassouras', 19, 3306206),
(3682, 'Volta Redonda', 19, 3306305),
(3683, 'Acari', 20, 2400109),
(3684, 'Açu', 20, 2400208),
(3685, 'Afonso Bezerra', 20, 2400307),
(3686, 'Água Nova', 20, 2400406),
(3687, 'Alexandria', 20, 2400505),
(3688, 'Almino Afonso', 20, 2400604),
(3689, 'Alto do Rodrigues', 20, 2400703),
(3690, 'Jardim de Angicos', 20, 2405504),
(3691, 'Antônio Martins', 20, 2400901),
(3692, 'Apodi', 20, 2401008),
(3693, 'Areia Branca', 20, 2401107),
(3694, 'Arês', 20, 2401206),
(3695, 'Augusto Severo', 20, 2401305),
(3696, 'Baía Formosa', 20, 2401404),
(3697, 'Baraúna', 20, 2401453),
(3698, 'Barcelona', 20, 2401503),
(3699, 'Bento Fernandes', 20, 2401602),
(3700, 'Bodó', 20, 2401651),
(3701, 'Bom Jesus', 20, 2401701),
(3702, 'Brejinho', 20, 2401800),
(3703, 'Caiçara do Norte', 20, 2401859),
(3704, 'Caiçara do Rio do Vento', 20, 2401909),
(3705, 'Caicó', 20, 2402006),
(3706, 'Campo Redondo', 20, 2402105),
(3707, 'Canguaretama', 20, 2402204),
(3708, 'Caraúbas', 20, 2402303),
(3709, 'Carnaúba dos Dantas', 20, 2402402),
(3710, 'Carnaubais', 20, 2402501),
(3711, 'Ceará-Mirim', 20, 2402600),
(3712, 'Cerro Corá', 20, 2402709),
(3713, 'Coronel Ezequiel', 20, 2402808),
(3714, 'Coronel João Pessoa', 20, 2402907),
(3715, 'Cruzeta', 20, 2403004),
(3716, 'Currais Novos', 20, 2403103),
(3717, 'Doutor Severiano', 20, 2403202),
(3718, 'Encanto', 20, 2403301),
(3719, 'Equador', 20, 2403400),
(3720, 'Espírito Santo', 20, 2403509),
(3721, 'Extremoz', 20, 2403608),
(3722, 'Felipe Guerra', 20, 2403707),
(3723, 'Fernando Pedroza', 20, 2403756),
(3724, 'Florânia', 20, 2403806),
(3725, 'Francisco Dantas', 20, 2403905),
(3726, 'Frutuoso Gomes', 20, 2404002),
(3727, 'Galinhos', 20, 2404101),
(3728, 'Goianinha', 20, 2404200),
(3729, 'Governador Dix-Sept Rosado', 20, 2404309),
(3730, 'Grossos', 20, 2404408),
(3731, 'Guamaré', 20, 2404507),
(3732, 'Ielmo Marinho', 20, 2404606),
(3733, 'Ipanguaçu', 20, 2404705),
(3734, 'Ipueira', 20, 2404804),
(3735, 'Itajá', 20, 2404853),
(3736, 'Itaú', 20, 2404903),
(3737, 'Jaçanã', 20, 2405009),
(3738, 'Jandaíra', 20, 2405108),
(3739, 'Janduís', 20, 2405207),
(3740, 'Januário Cicco', 20, 2405306),
(3741, 'Japi', 20, 2405405),
(3743, 'Jardim de Piranhas', 20, 2405603),
(3744, 'Jardim do Seridó', 20, 2405702),
(3745, 'João Câmara', 20, 2405801),
(3746, 'João Dias', 20, 2405900),
(3747, 'José da Penha', 20, 2406007),
(3748, 'Jucurutu', 20, 2406106),
(3749, 'Jundiá', 20, 2406155),
(3750, 'Lagoa d`Anta', 20, 2406205),
(3751, 'Lagoa de Pedras', 20, 2406304),
(3752, 'Lagoa de Velhos', 20, 2406403),
(3753, 'Lagoa Nova', 20, 2406502),
(3754, 'Lagoa Salgada', 20, 2406601),
(3755, 'Lajes', 20, 2406700),
(3756, 'Lajes Pintadas', 20, 2406809),
(3757, 'Lucrécia', 20, 2406908),
(3758, 'Luís Gomes', 20, 2407005),
(3759, 'Macaíba', 20, 2407104),
(3760, 'Macau', 20, 2407203),
(3761, 'Major Sales', 20, 2407252),
(3762, 'Marcelino Vieira', 20, 2407302),
(3763, 'Martins', 20, 2407401),
(3764, 'Maxaranguape', 20, 2407500),
(3765, 'Messias Targino', 20, 2407609),
(3766, 'Montanhas', 20, 2407708),
(3767, 'Monte Alegre', 20, 2407807),
(3768, 'Monte das Gameleiras', 20, 2407906),
(3769, 'Mossoró', 20, 2408003),
(3770, 'Natal', 20, 2408102),
(3771, 'Nísia Floresta', 20, 2408201),
(3772, 'Nova Cruz', 20, 2408300),
(3773, 'Olho-d`Água do Borges', 20, 2408409),
(3774, 'Ouro Branco', 20, 2408508),
(3775, 'Paraná', 20, 2408607),
(3776, 'Paraú', 20, 2408706),
(3777, 'Parazinho', 20, 2408805),
(3778, 'Parelhas', 20, 2408904),
(3779, 'Parnamirim', 20, 2403251),
(3780, 'Passa e Fica', 20, 2409100),
(3781, 'Passagem', 20, 2409209),
(3782, 'Patu', 20, 2409308),
(3783, 'Pau dos Ferros', 20, 2409407),
(3784, 'Pedra Grande', 20, 2409506),
(3785, 'Pedra Preta', 20, 2409605),
(3786, 'Pedro Avelino', 20, 2409704),
(3787, 'Pedro Velho', 20, 2409803),
(3788, 'Pendências', 20, 2409902),
(3789, 'Pilões', 20, 2410009),
(3790, 'Poço Branco', 20, 2410108),
(3791, 'Portalegre', 20, 2410207),
(3792, 'Porto do Mangue', 20, 2410256),
(3793, 'Presidente Juscelino', 20, 2410306),
(3794, 'Pureza', 20, 2410405),
(3795, 'Rafael Fernandes', 20, 2410504),
(3796, 'Rafael Godeiro', 20, 2410603),
(3797, 'Riacho da Cruz', 20, 2410702),
(3798, 'Riacho de Santana', 20, 2410801),
(3799, 'Riachuelo', 20, 2410900),
(3800, 'Rio do Fogo', 20, 2408953),
(3801, 'Rodolfo Fernandes', 20, 2411007),
(3802, 'Ruy Barbosa', 20, 2411106),
(3803, 'Santa Cruz', 20, 2411205),
(3804, 'Santa Maria', 20, 2409332),
(3805, 'Santana do Matos', 20, 2411403),
(3806, 'Santana do Seridó', 20, 2411429),
(3807, 'Santo Antônio', 20, 2411502),
(3808, 'São Bento do Norte', 20, 2411601),
(3809, 'São Bento do Trairí', 20, 2411700),
(3810, 'São Fernando', 20, 2411809),
(3811, 'São Francisco do Oeste', 20, 2411908),
(3812, 'São Gonçalo do Amarante', 20, 2412005),
(3813, 'São João do Sabugi', 20, 2412104),
(3814, 'São José de Mipibu', 20, 2412203),
(3815, 'São José do Campestre', 20, 2412302),
(3816, 'São José do Seridó', 20, 2412401),
(3817, 'São Miguel', 20, 2412500),
(3818, 'São Miguel do Gostoso', 20, 2412559),
(3819, 'São Paulo do Potengi', 20, 2412609),
(3820, 'São Pedro', 20, 2412708),
(3821, 'São Rafael', 20, 2412807),
(3822, 'São Tomé', 20, 2412906),
(3823, 'São Vicente', 20, 2413003),
(3824, 'Senador Elói de Souza', 20, 2413102),
(3825, 'Senador Georgino Avelino', 20, 2413201),
(3826, 'Serra de São Bento', 20, 2413300),
(3827, 'Serra do Mel', 20, 2413359),
(3828, 'Serra Negra do Norte', 20, 2413409),
(3829, 'Serrinha', 20, 2413508),
(3830, 'Serrinha dos Pintos', 20, 2413557),
(3831, 'Severiano Melo', 20, 2413607),
(3832, 'Sítio Novo', 20, 2413706),
(3833, 'Taboleiro Grande', 20, 2413805),
(3834, 'Taipu', 20, 2413904),
(3835, 'Tangará', 20, 2414001),
(3836, 'Tenente Ananias', 20, 2414100),
(3837, 'Tenente Laurentino Cruz', 20, 2414159),
(3838, 'Tibau', 20, 2411056),
(3839, 'Tibau do Sul', 20, 2414209),
(3840, 'Timbaúba dos Batistas', 20, 2414308),
(3841, 'Touros', 20, 2414407),
(3842, 'Triunfo Potiguar', 20, 2414456),
(3843, 'Umarizal', 20, 2414506),
(3844, 'Upanema', 20, 2414605),
(3845, 'Várzea', 20, 2414704),
(3846, 'Venha-Ver', 20, 2414753),
(3847, 'Vera Cruz', 20, 2414803),
(3848, 'Viçosa', 20, 2414902),
(3849, 'Vila Flor', 20, 2415008),
(3850, 'Aceguá', 23, 4300034),
(3851, 'Água Santa', 23, 4300059),
(3852, 'Agudo', 23, 4300109),
(3853, 'Ajuricaba', 23, 4300208),
(3854, 'Alecrim', 23, 4300307),
(3855, 'Alegrete', 23, 4300406),
(3856, 'Alegria', 23, 4300455),
(3857, 'Almirante Tamandaré do Sul', 23, 4300471),
(3858, 'Alpestre', 23, 4300505),
(3859, 'Alto Alegre', 23, 4300554),
(3860, 'Alto Feliz', 23, 4300570),
(3861, 'Alvorada', 23, 4300604),
(3862, 'Amaral Ferrador', 23, 4300638),
(3863, 'Ametista do Sul', 23, 4300646),
(3864, 'André da Rocha', 23, 4300661),
(3865, 'Anta Gorda', 23, 4300703),
(3866, 'Antônio Prado', 23, 4300802),
(3867, 'Arambaré', 23, 4300851),
(3868, 'Araricá', 23, 4300877),
(3869, 'Aratiba', 23, 4300901),
(3870, 'Arroio do Meio', 23, 4301008),
(3871, 'Arroio do Padre', 23, 4301073),
(3872, 'Arroio do Sal', 23, 4301057),
(3873, 'Arroio do Tigre', 23, 4301206),
(3874, 'Arroio dos Ratos', 23, 4301107),
(3875, 'Arroio Grande', 23, 4301305),
(3876, 'Arvorezinha', 23, 4301404),
(3877, 'Augusto Pestana', 23, 4301503),
(3878, 'Áurea', 23, 4301552),
(3879, 'Bagé', 23, 4301602),
(3880, 'Balneário Pinhal', 23, 4301636),
(3881, 'Barão', 23, 4301651),
(3882, 'Barão de Cotegipe', 23, 4301701),
(3883, 'Barão do Triunfo', 23, 4301750),
(3884, 'Barra do Guarita', 23, 4301859),
(3885, 'Barra do Quaraí', 23, 4301875),
(3886, 'Barra do Ribeiro', 23, 4301909),
(3887, 'Barra do Rio Azul', 23, 4301925),
(3888, 'Barra Funda', 23, 4301958),
(3889, 'Barracão', 23, 4301800),
(3890, 'Barros Cassal', 23, 4302006),
(3891, 'Benjamin Constant do Sul', 23, 4302055),
(3892, 'Bento Gonçalves', 23, 4302105),
(3893, 'Boa Vista das Missões', 23, 4302154),
(3894, 'Boa Vista do Buricá', 23, 4302204),
(3895, 'Boa Vista do Cadeado', 23, 4302220),
(3896, 'Boa Vista do Incra', 23, 4302238),
(3897, 'Boa Vista do Sul', 23, 4302253),
(3898, 'Bom Jesus', 23, 4302303),
(3899, 'Bom Princípio', 23, 4302352),
(3900, 'Bom Progresso', 23, 4302378),
(3901, 'Bom Retiro do Sul', 23, 4302402),
(3902, 'Boqueirão do Leão', 23, 4302451),
(3903, 'Bossoroca', 23, 4302501),
(3904, 'Bozano', 23, 4302584),
(3905, 'Braga', 23, 4302600),
(3906, 'Brochier', 23, 4302659),
(3907, 'Butiá', 23, 4302709),
(3908, 'Caçapava do Sul', 23, 4302808),
(3909, 'Cacequi', 23, 4302907),
(3910, 'Cachoeira do Sul', 23, 4303004),
(3911, 'Cachoeirinha', 23, 4303103),
(3912, 'Cacique Doble', 23, 4303202),
(3913, 'Caibaté', 23, 4303301),
(3914, 'Caiçara', 23, 4303400),
(3915, 'Camaquã', 23, 4303509),
(3916, 'Camargo', 23, 4303558),
(3917, 'Cambará do Sul', 23, 4303608),
(3918, 'Campestre da Serra', 23, 4303673),
(3919, 'Campina das Missões', 23, 4303707),
(3920, 'Campinas do Sul', 23, 4303806),
(3921, 'Campo Bom', 23, 4303905),
(3922, 'Campo Novo', 23, 4304002),
(3923, 'Campos Borges', 23, 4304101),
(3924, 'Candelária', 23, 4304200),
(3925, 'Cândido Godói', 23, 4304309),
(3926, 'Candiota', 23, 4304358),
(3927, 'Canela', 23, 4304408),
(3928, 'Canguçu', 23, 4304507),
(3929, 'Canoas', 23, 4304606),
(3930, 'Canudos do Vale', 23, 4304614),
(3931, 'Capão Bonito do Sul', 23, 4304622),
(3932, 'Capão da Canoa', 23, 4304630),
(3933, 'Capão do Cipó', 23, 4304655),
(3934, 'Capão do Leão', 23, 4304663),
(3935, 'Capela de Santana', 23, 4304689),
(3936, 'Capitão', 23, 4304697),
(3937, 'Capivari do Sul', 23, 4304671),
(3938, 'Caraá', 23, 4304713),
(3939, 'Carazinho', 23, 4304705),
(3940, 'Carlos Barbosa', 23, 4304804),
(3941, 'Carlos Gomes', 23, 4304853),
(3942, 'Casca', 23, 4304903),
(3943, 'Caseiros', 23, 4304952),
(3944, 'Catuípe', 23, 4305009),
(3945, 'Caxias do Sul', 23, 4305108),
(3946, 'Centenário', 23, 4305116),
(3947, 'Cerrito', 23, 4305124),
(3948, 'Cerro Branco', 23, 4305132),
(3949, 'Cerro Grande', 23, 4305157),
(3950, 'Cerro Grande do Sul', 23, 4305173),
(3951, 'Cerro Largo', 23, 4305207),
(3952, 'Chapada', 23, 4305306),
(3953, 'Charqueadas', 23, 4305355),
(3954, 'Charrua', 23, 4305371),
(3955, 'Chiapetta', 23, 4305405),
(3956, 'Chuí', 23, 4305439),
(3957, 'Chuvisca', 23, 4305447),
(3958, 'Cidreira', 23, 4305454),
(3959, 'Ciríaco', 23, 4305504),
(3960, 'Colinas', 23, 4305587),
(3961, 'Colorado', 23, 4305603),
(3962, 'Condor', 23, 4305702),
(3963, 'Constantina', 23, 4305801),
(3964, 'Coqueiro Baixo', 23, 4305835),
(3965, 'Coqueiros do Sul', 23, 4305850),
(3966, 'Coronel Barros', 23, 4305871),
(3967, 'Coronel Bicaco', 23, 4305900),
(3968, 'Coronel Pilar', 23, 4305934),
(3969, 'Cotiporã', 23, 4305959),
(3970, 'Coxilha', 23, 4305975),
(3971, 'Crissiumal', 23, 4306007),
(3972, 'Cristal', 23, 4306056),
(3973, 'Cristal do Sul', 23, 4306072),
(3974, 'Cruz Alta', 23, 4306106),
(3975, 'Cruzaltense', 23, 4306130),
(3976, 'Cruzeiro do Sul', 23, 4306205),
(3977, 'David Canabarro', 23, 4306304),
(3978, 'Derrubadas', 23, 4306320),
(3979, 'Dezesseis de Novembro', 23, 4306353),
(3980, 'Dilermando de Aguiar', 23, 4306379),
(3981, 'Dois Irmãos', 23, 4306403),
(3982, 'Dois Irmãos das Missões', 23, 4306429),
(3983, 'Dois Lajeados', 23, 4306452),
(3984, 'Dom Feliciano', 23, 4306502),
(3985, 'Dom Pedrito', 23, 4306601),
(3986, 'Dom Pedro de Alcântara', 23, 4306551),
(3987, 'Dona Francisca', 23, 4306700),
(3988, 'Doutor Maurício Cardoso', 23, 4306734),
(3989, 'Doutor Ricardo', 23, 4306759),
(3990, 'Eldorado do Sul', 23, 4306767),
(3991, 'Encantado', 23, 4306809),
(3992, 'Encruzilhada do Sul', 23, 4306908),
(3993, 'Engenho Velho', 23, 4306924),
(3994, 'Entre Rios do Sul', 23, 4306957),
(3995, 'Entre-Ijuís', 23, 4306932),
(3996, 'Erebango', 23, 4306973),
(3997, 'Erechim', 23, 4307005),
(3998, 'Ernestina', 23, 4307054),
(3999, 'Erval Grande', 23, 4307203),
(4000, 'Erval Seco', 23, 4307302),
(4001, 'Esmeralda', 23, 4307401),
(4002, 'Esperança do Sul', 23, 4307450),
(4003, 'Espumoso', 23, 4307500),
(4004, 'Estação', 23, 4307559),
(4005, 'Estância Velha', 23, 4307609),
(4006, 'Esteio', 23, 4307708),
(4007, 'Estrela', 23, 4307807),
(4008, 'Estrela Velha', 23, 4307815),
(4009, 'Eugênio de Castro', 23, 4307831),
(4010, 'Fagundes Varela', 23, 4307864),
(4011, 'Farroupilha', 23, 4307906),
(4012, 'Faxinal do Soturno', 23, 4308003),
(4013, 'Faxinalzinho', 23, 4308052),
(4014, 'Fazenda Vilanova', 23, 4308078),
(4015, 'Feliz', 23, 4308102),
(4016, 'Flores da Cunha', 23, 4308201),
(4017, 'Floriano Peixoto', 23, 4308250),
(4018, 'Fontoura Xavier', 23, 4308300),
(4019, 'Formigueiro', 23, 4308409),
(4020, 'Forquetinha', 23, 4308433),
(4021, 'Fortaleza dos Valos', 23, 4308458),
(4022, 'Frederico Westphalen', 23, 4308508),
(4023, 'Garibaldi', 23, 4308607),
(4024, 'Garruchos', 23, 4308656),
(4025, 'Gaurama', 23, 4308706),
(4026, 'General Câmara', 23, 4308805),
(4027, 'Gentil', 23, 4308854),
(4028, 'Getúlio Vargas', 23, 4308904),
(4029, 'Giruá', 23, 4309001),
(4030, 'Glorinha', 23, 4309050),
(4031, 'Gramado', 23, 4309100),
(4032, 'Gramado dos Loureiros', 23, 4309126),
(4033, 'Gramado Xavier', 23, 4309159),
(4034, 'Gravataí', 23, 4309209),
(4035, 'Guabiju', 23, 4309258),
(4036, 'Guaíba', 23, 4309308),
(4037, 'Guaporé', 23, 4309407),
(4038, 'Guarani das Missões', 23, 4309506),
(4039, 'Harmonia', 23, 4309555),
(4040, 'Herval', 23, 4307104),
(4041, 'Herveiras', 23, 4309571),
(4042, 'Horizontina', 23, 4309605),
(4043, 'Hulha Negra', 23, 4309654),
(4044, 'Humaitá', 23, 4309704),
(4045, 'Ibarama', 23, 4309753),
(4046, 'Ibiaçá', 23, 4309803),
(4047, 'Ibiraiaras', 23, 4309902),
(4048, 'Ibirapuitã', 23, 4309951),
(4049, 'Ibirubá', 23, 4310009),
(4050, 'Igrejinha', 23, 4310108),
(4051, 'Ijuí', 23, 4310207),
(4052, 'Ilópolis', 23, 4310306),
(4053, 'Imbé', 23, 4310330),
(4054, 'Imigrante', 23, 4310363),
(4055, 'Independência', 23, 4310405),
(4056, 'Inhacorá', 23, 4310413),
(4057, 'Ipê', 23, 4310439),
(4058, 'Ipiranga do Sul', 23, 4310462),
(4059, 'Iraí', 23, 4310504),
(4060, 'Itaara', 23, 4310538),
(4061, 'Itacurubi', 23, 4310553),
(4062, 'Itapuca', 23, 4310579),
(4063, 'Itaqui', 23, 4310603),
(4064, 'Itati', 23, 4310652),
(4065, 'Itatiba do Sul', 23, 4310702),
(4066, 'Ivorá', 23, 4310751),
(4067, 'Ivoti', 23, 4310801),
(4068, 'Jaboticaba', 23, 4310850),
(4069, 'Jacuizinho', 23, 4310876),
(4070, 'Jacutinga', 23, 4310900),
(4071, 'Jaguarão', 23, 4311007),
(4072, 'Jaguari', 23, 4311106),
(4073, 'Jaquirana', 23, 4311122),
(4074, 'Jari', 23, 4311130),
(4075, 'Jóia', 23, 4311155),
(4076, 'Júlio de Castilhos', 23, 4311205),
(4077, 'Lagoa Bonita do Sul', 23, 4311239),
(4078, 'Lagoa dos Três Cantos', 23, 4311270),
(4079, 'Lagoa Vermelha', 23, 4311304),
(4080, 'Lagoão', 23, 4311254),
(4081, 'Lajeado', 23, 4311403),
(4082, 'Lajeado do Bugre', 23, 4311429),
(4083, 'Lavras do Sul', 23, 4311502),
(4084, 'Liberato Salzano', 23, 4311601),
(4085, 'Lindolfo Collor', 23, 4311627),
(4086, 'Linha Nova', 23, 4311643),
(4087, 'Maçambara', 23, 4311718),
(4088, 'Machadinho', 23, 4311700),
(4089, 'Mampituba', 23, 4311734),
(4090, 'Manoel Viana', 23, 4311759),
(4091, 'Maquiné', 23, 4311775),
(4092, 'Maratá', 23, 4311791),
(4093, 'Marau', 23, 4311809),
(4094, 'Marcelino Ramos', 23, 4311908),
(4095, 'Mariana Pimentel', 23, 4311981),
(4096, 'Mariano Moro', 23, 4312005),
(4097, 'Marques de Souza', 23, 4312054),
(4098, 'Mata', 23, 4312104),
(4099, 'Mato Castelhano', 23, 4312138),
(4100, 'Mato Leitão', 23, 4312153),
(4101, 'Mato Queimado', 23, 4312179),
(4102, 'Maximiliano de Almeida', 23, 4312203),
(4103, 'Minas do Leão', 23, 4312252),
(4104, 'Miraguaí', 23, 4312302),
(4105, 'Montauri', 23, 4312351),
(4106, 'Monte Alegre dos Campos', 23, 4312377),
(4107, 'Monte Belo do Sul', 23, 4312385),
(4108, 'Montenegro', 23, 4312401),
(4109, 'Mormaço', 23, 4312427),
(4110, 'Morrinhos do Sul', 23, 4312443),
(4111, 'Morro Redondo', 23, 4312450),
(4112, 'Morro Reuter', 23, 4312476),
(4113, 'Mostardas', 23, 4312500),
(4114, 'Muçum', 23, 4312609),
(4115, 'Muitos Capões', 23, 4312617),
(4116, 'Muliterno', 23, 4312625),
(4117, 'Não-Me-Toque', 23, 4312658),
(4118, 'Nicolau Vergueiro', 23, 4312674),
(4119, 'Nonoai', 23, 4312708),
(4120, 'Nova Alvorada', 23, 4312757),
(4121, 'Nova Araçá', 23, 4312807),
(4122, 'Nova Bassano', 23, 4312906),
(4123, 'Nova Boa Vista', 23, 4312955),
(4124, 'Nova Bréscia', 23, 4313003),
(4125, 'Nova Candelária', 23, 4313011),
(4126, 'Nova Esperança do Sul', 23, 4313037),
(4127, 'Nova Hartz', 23, 4313060),
(4128, 'Nova Pádua', 23, 4313086),
(4129, 'Nova Palma', 23, 4313102),
(4130, 'Nova Petrópolis', 23, 4313201),
(4131, 'Nova Prata', 23, 4313300),
(4132, 'Nova Ramada', 23, 4313334),
(4133, 'Nova Roma do Sul', 23, 4313359),
(4134, 'Nova Santa Rita', 23, 4313375),
(4135, 'Novo Barreiro', 23, 4313490),
(4136, 'Novo Cabrais', 23, 4313391),
(4137, 'Novo Hamburgo', 23, 4313409),
(4138, 'Novo Machado', 23, 4313425),
(4139, 'Novo Tiradentes', 23, 4313441),
(4140, 'Novo Xingu', 23, 4313466),
(4141, 'Osório', 23, 4313508),
(4142, 'Paim Filho', 23, 4313607),
(4143, 'Palmares do Sul', 23, 4313656),
(4144, 'Palmeira das Missões', 23, 4313706),
(4145, 'Palmitinho', 23, 4313805),
(4146, 'Panambi', 23, 4313904),
(4147, 'Pantano Grande', 23, 4313953),
(4148, 'Paraí', 23, 4314001),
(4149, 'Paraíso do Sul', 23, 4314027),
(4150, 'Pareci Novo', 23, 4314035),
(4151, 'Parobé', 23, 4314050),
(4152, 'Passa Sete', 23, 4314068),
(4153, 'Passo do Sobrado', 23, 4314076),
(4154, 'Passo Fundo', 23, 4314100),
(4155, 'Paulo Bento', 23, 4314134),
(4156, 'Paverama', 23, 4314159),
(4157, 'Pedras Altas', 23, 4314175),
(4158, 'Pedro Osório', 23, 4314209),
(4159, 'Pejuçara', 23, 4314308),
(4160, 'Pelotas', 23, 4314407),
(4161, 'Picada Café', 23, 4314423),
(4162, 'Pinhal', 23, 4314456),
(4163, 'Pinhal da Serra', 23, 4314464),
(4164, 'Pinhal Grande', 23, 4314472),
(4165, 'Pinheirinho do Vale', 23, 4314498),
(4166, 'Pinheiro Machado', 23, 4314506),
(4167, 'Pirapó', 23, 4314555),
(4168, 'Piratini', 23, 4314605),
(4169, 'Planalto', 23, 4314704),
(4170, 'Poço das Antas', 23, 4314753),
(4171, 'Pontão', 23, 4314779),
(4172, 'Ponte Preta', 23, 4314787),
(4173, 'Portão', 23, 4314803),
(4174, 'Porto Alegre', 23, 4314902),
(4175, 'Porto Lucena', 23, 4315008),
(4176, 'Porto Mauá', 23, 4315057),
(4177, 'Porto Vera Cruz', 23, 4315073),
(4178, 'Porto Xavier', 23, 4315107),
(4179, 'Pouso Novo', 23, 4315131),
(4180, 'Presidente Lucena', 23, 4315149),
(4181, 'Progresso', 23, 4315156),
(4182, 'Protásio Alves', 23, 4315172),
(4183, 'Putinga', 23, 4315206),
(4184, 'Quaraí', 23, 4315305),
(4185, 'Quatro Irmãos', 23, 4315313),
(4186, 'Quevedos', 23, 4315321),
(4187, 'Quinze de Novembro', 23, 4315354),
(4188, 'Redentora', 23, 4315404),
(4189, 'Relvado', 23, 4315453),
(4190, 'Restinga Seca', 23, 4315503),
(4191, 'Rio dos Índios', 23, 4315552),
(4192, 'Rio Grande', 23, 4315602),
(4193, 'Rio Pardo', 23, 4315701),
(4194, 'Riozinho', 23, 4315750),
(4195, 'Roca Sales', 23, 4315800),
(4196, 'Rodeio Bonito', 23, 4315909),
(4197, 'Rolador', 23, 4315958),
(4198, 'Rolante', 23, 4316006),
(4199, 'Ronda Alta', 23, 4316105),
(4200, 'Rondinha', 23, 4316204),
(4201, 'Roque Gonzales', 23, 4316303),
(4202, 'Rosário do Sul', 23, 4316402),
(4203, 'Sagrada Família', 23, 4316428),
(4204, 'Saldanha Marinho', 23, 4316436),
(4205, 'Salto do Jacuí', 23, 4316451),
(4206, 'Salvador das Missões', 23, 4316477),
(4207, 'Salvador do Sul', 23, 4316501),
(4208, 'Sananduva', 23, 4316600),
(4209, 'Santa Bárbara do Sul', 23, 4316709),
(4210, 'Santa Cecília do Sul', 23, 4316733),
(4211, 'Santa Clara do Sul', 23, 4316758),
(4212, 'Santa Cruz do Sul', 23, 4316808),
(4213, 'Santa Margarida do Sul', 23, 4316972),
(4214, 'Santa Maria', 23, 4316907),
(4215, 'Santa Maria do Herval', 23, 4316956),
(4216, 'Santa Rosa', 23, 4317202),
(4217, 'Santa Tereza', 23, 4317251),
(4218, 'Santa Vitória do Palmar', 23, 4317301),
(4219, 'Santana da Boa Vista', 23, 4317004),
(4220, 'Santana do Livramento', 23, 4317103),
(4221, 'Santiago', 23, 4317400),
(4222, 'Santo Ângelo', 23, 4317509),
(4223, 'Santo Antônio da Patrulha', 23, 4317608),
(4224, 'Santo Antônio das Missões', 23, 4317707),
(4225, 'Santo Antônio do Palma', 23, 4317558),
(4226, 'Santo Antônio do Planalto', 23, 4317756),
(4227, 'Santo Augusto', 23, 4317806),
(4228, 'Santo Cristo', 23, 4317905),
(4229, 'Santo Expedito do Sul', 23, 4317954),
(4230, 'São Borja', 23, 4318002),
(4231, 'São Domingos do Sul', 23, 4318051),
(4232, 'São Francisco de Assis', 23, 4318101),
(4233, 'São Francisco de Paula', 23, 4318200),
(4234, 'São Gabriel', 23, 4318309),
(4235, 'São Jerônimo', 23, 4318408),
(4236, 'São João da Urtiga', 23, 4318424),
(4237, 'São João do Polêsine', 23, 4318432),
(4238, 'São Jorge', 23, 4318440),
(4239, 'São José das Missões', 23, 4318457),
(4240, 'São José do Herval', 23, 4318465),
(4241, 'São José do Hortêncio', 23, 4318481),
(4242, 'São José do Inhacorá', 23, 4318499),
(4243, 'São José do Norte', 23, 4318507),
(4244, 'São José do Ouro', 23, 4318606),
(4245, 'São José do Sul', 23, 4318614),
(4246, 'São José dos Ausentes', 23, 4318622),
(4247, 'São Leopoldo', 23, 4318705),
(4248, 'São Lourenço do Sul', 23, 4318804),
(4249, 'São Luiz Gonzaga', 23, 4318903),
(4250, 'São Marcos', 23, 4319000),
(4251, 'São Martinho', 23, 4319109),
(4252, 'São Martinho da Serra', 23, 4319125),
(4253, 'São Miguel das Missões', 23, 4319158),
(4254, 'São Nicolau', 23, 4319208),
(4255, 'São Paulo das Missões', 23, 4319307),
(4256, 'São Pedro da Serra', 23, 4319356),
(4257, 'São Pedro das Missões', 23, 4319364),
(4258, 'São Pedro do Butiá', 23, 4319372),
(4259, 'São Pedro do Sul', 23, 4319406),
(4260, 'São Sebastião do Caí', 23, 4319505),
(4261, 'São Sepé', 23, 4319604),
(4262, 'São Valentim', 23, 4319703),
(4263, 'São Valentim do Sul', 23, 4319711),
(4264, 'São Valério do Sul', 23, 4319737),
(4265, 'São Vendelino', 23, 4319752),
(4266, 'São Vicente do Sul', 23, 4319802),
(4267, 'Sapiranga', 23, 4319901),
(4268, 'Sapucaia do Sul', 23, 4320008),
(4269, 'Sarandi', 23, 4320107),
(4270, 'Seberi', 23, 4320206),
(4271, 'Sede Nova', 23, 4320230),
(4272, 'Segredo', 23, 4320263),
(4273, 'Selbach', 23, 4320305),
(4274, 'Senador Salgado Filho', 23, 4320321),
(4275, 'Sentinela do Sul', 23, 4320354),
(4276, 'Serafina Corrêa', 23, 4320404),
(4277, 'Sério', 23, 4320453),
(4278, 'Sertão', 23, 4320503),
(4279, 'Sertão Santana', 23, 4320552),
(4280, 'Sete de Setembro', 23, 4320578),
(4281, 'Severiano de Almeida', 23, 4320602),
(4282, 'Silveira Martins', 23, 4320651),
(4283, 'Sinimbu', 23, 4320677),
(4284, 'Sobradinho', 23, 4320701),
(4285, 'Soledade', 23, 4320800),
(4286, 'Tabaí', 23, 4320859),
(4287, 'Tapejara', 23, 4320909),
(4288, 'Tapera', 23, 4321006),
(4289, 'Tapes', 23, 4321105),
(4290, 'Taquara', 23, 4321204),
(4291, 'Taquari', 23, 4321303),
(4292, 'Taquaruçu do Sul', 23, 4321329),
(4293, 'Tavares', 23, 4321352),
(4294, 'Tenente Portela', 23, 4321402),
(4295, 'Terra de Areia', 23, 4321436),
(4296, 'Teutônia', 23, 4321451),
(4297, 'Tio Hugo', 23, 4321469),
(4298, 'Tiradentes do Sul', 23, 4321477),
(4299, 'Toropi', 23, 4321493),
(4300, 'Torres', 23, 4321501),
(4301, 'Tramandaí', 23, 4321600),
(4302, 'Travesseiro', 23, 4321626),
(4303, 'Três Arroios', 23, 4321634),
(4304, 'Três Cachoeiras', 23, 4321667),
(4305, 'Três Coroas', 23, 4321709),
(4306, 'Três de Maio', 23, 4321808),
(4307, 'Três Forquilhas', 23, 4321832),
(4308, 'Três Palmeiras', 23, 4321857),
(4309, 'Três Passos', 23, 4321907),
(4310, 'Trindade do Sul', 23, 4321956),
(4311, 'Triunfo', 23, 4322004),
(4312, 'Tucunduva', 23, 4322103),
(4313, 'Tunas', 23, 4322152),
(4314, 'Tupanci do Sul', 23, 4322186),
(4315, 'Tupanciretã', 23, 4322202),
(4316, 'Tupandi', 23, 4322251),
(4317, 'Tuparendi', 23, 4322301),
(4318, 'Turuçu', 23, 4322327),
(4319, 'Ubiretama', 23, 4322343),
(4320, 'União da Serra', 23, 4322350),
(4321, 'Unistalda', 23, 4322376),
(4322, 'Uruguaiana', 23, 4322400),
(4323, 'Vacaria', 23, 4322509),
(4324, 'Vale do Sol', 23, 4322533),
(4325, 'Vale Real', 23, 4322541),
(4326, 'Vale Verde', 23, 4322525),
(4327, 'Vanini', 23, 4322558),
(4328, 'Venâncio Aires', 23, 4322608),
(4329, 'Vera Cruz', 23, 4322707),
(4330, 'Veranópolis', 23, 4322806),
(4331, 'Vespasiano Correa', 23, 4322855),
(4332, 'Viadutos', 23, 4322905),
(4333, 'Viamão', 23, 4323002),
(4334, 'Vicente Dutra', 23, 4323101),
(4335, 'Victor Graeff', 23, 4323200),
(4336, 'Vila Flores', 23, 4323309),
(4337, 'Vila Lângaro', 23, 4323358),
(4338, 'Vila Maria', 23, 4323408),
(4339, 'Vila Nova do Sul', 23, 4323457),
(4340, 'Vista Alegre', 23, 4323507),
(4341, 'Vista Alegre do Prata', 23, 4323606),
(4342, 'Vista Gaúcha', 23, 4323705),
(4343, 'Vitória das Missões', 23, 4323754),
(4344, 'Westfália', 23, 4323770),
(4345, 'Xangri-lá', 23, 4323804),
(4346, 'Alta Floresta d`Oeste', 21, 1100015),
(4347, 'Alto Alegre dos Parecis', 21, 1100379),
(4348, 'Alto Paraíso', 21, 1100403),
(4349, 'Alvorada d`Oeste', 21, 1100346),
(4350, 'Ariquemes', 21, 1100023),
(4351, 'Buritis', 21, 1100452),
(4352, 'Cabixi', 21, 1100031),
(4353, 'Cacaulândia', 21, 1100601),
(4354, 'Cacoal', 21, 1100049),
(4355, 'Campo Novo de Rondônia', 21, 1100700),
(4356, 'Candeias do Jamari', 21, 1100809),
(4357, 'Castanheiras', 21, 1100908),
(4358, 'Cerejeiras', 21, 1100056),
(4359, 'Chupinguaia', 21, 1100924),
(4360, 'Colorado do Oeste', 21, 1100064),
(4361, 'Corumbiara', 21, 1100072),
(4362, 'Costa Marques', 21, 1100080),
(4363, 'Cujubim', 21, 1100940),
(4364, 'Espigão d`Oeste', 21, 1100098),
(4365, 'Governador Jorge Teixeira', 21, 1101005),
(4366, 'Guajará-Mirim', 21, 1100106),
(4367, 'Itapuã do Oeste', 21, 1101104),
(4368, 'Jaru', 21, 1100114),
(4369, 'Ji-Paraná', 21, 1100122),
(4370, 'Machadinho d`Oeste', 21, 1100130),
(4371, 'Ministro Andreazza', 21, 1101203),
(4372, 'Mirante da Serra', 21, 1101302),
(4373, 'Monte Negro', 21, 1101401),
(4374, 'Nova Brasilândia d`Oeste', 21, 1100148),
(4375, 'Nova Mamoré', 21, 1100338),
(4376, 'Nova União', 21, 1101435),
(4377, 'Novo Horizonte do Oeste', 21, 1100502),
(4378, 'Ouro Preto do Oeste', 21, 1100155),
(4379, 'Parecis', 21, 1101450),
(4380, 'Pimenta Bueno', 21, 1100189);
INSERT INTO `MAM_City` (`CT_ID`, `CT_NOME`, `CT_UF`, `CT_IBGE`) VALUES
(4381, 'Pimenteiras do Oeste', 21, 1101468),
(4382, 'Porto Velho', 21, 1100205),
(4383, 'Presidente Médici', 21, 1100254),
(4384, 'Primavera de Rondônia', 21, 1101476),
(4385, 'Rio Crespo', 21, 1100262),
(4386, 'Rolim de Moura', 21, 1100288),
(4387, 'Santa Luzia d`Oeste', 21, 1100296),
(4388, 'São Felipe d`Oeste', 21, 1101484),
(4389, 'São Francisco do Guaporé', 21, 1101492),
(4390, 'São Miguel do Guaporé', 21, 1100320),
(4391, 'Seringueiras', 21, 1101500),
(4392, 'Teixeirópolis', 21, 1101559),
(4393, 'Theobroma', 21, 1101609),
(4394, 'Urupá', 21, 1101708),
(4395, 'Vale do Anari', 21, 1101757),
(4396, 'Vale do Paraíso', 21, 1101807),
(4397, 'Vilhena', 21, 1100304),
(4398, 'Alto Alegre', 22, 1400050),
(4399, 'Amajari', 22, 1400027),
(4400, 'Boa Vista', 22, 1400100),
(4401, 'Bonfim', 22, 1400159),
(4402, 'Cantá', 22, 1400175),
(4403, 'Caracaraí', 22, 1400209),
(4404, 'Caroebe', 22, 1400233),
(4405, 'Iracema', 22, 1400282),
(4406, 'Mucajaí', 22, 1400308),
(4407, 'Normandia', 22, 1400407),
(4408, 'Pacaraima', 22, 1400456),
(4409, 'Rorainópolis', 22, 1400472),
(4410, 'São João da Baliza', 22, 1400506),
(4411, 'São Luiz', 22, 1400605),
(4412, 'Uiramutã', 22, 1400704),
(4413, 'Abdon Batista', 24, 4200051),
(4414, 'Abelardo Luz', 24, 4200101),
(4415, 'Agrolândia', 24, 4200200),
(4416, 'Agronômica', 24, 4200309),
(4417, 'Água Doce', 24, 4200408),
(4418, 'Águas de Chapecó', 24, 4200507),
(4419, 'Águas Frias', 24, 4200556),
(4420, 'Águas Mornas', 24, 4200606),
(4421, 'Alfredo Wagner', 24, 4200705),
(4422, 'Alto Bela Vista', 24, 4200754),
(4423, 'Anchieta', 24, 4200804),
(4424, 'Angelina', 24, 4200903),
(4425, 'Anita Garibaldi', 24, 4201000),
(4426, 'Anitápolis', 24, 4201109),
(4427, 'Antônio Carlos', 24, 4201208),
(4428, 'Apiúna', 24, 4201257),
(4429, 'Arabutã', 24, 4201273),
(4430, 'Araquari', 24, 4201307),
(4431, 'Araranguá', 24, 4201406),
(4432, 'Armazém', 24, 4201505),
(4433, 'Arroio Trinta', 24, 4201604),
(4434, 'Arvoredo', 24, 4201653),
(4435, 'Ascurra', 24, 4201703),
(4436, 'Atalanta', 24, 4201802),
(4437, 'Aurora', 24, 4201901),
(4438, 'Balneário Arroio do Silva', 24, 4201950),
(4439, 'Balneário Barra do Sul', 24, 4202057),
(4440, 'Balneário Camboriú', 24, 4202008),
(4441, 'Balneário Gaivota', 24, 4202073),
(4442, 'Bandeirante', 24, 4202081),
(4443, 'Barra Bonita', 24, 4202099),
(4444, 'Barra Velha', 24, 4202107),
(4445, 'Bela Vista do Toldo', 24, 4202131),
(4446, 'Belmonte', 24, 4202156),
(4447, 'Benedito Novo', 24, 4202206),
(4448, 'Biguaçu', 24, 4202305),
(4449, 'Blumenau', 24, 4202404),
(4450, 'Bocaina do Sul', 24, 4202438),
(4451, 'Bom Jardim da Serra', 24, 4202503),
(4452, 'Bom Jesus', 24, 4202537),
(4453, 'Bom Jesus do Oeste', 24, 4202578),
(4454, 'Bom Retiro', 24, 4202602),
(4455, 'Bombinhas', 24, 4202453),
(4456, 'Botuverá', 24, 4202701),
(4457, 'Braço do Norte', 24, 4202800),
(4458, 'Braço do Trombudo', 24, 4202859),
(4459, 'Brunópolis', 24, 4202875),
(4460, 'Brusque', 24, 4202909),
(4461, 'Caçador', 24, 4203006),
(4462, 'Caibi', 24, 4203105),
(4463, 'Calmon', 24, 4203154),
(4464, 'Camboriú', 24, 4203204),
(4465, 'Campo Alegre', 24, 4203303),
(4466, 'Campo Belo do Sul', 24, 4203402),
(4467, 'Campo Erê', 24, 4203501),
(4468, 'Campos Novos', 24, 4203600),
(4469, 'Canelinha', 24, 4203709),
(4470, 'Canoinhas', 24, 4203808),
(4471, 'Capão Alto', 24, 4203253),
(4472, 'Capinzal', 24, 4203907),
(4473, 'Capivari de Baixo', 24, 4203956),
(4474, 'Catanduvas', 24, 4204004),
(4475, 'Caxambu do Sul', 24, 4204103),
(4476, 'Celso Ramos', 24, 4204152),
(4477, 'Cerro Negro', 24, 4204178),
(4478, 'Chapadão do Lageado', 24, 4204194),
(4479, 'Chapecó', 24, 4204202),
(4480, 'Cocal do Sul', 24, 4204251),
(4481, 'Concórdia', 24, 4204301),
(4482, 'Cordilheira Alta', 24, 4204350),
(4483, 'Coronel Freitas', 24, 4204400),
(4484, 'Coronel Martins', 24, 4204459),
(4485, 'Correia Pinto', 24, 4204558),
(4486, 'Corupá', 24, 4204509),
(4487, 'Criciúma', 24, 4204608),
(4488, 'Cunha Porã', 24, 4204707),
(4489, 'Cunhataí', 24, 4204756),
(4490, 'Curitibanos', 24, 4204806),
(4491, 'Descanso', 24, 4204905),
(4492, 'Dionísio Cerqueira', 24, 4205001),
(4493, 'Dona Emma', 24, 4205100),
(4494, 'Doutor Pedrinho', 24, 4205159),
(4495, 'Entre Rios', 24, 4205175),
(4496, 'Ermo', 24, 4205191),
(4497, 'Erval Velho', 24, 4205209),
(4498, 'Faxinal dos Guedes', 24, 4205308),
(4499, 'Flor do Sertão', 24, 4205357),
(4500, 'Florianópolis', 24, 4205407),
(4501, 'Formosa do Sul', 24, 4205431),
(4502, 'Forquilhinha', 24, 4205456),
(4503, 'Fraiburgo', 24, 4205506),
(4504, 'Frei Rogério', 24, 4205555),
(4505, 'Galvão', 24, 4205605),
(4506, 'Garopaba', 24, 4205704),
(4507, 'Garuva', 24, 4205803),
(4508, 'Gaspar', 24, 4205902),
(4509, 'Governador Celso Ramos', 24, 4206009),
(4510, 'Grão Pará', 24, 4206108),
(4511, 'Gravatal', 24, 4206207),
(4512, 'Guabiruba', 24, 4206306),
(4513, 'Guaraciaba', 24, 4206405),
(4514, 'Guaramirim', 24, 4206504),
(4515, 'Guarujá do Sul', 24, 4206603),
(4516, 'Guatambú', 24, 4206652),
(4517, 'Herval d`Oeste', 24, 4206702),
(4518, 'Ibiam', 24, 4206751),
(4519, 'Ibicaré', 24, 4206801),
(4520, 'Ibirama', 24, 4206900),
(4521, 'Içara', 24, 4207007),
(4522, 'Ilhota', 24, 4207106),
(4523, 'Imaruí', 24, 4207205),
(4524, 'Imbituba', 24, 4207304),
(4525, 'Imbuia', 24, 4207403),
(4526, 'Indaial', 24, 4207502),
(4527, 'Iomerê', 24, 4207577),
(4528, 'Ipira', 24, 4207601),
(4529, 'Iporã do Oeste', 24, 4207650),
(4530, 'Ipuaçu', 24, 4207684),
(4531, 'Ipumirim', 24, 4207700),
(4532, 'Iraceminha', 24, 4207759),
(4533, 'Irani', 24, 4207809),
(4534, 'Irati', 24, 4207858),
(4535, 'Irineópolis', 24, 4207908),
(4536, 'Itá', 24, 4208005),
(4537, 'Itaiópolis', 24, 4208104),
(4538, 'Itajaí', 24, 4208203),
(4539, 'Itapema', 24, 4208302),
(4540, 'Itapiranga', 24, 4208401),
(4541, 'Itapoá', 24, 4208450),
(4542, 'Ituporanga', 24, 4208500),
(4543, 'Jaborá', 24, 4208609),
(4544, 'Jacinto Machado', 24, 4208708),
(4545, 'Jaguaruna', 24, 4208807),
(4546, 'Jaraguá do Sul', 24, 4208906),
(4547, 'Jardinópolis', 24, 4208955),
(4548, 'Joaçaba', 24, 4209003),
(4549, 'Joinville', 24, 4209102),
(4550, 'José Boiteux', 24, 4209151),
(4551, 'Jupiá', 24, 4209177),
(4552, 'Lacerdópolis', 24, 4209201),
(4553, 'Lages', 24, 4209300),
(4554, 'Laguna', 24, 4209409),
(4555, 'Lajeado Grande', 24, 4209458),
(4556, 'Laurentino', 24, 4209508),
(4557, 'Lauro Muller', 24, 4209607),
(4558, 'Lebon Régis', 24, 4209706),
(4559, 'Leoberto Leal', 24, 4209805),
(4560, 'Lindóia do Sul', 24, 4209854),
(4561, 'Lontras', 24, 4209904),
(4562, 'Luiz Alves', 24, 4210001),
(4563, 'Luzerna', 24, 4210035),
(4564, 'Macieira', 24, 4210050),
(4565, 'Mafra', 24, 4210100),
(4566, 'Major Gercino', 24, 4210209),
(4567, 'Major Vieira', 24, 4210308),
(4568, 'Maracajá', 24, 4210407),
(4569, 'Maravilha', 24, 4210506),
(4570, 'Marema', 24, 4210555),
(4571, 'Massaranduba', 24, 4210605),
(4572, 'Matos Costa', 24, 4210704),
(4573, 'Meleiro', 24, 4210803),
(4574, 'Mirim Doce', 24, 4210852),
(4575, 'Modelo', 24, 4210902),
(4576, 'Mondaí', 24, 4211009),
(4577, 'Monte Carlo', 24, 4211058),
(4578, 'Monte Castelo', 24, 4211108),
(4579, 'Morro da Fumaça', 24, 4211207),
(4580, 'Morro Grande', 24, 4211256),
(4581, 'Navegantes', 24, 4211306),
(4582, 'Nova Erechim', 24, 4211405),
(4583, 'Nova Itaberaba', 24, 4211454),
(4584, 'Nova Trento', 24, 4211504),
(4585, 'Nova Veneza', 24, 4211603),
(4586, 'Novo Horizonte', 24, 4211652),
(4587, 'Orleans', 24, 4211702),
(4588, 'Otacílio Costa', 24, 4211751),
(4589, 'Ouro', 24, 4211801),
(4590, 'Ouro Verde', 24, 4211850),
(4591, 'Paial', 24, 4211876),
(4592, 'Painel', 24, 4211892),
(4593, 'Palhoça', 24, 4211900),
(4594, 'Palma Sola', 24, 4212007),
(4595, 'Palmeira', 24, 4212056),
(4596, 'Palmitos', 24, 4212106),
(4597, 'Papanduva', 24, 4212205),
(4598, 'Paraíso', 24, 4212239),
(4599, 'Passo de Torres', 24, 4212254),
(4600, 'Passos Maia', 24, 4212270),
(4601, 'Paulo Lopes', 24, 4212304),
(4602, 'Pedras Grandes', 24, 4212403),
(4603, 'Penha', 24, 4212502),
(4604, 'Peritiba', 24, 4212601),
(4605, 'Petrolândia', 24, 4212700),
(4606, 'Balneário Piçarras', 24, 4212809),
(4607, 'Pinhalzinho', 24, 4212908),
(4608, 'Pinheiro Preto', 24, 4213005),
(4609, 'Piratuba', 24, 4213104),
(4610, 'Planalto Alegre', 24, 4213153),
(4611, 'Pomerode', 24, 4213203),
(4612, 'Ponte Alta', 24, 4213302),
(4613, 'Ponte Alta do Norte', 24, 4213351),
(4614, 'Ponte Serrada', 24, 4213401),
(4615, 'Porto Belo', 24, 4213500),
(4616, 'Porto União', 24, 4213609),
(4617, 'Pouso Redondo', 24, 4213708),
(4618, 'Praia Grande', 24, 4213807),
(4620, 'Presidente Getúlio', 24, 4214003),
(4621, 'Presidente Nereu', 24, 4214102),
(4622, 'Princesa', 24, 4214151),
(4623, 'Quilombo', 24, 4214201),
(4624, 'Rancho Queimado', 24, 4214300),
(4625, 'Rio das Antas', 24, 4214409),
(4626, 'Rio do Campo', 24, 4214508),
(4627, 'Rio do Oeste', 24, 4214607),
(4628, 'Rio do Sul', 24, 4214805),
(4629, 'Rio dos Cedros', 24, 4214706),
(4630, 'Rio Fortuna', 24, 4214904),
(4631, 'Rio Negrinho', 24, 4215000),
(4632, 'Rio Rufino', 24, 4215059),
(4633, 'Riqueza', 24, 4215075),
(4634, 'Rodeio', 24, 4215109),
(4635, 'Romelândia', 24, 4215208),
(4636, 'Salete', 24, 4215307),
(4637, 'Saltinho', 24, 4215356),
(4638, 'Salto Veloso', 24, 4215406),
(4639, 'Sangão', 24, 4215455),
(4640, 'Santa Cecília', 24, 4215505),
(4641, 'Santa Helena', 24, 4215554),
(4642, 'Santa Rosa de Lima', 24, 4215604),
(4643, 'Santa Rosa do Sul', 24, 4215653),
(4644, 'Santa Terezinha', 24, 4215679),
(4645, 'Santa Terezinha do Progresso', 24, 4215687),
(4646, 'Santiago do Sul', 24, 4215695),
(4647, 'Santo Amaro da Imperatriz', 24, 4215703),
(4648, 'São Bento do Sul', 24, 4215802),
(4649, 'São Bernardino', 24, 4215752),
(4650, 'São Bonifácio', 24, 4215901),
(4651, 'São Carlos', 24, 4216008),
(4652, 'São Cristovão do Sul', 24, 4216057),
(4653, 'São Domingos', 24, 4216107),
(4654, 'São Francisco do Sul', 24, 4216206),
(4655, 'São João Batista', 24, 4216305),
(4656, 'São João do Itaperiú', 24, 4216354),
(4657, 'São João do Oeste', 24, 4216255),
(4658, 'São João do Sul', 24, 4216404),
(4659, 'São Joaquim', 24, 4216503),
(4660, 'São José', 24, 4216602),
(4661, 'São José do Cedro', 24, 4216701),
(4662, 'São José do Cerrito', 24, 4216800),
(4663, 'São Lourenço do Oeste', 24, 4216909),
(4664, 'São Ludgero', 24, 4217006),
(4665, 'São Martinho', 24, 4217105),
(4666, 'São Miguel da Boa Vista', 24, 4217154),
(4667, 'São Miguel do Oeste', 24, 4217204),
(4668, 'São Pedro de Alcântara', 24, 4217253),
(4669, 'Saudades', 24, 4217303),
(4670, 'Schroeder', 24, 4217402),
(4671, 'Seara', 24, 4217501),
(4672, 'Serra Alta', 24, 4217550),
(4673, 'Siderópolis', 24, 4217600),
(4674, 'Sombrio', 24, 4217709),
(4675, 'Sul Brasil', 24, 4217758),
(4676, 'Taió', 24, 4217808),
(4677, 'Tangará', 24, 4217907),
(4678, 'Tigrinhos', 24, 4217956),
(4679, 'Tijucas', 24, 4218004),
(4680, 'Timbé do Sul', 24, 4218103),
(4681, 'Timbó', 24, 4218202),
(4682, 'Timbó Grande', 24, 4218251),
(4683, 'Três Barras', 24, 4218301),
(4684, 'Treviso', 24, 4218350),
(4685, 'Treze de Maio', 24, 4218400),
(4686, 'Treze Tílias', 24, 4218509),
(4687, 'Trombudo Central', 24, 4218608),
(4688, 'Tubarão', 24, 4218707),
(4689, 'Tunápolis', 24, 4218756),
(4690, 'Turvo', 24, 4218806),
(4691, 'União do Oeste', 24, 4218855),
(4692, 'Urubici', 24, 4218905),
(4693, 'Urupema', 24, 4218954),
(4694, 'Urussanga', 24, 4219002),
(4695, 'Vargeão', 24, 4219101),
(4696, 'Vargem', 24, 4219150),
(4697, 'Vargem Bonita', 24, 4219176),
(4698, 'Vidal Ramos', 24, 4219200),
(4699, 'Videira', 24, 4219309),
(4700, 'Vitor Meireles', 24, 4219358),
(4701, 'Witmarsum', 24, 4219408),
(4702, 'Xanxerê', 24, 4219507),
(4703, 'Xavantina', 24, 4219606),
(4704, 'Xaxim', 24, 4219705),
(4705, 'Zortéa', 24, 4219853),
(4706, 'Adamantina', 26, 3500105),
(4707, 'Adolfo', 26, 3500204),
(4708, 'Aguaí', 26, 3500303),
(4709, 'Águas da Prata', 26, 3500402),
(4710, 'Águas de Lindóia', 26, 3500501),
(4711, 'Águas de Santa Bárbara', 26, 3500550),
(4712, 'Águas de São Pedro', 26, 3500600),
(4713, 'Agudos', 26, 3500709),
(4714, 'Alambari', 26, 3500758),
(4715, 'Alfredo Marcondes', 26, 3500808),
(4716, 'Altair', 26, 3500907),
(4717, 'Altinópolis', 26, 3501004),
(4718, 'Alto Alegre', 26, 3501103),
(4719, 'Alumínio', 26, 3501152),
(4720, 'Álvares Florence', 26, 3501202),
(4721, 'Álvares Machado', 26, 3501301),
(4722, 'Álvaro de Carvalho', 26, 3501400),
(4723, 'Alvinlândia', 26, 3501509),
(4724, 'Americana', 26, 3501608),
(4725, 'Américo Brasiliense', 26, 3501707),
(4726, 'Américo de Campos', 26, 3501806),
(4727, 'Amparo', 26, 3501905),
(4728, 'Analândia', 26, 3502002),
(4729, 'Andradina', 26, 3502101),
(4730, 'Angatuba', 26, 3502200),
(4731, 'Anhembi', 26, 3502309),
(4732, 'Anhumas', 26, 3502408),
(4733, 'Aparecida', 26, 3502507),
(4734, 'Aparecida d`Oeste', 26, 3502606),
(4735, 'Apiaí', 26, 3502705),
(4736, 'Araçariguama', 26, 3502754),
(4737, 'Araçatuba', 26, 3502804),
(4738, 'Araçoiaba da Serra', 26, 3502903),
(4739, 'Aramina', 26, 3503000),
(4740, 'Arandu', 26, 3503109),
(4741, 'Arapeí', 26, 3503158),
(4742, 'Araraquara', 26, 3503208),
(4743, 'Araras', 26, 3503307),
(4744, 'Arco-Íris', 26, 3503356),
(4745, 'Arealva', 26, 3503406),
(4746, 'Areias', 26, 3503505),
(4747, 'Areiópolis', 26, 3503604),
(4748, 'Ariranha', 26, 3503703),
(4749, 'Artur Nogueira', 26, 3503802),
(4750, 'Arujá', 26, 3503901),
(4751, 'Aspásia', 26, 3503950),
(4752, 'Assis', 26, 3504008),
(4753, 'Atibaia', 26, 3504107),
(4754, 'Auriflama', 26, 3504206),
(4755, 'Avaí', 26, 3504305),
(4756, 'Avanhandava', 26, 3504404),
(4757, 'Avaré', 26, 3504503),
(4758, 'Bady Bassitt', 26, 3504602),
(4759, 'Balbinos', 26, 3504701),
(4760, 'Bálsamo', 26, 3504800),
(4761, 'Bananal', 26, 3504909),
(4762, 'Barão de Antonina', 26, 3505005),
(4763, 'Barbosa', 26, 3505104),
(4764, 'Bariri', 26, 3505203),
(4765, 'Barra Bonita', 26, 3505302),
(4766, 'Barra do Chapéu', 26, 3505351),
(4767, 'Barra do Turvo', 26, 3505401),
(4768, 'Barretos', 26, 3505500),
(4769, 'Barrinha', 26, 3505609),
(4770, 'Barueri', 26, 3505708),
(4771, 'Bastos', 26, 3505807),
(4772, 'Batatais', 26, 3505906),
(4773, 'Bauru', 26, 3506003),
(4774, 'Bebedouro', 26, 3506102),
(4775, 'Bento de Abreu', 26, 3506201),
(4776, 'Bernardino de Campos', 26, 3506300),
(4777, 'Bertioga', 26, 3506359),
(4778, 'Bilac', 26, 3506409),
(4779, 'Birigui', 26, 3506508),
(4780, 'Biritiba-Mirim', 26, 3506607),
(4781, 'Boa Esperança do Sul', 26, 3506706),
(4782, 'Bocaina', 26, 3506805),
(4783, 'Bofete', 26, 3506904),
(4784, 'Boituva', 26, 3507001),
(4785, 'Bom Jesus dos Perdões', 26, 3507100),
(4786, 'Bom Sucesso de Itararé', 26, 3507159),
(4787, 'Borá', 26, 3507209),
(4788, 'Boracéia', 26, 3507308),
(4789, 'Borborema', 26, 3507407),
(4790, 'Borebi', 26, 3507456),
(4791, 'Botucatu', 26, 3507506),
(4792, 'Bragança Paulista', 26, 3507605),
(4793, 'Braúna', 26, 3507704),
(4794, 'Brejo Alegre', 26, 3507753),
(4795, 'Brodowski', 26, 3507803),
(4796, 'Brotas', 26, 3507902),
(4797, 'Buri', 26, 3508009),
(4798, 'Buritama', 26, 3508108),
(4799, 'Buritizal', 26, 3508207),
(4800, 'Cabrália Paulista', 26, 3508306),
(4801, 'Cabreúva', 26, 3508405),
(4802, 'Caçapava', 26, 3508504),
(4803, 'Cachoeira Paulista', 26, 3508603),
(4804, 'Caconde', 26, 3508702),
(4805, 'Cafelândia', 26, 3508801),
(4806, 'Caiabu', 26, 3508900),
(4807, 'Caieiras', 26, 3509007),
(4808, 'Caiuá', 26, 3509106),
(4809, 'Cajamar', 26, 3509205),
(4810, 'Cajati', 26, 3509254),
(4811, 'Cajobi', 26, 3509304),
(4812, 'Cajuru', 26, 3509403),
(4813, 'Campina do Monte Alegre', 26, 3509452),
(4814, 'Campinas', 26, 3509502),
(4815, 'Campo Limpo Paulista', 26, 3509601),
(4816, 'Campos do Jordão', 26, 3509700),
(4817, 'Campos Novos Paulista', 26, 3509809),
(4818, 'Cananéia', 26, 3509908),
(4819, 'Canas', 26, 3509957),
(4820, 'Cândido Mota', 26, 3510005),
(4821, 'Cândido Rodrigues', 26, 3510104),
(4822, 'Canitar', 26, 3510153),
(4823, 'Capão Bonito', 26, 3510203),
(4824, 'Capela do Alto', 26, 3510302),
(4825, 'Capivari', 26, 3510401),
(4826, 'Caraguatatuba', 26, 3510500),
(4827, 'Carapicuíba', 26, 3510609),
(4828, 'Cardoso', 26, 3510708),
(4829, 'Casa Branca', 26, 3510807),
(4830, 'Cássia dos Coqueiros', 26, 3510906),
(4831, 'Castilho', 26, 3511003),
(4832, 'Catanduva', 26, 3511102),
(4833, 'Catiguá', 26, 3511201),
(4834, 'Cedral', 26, 3511300),
(4835, 'Cerqueira César', 26, 3511409),
(4836, 'Cerquilho', 26, 3511508),
(4837, 'Cesário Lange', 26, 3511607),
(4838, 'Charqueada', 26, 3511706),
(4839, 'Chavantes', 26, 3557204),
(4840, 'Clementina', 26, 3511904),
(4841, 'Colina', 26, 3512001),
(4842, 'Colômbia', 26, 3512100),
(4843, 'Conchal', 26, 3512209),
(4844, 'Conchas', 26, 3512308),
(4845, 'Cordeirópolis', 26, 3512407),
(4846, 'Coroados', 26, 3512506),
(4847, 'Coronel Macedo', 26, 3512605),
(4848, 'Corumbataí', 26, 3512704),
(4849, 'Cosmópolis', 26, 3512803),
(4850, 'Cosmorama', 26, 3512902),
(4851, 'Cotia', 26, 3513009),
(4852, 'Cravinhos', 26, 3513108),
(4853, 'Cristais Paulista', 26, 3513207),
(4854, 'Cruzália', 26, 3513306),
(4855, 'Cruzeiro', 26, 3513405),
(4856, 'Cubatão', 26, 3513504),
(4857, 'Cunha', 26, 3513603),
(4858, 'Descalvado', 26, 3513702),
(4859, 'Diadema', 26, 3513801),
(4860, 'Dirce Reis', 26, 3513850),
(4861, 'Divinolândia', 26, 3513900),
(4862, 'Dobrada', 26, 3514007),
(4863, 'Dois Córregos', 26, 3514106),
(4864, 'Dolcinópolis', 26, 3514205),
(4865, 'Dourado', 26, 3514304),
(4866, 'Dracena', 26, 3514403),
(4867, 'Duartina', 26, 3514502),
(4868, 'Dumont', 26, 3514601),
(4869, 'Echaporã', 26, 3514700),
(4870, 'Eldorado', 26, 3514809),
(4871, 'Elias Fausto', 26, 3514908),
(4872, 'Elisiário', 26, 3514924),
(4873, 'Embaúba', 26, 3514957),
(4874, 'Embu', 26, 3515004),
(4875, 'Embu-Guaçu', 26, 3515103),
(4876, 'Emilianópolis', 26, 3515129),
(4877, 'Engenheiro Coelho', 26, 3515152),
(4878, 'Espírito Santo do Pinhal', 26, 3515186),
(4879, 'Espírito Santo do Turvo', 26, 3515194),
(4880, 'Estiva Gerbi', 26, 3557303),
(4881, 'Estrela d`Oeste', 26, 3515202),
(4882, 'Estrela do Norte', 26, 3515301),
(4883, 'Euclides da Cunha Paulista', 26, 3515350),
(4884, 'Fartura', 26, 3515400),
(4885, 'Fernando Prestes', 26, 3515608),
(4886, 'Fernandópolis', 26, 3515509),
(4887, 'Fernão', 26, 3515657),
(4888, 'Ferraz de Vasconcelos', 26, 3515707),
(4889, 'Flora Rica', 26, 3515806),
(4890, 'Floreal', 26, 3515905),
(4891, 'Flórida Paulista', 26, 3516002),
(4892, 'Florínia', 26, 3516101),
(4893, 'Franca', 26, 3516200),
(4894, 'Francisco Morato', 26, 3516309),
(4895, 'Franco da Rocha', 26, 3516408),
(4896, 'Gabriel Monteiro', 26, 3516507),
(4897, 'Gália', 26, 3516606),
(4898, 'Garça', 26, 3516705),
(4899, 'Gastão Vidigal', 26, 3516804),
(4900, 'Gavião Peixoto', 26, 3516853),
(4901, 'General Salgado', 26, 3516903),
(4902, 'Getulina', 26, 3517000),
(4903, 'Glicério', 26, 3517109),
(4904, 'Guaiçara', 26, 3517208),
(4905, 'Guaimbê', 26, 3517307),
(4906, 'Guaíra', 26, 3517406),
(4907, 'Guapiaçu', 26, 3517505),
(4908, 'Guapiara', 26, 3517604),
(4909, 'Guará', 26, 3517703),
(4910, 'Guaraçaí', 26, 3517802),
(4911, 'Guaraci', 26, 3517901),
(4912, 'Guarani d`Oeste', 26, 3518008),
(4913, 'Guarantã', 26, 3518107),
(4914, 'Guararapes', 26, 3518206),
(4915, 'Guararema', 26, 3518305),
(4916, 'Guaratinguetá', 26, 3518404),
(4917, 'Guareí', 26, 3518503),
(4918, 'Guariba', 26, 3518602),
(4919, 'Guarujá', 26, 3518701),
(4920, 'Guarulhos', 26, 3518800),
(4921, 'Guatapará', 26, 3518859),
(4922, 'Guzolândia', 26, 3518909),
(4923, 'Herculândia', 26, 3519006),
(4924, 'Holambra', 26, 3519055),
(4925, 'Hortolândia', 26, 3519071),
(4926, 'Iacanga', 26, 3519105),
(4927, 'Iacri', 26, 3519204),
(4928, 'Iaras', 26, 3519253),
(4929, 'Ibaté', 26, 3519303),
(4930, 'Ibirá', 26, 3519402),
(4931, 'Ibirarema', 26, 3519501),
(4932, 'Ibitinga', 26, 3519600),
(4933, 'Ibiúna', 26, 3519709),
(4934, 'Icém', 26, 3519808),
(4935, 'Iepê', 26, 3519907),
(4936, 'Igaraçu do Tietê', 26, 3520004),
(4937, 'Igarapava', 26, 3520103),
(4938, 'Igaratá', 26, 3520202),
(4939, 'Iguape', 26, 3520301),
(4940, 'Ilha Comprida', 26, 3520426),
(4941, 'Ilha Solteira', 26, 3520442),
(4942, 'Ilhabela', 26, 3520400),
(4943, 'Indaiatuba', 26, 3520509),
(4944, 'Indiana', 26, 3520608),
(4945, 'Indiaporã', 26, 3520707),
(4946, 'Inúbia Paulista', 26, 3520806),
(4947, 'Ipaussu', 26, 3520905),
(4948, 'Iperó', 26, 3521002),
(4949, 'Ipeúna', 26, 3521101),
(4950, 'Ipiguá', 26, 3521150),
(4951, 'Iporanga', 26, 3521200),
(4952, 'Ipuã', 26, 3521309),
(4953, 'Iracemápolis', 26, 3521408),
(4954, 'Irapuã', 26, 3521507),
(4955, 'Irapuru', 26, 3521606),
(4956, 'Itaberá', 26, 3521705),
(4957, 'Itaí', 26, 3521804),
(4958, 'Itajobi', 26, 3521903),
(4959, 'Itaju', 26, 3522000),
(4960, 'Itanhaém', 26, 3522109),
(4961, 'Itaóca', 26, 3522158),
(4962, 'Itapecerica da Serra', 26, 3522208),
(4963, 'Itapetininga', 26, 3522307),
(4964, 'Itapeva', 26, 3522406),
(4965, 'Itapevi', 26, 3522505),
(4966, 'Itapira', 26, 3522604),
(4967, 'Itapirapuã Paulista', 26, 3522653),
(4968, 'Itápolis', 26, 3522703),
(4969, 'Itaporanga', 26, 3522802),
(4970, 'Itapuí', 26, 3522901),
(4971, 'Itapura', 26, 3523008),
(4972, 'Itaquaquecetuba', 26, 3523107),
(4973, 'Itararé', 26, 3523206),
(4974, 'Itariri', 26, 3523305),
(4975, 'Itatiba', 26, 3523404),
(4976, 'Itatinga', 26, 3523503),
(4977, 'Itirapina', 26, 3523602),
(4978, 'Itirapuã', 26, 3523701),
(4979, 'Itobi', 26, 3523800),
(4980, 'Itu', 26, 3523909),
(4981, 'Itupeva', 26, 3524006),
(4982, 'Ituverava', 26, 3524105),
(4983, 'Jaborandi', 26, 3524204),
(4984, 'Jaboticabal', 26, 3524303),
(4985, 'Jacareí', 26, 3524402),
(4986, 'Jaci', 26, 3524501),
(4987, 'Jacupiranga', 26, 3524600),
(4988, 'Jaguariúna', 26, 3524709),
(4989, 'Jales', 26, 3524808),
(4990, 'Jambeiro', 26, 3524907),
(4991, 'Jandira', 26, 3525003),
(4992, 'Jardinópolis', 26, 3525102),
(4993, 'Jarinu', 26, 3525201),
(4994, 'Jaú', 26, 3525300),
(4995, 'Jeriquara', 26, 3525409),
(4996, 'Joanópolis', 26, 3525508),
(4997, 'João Ramalho', 26, 3525607),
(4998, 'José Bonifácio', 26, 3525706),
(4999, 'Júlio Mesquita', 26, 3525805),
(5000, 'Jumirim', 26, 3525854),
(5001, 'Jundiaí', 26, 3525904),
(5002, 'Junqueirópolis', 26, 3526001),
(5003, 'Juquiá', 26, 3526100),
(5004, 'Juquitiba', 26, 3526209),
(5005, 'Lagoinha', 26, 3526308),
(5006, 'Laranjal Paulista', 26, 3526407),
(5007, 'Lavínia', 26, 3526506),
(5008, 'Lavrinhas', 26, 3526605),
(5009, 'Leme', 26, 3526704),
(5010, 'Lençóis Paulista', 26, 3526803),
(5011, 'Limeira', 26, 3526902),
(5012, 'Lindóia', 26, 3527009),
(5013, 'Lins', 26, 3527108),
(5014, 'Lorena', 26, 3527207),
(5015, 'Lourdes', 26, 3527256),
(5016, 'Louveira', 26, 3527306),
(5017, 'Lucélia', 26, 3527405),
(5018, 'Lucianópolis', 26, 3527504),
(5019, 'Luís Antônio', 26, 3527603),
(5020, 'Luiziânia', 26, 3527702),
(5021, 'Lupércio', 26, 3527801),
(5022, 'Lutécia', 26, 3527900),
(5023, 'Macatuba', 26, 3528007),
(5024, 'Macaubal', 26, 3528106),
(5025, 'Macedônia', 26, 3528205),
(5026, 'Magda', 26, 3528304),
(5027, 'Mairinque', 26, 3528403),
(5028, 'Mairiporã', 26, 3528502),
(5029, 'Manduri', 26, 3528601),
(5030, 'Marabá Paulista', 26, 3528700),
(5031, 'Maracaí', 26, 3528809),
(5032, 'Marapoama', 26, 3528858),
(5033, 'Mariápolis', 26, 3528908),
(5034, 'Marília', 26, 3529005),
(5035, 'Marinópolis', 26, 3529104),
(5036, 'Martinópolis', 26, 3529203),
(5037, 'Matão', 26, 3529302),
(5038, 'Mauá', 26, 3529401),
(5039, 'Mendonça', 26, 3529500),
(5040, 'Meridiano', 26, 3529609),
(5041, 'Mesópolis', 26, 3529658),
(5042, 'Miguelópolis', 26, 3529708),
(5043, 'Mineiros do Tietê', 26, 3529807),
(5044, 'Mira Estrela', 26, 3530003),
(5045, 'Miracatu', 26, 3529906),
(5046, 'Mirandópolis', 26, 3530102),
(5047, 'Mirante do Paranapanema', 26, 3530201),
(5048, 'Mirassol', 26, 3530300),
(5049, 'Mirassolândia', 26, 3530409),
(5050, 'Mococa', 26, 3530508),
(5051, 'Mogi das Cruzes', 26, 3530607),
(5052, 'Mogi Guaçu', 26, 3530706),
(5053, 'Mogi Mirim', 26, 3530805),
(5054, 'Mombuca', 26, 3530904),
(5055, 'Monções', 26, 3531001),
(5056, 'Mongaguá', 26, 3531100),
(5057, 'Monte Alegre do Sul', 26, 3531209),
(5058, 'Monte Alto', 26, 3531308),
(5059, 'Monte Aprazível', 26, 3531407),
(5060, 'Monte Azul Paulista', 26, 3531506),
(5061, 'Monte Castelo', 26, 3531605),
(5062, 'Monte Mor', 26, 3531803),
(5063, 'Monteiro Lobato', 26, 3531704),
(5064, 'Morro Agudo', 26, 3531902),
(5065, 'Morungaba', 26, 3532009),
(5066, 'Motuca', 26, 3532058),
(5067, 'Murutinga do Sul', 26, 3532108),
(5068, 'Nantes', 26, 3532157),
(5069, 'Narandiba', 26, 3532207),
(5070, 'Natividade da Serra', 26, 3532306),
(5071, 'Nazaré Paulista', 26, 3532405),
(5072, 'Neves Paulista', 26, 3532504),
(5073, 'Nhandeara', 26, 3532603),
(5074, 'Nipoã', 26, 3532702),
(5075, 'Nova Aliança', 26, 3532801),
(5076, 'Nova Campina', 26, 3532827),
(5077, 'Nova Canaã Paulista', 26, 3532843),
(5078, 'Nova Castilho', 26, 3532868),
(5079, 'Nova Europa', 26, 3532900),
(5080, 'Nova Granada', 26, 3533007),
(5081, 'Nova Guataporanga', 26, 3533106),
(5082, 'Nova Independência', 26, 3533205),
(5083, 'Nova Luzitânia', 26, 3533304),
(5084, 'Nova Odessa', 26, 3533403),
(5085, 'Novais', 26, 3533254),
(5086, 'Novo Horizonte', 26, 3533502),
(5087, 'Nuporanga', 26, 3533601),
(5088, 'Ocauçu', 26, 3533700),
(5089, 'Óleo', 26, 3533809),
(5090, 'Olímpia', 26, 3533908),
(5091, 'Onda Verde', 26, 3534005),
(5092, 'Oriente', 26, 3534104),
(5093, 'Orindiúva', 26, 3534203),
(5094, 'Orlândia', 26, 3534302),
(5095, 'Osasco', 26, 3534401),
(5096, 'Oscar Bressane', 26, 3534500),
(5097, 'Osvaldo Cruz', 26, 3534609),
(5098, 'Ourinhos', 26, 3534708),
(5099, 'Ouro Verde', 26, 3534807),
(5100, 'Ouroeste', 26, 3534757),
(5101, 'Pacaembu', 26, 3534906),
(5102, 'Palestina', 26, 3535002),
(5103, 'Palmares Paulista', 26, 3535101),
(5104, 'Palmeira d`Oeste', 26, 3535200),
(5105, 'Palmital', 26, 3535309),
(5106, 'Panorama', 26, 3535408),
(5107, 'Paraguaçu Paulista', 26, 3535507),
(5108, 'Paraibuna', 26, 3535606),
(5109, 'Paraíso', 26, 3535705),
(5110, 'Paranapanema', 26, 3535804),
(5111, 'Paranapuã', 26, 3535903),
(5112, 'Parapuã', 26, 3536000),
(5113, 'Pardinho', 26, 3536109),
(5114, 'Pariquera-Açu', 26, 3536208),
(5115, 'Parisi', 26, 3536257),
(5116, 'Patrocínio Paulista', 26, 3536307),
(5117, 'Paulicéia', 26, 3536406),
(5118, 'Paulínia', 26, 3536505),
(5119, 'Paulistânia', 26, 3536570),
(5120, 'Paulo de Faria', 26, 3536604),
(5121, 'Pederneiras', 26, 3536703),
(5122, 'Pedra Bela', 26, 3536802),
(5123, 'Pedranópolis', 26, 3536901),
(5124, 'Pedregulho', 26, 3537008),
(5125, 'Pedreira', 26, 3537107),
(5126, 'Pedrinhas Paulista', 26, 3537156),
(5127, 'Pedro de Toledo', 26, 3537206),
(5128, 'Penápolis', 26, 3537305),
(5129, 'Pereira Barreto', 26, 3537404),
(5130, 'Pereiras', 26, 3537503),
(5131, 'Peruíbe', 26, 3537602),
(5132, 'Piacatu', 26, 3537701),
(5133, 'Piedade', 26, 3537800),
(5134, 'Pilar do Sul', 26, 3537909),
(5135, 'Pindamonhangaba', 26, 3538006),
(5136, 'Pindorama', 26, 3538105),
(5137, 'Pinhalzinho', 26, 3538204),
(5138, 'Piquerobi', 26, 3538303),
(5139, 'Piquete', 26, 3538501),
(5140, 'Piracaia', 26, 3538600),
(5141, 'Piracicaba', 26, 3538709),
(5142, 'Piraju', 26, 3538808),
(5143, 'Pirajuí', 26, 3538907),
(5144, 'Pirangi', 26, 3539004),
(5145, 'Pirapora do Bom Jesus', 26, 3539103),
(5146, 'Pirapozinho', 26, 3539202),
(5147, 'Pirassununga', 26, 3539301),
(5148, 'Piratininga', 26, 3539400),
(5149, 'Pitangueiras', 26, 3539509),
(5150, 'Planalto', 26, 3539608),
(5151, 'Platina', 26, 3539707),
(5152, 'Poá', 26, 3539806),
(5153, 'Poloni', 26, 3539905),
(5154, 'Pompéia', 26, 3540002),
(5155, 'Pongaí', 26, 3540101),
(5156, 'Pontal', 26, 3540200),
(5157, 'Pontalinda', 26, 3540259),
(5158, 'Pontes Gestal', 26, 3540309),
(5159, 'Populina', 26, 3540408),
(5160, 'Porangaba', 26, 3540507),
(5161, 'Porto Feliz', 26, 3540606),
(5162, 'Porto Ferreira', 26, 3540705),
(5163, 'Potim', 26, 3540754),
(5164, 'Potirendaba', 26, 3540804),
(5165, 'Pracinha', 26, 3540853),
(5166, 'Pradópolis', 26, 3540903),
(5167, 'Praia Grande', 26, 3541000),
(5168, 'Pratânia', 26, 3541059),
(5169, 'Presidente Alves', 26, 3541109),
(5170, 'Presidente Bernardes', 26, 3541208),
(5171, 'Presidente Epitácio', 26, 3541307),
(5172, 'Presidente Prudente', 26, 3541406),
(5173, 'Presidente Venceslau', 26, 3541505),
(5174, 'Promissão', 26, 3541604),
(5175, 'Quadra', 26, 3541653),
(5176, 'Quatá', 26, 3541703),
(5177, 'Queiroz', 26, 3541802),
(5178, 'Queluz', 26, 3541901),
(5179, 'Quintana', 26, 3542008),
(5180, 'Rafard', 26, 3542107),
(5181, 'Rancharia', 26, 3542206),
(5182, 'Redenção da Serra', 26, 3542305),
(5183, 'Regente Feijó', 26, 3542404),
(5184, 'Reginópolis', 26, 3542503),
(5185, 'Registro', 26, 3542602),
(5186, 'Restinga', 26, 3542701),
(5187, 'Ribeira', 26, 3542800),
(5188, 'Ribeirão Bonito', 26, 3542909),
(5189, 'Ribeirão Branco', 26, 3543006),
(5190, 'Ribeirão Corrente', 26, 3543105),
(5191, 'Ribeirão do Sul', 26, 3543204),
(5192, 'Ribeirão dos Índios', 26, 3543238),
(5193, 'Ribeirão Grande', 26, 3543253),
(5194, 'Ribeirão Pires', 26, 3543303),
(5195, 'Ribeirão Preto', 26, 3543402),
(5196, 'Rifaina', 26, 3543600),
(5197, 'Rincão', 26, 3543709),
(5198, 'Rinópolis', 26, 3543808),
(5199, 'Rio Claro', 26, 3543907),
(5200, 'Rio das Pedras', 26, 3544004),
(5201, 'Rio Grande da Serra', 26, 3544103),
(5202, 'Riolândia', 26, 3544202),
(5203, 'Riversul', 26, 3543501),
(5204, 'Rosana', 26, 3544251),
(5205, 'Roseira', 26, 3544301),
(5206, 'Rubiácea', 26, 3544400),
(5207, 'Rubinéia', 26, 3544509),
(5208, 'Sabino', 26, 3544608),
(5209, 'Sagres', 26, 3544707),
(5210, 'Sales', 26, 3544806),
(5211, 'Sales Oliveira', 26, 3544905),
(5212, 'Salesópolis', 26, 3545001),
(5213, 'Salmourão', 26, 3545100),
(5214, 'Saltinho', 26, 3545159),
(5215, 'Salto', 26, 3545209),
(5216, 'Salto de Pirapora', 26, 3545308),
(5217, 'Salto Grande', 26, 3545407),
(5218, 'Sandovalina', 26, 3545506),
(5219, 'Santa Adélia', 26, 3545605),
(5220, 'Santa Albertina', 26, 3545704),
(5221, 'Santa Bárbara d`Oeste', 26, 3545803),
(5222, 'Santa Branca', 26, 3546009),
(5223, 'Santa Clara d`Oeste', 26, 3546108),
(5224, 'Santa Cruz da Conceição', 26, 3546207),
(5225, 'Santa Cruz da Esperança', 26, 3546256),
(5226, 'Santa Cruz das Palmeiras', 26, 3546306),
(5227, 'Santa Cruz do Rio Pardo', 26, 3546405),
(5228, 'Santa Ernestina', 26, 3546504),
(5229, 'Santa Fé do Sul', 26, 3546603),
(5230, 'Santa Gertrudes', 26, 3546702),
(5231, 'Santa Isabel', 26, 3546801),
(5232, 'Santa Lúcia', 26, 3546900),
(5233, 'Santa Maria da Serra', 26, 3547007),
(5234, 'Santa Mercedes', 26, 3547106),
(5235, 'Santa Rita d`Oeste', 26, 3547403),
(5236, 'Santa Rita do Passa Quatro', 26, 3547502),
(5237, 'Santa Rosa de Viterbo', 26, 3547601),
(5238, 'Santa Salete', 26, 3547650),
(5239, 'Santana da Ponte Pensa', 26, 3547205),
(5240, 'Santana de Parnaíba', 26, 3547304),
(5241, 'Santo Anastácio', 26, 3547700),
(5242, 'Santo André', 26, 3547809),
(5243, 'Santo Antônio da Alegria', 26, 3547908),
(5244, 'Santo Antônio de Posse', 26, 3548005),
(5245, 'Santo Antônio do Aracanguá', 26, 3548054),
(5246, 'Santo Antônio do Jardim', 26, 3548104),
(5247, 'Santo Antônio do Pinhal', 26, 3548203),
(5248, 'Santo Expedito', 26, 3548302),
(5249, 'Santópolis do Aguapeí', 26, 3548401),
(5250, 'Santos', 26, 3548500),
(5251, 'São Bento do Sapucaí', 26, 3548609),
(5252, 'São Bernardo do Campo', 26, 3548708),
(5253, 'São Caetano do Sul', 26, 3548807),
(5254, 'São Carlos', 26, 3548906),
(5255, 'São Francisco', 26, 3549003),
(5256, 'São João da Boa Vista', 26, 3549102),
(5257, 'São João das Duas Pontes', 26, 3549201),
(5258, 'São João de Iracema', 26, 3549250),
(5259, 'São João do Pau d`Alho', 26, 3549300),
(5260, 'São Joaquim da Barra', 26, 3549409),
(5261, 'São José da Bela Vista', 26, 3549508),
(5262, 'São José do Barreiro', 26, 3549607),
(5263, 'São José do Rio Pardo', 26, 3549706),
(5264, 'São José do Rio Preto', 26, 3549805),
(5265, 'São José dos Campos', 26, 3549904),
(5266, 'São Lourenço da Serra', 26, 3549953),
(5267, 'São Luís do Paraitinga', 26, 3550001),
(5268, 'São Manuel', 26, 3550100),
(5269, 'São Miguel Arcanjo', 26, 3550209),
(5270, 'São Paulo', 26, 3550308),
(5271, 'São Pedro', 26, 3550407),
(5272, 'São Pedro do Turvo', 26, 3550506),
(5273, 'São Roque', 26, 3550605),
(5274, 'São Sebastião', 26, 3550704),
(5275, 'São Sebastião da Grama', 26, 3550803),
(5276, 'São Simão', 26, 3550902),
(5277, 'São Vicente', 26, 3551009),
(5278, 'Sarapuí', 26, 3551108),
(5279, 'Sarutaiá', 26, 3551207),
(5280, 'Sebastianópolis do Sul', 26, 3551306),
(5281, 'Serra Azul', 26, 3551405),
(5282, 'Serra Negra', 26, 3551603),
(5283, 'Serrana', 26, 3551504),
(5284, 'Sertãozinho', 26, 3551702),
(5285, 'Sete Barras', 26, 3551801),
(5286, 'Severínia', 26, 3551900),
(5287, 'Silveiras', 26, 3552007),
(5288, 'Socorro', 26, 3552106),
(5289, 'Sorocaba', 26, 3552205),
(5290, 'Sud Mennucci', 26, 3552304),
(5291, 'Sumaré', 26, 3552403),
(5292, 'Suzanápolis', 26, 3552551),
(5293, 'Suzano', 26, 3552502),
(5294, 'Tabapuã', 26, 3552601),
(5295, 'Tabatinga', 26, 3552700),
(5296, 'Taboão da Serra', 26, 3552809),
(5297, 'Taciba', 26, 3552908),
(5298, 'Taguaí', 26, 3553005),
(5299, 'Taiaçu', 26, 3553104),
(5300, 'Taiúva', 26, 3553203),
(5301, 'Tambaú', 26, 3553302),
(5302, 'Tanabi', 26, 3553401),
(5303, 'Tapiraí', 26, 3553500),
(5304, 'Tapiratiba', 26, 3553609),
(5305, 'Taquaral', 26, 3553658),
(5306, 'Taquaritinga', 26, 3553708),
(5307, 'Taquarituba', 26, 3553807),
(5308, 'Taquarivaí', 26, 3553856),
(5309, 'Tarabai', 26, 3553906),
(5310, 'Tarumã', 26, 3553955),
(5311, 'Tatuí', 26, 3554003),
(5312, 'Taubaté', 26, 3554102),
(5313, 'Tejupá', 26, 3554201),
(5314, 'Teodoro Sampaio', 26, 3554300),
(5315, 'Terra Roxa', 26, 3554409),
(5316, 'Tietê', 26, 3554508),
(5317, 'Timburi', 26, 3554607),
(5318, 'Torre de Pedra', 26, 3554656),
(5319, 'Torrinha', 26, 3554706),
(5320, 'Trabiju', 26, 3554755),
(5321, 'Tremembé', 26, 3554805),
(5322, 'Três Fronteiras', 26, 3554904),
(5323, 'Tuiuti', 26, 3554953),
(5324, 'Tupã', 26, 3555000),
(5325, 'Tupi Paulista', 26, 3555109),
(5326, 'Turiúba', 26, 3555208),
(5327, 'Turmalina', 26, 3555307),
(5328, 'Ubarana', 26, 3555356),
(5329, 'Ubatuba', 26, 3555406),
(5330, 'Ubirajara', 26, 3555505),
(5331, 'Uchoa', 26, 3555604),
(5332, 'União Paulista', 26, 3555703),
(5333, 'Urânia', 26, 3555802),
(5334, 'Uru', 26, 3555901),
(5335, 'Urupês', 26, 3556008),
(5336, 'Valentim Gentil', 26, 3556107),
(5337, 'Valinhos', 26, 3556206),
(5338, 'Valparaíso', 26, 3556305),
(5339, 'Vargem', 26, 3556354),
(5340, 'Vargem Grande do Sul', 26, 3556404),
(5341, 'Vargem Grande Paulista', 26, 3556453),
(5342, 'Várzea Paulista', 26, 3556503),
(5343, 'Vera Cruz', 26, 3556602),
(5344, 'Vinhedo', 26, 3556701),
(5345, 'Viradouro', 26, 3556800),
(5346, 'Vista Alegre do Alto', 26, 3556909),
(5347, 'Vitória Brasil', 26, 3556958),
(5348, 'Votorantim', 26, 3557006),
(5349, 'Votuporanga', 26, 3557105),
(5350, 'Zacarias', 26, 3557154),
(5351, 'Amparo de São Francisco', 25, 2800100),
(5352, 'Aquidabã', 25, 2800209),
(5353, 'Aracaju', 25, 2800308),
(5354, 'Arauá', 25, 2800407),
(5355, 'Areia Branca', 25, 2800506),
(5356, 'Barra dos Coqueiros', 25, 2800605),
(5357, 'Boquim', 25, 2800670),
(5358, 'Brejo Grande', 25, 2800704),
(5359, 'Campo do Brito', 25, 2801009),
(5360, 'Canhoba', 25, 2801108),
(5361, 'Canindé de São Francisco', 25, 2801207),
(5362, 'Capela', 25, 2801306),
(5363, 'Carira', 25, 2801405),
(5364, 'Carmópolis', 25, 2801504),
(5365, 'Cedro de São João', 25, 2801603),
(5366, 'Cristinápolis', 25, 2801702),
(5367, 'Cumbe', 25, 2801900),
(5368, 'Divina Pastora', 25, 2802007),
(5369, 'Estância', 25, 2802106),
(5370, 'Feira Nova', 25, 2802205),
(5371, 'Frei Paulo', 25, 2802304),
(5372, 'Gararu', 25, 2802403),
(5373, 'General Maynard', 25, 2802502),
(5374, 'Gracho Cardoso', 25, 2802601),
(5375, 'Ilha das Flores', 25, 2802700),
(5376, 'Indiaroba', 25, 2802809),
(5377, 'Itabaiana', 25, 2802908),
(5378, 'Itabaianinha', 25, 2803005),
(5379, 'Itabi', 25, 2803104),
(5380, 'Itaporanga d`Ajuda', 25, 2803203),
(5381, 'Japaratuba', 25, 2803302),
(5382, 'Japoatã', 25, 2803401),
(5383, 'Lagarto', 25, 2803500),
(5384, 'Laranjeiras', 25, 2803609),
(5385, 'Macambira', 25, 2803708),
(5386, 'Malhada dos Bois', 25, 2803807),
(5387, 'Malhador', 25, 2803906),
(5388, 'Maruim', 25, 2804003),
(5389, 'Moita Bonita', 25, 2804102),
(5390, 'Monte Alegre de Sergipe', 25, 2804201),
(5391, 'Muribeca', 25, 2804300),
(5392, 'Neópolis', 25, 2804409),
(5393, 'Nossa Senhora Aparecida', 25, 2804458),
(5394, 'Nossa Senhora da Glória', 25, 2804508),
(5395, 'Nossa Senhora das Dores', 25, 2804607),
(5396, 'Nossa Senhora de Lourdes', 25, 2804706),
(5397, 'Nossa Senhora do Socorro', 25, 2804805),
(5398, 'Pacatuba', 25, 2804904),
(5399, 'Pedra Mole', 25, 2805000),
(5400, 'Pedrinhas', 25, 2805109),
(5401, 'Pinhão', 25, 2805208),
(5402, 'Pirambu', 25, 2805307),
(5403, 'Poço Redondo', 25, 2805406),
(5404, 'Poço Verde', 25, 2805505),
(5405, 'Porto da Folha', 25, 2805604),
(5406, 'Propriá', 25, 2805703),
(5407, 'Riachão do Dantas', 25, 2805802),
(5408, 'Riachuelo', 25, 2805901),
(5409, 'Ribeirópolis', 25, 2806008),
(5410, 'Rosário do Catete', 25, 2806107),
(5411, 'Salgado', 25, 2806206),
(5412, 'Santa Luzia do Itanhy', 25, 2806305),
(5413, 'Santa Rosa de Lima', 25, 2806503),
(5414, 'Santana do São Francisco', 25, 2806404),
(5415, 'Santo Amaro das Brotas', 25, 2806602),
(5416, 'São Cristóvão', 25, 2806701),
(5417, 'São Domingos', 25, 2806800),
(5418, 'São Francisco', 25, 2806909),
(5419, 'São Miguel do Aleixo', 25, 2807006),
(5420, 'Simão Dias', 25, 2807105),
(5421, 'Siriri', 25, 2807204),
(5422, 'Telha', 25, 2807303),
(5423, 'Tobias Barreto', 25, 2807402),
(5424, 'Tomar do Geru', 25, 2807501),
(5425, 'Umbaúba', 25, 2807600),
(5426, 'Abreulândia', 27, 1700251),
(5427, 'Aguiarnópolis', 27, 1700301),
(5428, 'Aliança do Tocantins', 27, 1700350),
(5429, 'Almas', 27, 1700400),
(5430, 'Alvorada', 27, 1700707),
(5431, 'Ananás', 27, 1701002),
(5432, 'Angico', 27, 1701051),
(5433, 'Aparecida do Rio Negro', 27, 1701101),
(5434, 'Aragominas', 27, 1701309),
(5435, 'Araguacema', 27, 1701903),
(5436, 'Araguaçu', 27, 1702000),
(5437, 'Araguaína', 27, 1702109),
(5438, 'Araguanã', 27, 1702158),
(5439, 'Araguatins', 27, 1702208),
(5440, 'Arapoema', 27, 1702307),
(5441, 'Arraias', 27, 1702406),
(5442, 'Augustinópolis', 27, 1702554),
(5443, 'Aurora do Tocantins', 27, 1702703),
(5444, 'Axixá do Tocantins', 27, 1702901),
(5445, 'Babaçulândia', 27, 1703008),
(5446, 'Bandeirantes do Tocantins', 27, 1703057),
(5447, 'Barra do Ouro', 27, 1703073),
(5448, 'Barrolândia', 27, 1703107),
(5449, 'Bernardo Sayão', 27, 1703206),
(5450, 'Bom Jesus do Tocantins', 27, 1703305),
(5451, 'Brasilândia do Tocantins', 27, 1703602),
(5452, 'Brejinho de Nazaré', 27, 1703701),
(5453, 'Buriti do Tocantins', 27, 1703800),
(5454, 'Cachoeirinha', 27, 1703826),
(5455, 'Campos Lindos', 27, 1703842),
(5456, 'Cariri do Tocantins', 27, 1703867),
(5457, 'Carmolândia', 27, 1703883),
(5458, 'Carrasco Bonito', 27, 1703891),
(5459, 'Caseara', 27, 1703909),
(5460, 'Centenário', 27, 1704105),
(5461, 'Chapada da Natividade', 27, 1705102),
(5462, 'Chapada de Areia', 27, 1704600),
(5463, 'Colinas do Tocantins', 27, 1705508),
(5464, 'Colméia', 27, 1716703),
(5465, 'Combinado', 27, 1705557),
(5466, 'Conceição do Tocantins', 27, 1705607),
(5467, 'Couto de Magalhães', 27, 1706001),
(5468, 'Cristalândia', 27, 1706100),
(5469, 'Crixás do Tocantins', 27, 1706258),
(5470, 'Darcinópolis', 27, 1706506),
(5471, 'Dianópolis', 27, 1707009),
(5472, 'Divinópolis do Tocantins', 27, 1707108),
(5473, 'Dois Irmãos do Tocantins', 27, 1707207),
(5474, 'Dueré', 27, 1707306),
(5475, 'Esperantina', 27, 1707405),
(5476, 'Fátima', 27, 1707553),
(5477, 'Figueirópolis', 27, 1707652),
(5478, 'Filadélfia', 27, 1707702),
(5479, 'Formoso do Araguaia', 27, 1708205),
(5480, 'Fortaleza do Tabocão', 27, 1708254),
(5481, 'Goianorte', 27, 1708304),
(5482, 'Goiatins', 27, 1709005),
(5483, 'Guaraí', 27, 1709302),
(5484, 'Gurupi', 27, 1709500),
(5485, 'Ipueiras', 27, 1709807),
(5486, 'Itacajá', 27, 1710508),
(5487, 'Itaguatins', 27, 1710706),
(5488, 'Itapiratins', 27, 1710904),
(5489, 'Itaporã do Tocantins', 27, 1711100),
(5490, 'Jaú do Tocantins', 27, 1711506),
(5491, 'Juarina', 27, 1711803),
(5492, 'Lagoa da Confusão', 27, 1711902),
(5493, 'Lagoa do Tocantins', 27, 1711951),
(5494, 'Lajeado', 27, 1712009),
(5495, 'Lavandeira', 27, 1712157),
(5496, 'Lizarda', 27, 1712405),
(5497, 'Luzinópolis', 27, 1712454),
(5498, 'Marianópolis do Tocantins', 27, 1712504),
(5499, 'Mateiros', 27, 1712702),
(5500, 'Maurilândia do Tocantins', 27, 1712801),
(5501, 'Miracema do Tocantins', 27, 1713205),
(5502, 'Miranorte', 27, 1713304),
(5503, 'Monte do Carmo', 27, 1713601),
(5504, 'Monte Santo do Tocantins', 27, 1713700),
(5505, 'Muricilândia', 27, 1713957),
(5506, 'Natividade', 27, 1714203),
(5507, 'Nazaré', 27, 1714302),
(5508, 'Nova Olinda', 27, 1714880),
(5509, 'Nova Rosalândia', 27, 1715002),
(5510, 'Novo Acordo', 27, 1715101),
(5511, 'Novo Alegre', 27, 1715150),
(5512, 'Novo Jardim', 27, 1715259),
(5513, 'Oliveira de Fátima', 27, 1715507),
(5514, 'Palmas', 27, 1721000),
(5515, 'Palmeirante', 27, 1715705),
(5516, 'Palmeiras do Tocantins', 27, 1713809),
(5517, 'Palmeirópolis', 27, 1715754),
(5518, 'Paraíso do Tocantins', 27, 1716109),
(5519, 'Paranã', 27, 1716208),
(5520, 'Pau d`Arco', 27, 1716307),
(5521, 'Pedro Afonso', 27, 1716505),
(5522, 'Peixe', 27, 1716604),
(5523, 'Pequizeiro', 27, 1716653),
(5524, 'Pindorama do Tocantins', 27, 1717008),
(5525, 'Piraquê', 27, 1717206),
(5526, 'Pium', 27, 1717503),
(5527, 'Ponte Alta do Bom Jesus', 27, 1717800),
(5528, 'Ponte Alta do Tocantins', 27, 1717909),
(5529, 'Porto Alegre do Tocantins', 27, 1718006),
(5530, 'Porto Nacional', 27, 1718204),
(5531, 'Praia Norte', 27, 1718303),
(5532, 'Presidente Kennedy', 27, 1718402),
(5533, 'Pugmil', 27, 1718451),
(5534, 'Recursolândia', 27, 1718501),
(5535, 'Riachinho', 27, 1718550),
(5536, 'Rio da Conceição', 27, 1718659),
(5537, 'Rio dos Bois', 27, 1718709),
(5538, 'Rio Sono', 27, 1718758),
(5539, 'Sampaio', 27, 1718808),
(5540, 'Sandolândia', 27, 1718840),
(5541, 'Santa Fé do Araguaia', 27, 1718865),
(5542, 'Santa Maria do Tocantins', 27, 1718881),
(5543, 'Santa Rita do Tocantins', 27, 1718899),
(5544, 'Santa Rosa do Tocantins', 27, 1718907),
(5545, 'Santa Tereza do Tocantins', 27, 1719004),
(5546, 'Santa Terezinha do Tocantins', 27, 1720002),
(5547, 'São Bento do Tocantins', 27, 1720101),
(5548, 'São Félix do Tocantins', 27, 1720150),
(5549, 'São Miguel do Tocantins', 27, 1720200),
(5550, 'São Salvador do Tocantins', 27, 1720259),
(5551, 'São Sebastião do Tocantins', 27, 1720309),
(5552, 'São Valério da Natividade', 27, 1720499),
(5553, 'Silvanópolis', 27, 1720655),
(5554, 'Sítio Novo do Tocantins', 27, 1720804),
(5555, 'Sucupira', 27, 1720853),
(5556, 'Taguatinga', 27, 1720903),
(5557, 'Taipas do Tocantins', 27, 1720937),
(5558, 'Talismã', 27, 1720978),
(5559, 'Tocantínia', 27, 1721109),
(5560, 'Tocantinópolis', 27, 1721208),
(5561, 'Tupirama', 27, 1721257),
(5562, 'Tupiratins', 27, 1721307),
(5563, 'Wanderlândia', 27, 1722081),
(5564, 'Xambioá', 27, 1722107),
(5575, 'Gama', 7, 5300108),
(5576, 'Taguatinga', 7, 5300108),
(5577, 'Brazlândia', 7, 5300108),
(5578, 'Sobradinho', 7, 5300108),
(5579, 'Planaltina', 7, 5300108),
(5580, 'Paranoá', 7, 5300108),
(5581, 'Núcleo Bandeirante', 7, 5300108),
(5582, 'Ceilândia', 7, 5300108),
(5583, 'Guará', 7, 5300108),
(5584, 'Cruzeiro', 7, 5300108),
(5585, 'Samambaia', 7, 5300108),
(5586, 'Santa Maria', 7, 5300108),
(5587, 'São Sebastião', 7, 5300108),
(5589, 'Lago Sul', 7, 5300108),
(5590, 'Riacho Fundo', 7, 5300108),
(5591, 'Lago Norte', 7, 5300108),
(5592, 'Candangolândia', 7, 5300108),
(5593, 'Águas Claras', 7, 5300108),
(5594, 'Riacho Fundo II', 7, 5300108),
(5595, 'Sudoeste/Octogonal', 7, 5300108),
(5596, 'Varjão', 7, 5300108),
(5597, 'Park Way', 7, 5300108),
(5598, 'SCIA', 7, 5300108),
(5599, 'Sobradinho II', 7, 5300108),
(5601, 'Itapoã', 7, 5300108),
(5602, 'SIA', 7, 5300108),
(5603, 'Vicente Pires', 7, 5300108),
(5604, 'Fercal', 7, 5300108),
(5574, 'Recanto das Emas', 7, 5300108),
(5600, 'Jardim Botânico', 7, 5300108),
(5605, 'Nazária', 17, 2206720),
(5606, 'Paraíso das Águas', 12, 5006275),
(5607, 'Pinto Bandeira', 23, 4314548),
(5608, 'Balneário Rincão', 24, 4220000),
(5609, 'Pescaria Brava', 24, 4212650);

-- --------------------------------------------------------

--
-- Estrutura da tabela `MAM_Country`
--

CREATE TABLE `MAM_Country` (
  `SL_ID` int(11) NOT NULL,
  `SL_NOME` varchar(60) DEFAULT NULL,
  `SL_NOME_PT` varchar(60) DEFAULT NULL,
  `SL_SIGLA` varchar(2) DEFAULT NULL,
  `SL_BACEN` int(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Países e Nações';

--
-- Extraindo dados da tabela `MAM_Country`
--

INSERT INTO `MAM_Country` (`SL_ID`, `SL_NOME`, `SL_NOME_PT`, `SL_SIGLA`, `SL_BACEN`) VALUES
(1, 'Brazil', 'Brasil', 'BR', 1058),
(2, 'Afghanistan', 'Afeganistão', 'AF', 132),
(3, 'Albania', 'Albânia, Republica da', 'AL', 175),
(4, 'Algeria', 'Argélia', 'DZ', 590),
(5, 'American Samoa', 'Samoa Americana', 'AS', 6912),
(6, 'Andorra', 'Andorra', 'AD', 370),
(7, 'Angola', 'Angola', 'AO', 400),
(8, 'Anguilla', 'Anguilla', 'AI', 418),
(9, 'Antarctica', 'Antártida', 'AQ', 3596),
(10, 'Antigua and Barbuda', 'Antigua e Barbuda', 'AG', 434),
(11, 'Argentina', 'Argentina', 'AR', 639),
(12, 'Armenia', 'Armênia, Republica da', 'AM', 647),
(13, 'Aruba', 'Aruba', 'AW', 655),
(14, 'Australia', 'Austrália', 'AU', 698),
(15, 'Austria', 'Áustria', 'AT', 728),
(16, 'Azerbaijan', 'Azerbaijão, Republica do', 'AZ', 736),
(17, 'Bahamas', 'Bahamas, Ilhas', 'BS', 779),
(18, 'Bahrain', 'Bahrein, Ilhas', 'BH', 809),
(19, 'Bangladesh', 'Bangladesh', 'BD', 817),
(20, 'Barbados', 'Barbados', 'BB', 833),
(21, 'Belarus', 'Belarus, Republica da', 'BY', 850),
(22, 'Belgium', 'Bélgica', 'BE', 876),
(23, 'Belize', 'Belize', 'BZ', 884),
(24, 'Benin', 'Benin', 'BJ', 2291),
(25, 'Bermuda', 'Bermudas', 'BM', 906),
(26, 'Bhutan', 'Butão', 'BT', 1198),
(27, 'Bolivia', 'Bolívia', 'BO', 973),
(28, 'Bosnia and Herzegowina', 'Bósnia-herzegovina (Republica da)', 'BA', 981),
(29, 'Botswana', 'Botsuana', 'BW', 1015),
(30, 'Bouvet Island', 'Ilha Bouvet', 'BV', 1023),
(31, 'British Indian Ocean Territory', 'Território Britânico do Oceano Indico', 'IO', 7820),
(32, 'Brunei Darussalam', 'Brunei', 'BN', 1082),
(33, 'Bulgaria', 'Bulgária, Republica da', 'BG', 1112),
(34, 'Burkina Faso', 'Burkina Faso', 'BF', 310),
(35, 'Burundi', 'Burundi', 'BI', 1155),
(36, 'Cambodia', 'Camboja', 'KH', 1414),
(37, 'Cameroon', 'Camarões', 'CM', 1457),
(38, 'Canada', 'Canada', 'CA', 1490),
(39, 'Cape Verde', 'Cabo Verde, Republica de', 'CV', 1279),
(40, 'Cayman Islands', 'Cayman, Ilhas', 'KY', 1376),
(41, 'Central African Republic', 'Republica Centro-Africana', 'CF', 6408),
(42, 'Chad', 'Chade', 'TD', 7889),
(43, 'Chile', 'Chile', 'CL', 1589),
(44, 'China', 'China, Republica Popular', 'CN', 1600),
(45, 'Christmas Island', 'Christmas, Ilha (Navidad)', 'CX', 5118),
(46, 'Cocos (Keeling) Islands', 'Cocos (Keeling), Ilhas', 'CC', 1651),
(47, 'Colombia', 'Colômbia', 'CO', 1694),
(48, 'Comoros', 'Comores, Ilhas', 'KM', 1732),
(49, 'Congo', 'Congo', 'CG', 1775),
(50, 'Congo, the Democratic Republic of the', 'Congo, Republica Democrática do', 'CD', 8885),
(51, 'Cook Islands', 'Cook, Ilhas', 'CK', 1830),
(52, 'Costa Rica', 'Costa Rica', 'CR', 1961),
(53, 'Cote d`Ivoire', 'Costa do Marfim', 'CI', 1937),
(54, 'Croatia (Hrvatska)', 'Croácia (Republica da)', 'HR', 1953),
(55, 'Cuba', 'Cuba', 'CU', 1996),
(56, 'Cyprus', 'Chipre', 'CY', 1635),
(57, 'Czech Republic', 'Tcheca, Republica', 'CZ', 7919),
(58, 'Denmark', 'Dinamarca', 'DK', 2321),
(59, 'Djibouti', 'Djibuti', 'DJ', 7838),
(60, 'Dominica', 'Dominica, Ilha', 'DM', 2356),
(61, 'Dominican Republic', 'Republica Dominicana', 'DO', 6475),
(62, 'East Timor', 'Timor Leste', 'TL', 7951),
(63, 'Ecuador', 'Equador', 'EC', 2399),
(64, 'Egypt', 'Egito', 'EG', 2402),
(65, 'El Salvador', 'El Salvador', 'SV', 6874),
(66, 'Equatorial Guinea', 'Guine-Equatorial', 'GQ', 3310),
(67, 'Eritrea', 'Eritreia', 'ER', 2437),
(68, 'Estonia', 'Estônia, Republica da', 'EE', 2518),
(69, 'Ethiopia', 'Etiópia', 'ET', 2534),
(70, 'Falkland Islands (Malvinas)', 'Falkland (Ilhas Malvinas)', 'FK', 2550),
(71, 'Faroe Islands', 'Feroe, Ilhas', 'FO', 2593),
(72, 'Fiji', 'Fiji', 'FJ', 8702),
(73, 'Finland', 'Finlândia', 'FI', 2712),
(74, 'France', 'Franca', 'FR', 2755),
(76, 'French Guiana', 'Guiana francesa', 'GF', 3255),
(77, 'French Polynesia', 'Polinésia Francesa', 'PF', 5991),
(78, 'French Southern Territories', 'Terras Austrais e Antárticas Francesas', 'TF', 3607),
(79, 'Gabon', 'Gabão', 'GA', 2810),
(80, 'Gambia', 'Gambia', 'GM', 2852),
(81, 'Georgia', 'Georgia, Republica da', 'GE', 2917),
(82, 'Germany', 'Alemanha', 'DE', 230),
(83, 'Ghana', 'Gana', 'GH', 2895),
(84, 'Gibraltar', 'Gibraltar', 'GI', 2933),
(85, 'Greece', 'Grécia', 'GR', 3018),
(86, 'Greenland', 'Groenlândia', 'GL', 3050),
(87, 'Grenada', 'Granada', 'GD', 2976),
(88, 'Guadeloupe', 'Guadalupe', 'GP', 3093),
(89, 'Guam', 'Guam', 'GU', 3131),
(90, 'Guatemala', 'Guatemala', 'GT', 3174),
(91, 'Guinea', 'Guine', 'GN', 3298),
(92, 'Guinea-Bissau', 'Guine-Bissau', 'GW', 3344),
(93, 'Guyana', 'Guiana', 'GY', 3379),
(94, 'Haiti', 'Haiti', 'HT', 3417),
(95, 'Heard and Mc Donald Islands', 'Ilha Heard e Ilhas McDonald', 'HM', 3603),
(96, 'Holy See (Vatican City State)', 'Vaticano, Estado da Cidade do', 'VA', 8486),
(97, 'Honduras', 'Honduras', 'HN', 3450),
(98, 'Hong Kong', 'Hong Kong', 'HK', 3514),
(99, 'Hungary', 'Hungria, Republica da', 'HU', 3557),
(100, 'Iceland', 'Islândia', 'IS', 3794),
(101, 'India', 'Índia', 'IN', 3611),
(102, 'Indonesia', 'Indonésia', 'ID', 3654),
(103, 'Iran (Islamic Republic of)', 'Ira, Republica Islâmica do', 'IR', 3727),
(104, 'Iraq', 'Iraque', 'IQ', 3697),
(105, 'Ireland', 'Irlanda', 'IE', 3751),
(106, 'Israel', 'Israel', 'IL', 3832),
(107, 'Italy', 'Itália', 'IT', 3867),
(108, 'Jamaica', 'Jamaica', 'JM', 3913),
(109, 'Japan', 'Japão', 'JP', 3999),
(110, 'Jordan', 'Jordânia', 'JO', 4030),
(111, 'Kazakhstan', 'Cazaquistão, Republica do', 'KZ', 1538),
(112, 'Kenya', 'Quênia', 'KE', 6238),
(113, 'Kiribati', 'Kiribati', 'KI', 4111),
(114, 'Korea, Democratic People`s Republic of', 'Coreia, Republica Popular Democrática da', 'KP', 1872),
(115, 'Korea, Republic of', 'Coreia, Republica da', 'KR', 1902),
(116, 'Kuwait', 'Kuwait', 'KW', 1988),
(117, 'Kyrgyzstan', 'Quirguiz, Republica', 'KG', 6254),
(118, 'Lao People`s Democratic Republic', 'Laos, Republica Popular Democrática do', 'LA', 4200),
(119, 'Latvia', 'Letônia, Republica da', 'LV', 4278),
(120, 'Lebanon', 'Líbano', 'LB', 4316),
(121, 'Lesotho', 'Lesoto', 'LS', 4260),
(122, 'Liberia', 'Libéria', 'LR', 4340),
(123, 'Libyan Arab Jamahiriya', 'Líbia', 'LY', 4383),
(124, 'Liechtenstein', 'Liechtenstein', 'LI', 4405),
(125, 'Lithuania', 'Lituânia, Republica da', 'LT', 4421),
(126, 'Luxembourg', 'Luxemburgo', 'LU', 4456),
(127, 'Macau', 'Macau', 'MO', 4472),
(128, 'Macedonia, The Former Yugoslav Republic of', 'Macedônia, Antiga Republica Iugoslava', 'MK', 4499),
(129, 'Madagascar', 'Madagascar', 'MG', 4502),
(130, 'Malawi', 'Malavi', 'MW', 4588),
(131, 'Malaysia', 'Malásia', 'MY', 4553),
(132, 'Maldives', 'Maldivas', 'MV', 4618),
(133, 'Mali', 'Mali', 'ML', 4642),
(134, 'Malta', 'Malta', 'MT', 4677),
(135, 'Marshall Islands', 'Marshall, Ilhas', 'MH', 4766),
(136, 'Martinique', 'Martinica', 'MQ', 4774),
(137, 'Mauritania', 'Mauritânia', 'MR', 4880),
(138, 'Mauritius', 'Mauricio', 'MU', 4855),
(139, 'Mayotte', 'Mayotte (Ilhas Francesas)', 'YT', 4898),
(140, 'Mexico', 'México', 'MX', 4936),
(141, 'Micronesia, Federated States of', 'Micronesia', 'FM', 4995),
(142, 'Moldova, Republic of', 'Moldávia, Republica da', 'MD', 4944),
(143, 'Monaco', 'Mônaco', 'MC', 4952),
(144, 'Mongolia', 'Mongólia', 'MN', 4979),
(145, 'Montserrat', 'Montserrat, Ilhas', 'MS', 5010),
(146, 'Morocco', 'Marrocos', 'MA', 4740),
(147, 'Mozambique', 'Moçambique', 'MZ', 5053),
(148, 'Myanmar', 'Mianmar (Birmânia)', 'MM', 930),
(149, 'Namibia', 'Namíbia', 'NA', 5070),
(150, 'Nauru', 'Nauru', 'NR', 5088),
(151, 'Nepal', 'Nepal', 'NP', 5177),
(152, 'Netherlands', 'Países Baixos (Holanda)', 'NL', 5738),
(154, 'New Caledonia', 'Nova Caledonia', 'NC', 5428),
(155, 'New Zealand', 'Nova Zelândia', 'NZ', 5487),
(156, 'Nicaragua', 'Nicarágua', 'NI', 5215),
(157, 'Niger', 'Níger', 'NE', 5258),
(158, 'Nigeria', 'Nigéria', 'NG', 5282),
(159, 'Niue', 'Niue, Ilha', 'NU', 5312),
(160, 'Norfolk Island', 'Norfolk, Ilha', 'NF', 5355),
(161, 'Northern Mariana Islands', 'Marianas do Norte', 'MP', 4723),
(162, 'Norway', 'Noruega', 'NO', 5380),
(163, 'Oman', 'Oma', 'OM', 5568),
(164, 'Pakistan', 'Paquistão', 'PK', 5762),
(165, 'Palau', 'Palau', 'PW', 5754),
(166, 'Panama', 'Panamá', 'PA', 5800),
(167, 'Papua New Guinea', 'Papua Nova Guine', 'PG', 5452),
(168, 'Paraguay', 'Paraguai', 'PY', 5860),
(169, 'Peru', 'Peru', 'PE', 5894),
(170, 'Philippines', 'Filipinas', 'PH', 2674),
(171, 'Pitcairn', 'Pitcairn, Ilha', 'PN', 5932),
(172, 'Poland', 'Polônia, Republica da', 'PL', 6033),
(173, 'Portugal', 'Portugal', 'PT', 6076),
(174, 'Puerto Rico', 'Porto Rico', 'PR', 6114),
(175, 'Qatar', 'Catar', 'QA', 1546),
(176, 'Reunion', 'Reunião, Ilha', 'RE', 6602),
(177, 'Romania', 'Romênia', 'RO', 6700),
(178, 'Russian Federation', 'Rússia, Federação da', 'RU', 6769),
(179, 'Rwanda', 'Ruanda', 'RW', 6750),
(180, 'Saint Kitts and Nevis', 'São Cristovão e Neves, Ilhas', 'KN', 6955),
(181, 'Saint LUCIA', 'Santa Lucia', 'LC', 7153),
(182, 'Saint Vincent and the Grenadines', 'São Vicente e Granadinas', 'VC', 7056),
(183, 'Samoa', 'Samoa', 'WS', 6904),
(184, 'San Marino', 'San Marino', 'SM', 6971),
(185, 'Sao Tome and Principe', 'São Tome e Príncipe, Ilhas', 'ST', 7200),
(186, 'Saudi Arabia', 'Arábia Saudita', 'SA', 531),
(187, 'Senegal', 'Senegal', 'SN', 7285),
(188, 'Seychelles', 'Seychelles', 'SC', 7315),
(189, 'Sierra Leone', 'Serra Leoa', 'SL', 7358),
(190, 'Singapore', 'Cingapura', 'SG', 7412),
(191, 'Slovakia (Slovak Republic)', 'Eslovaca, Republica', 'SK', 2470),
(192, 'Slovenia', 'Eslovênia, Republica da', 'SI', 2461),
(193, 'Solomon Islands', 'Salomão, Ilhas', 'SB', 6777),
(194, 'Somalia', 'Somalia', 'SO', 7480),
(195, 'South Africa', 'África do Sul', 'ZA', 7560),
(196, 'South Georgia and the South Sandwich Islands', 'Ilhas Geórgia do Sul e Sandwich do Sul', 'GS', 2925),
(197, 'Spain', 'Espanha', 'ES', 2453),
(198, 'Sri Lanka', 'Sri Lanka', 'LK', 7501),
(199, 'St. Helena', 'Santa Helena', 'SH', 7102),
(200, 'St. Pierre and Miquelon', 'São Pedro e Miquelon', 'PM', 7005),
(201, 'Sudan', 'Sudão', 'SD', 7595),
(202, 'Suriname', 'Suriname', 'SR', 7706),
(203, 'Svalbard and Jan Mayen Islands', 'Svalbard e Jan Mayen', 'SJ', 7552),
(204, 'Swaziland', 'Suazilândia', 'SZ', 7544),
(205, 'Sweden', 'Suécia', 'SE', 7641),
(206, 'Switzerland', 'Suíça', 'CH', 7676),
(207, 'Syrian Arab Republic', 'Síria, Republica Árabe da', 'SY', 7447),
(208, 'Taiwan, Province of China', 'Formosa (Taiwan)', 'TW', 1619),
(209, 'Tajikistan', 'Tadjiquistao, Republica do', 'TJ', 7722),
(210, 'Tanzania, United Republic of', 'Tanzânia, Republica Unida da', 'TZ', 7803),
(211, 'Thailand', 'Tailândia', 'TH', 7765),
(212, 'Togo', 'Togo', 'TG', 8001),
(213, 'Tokelau', 'Toquelau, Ilhas', 'TK', 8052),
(214, 'Tonga', 'Tonga', 'TO', 8109),
(215, 'Trinidad and Tobago', 'Trinidad e Tobago', 'TT', 8150),
(216, 'Tunisia', 'Tunísia', 'TN', 8206),
(217, 'Turkey', 'Turquia', 'TR', 8273),
(218, 'Turkmenistan', 'Turcomenistão, Republica do', 'TM', 8249),
(219, 'Turks and Caicos Islands', 'Turcas e Caicos, Ilhas', 'TC', 8230),
(220, 'Tuvalu', 'Tuvalu', 'TV', 8281),
(221, 'Uganda', 'Uganda', 'UG', 8338),
(222, 'Ukraine', 'Ucrânia', 'UA', 8311),
(223, 'United Arab Emirates', 'Emirados Árabes Unidos', 'AE', 2445),
(224, 'United Kingdom', 'Reino Unido', 'GB', 6289),
(225, 'United States', 'Estados Unidos', 'US', 2496),
(226, 'United States Minor Outlying Islands', 'Ilhas Menores Distantes dos Estados Unidos', 'UM', 18664),
(227, 'Uruguay', 'Uruguai', 'UY', 8451),
(228, 'Uzbekistan', 'Uzbequistão, Republica do', 'UZ', 8478),
(229, 'Vanuatu', 'Vanuatu', 'VU', 5517),
(230, 'Venezuela', 'Venezuela', 'VE', 8508),
(231, 'Viet Nam', 'Vietnã', 'VN', 8583),
(232, 'Virgin Islands (British)', 'Virgens, Ilhas (Britânicas)', 'VG', 8630),
(233, 'Virgin Islands (U.S.)', 'Virgens, Ilhas (E.U.A.)', 'VI', 8664),
(234, 'Wallis and Futuna Islands', 'Wallis e Futuna, Ilhas', 'WF', 8753),
(235, 'Western Sahara', 'Saara Ocidental', 'EH', 6858),
(236, 'Yemen', 'Iémen', 'YE', 3573),
(237, 'Yugoslavia', 'Iugoslávia, República Fed. da', 'YU', 3883),
(238, 'Zambia', 'Zâmbia', 'ZM', 8907),
(239, 'Zimbabwe', 'Zimbabue', 'ZW', 6653),
(240, 'Bailiwick of Guernsey', 'Guernsey, Ilha do Canal (Inclui Alderney e Sark)', 'GG', 3212),
(241, 'Bailiwick of Jersey', 'Jersey, Ilha do Canal', 'JE', 3930),
(243, 'Isle of Man', 'Man, Ilha de', 'IM', 3595),
(246, 'Crna Gora (Montenegro)', 'Montenegro', 'ME', 4985),
(247, 'SÉRVIA', 'Republika Srbija', 'RS', 7370),
(248, 'Republic of South Sudan', 'Sudao do Sul', 'SS', 7600),
(249, 'Zona del Canal de Panamá', 'Zona do Canal do Panamá', NULL, 8958),
(252, 'Dawlat Filasṭīn', 'Palestina', 'PS', 5780),
(253, 'Åland Islands', 'Ilhas de Aland', 'AX', 153),
(254, 'Saint Barthélemy', 'Coletividade de São Bartolomeu', 'BL', 3598),
(255, 'Curaçao', 'Curaçao', 'CW', 2003),
(256, 'Saint Martin', 'Ilha de São Martinho (França)', 'MF', 3604),
(257, 'Sint Maarten (Dutch part)', 'São Martinho (Países Baixos)', 'SX', 3606),
(258, 'Bonaire', 'Bonaire', 'AN', 990),
(259, 'Antartica', 'Antartica', 'AQ', 420),
(260, 'Heard Island and McDonald Islands', 'Ilha Herad e Ilhas Macdonald', 'AU', 3433),
(261, 'Collectivité de Saint-Barthélemy', 'Colectividade de São Bartolomeu', 'FR', 6939),
(262, 'Saint Martin', 'Ilha de São Martinho', 'FR', 6980),
(263, 'Territoire des Terres australes et antarctiques françaises', 'Território das Terras Austrais e Antárcticas Francesas', 'TF', 7811);

-- --------------------------------------------------------

--
-- Estrutura da tabela `MAM_State`
--

CREATE TABLE `MAM_State` (
  `UF_ID` int(11) NOT NULL,
  `UF_NOME` varchar(75) DEFAULT NULL,
  `UF_UF` varchar(2) DEFAULT NULL,
  `UF_IBGE` int(2) DEFAULT NULL,
  `UF_SL` int(3) DEFAULT NULL,
  `UF_DDD` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Unidades Federativas';

--
-- Extraindo dados da tabela `MAM_State`
--

INSERT INTO `MAM_State` (`UF_ID`, `UF_NOME`, `UF_UF`, `UF_IBGE`, `UF_SL`, `UF_DDD`) VALUES
(1, 'Acre', 'AC', 12, 1, '68'),
(2, 'Alagoas', 'AL', 27, 1, '82'),
(3, 'Amazonas', 'AM', 13, 1, '97,92'),
(4, 'Amapá', 'AP', 16, 1, '96'),
(5, 'Bahia', 'BA', 29, 1, '77,75,73,74,71'),
(6, 'Ceará', 'CE', 23, 1, '88,85'),
(7, 'Distrito Federal', 'DF', 53, 1, '61'),
(8, 'Espírito Santo', 'ES', 32, 1, '28,27'),
(9, 'Goiás', 'GO', 52, 1, '62,64,61'),
(10, 'Maranhão', 'MA', 21, 1, '99,98'),
(11, 'Minas Gerais', 'MG', 31, 1, '34,37,31,33,35,38,32'),
(12, 'Mato Grosso do Sul', 'MS', 50, 1, '67'),
(13, 'Mato Grosso', 'MT', 51, 1, '65,66'),
(14, 'Pará', 'PA', 15, 1, '91,94,93'),
(15, 'Paraíba', 'PB', 25, 1, '83'),
(16, 'Pernambuco', 'PE', 26, 1, '81,87'),
(17, 'Piauí', 'PI', 22, 1, '89,86'),
(18, 'Paraná', 'PR', 41, 1, '43,41,42,44,45,46'),
(19, 'Rio de Janeiro', 'RJ', 33, 1, '24,22,21'),
(20, 'Rio Grande do Norte', 'RN', 24, 1, '84'),
(21, 'Rondônia', 'RO', 11, 1, '69'),
(22, 'Roraima', 'RR', 14, 1, '95'),
(23, 'Rio Grande do Sul', 'RS', 43, 1, '53,54,55,51'),
(24, 'Santa Catarina', 'SC', 42, 1, '47,48,49'),
(25, 'Sergipe', 'SE', 28, 1, '79'),
(26, 'São Paulo', 'SP', 35, 1, '11,12,13,14,15,16,17,18,19'),
(27, 'Tocantins', 'TO', 17, 1, '63'),
(99, 'Exterior', 'EX', 99, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Activities`
--

CREATE TABLE `Wo_Activities` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL DEFAULT 0,
  `post_id` int(255) NOT NULL DEFAULT 0,
  `activity_type` varchar(32) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Activities`
--

INSERT INTO `Wo_Activities` (`id`, `user_id`, `post_id`, `activity_type`, `time`) VALUES
(400, 85, 356, 'commented_post', 1541781654),
(402, 85, 356, 'liked_post', 1541781664),
(403, 85, 348, 'commented_post', 1541781802),
(404, 85, 348, 'liked_post', 1541781804),
(405, 85, 346, 'wondered_post', 1541781808),
(406, 85, 346, 'commented_post', 1541781815),
(418, 102, 369, 'liked_post', 1542754625),
(421, 104, 384, 'wondered_post', 1542781893),
(422, 104, 369, 'wondered_post', 1542781897),
(423, 104, 366, 'liked_post', 1542781900),
(424, 104, 356, 'liked_post', 1542781905),
(425, 104, 355, 'wondered_post', 1542781910),
(426, 104, 353, 'liked_post', 1542781951),
(427, 104, 354, 'wondered_post', 1542781952),
(428, 104, 354, 'commented_post', 1542781967),
(435, 104, 394, 'commented_post', 1542808308),
(436, 104, 394, 'wondered_post', 1542808309),
(445, 1, 406, 'liked_post', 1543021981),
(447, 102, 410, 'liked_post', 1543098041),
(448, 1, 408, 'commented_post', 1543098052),
(450, 1, 408, 'liked_post', 1543109670),
(451, 85, 410, 'liked_post', 1543152854);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_AdminInvitations`
--

CREATE TABLE `Wo_AdminInvitations` (
  `id` int(11) NOT NULL,
  `code` varchar(300) NOT NULL DEFAULT '0',
  `posted` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_AdminInvitations`
--

INSERT INTO `Wo_AdminInvitations` (`id`, `code`, `posted`) VALUES
(2, '14493812005bac95183013d4.28223967', '1538037016');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Ads`
--

CREATE TABLE `Wo_Ads` (
  `id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT '',
  `code` text DEFAULT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Ads`
--

INSERT INTO `Wo_Ads` (`id`, `type`, `code`, `active`) VALUES
(1, 'header', '<meta name=\"theme-color\" content=\"#35065c\">\n<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"#35065c\">\n<meta name=\"msapplication-navbutton-color\" content=\"#35065c\">\n\n <TITLE>Redelive</TITLE>\n <meta name=\"google-site-verification\" content=\"STT6bj1tUVv49On9FxYO_pBKbf7vhKmYsdt9Vi2AAUo\" />', '1'),
(2, 'sidebar', '</script>\n<script>\n  (adsbygoogle = window.adsbygoogle || []).push({\n    google_ad_client: \"ca-pub-6777534142039996\",\n    enable_page_level_ads: true\n  });\n</script>\n', '1'),
(4, 'footer', '<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>\n<script>\n  (adsbygoogle = window.adsbygoogle || []).push({\n    google_ad_client: \"ca-pub-6777534142039996\",\n    enable_page_level_ads: true\n  });\n</script>', '1'),
(5, 'post_first', '', '0'),
(6, 'post_second', '', '0'),
(7, 'post_third', '<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>\n<script>\n  (adsbygoogle = window.adsbygoogle || []).push({\n    google_ad_client: \"ca-pub-6777534142039996\",\n    enable_page_level_ads: true\n  });\n</script>', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Affiliates_Requests`
--

CREATE TABLE `Wo_Affiliates_Requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `amount` varchar(100) NOT NULL DEFAULT '0',
  `full_amount` varchar(100) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_AgoraVideoCall`
--

CREATE TABLE `Wo_AgoraVideoCall` (
  `id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL DEFAULT 0,
  `to_id` int(11) NOT NULL DEFAULT 0,
  `type` varchar(50) NOT NULL DEFAULT 'video',
  `room_name` varchar(50) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT 0,
  `status` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Albums_Media`
--

CREATE TABLE `Wo_Albums_Media` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `image` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Announcement`
--

CREATE TABLE `Wo_Announcement` (
  `id` int(11) NOT NULL,
  `text` text DEFAULT NULL,
  `time` int(32) NOT NULL DEFAULT 0,
  `active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Announcement_Views`
--

CREATE TABLE `Wo_Announcement_Views` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `announcement_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Apps`
--

CREATE TABLE `Wo_Apps` (
  `id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL DEFAULT 0,
  `app_name` varchar(32) NOT NULL,
  `app_website_url` varchar(55) NOT NULL,
  `app_description` text NOT NULL,
  `app_avatar` varchar(100) NOT NULL DEFAULT 'upload/photos/app-default-icon.png',
  `app_callback_url` varchar(255) NOT NULL,
  `app_id` varchar(32) NOT NULL,
  `app_secret` varchar(55) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Wo_Apps`
--

INSERT INTO `Wo_Apps` (`id`, `app_user_id`, `app_name`, `app_website_url`, `app_description`, `app_avatar`, `app_callback_url`, `app_id`, `app_secret`, `active`) VALUES
(1, 1, 'redelive', 'https://facebook.com', 'aaaaaaaaaaa', 'upload/photos/2018/09/HnK48fb8yShCzLMiNKDb_27_535209179d4f390b2f49d6c8b1929b0c_avatar.png', 'https://google.com', 'aca0a63eb58f8a283d21', '0f9dbfa2d963e418044bf0b3146b1bc6355d641', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_AppsSessions`
--

CREATE TABLE `Wo_AppsSessions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `session_id` varchar(120) NOT NULL DEFAULT '',
  `platform` varchar(32) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Wo_AppsSessions`
--

INSERT INTO `Wo_AppsSessions` (`id`, `user_id`, `session_id`, `platform`, `time`) VALUES
(7, 1, 'de034949490c52cacfafae8ff6ef7fdf37ba9f7c1b2129e8620811d71fb075f8407c36c255404648251d52afeb09449719aa7ba0b842c755', 'web', 1543158022),
(15, 1, '467fef901ebd4d7dcbe98beb37323a28b0e257e0ec82b86a26b540b954cbd9da45c6f7797320055994c0915ab3bcbc61c1c61624dd6d7cd5', 'web', 1543158022),
(16, 2, '0493c5b65f353f3b52da318c4332c5a0e1efb2975b9c87016a92170669c90293df91ee3c111345876b54185ccddbef6cc10b6586179db501', 'web', 1537837008),
(18, 1, 'ac0b2d894643f102e5a723ce2c75713b94a2ce93164c51cfad432afc2c580cf3f3bc85d1245421490ea6f098a59fcf2462afc50d130ff034', 'web', 1543158022),
(20, 2, '5bf73bfa6940631e447a42ef8ca0a25888592d5420b6225a437457119cf71efadcba8ac7954028830cc918dec28dbd91f6006a2ce8101e2e', 'web', 1537837008),
(21, 2, '658a8a43202e4041b906892bf78aaf1288a734c779fc5e10e93d74d4789612370436b52d6954165348fbab00052197bc8bd943498b89dd71', 'web', 1537837008),
(23, 2, '383cde0845b2515cd25160c91b43e33eb9872689f0c22d5b7862ab4cbee256c867ebfc3622766174ae5eb824ef87499f644c3f11a7176157', 'web', 1537837008),
(29, 9, '2b1fbbbd5909509148408b2269de5afb37f2e0c6dadf436db135f8b4cda94fd7b53e63c9807455650b7a9d54deeb611edc4540d286e9a042', 'web', 1540538162),
(30, 9, '16ce47e4a57765e1b3dc328cb34f39f3ac0987b1d3d791f79a2a4abb2d5410d0bee2c876679351827fa1575cbd7027c9a799983a485c3c2f', 'web', 1540538162),
(31, 9, '717987e18356c673e6edc1900f2e1c8c0d94e84d258a42f48ddb7f00be332bf12049d95f72408983d9fbed9da256e344c1fa46bb46c34c5f', 'web', 1540538162),
(32, 10, 'b602f1ba9ae4e95775a750beedc85de577abde34ca6eb1d7b4ffcfb04262aedc0051bd9b1218231776bba5ee241042431796c97a92e64f5d', 'web', 1538029389),
(34, 9, '00dab97c580785f348aa6555f51bbf45984fef4f7096df50ba1776db9ae3204118e11f9f12220997ad0f7a25211abc3889cb0f420c85e671', 'web', 1540538162),
(39, 9, '5e553b3a57b935dad480b957949a19329c0e9c7920849414c681983aaf95c7dc783c4bfb80623654ecf9902e0f61677c8de25ae60b654669', 'web', 1540538162),
(40, 9, 'd08ffc4b9c7e338239394e8594ecb52c4be130281ed8f476712cda1371f79b25a9f0c36645064943ced92d38d8fec10f1da779dfa44f8bbe', 'web', 1540538162),
(41, 9, '1d3d707fd6d970b2e4f7c412efd484df2a3be1b461ffad466f2d17df0bb178eb6c9654c67714384060b2149f6bafd1cc9d505496f09160ba', 'web', 1540538162),
(42, 9, '59d5a38247830fc9462eda0a0d4d5147888be11a43c2f2b6b642706f14a39f5d0eb6159012905890ce11641e056f7b59aef8e9a42eaeb65b', 'web', 1540538162),
(45, 1, '15b09caeeb57f6dcf32592905b7ad0df89f7e3bc04090a6d73a6a7ba6c83754e72b2022b3325643561bfdc160e4c099203c72258d8825340', 'web', 1543158022),
(46, 9, '10de009f32f5adcf1d8668d5b2656565c798a14efbd127b93d5ff9e0507ad1647a9d474814501261828c3938b662961ed8f775ed638b97f2', 'web', 1540538162),
(50, 15, 'e4190321ed8a4c2c70e07a89cdaba4c5e833af02683c18eb9907fe7d712138d6491b31d324495442fd4771e85e1f916f239624486bff502d', 'web', 1538368242),
(64, 17, '68eaebd759340f7c25eb74dfd61f327f1ce743281dc122c65ae8e4296bea70a9cff2359056027130713fd63d76c8a57b16fc433fb4ae718a', 'web', 1538372942),
(70, 19, '47322a684cc720625416e8dca37825f2148f78a978aa716eb7e7323f2d581572678066d0438647810660895c22f8a14eb039bfb9beb0778f', 'web', 1538428038),
(71, 19, 'f967abae6c85ba65b5e7b900b1dea34f34a066f2a349c2406a0c36b7a9eb6566910c04e912177273ce4449660c6523b377b22a1dc2da5556', 'web', 1538428052),
(72, 9, '1dd16950518532afdcc2224592be5da9af67eddef4c246e0c8c5341a58291204b8d7ceb2682679823dcaf04c357c577a857f3ffadc555f9b', 'web', 1540538162),
(76, 9, '3704a4fa5753773226c7885f6d2065b8ecda53a783919a44c50b8774b69ce6d84f8c5ea2898423896c2fdcf862a752ca2c9e49866a05e1df', 'web', 1540538162),
(79, 9, '6fa1aee736ac6c187a89fb4f3750f2b0119e2b4deb3da08ab1ec33cbbc599099b04f4ca172455071c4bfbf68f5d8d0f8b9a0752ca08ea01d', 'web', 1540538162),
(80, 9, 'bbb36456756774f8bbeb9dde0dbfbfb476ce9d1dbc14547364c3eaa187ec198405c4b62f133713254d630f9347177b17ec7a362f19489239', 'web', 1540538162),
(81, 9, '281d3e843d5de9446db3a6cedfc0b5338bf8ed51fc5ea848ff2f3fb2bbdd336128f4d66e37128623d77c703536718b95308130ff2e5cf9ee', 'web', 1540538162),
(83, 9, 'e7451ed3a78395d3e6a945b713e507f1099af134eaac31ff088d791233dc7f090b0a009965588909d58cc99982459ca8d73b89068f53e446', 'web', 1540538162),
(84, 20, '14cae565559f5ab6a0bfb7d155348448299e1dbe3c588b830532c33c983f8897ea1f47c640946622959ab9a0695c467e7caf75431a872e5c', 'web', 1538484609),
(91, 22, '8ec24792cd569c8c5adf8a732230a054e435277ce2beae91ff07fce2ff46bab3779f4c0b39908544d428d070622e0f4363fceae11f4a3576', 'web', 1538716327),
(93, 28, '5b8f0e85be66b06c34e977ecaa062a77c3fe3572ab5322adc54f00bdf7f2a01963f652335845162871dd9b48ff8928e726d4d21a5af243f3', 'web', 1538749933),
(103, 29, '15067aa8b0d5856d25080a164f50196cac9ffc415b1842e93e3a4cec9579e57583a1669013440388d384dec9f5f7a64a36b5c8f03b8a6d92', 'web', 1538716114),
(104, 22, '239dfbd34ba3c940e1ae6dd22f9d308163012f80f96c8a8269024efc69bf1b739a16b4d447200709ad185a320a9c8af2e25d53ef7cbacd4f', 'web', 1538716327),
(105, 1, 'e49eaaa00296eed9e130dd7be3643aba2b43580dd96d957cc02618f8165b232f1a7bd4739060394425b07af81d8c74341f00dc139652fdb0', 'web', 1543158022),
(106, 33, 'ddbb2ee40d8ba531b7067751003923783a67986b705fb0875d0455eb5b309d46a8138b1e50551477c39e1a03859f9ee215bc49131d0caf33', 'web', 1538966326),
(107, 33, '47369e13a60e69171f02b18ac1bde5ccf95950b92b7d317cd01e0464c983fe9ea1b20dd7445891276de59d960d3bb8a6346c058930f3cd28', 'web', 1538966330),
(108, 1, 'e8b7285e1f43c75a4f7d5cb5710f00101cff0440065fe2a3431dff1dd2b911ec272c061f950793051e44fdf9c44d7328fecc02d677ed704d', 'web', 1543158022),
(109, 1, '869ca06db26eb195963edec0184390ad9471a7c47615349078035008768f08830135a01f82637150de8aa43e5d5fa8536cf23e54244476fa', 'web', 1543158022),
(111, 9, '666654b3de5ae7bf86f0be11a051713cebd43c01d30d201e276607bdc0681e171713581f7663422898baeb82b676b662e12a7af8ad9212f6', 'web', 1540538162),
(117, 9, 'f7e0fef6303cb7d016f7b1c067fc8df1ba3ea6b07629d588cad383a044bc65c61221c9fe34058328a860a7886d7c7e2a8d3eaac96f76dc0d', 'web', 1540538162),
(121, 1, '22439ce612af2fda335f378599d51786596cfada710ab3bd25e498da643c163e94e1109e37391107940392f5f32a7ade1cc201767cf83e31', 'web', 1543158022),
(122, 1, 'e9cb16074685adfc78ee04340f9d070280e42804978d31311f4aef16fe77278081988a6475358231593906af0d138e69f49d251d3e7cbed0', 'web', 1543158022),
(123, 1, '9b8774cfacdaecd43870230e85e13e0173c3da1d7b4a1b2aad9f94d90a445749cdf80fe957181993f610a13de080fb8df6cf972fc01ad93f', 'web', 1543158022),
(125, 9, '2325f5a88249ad2607b724a50f745373ae9c720ed8c910bc656ea40651580d84c3329e0b7139725814c4f36143b4b09cbc320d7c95a50ee7', 'web', 1540538162),
(127, 35, '793cda761e9726fc126544f918a7076734b6de345f2b581d0b0ac71c5f9c878eaf8b1ef48505968071dd9b48ff8928e726d4d21a5af243f3', 'web', 1539144095),
(128, 1, '3b195f2c7638ed9e5cb1d955c61e354387a1e1bbc100d1540b1d25fed438917c679914941769679769c7e73fea7ad35e9000ce41e1622036', 'web', 1543158022),
(129, 1, 'd6a0661f78dd0826c176688e29cdbbc597aecf657b2b9e054120e2acee2c8737bc2296a13457254095cc7ef498e141173576365264fc5fba', 'web', 1543158022),
(130, 1, 'd2351610be2c48ebc9751180611111f37d20d3d3dd302d0c25aca22e6f3ff2e37b8f07af52569634cdfa4c42f465a5a66871587c69fcfa34', 'web', 1543158022),
(131, 1, '24b7b0912c8040d15e740eaa79cfe75b57025f01dadb408f1319161153ea54b9c25adf5b35190260e7dfca01f394755c11f853602cb2608a', 'web', 1543158022),
(132, 1, '2be8aa18aa3f5487899efdc9a19e6b31a108b427aa2b036a76eb13a47e9c41813372aab052303885cf5ff72ca35f112b361de3e312c088f4', 'web', 1543158022),
(134, 1, 'a6a091fac61eba40d02081a8ef3cbd104b24c1f0f217a38a02f3aed6499f0ec2528407c570759149e7d6e2e80f0955c01f3e043ee79abbb6', 'web', 1543158022),
(135, 1, '7db526d14b8483c649a193c83b816f1a081e215c349c521220ad0434a7fa77bccc87253a295962856acb084470c0a8bdf431d5427d1f29bc', 'web', 1543158022),
(136, 1, '98dbf7915195f66daab09955a35f2f9cb74b6595fd65ecaa0938e9f205e5137df02520d88725407276e9a17937b75b73a8a430acf210feaf', 'web', 1543158022),
(137, 1, '9f89e13f5c92afe98e674008dc53d47e690cc78c255abce80fbd354324aa3d6361de292f30208370532923f11ac97d3e7cb0130315b067dc', 'web', 1543158022),
(139, 1, '5250c4511f8d096f76e26824c7acf94df431826c46f19870f2803e1e3800fd8ebf02a17f52762907b6846b0186a035fcc76b1b1d26fd42fa', 'web', 1543158022),
(140, 1, 'e79f7b26947766072313927df4b0427d53742f41b5e5bbcb258181b27ac279315f329760846545617f848746fe2599dc199a75f0d02fc3d6', 'web', 1543158022),
(141, 1, '8abe6828595a16fab92b0a473cf779ce34c252f057d3707f49e20dd0ab92cdc2a828214e74055829498f2c21688f6451d9f5fd09d53edda7', 'web', 1543158022),
(142, 1, '1a6a17756ecd4336c2820dbeccdb6d9b36d106c7071e11a71cfd497d789de1d119e2e1768888693408b7dc6e8b36bcaac15847827b7951a9', 'web', 1543158022),
(143, 1, '402bdeb3bd457b5ace2483c9a989b86f253ae392c5f7560e86ff43445f72209d3d59a58193235554256bf8e6923a52fda8ddf7dc050a1148', 'web', 1543158022),
(144, 1, 'fc2f864bb89f5cc8205d72ab6028c6bfbd37fd08ab3a390f401c08846c0758e3c92e8b2e49849273682e0e796084e163c5ca053dd8573b0c', 'web', 1543158022),
(146, 1, 'ca6bbd17e5dcb8a8085bda7ce8da6eff9098774803a7e0035f909b35dbe5af6d44f014c083364035b91a76b0b2fa7ce160212f53f3d2edba', 'web', 1543158022),
(154, 9, 'ad2ef7cb2dbee30c7ab8347d845aec8e022a3819a7ba4965b3c04d8ee2b01a26c434bd7f575533473488330ba18d83e3d0ab177178ca66eb', 'web', 1540538162),
(156, 1, 'fe30096302dc31461baf9a93a7776126f74f098e27c15056b719c553dde4b409467d21a949078094580760fb5def6e2ca8eaf601236d5b08', 'web', 1543158022),
(157, 1, '0d3e2b06b5a3af56354eeda87bc1a827adf08d0c684a1690b018a07d0147a3c9bbbed071124933836a13382a520e0420014027350a0b3eb4', 'web', 1543158022),
(158, 1, 'af31c6ce10816c255b2055344fcd899bba5b91bc19d3239bdf832eb866ce248a96d442b3993729209f9a2e9ab3dc74b58ef7c5b974807751', 'web', 1543158022),
(159, 1, 'b61d12d204e938008faaccaa8ef18a56110a241f9b96006cf2c00525b61057f30358a626331897587ac52e3f2729d1b3f6d2b7e8f6467226', 'web', 1543158022),
(160, 1, '988b126a5b1d12cedb8af745273702914bcc8b6f133205a2918d27d205a37a10a8bce42d814150765e15fb59326e7a9c3d6558ca74621683', 'web', 1543158022),
(161, 1, 'd0f3c1630e7b797179d572f1661f3b1f154f87cb52be6cc4112a9bb5f6dfbf2e460bd7eb424033027aee5d5dfa97b2516e5f639672c7e199', 'web', 1543158022),
(162, 1, '0eb5b433c90aa79c852d71007d4051954efe03b80ecb63e239a91c0df8e2f4ff582f707414258281341cd40532980c4909c8c647f2138c03', 'web', 1543158022),
(163, 1, '62dd46cf6d98805bac13f5c7d635a381d15b4eac87bc42707b00310cc13e66fb33d0bd928312025449562478de4c54fafd4ec46fdb297de5', 'web', 1543158022),
(164, 1, '24085b4fbd066131448f26a3dfedb7f17a00854ace5497b9a4ea4fd86d6de6ea551f712f533026376b1a864b240fc77e3a901e3470b243f9', 'web', 1543158022),
(165, 1, '74f38fd119fc9809f6910e45c99551c17159fed2fa1852ee1d9293f35878e2148b9dd118185744046e8404c3b93a9527c8db241a1846599a', 'web', 1543158022),
(166, 1, '4eb217caec5a6fb935cf632bfcab2b4dc8cfc001c1e830fdb7a70df6b7588df4c696da6c98842497b665bf733325cebbccee5935b1272616', 'web', 1543158022),
(167, 1, '4c272aa8df60fa9b679f20d8c35d27dbcff606122f94955de816cc217933b51881026a6413614926b59442085644532ef03417a3e5a76437', 'web', 1543158022),
(168, 1, '7aaf92ce270fc8be7d36ae14ce43a6f29453277a5a28301136f332325f58859d6577cfaf44829708e033fdb11f1faad86c9a492d4696ec31', 'web', 1543158022),
(169, 1, '5b02d689f2421e1ce098ce9872597af46d6de4e5943a89ad329b6be422d02575c8a8b5b39160204630aaa42805c04522a16e12d7e5b87437', 'web', 1543158022),
(170, 1, '67dff201f93ddd2c69bc6b8daacc0119111b5e7d897260c2d7cc3403da38b45e8063b5f14720458333b3214d792caf311e1f00fd22b392c5', 'web', 1543158022),
(171, 1, 'ca418c98d75d35c6d44ee30f8176f058829272f8851704603fe003e2a52f31e788306c1613751367d1d5923fc822531bbfd9d87d4760914b', 'web', 1543158022),
(172, 9, '4c547f442d8f4f0fc4f3ea0a4b9fea878a838f463bf96603477f382e247693f36011950890661807b8c8c63d4b8856c7872b225e53a6656c', 'web', 1540538162),
(173, 1, 'b634cc9f6e26ff3076871db2a4471a0c6811431d4cac9d68387f5096ca30994fc866a9ec410144849fb2c3bda2393ae337242f405c93a010', 'web', 1543158022),
(175, 1, '507308df1a87ef4e7bea2d9658fc5a8cead0035550631d2bb2cbbc9b607091d552c367b930426376a57ecd54d4df7d999bd9c5e3b973ec75', 'web', 1543158022),
(176, 9, 'cd1e3183ff200b64ce619d529e3513a5971fb9a8b06771e9b94ebd1bfe000327d1016aa47369813141f860e3b7f548abc1f8b812059137bf', 'web', 1540538162),
(177, 1, 'cc191c09204993d692a280a42174dbd836fb89253704265c4304a2f6114b8379f811b1aa69614326c28f6ae146390a9c1b923233ef556c66', 'web', 1543158022),
(178, 1, '4b635e979ad51f9f033d5e88ac85006b0fe911551b7a825ae25b7473ef06ab82d265f25993433721a6640ad0aca7033809ffa7165c3040f9', 'web', 1543158022),
(179, 1, 'b99d1460c55190bd658bdad38b7cb64f75e256e601bd16826ca08ce1fc31628d101e505e13267420e3a54649aeec04cf1c13907bc6c5c8aa', 'web', 1543158022),
(180, 1, 'b09963a725447f35ce70acfdc0a843bae58014b4420645d7ad6425a6200f3df2b5d5d08a3345724603db60c2331018b18c4166c1787072fe', 'web', 1543158022),
(181, 1, 'bf1e6876780ece46b39fa7b2d5a6553f4219c7ba4fbfdffc5268b512e402d124ef1ee88f286715414efa8b668ee1198289bb15965d9705b6', 'web', 1543158022),
(182, 1, '0f376e6d549e8db1393e8bca3dd7ff5e84ea0249acad58f293cc8e1d0c431c6c547e1a4186225714ebe922af8d4560c73368a88eeac07d16', 'web', 1543158022),
(183, 1, '2747c1b375924aa81fd73adce954e101aaf1af341d2356344f9a141b10ccc9d540a1fc32648823440a4dc6dae338c9cb08947c07581f77a2', 'web', 1543158022),
(184, 1, 'a64b808371a7028b407896a0604ebe642e9bd914935dd651a3b8425c384e30c9e5af1bb347746661eca336bc46296c1aced239fbfb803b5c', 'web', 1543158022),
(185, 1, '0476bc1ff1065d8be96fd1aaf75c0fe09effcdaea95f93e3d9b28accc487ecc15924b869475898773b199f42a9909061516b6ce6d334af6d', 'web', 1543158022),
(186, 1, 'c6809e50448e8a13bdefd09c38706bcc5e74a77b86b10457094f0ef63e2450d49a74c82546793102f6b6d2a114a9644419dc8d2315f22401', 'web', 1543158022),
(187, 1, 'e05154b4b1c31bcadf7fad67e31fc9dcfb2d58ea5773dda68521ea0bd2c11acc0d6eb980853135598ef99bfe02f6d9e5c920cfebe29ee9fb', 'web', 1543158022),
(188, 1, 'bdb7d06fdc3adc701997a66abe6394f94a12ca03d057c5a72a48efb2e5013ffeb34f08e0911578716dcb94fb55921f2416219b454651bffc', 'web', 1543158022),
(189, 1, 'c918adfa89560f9d20016fbd387fd2720a88785832c7b14c11f3e914c3c87e3c2ff978386399157233d3b157ddc0896addfb22fa2a519097', 'web', 1543158022),
(190, 1, '3e875177bade6571df7f174eaae922f9c0cfed4cf458d9cf88b3fb8d4e1446764440348e41476182c4fac8fb3c9e17a2f4553a001f631975', 'web', 1543158022),
(191, 1, 'efce9a806fa60971bb5759210b15b79e3ffe2a5df7a30403fc34aee791816db618175e8440295995b1c1c47f20cf1d3253555b8cf83949c0', 'web', 1543158022),
(192, 1, 'cfcce49cb7190207b5eb2a98036b71c3b6bc3cf5abd6446a4b686309229edf8b3bdc0206588831543ebd728de6fa78aa8bc932e9abece9c0', 'web', 1543158022),
(193, 1, '816141feaaa1643b9ca577c86869ae68e2d72b40fd6ce1a699f8b1411d08c012315bb12d2206210421be992eb8016e541a15953eee90760e', 'web', 1543158022),
(196, 1, '21f9dec39c847c2cdc0126a870d8b9c96c6d1d902e47042206dcc13653efef8481a3545945775178007202387c4274c570d9cb72943fc873', 'web', 1543158022),
(199, 9, '78613d3c488bc8a9a288130f206c4a94d0b5c9e9387e6d602fa7aec997fb6db725cf76f61620518229000b029c61328a948b1c7afa01cea3', 'web', 1540538162),
(200, 9, 'c049b0375f3b1b00f95f6f2978896cd65954a2536fecb1baf206153e546444cfb231cfa55493558603c874ab55baa3c1f835d108415fac44', 'web', 1540538162),
(201, 1, 'f89c93db5b059bb3ac5c84df7d7014684afe56fb1074706eb95df47fb101598b4eaedac150697845d4a93297083a23cc099f7bd6a8621131', 'web', 1543158022),
(205, 9, 'b6d3d74b3a6309b6ddc26f7df68678fde0089ed833d75a3adaf43eebac76db3eab58600f13903866fd4771e85e1f916f239624486bff502d', 'web', 1540538162),
(206, 37, '1dc31fa1ab20a11452413e66bfc238101a6a12a6a39a2bea2348c10718856bbddbdec6cb67994703bc047286b224b7bfa73d4cb02de1238d', 'web', 1540822232),
(207, 9, '0a70ba2a7a73fcca03ff79534318875df253fe487b2553ec5b720aa43a38d8f62f88bb3364001870d958e1f17d03638bba20ad39b2fb989e', 'web', 1540538162),
(216, 1, '0099a947425028f83f65382ca5698de1ab1151bae24966f9d9429acf7b7803b55b96bc07298793734f1927fff6e092185d6a6f4c8466e421', 'web', 1543158022),
(219, 37, 'ff9de9ec7c671ac31ed16da9593b212308fb085240ac280483135c63038be6b471e48ce158335238a13e00b0854808128933f99f4955f338', 'web', 1540822232),
(222, 37, 'b9edebb14889316b614c5ea8ea86879a725393b817a7e75e0bef5c1535eea1ab406cfe1423452025ce11641e056f7b59aef8e9a42eaeb65b', 'web', 1540822232),
(223, 1, 'c7b769acfdc34b6c56cc7f0cc56966cbd7106af4e338134c8422e78fa8118cd32351816521676881c4bf1e24f3e6f92ca9dfd9a7a1a1049c', 'web', 1543158022),
(225, 38, '0a880c82dc72cf8dfc3b93b6fc56d0152f107846f33baae64ad10baef9c6bf8e9fcfce3487239135bf65417dcecc7f2b0006e1f5793b7143', 'web', 1539653389),
(226, 38, '498fee0046cf507ba19448385c13a02773e53d720dfe78e5a754a81045609f4e0dc61b5b46614857802a5fd4efb36391dfa8f1991fd0f849', 'web', 1539653389),
(227, 38, '70dfbcc6578c1a15f042a057b2fced0acaf550dd09565caf7a46119113ac2274e8ab38c447122904dfea0768cc6ba51dd20c7224016b0bd7', 'web', 1539653389),
(228, 37, 'b8a6639d99ee0eed106cbac990fa8f63f8a670f5a14b6f3904a928fce74dbbed91502fbe396466095905aa3361a00b7d9356fa6cf222396d', 'web', 1540822232),
(229, 38, 'b0f3c33ee0cf2b7b377a1441c6feff21feeb54510eae598b80ceb393918c30e81ff68e8c491448132118d8a1b7004ed5baf5347a4f99f502', 'web', 1539653389),
(230, 38, '4428c4031fe7a4b2c35ab3f819561063c6eb993055719bd46773994fdd9ecc4226f73a4557434679135593dd9bc3d98e8d8e71d788c9dda6', 'web', 1539653389),
(231, 37, 'efaf828dd99a42b11feb920c50d1be174d5f6480395d3ef79c4f0c5a3753eb8e8a8b014918388373e6051b3bfe716cc4a38c2f39ec199873', 'web', 1540822232),
(232, 37, 'a3fc5fab3eb8931bc1d18c8bfeaa37a19933ceabc10da319bf64c56e1a3ab4eea18d5a8794512957a33f5792b2a9a51ddd0111b3ac6e0e76', 'web', 1540822232),
(233, 37, '3aca9346b6d8a5f889374c509d55007dc45c88ef77fe3b97f28ddcd517fb3c640ed9cd0421154016141aa4fef48df77f954d60a373a3c322', 'web', 1540822232),
(234, 9, 'bbe41c2af3d665289d78701765bed6426df8eb5a74b1b7ea34202b79fc5f24a8099fa9a853352222e13748298cfb23c19fdfd134a2221e7b', 'web', 1540538162),
(238, 9, 'b88d8836bba68344b1b78ca16cf126d9f0c97fbd322cb5a9d5c15f96e0ef761b9e76f6d246683672deee9fe5195586bf4545518a76aff2f7', 'web', 1540538162),
(239, 1, '6c655e4bb06269eb49b0306676e34476d9427d16f0abd12863c27bfb6bd03a27a730b96253899629cfa45151ccad6bf11ea146ed563f2119', 'web', 1543158022),
(240, 9, '7ec7f52512ab1d7364767d8c0e0d628c20d8fcd76fd39ec654018e372e069843446089341239985645ab12afa05e563bb484781693dffc87', 'web', 1540538162),
(243, 9, '0146b7dda9085c3e73aee35fed54f1219decdb72ec0115aa3444acb7cacb85dad2ec2fb583374923b7b58836dc941cc4ba33d16dab6e3059', 'web', 1540538162),
(254, 9, 'a944faddea0773342f6ca78540d2cd4107b64c9cd21cad678320c0110195bb084ea48dee672548231906f3350e1fa43adced642ff351c943', 'web', 1540538162),
(255, 9, 'f71c10a15c31702b9ef74411b8257fe2bcaa576faa9c110f67759f0b756dd862e31122a392845770b180da1593ce9ff93d453eccc44021ad', 'web', 1540538162),
(259, 38, '26006e5d107c3c093488466516df7ae73eaa9bbf132a1a84f1c58928cc981908fce69d12945007565e9fb672ee46be628a141b594d7c6f3e', 'web', 1539653389),
(273, 46, '96674899f2028e0aeb22495fcd1673b8b33da9318d6ab1fcdf6acaf997bad8bb2d9e6cb09942611303492e99e42e7ea8480cdfb4899604f5', 'web', 1539731050),
(274, 46, '5f211921be43cd9c4818f1b721b11a78cadb2f9f73ea44591112d7b489728c65b168a1422485415123937b42f9273974570fb5a56a6652ee', 'web', 1539731050),
(288, 47, '1936edc4f4970132a93ba4de794b43df6325ab85ccdfc03a108592a028699b0fc58436065289835112fb63ba1566cb03484e1e5e290a73f4', 'web', 1539877659),
(289, 54, '4b6c84909b9bef3aa45825334aa9b29d4976fcdbf34e745ea7046d99e7aedcf97533bfd615526616565e8a413d0562de9ee4378402d2b481', 'web', 1539854645),
(290, 9, 'eb1f9db507446d6f8075cdb1fc6e8c19ab8704ad815e8ca69dfa9c660e902fdb27b7a40795048328274e6fcf4a583de4a81c6376f17673e7', 'web', 1540538162),
(291, 42, 'e23ccd6b2531a8803a968129c5326f0583dbdd76204d3af3b2bb753aa53486cc66972b9728094296e9287a53b94620249766921107fe70a3', 'web', 1539833496),
(293, 54, 'e432c569a5244d3e61d97d30e1134525e0d796759d846c9d2c42d5ff4960bc03bbe56ebd26273155f3957fa3bea9138b3f54f0e18975a30c', 'web', 1539854645),
(297, 1, '27adb2f018f73138c1a14505b9e86e05d09ea1df2a4ee17fab4e2bda55af4b34bfe98d2f43148280fc2dc7d20994a777cfd5e6de734fe254', 'web', 1543158022),
(298, 9, 'f4e6f892ecb77f1fcf942025c51a92add8b7577ab7c5844d4b099e915586288428ae65ac5797797173f124c77df247a60e1963b8ab5940da', 'web', 1540538162),
(300, 1, 'd28bbd4f83eddd4a24a1e66aa8826567e39d7356bb396da6034fa3be2e455efa7725be6419574685c494d9524143b2ebe567475e985c19f7', 'web', 1543158022),
(303, 1, 'a4df67ae6c14bd09ac8042397a0466e5d518727cc9bb9ce4136841d991d2211193599c02743095557c21c080c204c2ec7523ae6fc12033a6', 'web', 1543158022),
(304, 1, '03d2d40e43844716ec12f61c6ff37893ecb873cdc30d0359fc3d02b76eaddfd03ef0a51126525763654516d1b4df6917094de807156adc14', 'web', 1543158022),
(314, 37, '0fcf1b51e48b8d3b0e498745c915051e6f5986880ccfd33049bf3d3998613f2e128ce28a46226525ab6b331e94c28169d15cca0cb3bbc73e', 'web', 1540822232),
(316, 1, '3cd373cd3723b268f7c71290d885f344c510ccdb5ae138f9e8a84b3b0d391ed840ce470f86487229b367e525a7e574817c19ad24b7b35607', 'web', 1543158022),
(317, 1, '457018a3424e4fa09c9b76c674ad73ff250aad7879301c2e7ffc3f1eb72950c0bbed14d39137281912e1b435e5e53888e787d22c1e8d262d', 'web', 1543158022),
(321, 80, '6ab09ceb75e90a14d3cb975258652f1be1c5d7f66f036d6a73dcef628360f1f3494d0c8636297645cc9b3c69b56df284846bf2432f1cba90', 'web', 1541408996),
(324, 80, '9c0999990e527b99d165f7714f2cd38b72de6d0605e426748c30e19d8570a9e96e4e3d8751695724c1619d2ad66f7629c12c87fe21d32a58', 'web', 1541408996),
(334, 82, 'c059b120e748b77065e320b23f50b25025cffcbcf0262ec6f037222e84356949c8306d7715878514e046ede63264b10130007afca077877f', 'web', 1540932967),
(336, 83, '053af521ca47a4ef6649c45533ecdb34e6257c493020984c3707192021403065ae9e23f9156287905513c36e7c334dd20ab0ffeac130dca8', 'web', 1540936891),
(339, 83, '614ce62d9b9ac0e90a27cad1b725848093320299bc8cffe0c164d1bde90608524e967a5832443480dfbbdd2f1838330476c63189fe6bf2ac', 'web', 1540936891),
(342, 83, 'a0bcd06bc26810863239c32a30efe21351d36dfe23f57543dce32322b0ea62759fdb967c869211046925f2a16026e36e4fc112f82dd79406', 'web', 1540936891),
(343, 83, 'b9492a42331189107caf0103d18f3d4686e5e6948e2e0d4f1a89b5ce8d9317c14d6ca23e9307494695cc848bdbf89f5187fbfa8aa2ba1a5e', 'web', 1540936891),
(348, 86, 'baff07b9aa6ab30b36b9f63d0d22759d3e3346df349d9cf520f9f16a80c30dd6a5468ea1625074060396df57e78b6d04b6854dd682e27b3c', 'web', 1540963970),
(355, 80, 'ba77596f094a9cdc259957b903b28923b6afba958c9d20525df3de2264ec5c53b92161b0171952541ca5c750a30312d1919ae6a4d636dcc4', 'web', 1541408996),
(358, 1, '1349838588434fd8d3236054dd6339779e5898967036c695360bf25bc96c3ae796180f75368623884f0bf7b7b1aca9ad15317a0b4efdca14', 'web', 1543158022),
(359, 80, 'a998b19ced8f93abf445463b27904f7fff6b9746b4e19693bae52953ec050c4e62f6a7ba230968437acba01022004f2ce03bf56ca56ec6f4', 'web', 1541408996),
(361, 80, '83abacfc4ea32a99cfa3499f05de6661c01d7b10de1aab75bf95010cbab8d19793f874bb801447749ecff5455677b38d19f49ce658ef0608', 'web', 1541408996),
(365, 80, '7b7998439343aea2bf2e36106456b2e9a541e8b65cf39038837a2b7dd340d83665b4cdc820313241a91bc76c2a6302e573badedcbf57bf7a', 'web', 1541408996),
(367, 80, 'e59d69d4d10275551ef94d868ad41d6ba6b4047688e036cd8dc0f3541554454d1aaffef362226064968c9b4f09cbb7d7925f38aea3484111', 'web', 1541408996),
(368, 1, '4b16f6dbd470aed6c0185ff04b8341d0200126b3fe4bef73e9ea48b5d78aa6450372b80e33314231a378383b89e6719e15cd1aa45478627c', 'web', 1543158022),
(369, 80, '88f7c5757c5cc6e9602883965125f5851938aff128b98b6d8f092190bf4d9b2fe6c1da6964033917fb6c4e0b4b90ebfb5a35ca7a9cbf1d16', 'web', 1541408996),
(371, 1, 'c01cbd2ba677b79f171dfd191e0295e2a13057f86f36f799363e85f119f775ddbba54d2e833160766e69ebbfad976d4637bb4b39de261bf7', 'web', 1543158022),
(372, 80, 'fa19df2ab7647dc89968d01d1331a067f0f5ad94dc5f7238b28de4c9d22d54f86acffc8d692586089cd3598a91516950427c605ae29cff68', 'web', 1541408996),
(373, 80, '50e0637056c7948b7cc81c91ff8346576c2883b7dfc17f628bc4402ba252b180ccf92b3e7269354221be992eb8016e541a15953eee90760e', 'web', 1541408996),
(379, 80, '1dda34f6a063f8721ab170c9c8577da567db13f147c8f7cc6f402272b9399061d55556f86039663101ce84968c6969bdd5d51c5eeaa3946a', 'web', 1541408996),
(383, 1, '2f6d81f34c5027983bf65868babd151d19d5b65d8311a804870872b86b181a99065e0d1e55632513c1285fcadc52c0d3dc8813fc2c2e2b2a', 'web', 1543158022),
(385, 1, '7e958229a387f40d4bd4c9169666786c102b055ff27d22b6cae25502fbd33184af55f40d453458786a0724f1d3e80fd5f761cacb7efe8593', 'web', 1543158022),
(394, 89, 'ba2a12dc18ce5b84d091c991cf0a376c0c613330e4aa37d42931643fbef62e71f51c55cc691671838bd2c3f7fd5b165b470beb1cc83071ab', 'phone', 1541507160),
(396, 1, '96e4aa3754c3c874832033d6d53092b83a7d36df6747d4ccf3b90e57c622fe4a0f43e1c57062756605e97c207235d63ceb1db43c60db7bbb', 'web', 1543158022),
(401, 92, '075d95b32b81d94bc084b516bb4f3deefa999fe87a006979803c17d4eb06b7fe27638ca8938429434efa8b668ee1198289bb15965d9705b6', 'web', 1541633143),
(403, 92, '6db1b88d49c92c1091f787b5539dec8a82d29d8f2d83037cb01c9f93bb8fb65040c115ff27878102d79c8788088c2193f0244d8f1f36d2db', 'web', 1541633143),
(409, 1, 'cab89c0844258f28a973e8d54e45c43f6dbec405b8955a0a783a77132f5efa25d5fa073e98845130cca8f108b55ec9e39d7885e24f7da0af', 'web', 1543158022),
(417, 85, 'c96e887d90e39682461c9c038aa0ab25dcd81b52a5ea77719dfd166931eb8a7b3da83265704939337edcfb2d8f6a659ef4cd1e6c9b6d7079', 'web', 1543157033),
(422, 1, '6b181c9dccccba13b200876e2b36748b890a82196e11c680e2c130a2840763eb8ca3b61819106127958ad0d05d3259750be0b041d10adbb1', 'web', 1543158022),
(440, 85, 'b70b547185232b3670455f983f8ba6d929f7bef37978a91390a43ba79743c2d6e5d28c306650439264697505ab8add3aa07f761321d06014', 'web', 1543157033),
(445, 1, '916781833007e16e70a0e6222ba7556774965c1c886fec3ec914e59a576adad169965ebb70129112ffe10334251de1dc98339d99ae4743ba', 'web', 1543158022),
(447, 96, 'c6c1a9add161eabadea478fed3662b13612ab7383497ad68580c33ead85762e5b66994383697887807bba581a2dd8d098a3be0f683560643', 'web', 1542057325),
(448, 96, 'eaea622336aaf9a0aa98a1bae87b21ea8d665a2fec7e055eec98c45ba726909d624200951327469213403518ef9c1ce843a289d991f79bf5', 'web', 1542057325),
(449, 96, '096a3c4a2805ff3a80d2dc078d96335c6e6ec37e415f6e30e0a1f81ca74a57b61435084212777747c75e10ef9ef6d295d4ceba8335d93bdd', 'web', 1542057325),
(450, 96, 'c910683ecf5abbc7d71d22a8cee7248c963131e8bb30088027f20ebb05331f6fbde3f54163013457d958628e70134d9e1e17499a9d815a71', 'web', 1542057325),
(453, 1, '87d0a387560ac8ed0ecebd11b692e4b8e035f621a4925673b51de963bff0ec099819ad7a85751116872694b2ab50601615cbad2bc50d98d6', 'web', 1543158022),
(460, 99, '37e7191dda131ba54617a765ce3dc9dd34cc97e36c127ba43a2c7f2e29f263b57ade6eb639582010653cd6f9efefe6d273e2c116d2a6b765', 'web', 1542086941),
(461, 100, '5242bc2002d7ffc97908b50bde52dc1bd7cf37608b485681e2c02ecd9bcef84de02a5406162320885cdf0f9533d6b4c0984fc5ae00913459', 'web', 1542087281),
(465, 98, '8f48726fccf5f50ae3d5d51a04ef57b56d83ee949d47a7fa2362edaf9cc414d625ed0f2283178094167ccbe15cc1664c9a63c20ac4c6a55a', 'web', 1543161267),
(467, 98, '372c60f315b06ca847c5100557a448386ba278d61c0026369075c043c63c52c910ed290266435442d47bf0af618a3523a226ed7cada85ce3', 'web', 1543161267),
(468, 1, 'c12f2ddc8f163993b7ce62eeda12966671f42f952a639ccd06548e6aca9d30719f586fd7824981855e34a2b4c23f4de585fb09a7f546f527', 'web', 1543158022),
(469, 98, 'bcf6e8199575101f4b1745c1870e4788bbe48918f1a6dc7203d6f4e7d39119c4dfeb4eeb7822315136e51f22c86d237a5bb2e3451f8a7072', 'web', 1543161267),
(470, 1, 'a86989c46b1a6138495c41164f8b96415b1f7e3c12583cad56c78f50c8db32eae1544fd861258743a3de03cb426b5e36f5c7167b21395323', 'web', 1543158022),
(471, 101, 'cfa5fadd0967b44cfe0469b8d3d3e8d1a47d544860d7741ca266091b5ba5075a2f9b1024782894764efb80f630ccecb2d3b9b2087b0f9c89', 'phone', 1542566906),
(472, 101, '3b316ac45039538e70e4110b4974013002390cc37319adb41d61573ecfbe41f92841becb518393834bb236de7787ceedafdff83bb8ea4710', 'phone', 1542568298),
(473, 101, '7032a3ebaec58b468d5846b0e75ab25ef99980a49aa9b3bb1e2bad24f58cf2921f89c05d9183240021186d7b1482412ab14f0332b8aee119', 'phone', 1542568302),
(474, 101, '0cacd9513eb17a39938cb4c1f3222c14506676d3309150d518fbffcba6d96c874a2189de70146790cdaeb1282d614772beb1e74c192bebda', 'phone', 1542568304),
(475, 101, 'f67553f4cb711604696560ae43d996e9d2a47e69fec32672e13d30a6ad670076f89c2fbc120462178830c97ab60254cd05628c6e61e8c54c', 'phone', 1542568310),
(476, 101, '130f43969bd9ea781f956490f75af9fb56813e90e503865817619060e3fb70472b47f3b084070137ba7609ee5789cc4dff171045a693a65f', 'phone', 1542568312),
(477, 101, '32c321697fc997b593b8cc6f7bf561bcc4f9a16c5f8f82c23c3a90df8bc982f8bec61b8822684813b154e7b21b2ff0a14d96affa6d3fb958', 'phone', 1542568416),
(478, 101, '2217acf6ad9f1844dcf029ae3df904c42a06db61250d72218a5ea8b138a69db93b69d2286810352067ba02d73c54f0b83c05507b7fb7267f', 'phone', 1542568422),
(490, 1, '63aac68f3403ba28b668f2179e6116db5a0a7e964e0a1bb57e1d7330abd55736a59f493e90841824f29e2360ef277f77595dfae0aab78138', 'web', 1543158022),
(494, 104, 'ff9f55d73a50c10c1862236962f3f24745c8f192e058d779133504e468cf4df73f3adca320106079bbc12a3a98d8487f58a87d3a3070516e', 'web', 1542825114),
(496, 105, 'e8d183ea963983d28ab5cf516d783509dfcc53fd1f1c70ecd7b8459215becdcba89cf95c468770037695ea769f021803c508817dd374bb27', 'web', 1542798587),
(498, 1, '5710f8d1881c538cd3ad154068bf362c68200f97492b8250826ec14e63697dfc70fa41d0127748960dfd8a39e2a5dd536c185e19a804a73b', 'web', 1543158022),
(499, 1, '3c701eb774d0b98188989570d1a39608b299c149d189200d01fea5e84f4f501b90870e869293681577ab78641fb740c101b96c74ca7c15f6', 'web', 1543158022),
(501, 1, '2a0f849ae5b668362963c666d278dec1e2ab3cef099d8b9e9935700096441abc2c073c4c23770294122e27d57ae8ecb37f3f1da67abb33cb', 'web', 1543158022),
(502, 1, 'dd8f7849b412d0a6214a7a125bae12ff0696d1274e2892bb5fdc1165fc6b87544dd2319a918628047f83c19d8adc72f08f8fde30a57eef79', 'web', 1543158022),
(503, 104, 'b5a47cd11af29f46ee2637b21cbdceee76864f0b4bd236bbd583f1f95c563128e6467e56164444282c2fb9efd4b8a1f837bf47004a49ce45', 'web', 1542825114),
(504, 1, '4c5e5304cc452e471853316eee3712ef68c42b04598c82042e2bc10c0f31594effea0c8c5740022189db09d856d45d361982edc10ce738a2', 'web', 1543158022),
(505, 1, '462715c122102d00856c7e54787c909f8151d6cd43152efd6d60690871bc4109a1d463c478704063349f8e8088df63050757dd8be4356216', 'web', 1543158022),
(506, 98, '9dceb4c5196190663325fdb1ceae12321f0d532fda3345caad594072ab7a1dbaf82dbc3346090815bb96ff7f5c9505fd971126ecd171bec2', 'web', 1543161267),
(507, 107, '131430c7d417a2c0911d4ff03ac9d7ea986417fc60369c48b1f153cfb742a70cb046781694804299b665bf733325cebbccee5935b1272616', 'web', 1542818859),
(508, 108, 'bbbb6cabe08b99518da0ea02df06f73a94bafbb62afb8ae2c3d8f48b5f23e96534a5afe1822563921dacb10f0623c67cb7dbb37587d8b38a', 'web', 1543169132),
(511, 1, '2f52f2707ea06be7e245ca520ff5827805b43a71e43035a466e74474aaf153d8b3f461859857335863ceea56ae1563b4477506246829b386', 'web', 1543158022),
(512, 108, 'f6281721a13c1c664c730a43e24c4d36c19e82d5dad83de2e2276d54a4748aa96df6f2268978903175c58d36157505a600e0695ed0b3a22d', 'web', 1543169132),
(513, 1, 'aacbb6c4a4c4a00667d4a9d3351749750f1f1e19e8028b8a63a1218c5dd2e919ba366db91211882151f15efdd170e6043fa02a74882f0470', 'web', 1543158022),
(514, 108, '59fc34c5ca100ca563ef545bc6f1c58451eb505b7cc635e16e5ae25f6071a2d1224c98f57208162394cb02feb750f20bad8a85dfe7e18d11', 'web', 1543169132),
(515, 108, '1a20bf29b5a1b0186b8c9e6cd521e4cfbace910d1e1b04ed47fa7644c467c524bf8ebbb088971914ccdf3864e2fa9089f9eca4fc7a48ea0a', 'web', 1543169132),
(516, 102, '69c7737293a7832c98f738d244a172609a53c4e9983a00e21e12482cd14b52b6f3bd9c3f394228616bb56208f672af0dd65451f869fedfd9', 'web', 1543098257),
(518, 108, '33bb8fdb3eb2d503dd16008925f96cea34d06379d9cf87296db1476579f50827eb91868849275239b937384a573b94c4d7cc6004c496f919', 'web', 1543169132),
(519, 108, 'e72631fe11524fcec64f9fbaf0c7e1b65ad7531403898f3a9552fe280bf8f408326ad76e1789551410b4945abe2e627db646b3c5226a4e50', 'web', 1543169132),
(522, 98, 'a0406aef6f2c46c5346f426dd89f90b48662fde8e24a7b0801ead59f0deecee6ea25e67519037048a4939072217b46203c4f5764cb4dac40', 'web', 1543161267),
(523, 108, '64b41f03a9af6ea8aae871121b060ebeae5177108870036946c7eafa8f8544e1abd0edd485986022624567140fecc40163fed3c45a959a7c', 'web', 1543169132),
(524, 108, 'ce2bf1b0aab4ed0b6d93d168aafffa3572cda7e3516ba09ba3bb7c73aaa4207092d891cc62229375aa677d660eefd1fe0d323c1dc9bfa869', 'web', 1543169132);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Apps_Hash`
--

CREATE TABLE `Wo_Apps_Hash` (
  `id` int(11) NOT NULL,
  `hash_id` varchar(200) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `active` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Apps_Permission`
--

CREATE TABLE `Wo_Apps_Permission` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_AudioCalls`
--

CREATE TABLE `Wo_AudioCalls` (
  `id` int(11) NOT NULL,
  `call_id` varchar(30) NOT NULL DEFAULT '0',
  `access_token` text DEFAULT NULL,
  `call_id_2` varchar(30) NOT NULL DEFAULT '',
  `access_token_2` text DEFAULT NULL,
  `from_id` int(11) NOT NULL DEFAULT 0,
  `to_id` int(11) NOT NULL DEFAULT 0,
  `room_name` varchar(50) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT 0,
  `called` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0,
  `declined` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Wo_AudioCalls`
--

INSERT INTO `Wo_AudioCalls` (`id`, `call_id`, `access_token`, `call_id_2`, `access_token_2`, `from_id`, `to_id`, `room_name`, `active`, `called`, `time`, `declined`) VALUES
(1, '0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJBZG1pbjF4LTE1Mzc4MjU3MTEiLCJpc3MiOiJBZG1pbjF4Iiwic3ViIjoiIiwiZXhwIjoxNTM3ODI5MzExLCJncmFudHMiOnsiaWRlbnRpdHkiOiI0YTkzYmFiYjMwZTIzMDciLCJ2aWRlbyI6eyJyb29tIjoiODVmN2ExMDdiZDY1OWViZThhYzIxMGRlODlhYjQ3MGQwYzczNzQzZSJ9fX0.JofyhAnc8Eqx982L9UNt6nyi-ub6CiS5Zk8x1A8UZ2I', '', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJBZG1pbjF4LTE1Mzc4MjU3MTEiLCJpc3MiOiJBZG1pbjF4Iiwic3ViIjoiIiwiZXhwIjoxNTM3ODI5MzExLCJncmFudHMiOnsiaWRlbnRpdHkiOiI1NmJhOWQxZTA2YTU1ZDQiLCJ2aWRlbyI6eyJyb29tIjoiODVmN2ExMDdiZDY1OWViZThhYzIxMGRlODlhYjQ3MGQwYzczNzQzZSJ9fX0.IYmHGGIQYvGoIhPjrBdkNAmUvf-fjmz-aZCZQnO6Z54', 1, 2, '85f7a107bd659ebe8ac210de89ab470d0c73743e', 1, 1, 1537825711, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Banned_Ip`
--

CREATE TABLE `Wo_Banned_Ip` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(32) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Blocks`
--

CREATE TABLE `Wo_Blocks` (
  `id` int(11) NOT NULL,
  `blocker` int(11) NOT NULL DEFAULT 0,
  `blocked` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Blog`
--

CREATE TABLE `Wo_Blog` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL DEFAULT 0,
  `title` varchar(120) NOT NULL DEFAULT '',
  `content` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `posted` varchar(300) DEFAULT '0',
  `category` int(255) DEFAULT 0,
  `thumbnail` varchar(100) DEFAULT 'upload/photos/d-blog.jpg',
  `view` int(11) DEFAULT 0,
  `shared` int(11) DEFAULT 0,
  `tags` varchar(300) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_BlogCommentReplies`
--

CREATE TABLE `Wo_BlogCommentReplies` (
  `id` int(11) NOT NULL,
  `comm_id` int(11) NOT NULL DEFAULT 0,
  `blog_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `text` text DEFAULT NULL,
  `likes` int(11) NOT NULL DEFAULT 0,
  `posted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_BlogComments`
--

CREATE TABLE `Wo_BlogComments` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likes` int(11) NOT NULL DEFAULT 0,
  `posted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_BlogMovieDisLikes`
--

CREATE TABLE `Wo_BlogMovieDisLikes` (
  `id` int(11) NOT NULL,
  `blog_comm_id` int(20) NOT NULL DEFAULT 0,
  `blog_commreply_id` int(20) NOT NULL DEFAULT 0,
  `movie_comm_id` int(20) NOT NULL DEFAULT 0,
  `movie_commreply_id` int(20) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `blog_id` int(50) NOT NULL DEFAULT 0,
  `movie_id` int(50) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_BlogMovieLikes`
--

CREATE TABLE `Wo_BlogMovieLikes` (
  `id` int(11) NOT NULL,
  `blog_comm_id` int(20) NOT NULL DEFAULT 0,
  `blog_commreply_id` int(20) NOT NULL DEFAULT 0,
  `movie_comm_id` int(20) NOT NULL DEFAULT 0,
  `movie_commreply_id` int(20) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `blog_id` int(50) NOT NULL DEFAULT 0,
  `movie_id` int(50) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Codes`
--

CREATE TABLE `Wo_Codes` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL DEFAULT '',
  `app_id` varchar(50) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_CommentLikes`
--

CREATE TABLE `Wo_CommentLikes` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `comment_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_CommentLikes`
--

INSERT INTO `Wo_CommentLikes` (`id`, `post_id`, `comment_id`, `user_id`) VALUES
(1, 33, 7, 1),
(13, 346, 387, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Comments`
--

CREATE TABLE `Wo_Comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `page_id` int(11) NOT NULL DEFAULT 0,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `record` varchar(255) NOT NULL DEFAULT '',
  `c_file` varchar(255) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Comments`
--

INSERT INTO `Wo_Comments` (`id`, `user_id`, `page_id`, `post_id`, `text`, `record`, `c_file`, `time`) VALUES
(130, 1, 0, 263, 'GEWRGWR', '', '', 1539749204),
(378, 1, 0, 287, 'Testando', '', '', 1540814277),
(383, 85, 0, 356, 'Teste', '', '', 1541781641),
(384, 85, 0, 356, 'Teste pelo celular', '', '', 1541781654),
(385, 85, 0, 360, 'Outro teste', '', '', 1541781771),
(386, 85, 0, 348, 'Teste', '', '', 1541781802),
(387, 85, 0, 346, 'Teste', '', '', 1541781815),
(388, 85, 0, 362, 'Porta', '', '', 1541781889),
(392, 104, 0, 354, 'Test', '', '', 1542781967),
(396, 104, 0, 394, '3rw4e', '', '', 1542808308),
(399, 1, 0, 408, 'ola', '', '', 1542941546),
(400, 1, 0, 408, 'boa noite', '', '', 1542941770),
(401, 1, 0, 408, 'boa noite', '', '', 1543021844),
(402, 1, 0, 408, 'como vai você?', '', '', 1543022198),
(403, 102, 0, 408, 'Opa', '', '', 1543022694),
(404, 1, 0, 408, 'ola', '', '', 1543098052);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_CommentWonders`
--

CREATE TABLE `Wo_CommentWonders` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `comment_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_CommentWonders`
--

INSERT INTO `Wo_CommentWonders` (`id`, `post_id`, `comment_id`, `user_id`) VALUES
(5, 360, 385, 85);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Comment_Replies`
--

CREATE TABLE `Wo_Comment_Replies` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `page_id` int(11) NOT NULL DEFAULT 0,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Comment_Replies_Likes`
--

CREATE TABLE `Wo_Comment_Replies_Likes` (
  `id` int(11) NOT NULL,
  `reply_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Comment_Replies_Wonders`
--

CREATE TABLE `Wo_Comment_Replies_Wonders` (
  `id` int(11) NOT NULL,
  `reply_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Config`
--

CREATE TABLE `Wo_Config` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `value` varchar(1000) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Config`
--

INSERT INTO `Wo_Config` (`id`, `name`, `value`) VALUES
(1, 'siteName', 'Redelive'),
(2, 'siteTitle', 'Redelive'),
(3, 'siteKeywords', 'nova rede social, por distancia'),
(4, 'siteDesc', 'Quanto tempo não faz algo novo? Vem para Redelive!'),
(5, 'siteEmail', 'contato@redelive.com'),
(6, 'defualtLang', 'portuguese'),
(7, 'emailValidation', '0'),
(8, 'emailNotification', '0'),
(9, 'fileSharing', '1'),
(10, 'seoLink', '1'),
(11, 'cacheSystem', '0'),
(12, 'chatSystem', '0'),
(13, 'useSeoFrindly', '1'),
(14, 'reCaptcha', '1'),
(15, 'reCaptchaKey', '6Ld6bXUUAAAAAByREWzGYYuTOL536uk6gKQau4Dh'),
(16, 'user_lastseen', '1'),
(17, 'age', '1'),
(18, 'deleteAccount', '1'),
(19, 'connectivitySystem', '0'),
(20, 'profileVisit', '0'),
(21, 'maxUpload', '256000000'),
(22, 'maxCharacters', '640'),
(23, 'message_seen', '1'),
(24, 'message_typing', '1'),
(25, 'google_map_api', 'AIzaSyCmJYukwuT6vYNG1YExqNmHwQGfh4GnGfk'),
(26, 'allowedExtenstion', 'jpg,png,jpeg,gif,mkv,docx,zip,rar,pdf,doc,mp3,mp4,flv,wav,txt,mov,avi,webm,wav,mpeg'),
(27, 'censored_words', 'comunismo'),
(28, 'googleAnalytics', ''),
(29, 'AllLogin', '0'),
(30, 'googleLogin', '0'),
(31, 'facebookLogin', '1'),
(32, 'twitterLogin', '0'),
(33, 'linkedinLogin', '0'),
(34, 'VkontakteLogin', '0'),
(35, 'facebookAppId', '293947901208912'),
(36, 'facebookAppKey', 'af2980aec0e69b483c1213b8504e88b2'),
(37, 'googleAppId', ''),
(38, 'googleAppKey', ''),
(39, 'twitterAppId', ''),
(40, 'twitterAppKey', ''),
(41, 'linkedinAppId', ''),
(42, 'linkedinAppKey', ''),
(43, 'VkontakteAppId', ''),
(44, 'VkontakteAppKey', ''),
(45, 'theme', 'wowonder'),
(46, 'second_post_button', 'dislike'),
(47, 'instagramAppId', 'd418d21de6ae4d12bddee97af4cdcc45'),
(48, 'instagramAppkey', '3f6bf3f78e2747cd86fc563ffb66da18'),
(49, 'instagramLogin', '0'),
(50, 'header_background', '#35065c'),
(51, 'header_hover_border', '#333333'),
(52, 'header_color', '#ffffff'),
(53, 'body_background', '#e8eaed'),
(54, 'btn_color', '#ffffff'),
(55, 'btn_background_color', '#1f89f6'),
(56, 'btn_hover_color', '#ffffff'),
(57, 'btn_hover_background_color', '#1f89f6'),
(58, 'setting_header_color', '#ffffff'),
(59, 'setting_header_background', '#a84849'),
(60, 'setting_active_sidebar_color', '#ffffff'),
(61, 'setting_active_sidebar_background', '#a84849'),
(62, 'setting_sidebar_background', '#ffffff'),
(63, 'setting_sidebar_color', '#444444'),
(64, 'logo_extension', 'png'),
(65, 'online_sidebar', '1'),
(66, 'background_extension', 'png'),
(67, 'profile_privacy', '1'),
(68, 'video_upload', '1'),
(69, 'audio_upload', '1'),
(70, 'smtp_or_mail', 'mail'),
(71, 'smtp_username', 'e-mail@redelive.com'),
(72, 'smtp_host', 'smtp.gmail.com'),
(73, 'smtp_password', '10101150Lu'),
(74, 'smtp_port', '587'),
(75, 'smtp_encryption', 'ssl'),
(76, 'sms_or_email', 'mail'),
(77, 'sms_username', 'redelive'),
(78, 'sms_password', '@f#atex3d8f6427z1'),
(79, 'sms_phone_number', ''),
(80, 'is_ok', '1'),
(81, 'pro', '1'),
(82, 'paypal_id', 'AfWn1IDZtEWLUcY2ZI_oeB7Scb3ZzBRjPaAreVlahacAJbfPH0uXFvdoum6adgcINRdCBrQziccHZQWl'),
(83, 'paypal_secret', 'ENNraD_7sHtFvc-9yMxydozLonvdOJ2dfcNhDlO3-1D4_mHR95_-aIFwMw9wOT9MSxx6iZpdgoacscuo'),
(84, 'paypal_mode', 'sandbox'),
(85, 'weekly_price', '5'),
(86, 'monthly_price', '50'),
(87, 'yearly_price', '500'),
(88, 'lifetime_price', '5000'),
(89, 'post_limit', '15'),
(90, 'user_limit', '10'),
(91, 'css_upload', '0'),
(92, 'smooth_loading', '1'),
(93, 'header_search_color', 'rgba(255,255,255,0)'),
(94, 'header_button_shadow', 'rgba(16,0,27,0)'),
(95, 'currency', 'BRL'),
(97, 'games', '0'),
(98, 'last_backup', '00-00-0000'),
(99, 'pages', '0'),
(100, 'groups', '0'),
(101, 'order_posts_by', '1'),
(102, 'btn_disabled', '#dedede'),
(103, 'developers_page', '0'),
(104, 'user_registration', '1'),
(105, 'maintenance_mode', '0'),
(106, 'video_chat', '0'),
(107, 'video_accountSid', ''),
(108, 'video_apiKeySid', 'Admin1x'),
(109, 'video_apiKeySecret', '@f#atex3d8f6427z1'),
(110, 'video_configurationProfileSid', ''),
(111, 'eapi', ''),
(112, 'favicon_extension', 'png'),
(113, 'monthly_boosts', '1'),
(114, 'yearly_boosts', '12'),
(115, 'lifetime_boosts', '30'),
(116, 'chat_outgoing_background', '#fff9f9'),
(117, 'windows_app_version', '1.0'),
(118, 'widnows_app_api_id', 'c7a82230bee420f97a9574692e1b4d49'),
(119, 'widnows_app_api_key', '5b151a5603b81711c48a009b61fafefdc04fecc6-b2a1fa92ffbb9d634943793ac4e168aa-28376429'),
(120, 'stripe_id', ''),
(121, 'stripe_secret', ''),
(122, 'credit_card', 'no'),
(123, 'bitcoin', 'no'),
(124, 'm_withdrawal', '100'),
(125, 'amount_ref', '1.00'),
(126, 'affiliate_type', '1'),
(127, 'affiliate_system', '0'),
(128, 'classified', '0'),
(129, 'amazone_s3', '0'),
(130, 'bucket_name', ''),
(131, 'amazone_s3_key', ''),
(132, 'amazone_s3_s_key', ''),
(133, 'region', 'us-east-1'),
(134, 'alipay', 'no'),
(135, 'is_utf8', '1'),
(136, 'sms_t_phone_number', ''),
(137, 'audio_chat', '0'),
(138, 'sms_twilio_username', ''),
(139, 'sms_twilio_password', ''),
(140, 'sms_provider', 'twilio'),
(141, 'footer_background', ''),
(142, 'footer_background_2', ''),
(143, 'footer_text_color', ''),
(144, 'classified_currency', 'USD'),
(145, 'classified_currency_s', '$'),
(146, 'mime_types', 'text/plain,video/mp4,video/mov,video/mpeg,video/flv,video/avi,video/webm,audio/wav,audio/mpeg,video/quicktime,audio/mp3,image/png,image/jpeg,image/gif,application/pdf,application/msword,application/zip,application/x-rar-compressed,text/pdf,application/x-pointplus,text/css'),
(147, 'footer_background_n', ''),
(148, 'blogs', '1'),
(149, 'can_blogs', '0'),
(150, 'push', '1'),
(151, 'push_id', '566a2ec0-e80a-43e5-89f5-862dae2ff3dc'),
(152, 'push_key', 'NTY5NTBkYTYtNTE4Zi00YTYzLTg4NzYtMGU1NWZhZmY5Y2Jk'),
(153, 'events', '0'),
(154, 'forum', '0'),
(155, 'last_update', '1529921437'),
(156, 'movies', '0'),
(157, 'yandex_translation_api', 'trnsl.1.1.20170601T212421Z.5834b565b8d47a18.2620528213fbf6ee3335f750659fc342e0ea7923'),
(158, 'update_db_15', '1503149537'),
(159, 'ad_v_price', '0.05'),
(160, 'ad_c_price', '0.10'),
(161, 'emo_cdn', 'https://cdnjs.cloudflare.com/ajax/libs/emojione/2.1.4/assets/png/'),
(162, 'user_ads', '1'),
(163, 'user_status', '0'),
(164, 'date_style', 'd/m/y'),
(165, 'stickers', '0'),
(166, 'giphy_api', '420d477a542b4287b2bf91ac134ae041'),
(167, 'find_friends', '1'),
(168, 'update_db_152', '1504450479'),
(169, 'push_notifications', '1'),
(170, 'push_messages', '1'),
(171, 'update_db_153', 'updated'),
(172, 'ads_currency', 'BRL'),
(173, 'web_push', '1'),
(174, 'playtube_url', 'https://youtube.com'),
(175, 'connectivitySystemLimit', '100000000'),
(176, 'video_ad_skip', '6'),
(177, 'update_user_profile', '120'),
(178, 'cache_sidebar', '0'),
(179, 'push_id_2', '566a2ec0-e80a-43e5-89f5-862dae2ff3dc'),
(180, 'push_key_2', 'NTY5NTBkYTYtNTE4Zi00YTYzLTg4NzYtMGU1NWZhZmY5Y2Jk'),
(181, 'ftp_host', ''),
(182, 'ftp_port', '21'),
(183, 'ftp_username', ''),
(184, 'ftp_password', ''),
(185, 'ftp_upload', '0'),
(186, 'ftp_endpoint', ''),
(187, 'ftp_path', './');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_CustomPages`
--

CREATE TABLE `Wo_CustomPages` (
  `id` int(11) NOT NULL,
  `page_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `page_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `page_content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Egoing`
--

CREATE TABLE `Wo_Egoing` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Einterested`
--

CREATE TABLE `Wo_Einterested` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Einvited`
--

CREATE TABLE `Wo_Einvited` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `inviter_id` int(11) NOT NULL,
  `invited_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Emails`
--

CREATE TABLE `Wo_Emails` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `email_to` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subject` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Events`
--

CREATE TABLE `Wo_Events` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL DEFAULT '',
  `location` varchar(300) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date NOT NULL,
  `end_time` time NOT NULL,
  `poster_id` int(11) NOT NULL,
  `cover` varchar(500) NOT NULL DEFAULT 'upload/photos/d-cover.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Family`
--

CREATE TABLE `Wo_Family` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `member` int(11) NOT NULL DEFAULT 0,
  `active` enum('0','1') NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `requesting` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Followers`
--

CREATE TABLE `Wo_Followers` (
  `id` int(11) NOT NULL,
  `following_id` int(11) NOT NULL DEFAULT 0,
  `follower_id` int(11) NOT NULL DEFAULT 0,
  `is_typing` int(11) NOT NULL DEFAULT 0,
  `active` int(255) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Followers`
--

INSERT INTO `Wo_Followers` (`id`, `following_id`, `follower_id`, `is_typing`, `active`) VALUES
(35, 1, 33, 0, 1),
(40, 1, 36, 0, 1),
(43, 36, 37, 0, 1),
(44, 35, 37, 0, 1),
(45, 1, 37, 0, 1),
(46, 33, 37, 0, 1),
(47, 34, 37, 0, 1),
(49, 38, 37, 0, 1),
(52, 1, 42, 0, 1),
(54, 33, 1, 0, 1),
(66, 1, 82, 0, 1),
(68, 82, 85, 0, 1),
(76, 1, 85, 0, 1),
(80, 1, 98, 0, 1),
(84, 1, 102, 0, 1),
(85, 1, 103, 0, 1),
(93, 103, 107, 0, 1),
(94, 105, 107, 0, 1),
(95, 98, 107, 0, 1),
(96, 1, 107, 0, 1),
(97, 105, 108, 0, 1),
(98, 1, 108, 0, 1),
(99, 98, 108, 0, 1),
(100, 103, 108, 0, 1),
(102, 104, 1, 0, 1),
(103, 85, 1, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Forums`
--

CREATE TABLE `Wo_Forums` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(300) NOT NULL DEFAULT '',
  `sections` int(11) NOT NULL DEFAULT 0,
  `posts` int(11) NOT NULL DEFAULT 0,
  `last_post` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_ForumThreadReplies`
--

CREATE TABLE `Wo_ForumThreadReplies` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL DEFAULT 0,
  `forum_id` int(11) NOT NULL DEFAULT 0,
  `poster_id` int(11) NOT NULL DEFAULT 0,
  `post_subject` varchar(300) NOT NULL DEFAULT '',
  `post_text` text NOT NULL,
  `post_quoted` int(11) NOT NULL DEFAULT 0,
  `posted_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Forum_Sections`
--

CREATE TABLE `Wo_Forum_Sections` (
  `id` int(11) NOT NULL,
  `section_name` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(300) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Forum_Threads`
--

CREATE TABLE `Wo_Forum_Threads` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL DEFAULT 0,
  `views` int(11) NOT NULL DEFAULT 0,
  `headline` varchar(300) NOT NULL DEFAULT '',
  `post` text NOT NULL,
  `posted` varchar(20) NOT NULL DEFAULT '0',
  `last_post` int(11) NOT NULL DEFAULT 0,
  `forum` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Games`
--

CREATE TABLE `Wo_Games` (
  `id` int(11) NOT NULL,
  `game_name` varchar(50) NOT NULL,
  `game_avatar` varchar(100) NOT NULL,
  `game_link` varchar(100) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Games_Players`
--

CREATE TABLE `Wo_Games_Players` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `game_id` int(11) NOT NULL DEFAULT 0,
  `last_play` int(11) NOT NULL DEFAULT 0,
  `active` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_GroupAdmins`
--

CREATE TABLE `Wo_GroupAdmins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_GroupChat`
--

CREATE TABLE `Wo_GroupChat` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_name` varchar(20) NOT NULL DEFAULT '',
  `avatar` varchar(3000) NOT NULL DEFAULT 'upload/photos/d-group.jpg',
  `time` varchar(30) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_GroupChat`
--

INSERT INTO `Wo_GroupChat` (`group_id`, `user_id`, `group_name`, `avatar`, `time`) VALUES
(1, 1, 'rede', 'upload/photos/d-group.jpg', '1539750408');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_GroupChatUsers`
--

CREATE TABLE `Wo_GroupChatUsers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `active` enum('1','0') NOT NULL DEFAULT '1',
  `last_seen` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_GroupChatUsers`
--

INSERT INTO `Wo_GroupChatUsers` (`id`, `user_id`, `group_id`, `active`, `last_seen`) VALUES
(1, 33, 1, '1', '0'),
(2, 1, 1, '1', '1542034606');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Groups`
--

CREATE TABLE `Wo_Groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `group_name` varchar(32) NOT NULL DEFAULT '',
  `group_title` varchar(40) NOT NULL DEFAULT '',
  `avatar` varchar(120) NOT NULL DEFAULT 'upload/photos/d-group.jpg ',
  `cover` varchar(120) NOT NULL DEFAULT 'upload/photos/d-cover.jpg  ',
  `about` varchar(550) NOT NULL DEFAULT '',
  `category` int(11) NOT NULL DEFAULT 1,
  `privacy` enum('1','2') NOT NULL DEFAULT '1',
  `join_privacy` enum('1','2') NOT NULL DEFAULT '1',
  `active` enum('0','1') NOT NULL DEFAULT '0',
  `registered` varchar(32) NOT NULL DEFAULT '0/0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Group_Members`
--

CREATE TABLE `Wo_Group_Members` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0,
  `active` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Hashtags`
--

CREATE TABLE `Wo_Hashtags` (
  `id` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '',
  `tag` varchar(255) NOT NULL DEFAULT '',
  `last_trend_time` int(11) NOT NULL DEFAULT 0,
  `trend_use_num` int(11) NOT NULL DEFAULT 0,
  `expire` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Hashtags`
--

INSERT INTO `Wo_Hashtags` (`id`, `hash`, `tag`, `last_trend_time`, `trend_use_num`, `expire`) VALUES
(3, '354dccf5ceeef665871f03bad5f2bcc3', 'redelive', 1542136909, 4, '2018-11-20'),
(5, 'caa9c8f8620cbb30679026bb6427e11f', 'testando', 1538648505, 1, '2018-10-11'),
(11, '2e392f0d073d90e90a19c5491833fc1b', 'erfd', 1541567938, 1, '2018-11-14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_HiddenPosts`
--

CREATE TABLE `Wo_HiddenPosts` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_HiddenPosts`
--

INSERT INTO `Wo_HiddenPosts` (`id`, `post_id`, `user_id`) VALUES
(10, 356, 85),
(11, 360, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Langs`
--

CREATE TABLE `Wo_Langs` (
  `id` int(11) NOT NULL,
  `lang_key` varchar(160) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `english` text DEFAULT NULL,
  `arabic` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `dutch` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `french` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `german` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `italian` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `portuguese` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `russian` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `spanish` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `turkish` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Langs`
--

INSERT INTO `Wo_Langs` (`id`, `lang_key`, `english`, `arabic`, `dutch`, `french`, `german`, `italian`, `portuguese`, `russian`, `spanish`, `turkish`) VALUES
(1, 'login', 'Login', 'تسجيل الدخول', 'Inloggen', 'S&#39;identifier', 'Anmelden', 'Entra', 'Login', 'Вход', 'Acceder', 'Giriş'),
(2, 'register', 'Register', 'التسجيل', 'Registereren', 'Enregistrez', 'Registrieren', 'Iscriviti', 'Registrar', 'Регистрация', 'Registrar', 'Kayıt'),
(3, 'guest', 'Guest', 'زائر', 'Gast', 'Client', 'Gast', 'Ospite', 'Visitante', 'Гость', 'Huésped', 'Konuk'),
(4, 'username', 'Username', 'اسم المستخدم', 'Gebruikersnaam', 'le nom d&#39;utilisateur', 'Benutzername', 'Nome Utente', 'Nome de usu&amp;aacute;rio', 'Имя пользователя', 'Nombre de Usuario', 'Kullanıcı adı'),
(5, 'email', 'E-mail', 'البريد الإلكتروني', 'E-mail', 'E-mail', 'Email', 'E-mail', 'E-mail', 'E-mail адрес', 'E-mail', 'E-posta'),
(6, 'password', 'Password', 'كلمة المرور', 'Wachtwoord', 'Mot de passe', 'Passwort', 'Password', 'Senha', 'Пароль', 'Contraseña', 'Şifre'),
(7, 'new_password', 'New password', 'كلمة المرورالجديدة', 'Nieuw wachtwoord', 'Nouveau mot de passe', 'Neues Passwort', 'Nuova password', 'Nova senha', 'Новый пароль', 'Nueva Contraseña', 'Yeni Şifre'),
(8, 'remember_me', 'Remember me', 'تذكرني', 'Onthoud mij', 'Souviens-toi de moi', 'Angemeldet bleiben', 'Resta collegato', 'Lembrar', 'Запомнить меня', 'Recordarme', 'Beni hatırla'),
(9, 'or_login_with', 'Or login with', 'أو أدخل عن طريق', 'Of login met', 'Ou connectez-vous avec', 'oder Anmeldung mit', 'o entra con', 'Ou ent&amp;atilde;o fa&amp;ccedil;a login por', 'Или войдите через', 'O Acceder con:', 'Ya ile giriş'),
(10, 'forget_password', 'Forgot Password?', 'هل نسيت كلمة المرور؟', 'Wachtwoord vergeten?', 'Mot de passe oublié?', 'Passwort Vergessen?', 'Password dimenticata?', 'Esqueceu sua senha?', 'Забыли пароль?', '¿Olvidaste tu Contraseña?', 'Parolanızı unuttunuz mu?'),
(11, 'email_address', 'E-mail address', 'البريد الإلكتروني', 'Email', 'E-mail address', 'Emailadresse', 'Indirizo email', 'E-mail', 'E-mail адрес', 'Direcci&amp;oacute; de E-mail', 'E-posta'),
(12, 'confirm_password', 'Confirm Password', 'تأكيد كلمة المرور', 'Bevestig wachtwoord', 'Confirmez le mot de passe', 'Passwort bestätigen', 'Conferma Password', 'Confirmar senha', 'Подтвердите Пароль', 'Confirmar Contraseña', 'Şifreyi Onayla'),
(13, 'lets_go', 'Let&#039;s Go !', 'تسجيل', 'Ga verder!', 'Allons-y!', 'Los gehts!', 'Andiamo! !', 'Vamos l&amp;aacute;!', 'Войти!', '¡Vamos!', 'Haydi gidelim !'),
(14, 'recover_password', 'Recover', 'إعادة تعيين', 'Recover', 'Récupérer', 'Passwort wiederherstellen', 'Recuperare', 'Recuperar', 'Отправить', 'Recuperar', 'Kurtarmak'),
(15, 'reset_new_password_label', 'Reset Your Password', 'إعادة تعيين كلمة المرور', 'Reset Je Wachtwoord', 'Réinitialisez votre mot de passe', 'Passwort zurücksetzen', 'Resetta la tua password', 'Redefinir senha', 'Сбросить пароль', 'Reiniciar Contraseña', 'Şifrenizi sıfırlamak'),
(16, 'reset_password', 'Reset', 'إعادة تعيين', 'Reset', 'Réinitialiser', 'Zurücksetzen', 'Resetta', 'Resetar', 'Сброс', 'Reiniciar', 'Reset'),
(17, 'invalid_token', 'Invalid Token', 'رابط خاطأ', 'Verkeerde sleutel', 'Jeton Invalide', 'Ungültiges Zeichen', 'Gettone non valido', 'Token inválido', 'Недопустимый маркер', 'Token Invalido', 'Geçersiz Jetonu'),
(18, 'incorrect_username_or_password_label', 'Incorrect username or password', 'اسم المستخدم أو كلمة المرور غير صحيح', 'Gebruikersnaam of wachtwoord klopt niet', 'Identifiant ou mot de passe incorrect', 'Benutzername oder Passwort falsch', 'Nome utente o password errati', 'Nome de usu&amp;aacute;rio ou senha incorreto', 'Неверное имя пользователя или пароль', 'Usuario y/o Contraseña incorrectos', 'Yanlış kullanıcı adı ya da parola'),
(19, 'account_disbaled_contanct_admin_label', 'Your account has been disabled, please contact us .', 'لقد تم إيقاف حسابك مؤقتاَ , يرجى الإتصال بنا .', 'Je account is inactief gesteld. Neem contact op met account@babster.nl.', 'Votre compte a été désactivé, s&#39;il vous plaît contactez-nous .', 'Dein Konto wurde deaktiviert. Bitte setze dich mit uns in Verbindung.', 'Il tuo account è stato disabilitato, non esitate a contattarci.', 'Sua conta foi desativada.', 'Ваш аккаунт был отключен, пожалуйста, свяжитесь с нами.', 'Tu cuenta ha sido des habilitada, por favor cont&amp;aacute;ctanos .', 'Hesabınız devre dışı bırakıldı, lütfen bize ulaşın.'),
(20, 'account_not_active_label', 'You have to activate your account.', 'يجب عليك تفعيل الحساب', 'Je moet je account eerst activeren.', 'Vous devez activer votre compte.', 'Bitte aktiviere dein Konto.', 'Devi attivare il tuo account.', 'Você tem que ativar sua conta.', 'Вы должны активировать свою учетную запись.', 'Primero debes activar tu cuenta.', 'Hesabınızı etkinleştirmek gerekiyor.'),
(21, 'successfully_logged_label', 'Successfully Logged in, Please wait..', 'تم تسجيل الدخول .. الرجاء الإنتظار', 'Succesvol ingelogt, een momentje..', 'Connecté avec succès, S&#39;il vous plaît attendre..', 'Erfolgreich angemeldet, bitte warten..', 'Collegato con successo, Siete pregati di attendere..', 'Login efetuado com sucesso. Por favor aguarde...', 'Успешный вход. Пожалуйста, подождите...', 'Acceso permitido, por favor espere..', 'Başarıyla Girildi, lütfen bekleyin ..'),
(22, 'please_check_details', 'Please check your details.', 'الرجاء مراجعة المعلومات التي أدخلتها', 'Controleer je details.', 'S&#39;il vous plaît vérifier vos coordonnées.', 'Bitte überprüfe deine Angaben.', 'Si prega di verificare i tuoi dati.', 'Por favor marque os detalhes', 'Пожалуйста, проверьте свои данные.', 'Por favor revisa tus detalles.', 'Bilgilerinizi kontrol edin.'),
(23, 'username_exists', 'Username is already exists.', 'اسم المستخدم موجود بالفعل', 'Gebruikersnaam bestaad al.', 'Nom d&#39;utilisateur est existe déjà.', 'Benutzername existiert bereits.', 'Il nome utente è già esistente.', 'Desculpe, este nome de usu&amp;aacute;rio j&amp;aacute; esta em uso.', 'Имя пользователя уже существует.', 'Nombre de usuario ya existe.', 'Kullanıcı adı zaten var olduğunu.'),
(24, 'username_characters_length', 'Username must be between 5 / 32', 'اسم المستخدم يجب ان يكون بين 5 إلى 32 حرف', 'Gebruikersnaam moet tussen de 5 en 32 tekens lang zijn', 'Nom d&#39;utilisateur doit être comprise entre 5/32', 'Benutzername muss zwischen 5 und 32 Zeichen sein', 'Nome utente deve essere compresa tra 5 a 32 lettere', 'O nome de usu&amp;aacute;rio deve conter entre 5 / 32 caracteres.', 'Имя пользователя должно быть между 5/32 символами', 'Nombre de usuario debe ser de entre 5 / 32 caracteres', 'Kullanıcı adı 5/32 arasında olmalıdır'),
(25, 'username_invalid_characters', 'Invalid username characters', 'صيغة اسم المستخدم خاطئة، الرجاء كتابة اسم المستخدم بالإنجليزية وبلا مسافة مثال enbrash', 'Ongeldige tekens in je gebruikersnaam', 'Caractères de nom d&#039;utilisateur non valides', 'Benutzername enthält unzulässige Zeichen', 'Caratteri Nome utente non valido', 'O nome de usuário Não deve conter nenhum caractere especial, símbolo ou espaços. Apenas alfanuméricos ( A-Z, 0-9 ) e underline ( _ )', 'Недопустимые символы в Имени пользователя', 'Caracteres Inv&amp;aacute;lidos', 'Geçersiz kullanıcı adı karakterleri'),
(26, 'password_invalid_characters', 'Invalid password characters', 'صيغة كلمة المرور خاطئة', 'Ongeldige tekens in je wachtwoord', 'Caractères de mot de passe invalide', 'Passwort enthält unzulässige Zeichen', 'Caratteri della password non validi', 'Caracteres inválidos', 'Недопустимые символы в пароле', 'Caracteres Inv&amp;aacute;lidos', 'Geçersiz şifre karakteri'),
(27, 'email_exists', 'This e-mail is already in use', 'البريد الإلكتروني مستخدم بالفعل', 'Dit email adres is al ingebruik.', 'Cet e-mail est déjà utilisée', 'Emailadresse wird bereits benutzt', 'Questa e-mail è già in uso', 'J&amp;aacute; existe uma conta registrar nesse e-mail.', 'E-mail адре уже используется', 'Este correo ya est&amp;aacute; en uso', 'E-posta kullanımda'),
(28, 'email_invalid_characters', 'This e-mail is invalid.', 'صيغة البريد الإلكتروني خاطئة', 'Dit is een ongeldige email.', 'Cet e-mail est invalide.', 'Ungültige Emailadresse.', 'Questa e-mail non è valido.', 'E-mail inv&amp;aacute;lido.', 'Недействительный адрес электронной почты.', 'Este correo es invalido.', 'E-posta geçersiz.'),
(29, 'password_short', 'Password is too short.', 'كلمة المرور قصيرة جداَ', 'Wachtwoord is te kort.', 'Mot de passe est trop court.', 'Passwort ist zu kurz.', 'La password è troppo corta.', 'Senha muito pequena.', 'Пароль слишком короткий.', 'Contrase&amp;ntilde;a muy corta.', 'Şifre çok kısa.'),
(30, 'password_mismatch', 'Password not match.', 'كلمة المرور غير متطابقة', 'Wachtwoorden komen niet overeen.', 'Mot de passe ne correspond.', 'Passwörter stimmen nicht überein.', 'La password non corrisponde.', 'As senhas não conferem.', 'Пароли не совпадают.', 'Contrase&amp;ntilde; diferente.', 'Şifre eşleşmiyor.'),
(31, 'reCaptcha_error', 'Please Check the re-captcha.', 'الرجاء فحص ال reCaptcha', 'Controleer de beveiligingscode.', 'S&#39;il vous plaît Vérifiez la ré-captcha.', 'Bitte überprüfe den re-captcha.', 'Ricontrollare la Recaptcha.', 'Por favor, marque o captcha.', 'Пожалуйста, введите повторно капчу.', 'Favor de marcar el Re-Captcha.', 'ReCAPTCHA&#039;yı kontrol ediniz.'),
(32, 'successfully_joined_label', 'Successfully joined, Please wait..', 'تم الإشتراك بنجاح , الرجاء الإنتظار ..', 'Succesvol geregistreerd, een momentje..', 'Réussir rejoint, S&#39;il vous plaît attendre..', 'Erfolgreich beigetreten, bitte warten..', 'Iscrizione con sucesso, attendere prego..', 'Registrado com sucesso, Por favor aguarde..', 'Успешный вход. Пожалуйста, подождите...', 'Unido satisfactoriamente, Por favor espera..', 'Başarıyla katıldı! Lütfen bekleyin ..'),
(33, 'account_activation', 'Account Activation', 'تفعيل الحساب', 'Account activicatie', 'Activation de compte', 'Konto Aktivierung', 'Account attivato', 'Ative sua conta em Redelive', 'Активация аккаунта', 'Activaci&amp;oacute;n de cuenta', 'Hesap Aktivasyonu'),
(34, 'successfully_joined_verify_label', 'Registration successful! We have sent you an email, Please check your inbox/spam to verify your email.', 'تم الإشتراك بنجاح! لقد تم إرسال رمز التعيل إلى بريدك الإلكتروني', 'Succesvol geregistreerd, check je inbox/spam voor de activicatie mail.', 'Inscription réussi! Nous vous avons envoyé un e-mail, S&#39;il vous plaît vérifier votre boîte de réception / spam pour vérifier votre email.', 'Registrierung war erfolgreich! Wir haben dir eine Email gesandt: Bitte überprüfe dein Postfach und Spamordner zum aktivieren deines Kontos.', 'Registrazione di successo! Ti abbiamo inviato una e-mail, controlla la tua posta in arrivo / spam per verificare la tua email.', 'Registrado com sucesso! Enviamos um email, verifique a caixa de entrada/spam para verificar seu e-mail.', 'Поздравляем вы успешно зарегистрировались! Мы отправили Вам письмо с ссылкой для подтверждения регистрации. Пожалуйста, проверьте ваш почтовый ящик. Рекомендуем проверить папку «Спам» — возможно письмо попало туда.', 'Registro exitoso, te hemos enviado un correo de verificaci&amp;oacute;n, Revisa tu bandeja de entrada de correo.', 'Kayıt başarılı! Size bir e-posta gönderdik, e-postanızı doğrulamak için gelen / spam kontrol edin.'),
(35, 'email_not_found', 'We can&#039;t find this email.', 'البريد الإلكتروني غير موجود', 'We kunnen deze email niet vinden.', 'Nous ne pouvons pas trouver cet e-mail.', 'Email konnte nicht gefunden werden.', 'Non e possibile trovare questo indirizzo mail.', 'Não podemos encontrar este e-mail.', 'Мы не можем найти этот E-mail.', 'No encontramos este E-mail.', 'Biz bu e-postayı bulamıyor.'),
(36, 'password_rest_request', 'Password reset request', 'طلب إعادة تعيين كلمة المرور', 'Wachtwoord reset aanvraag', 'Demande de réinitialisation de mot', 'Passwort-Reset-Anfrage', 'Richiesta di reimpostazione della password', 'Pedido para resetar senha', 'Запрос Восстановление пароля', 'Solicitud de reinicio de contraseña', 'Parola sıfırlama isteği'),
(37, 'email_sent', 'E-mail has been sent successfully', 'لقد تم إرسال الرسالة', 'Email is succesvol verzonden', 'Le courriel a été envoyé avec succès', 'Email wurde erfolgreich versendet', 'E-mail è stata inviata con successo', 'E-mail enviado com sucesso.', 'Письмо отправлено', 'Correo enviado correctamente', 'E-posta başarıyla gönderildi'),
(38, 'processing_error', 'An error found while processing your request, please try again later.', 'حدث خطأ عند المعالجة , الرجاء المحاولة لاحقاَ', 'Er is een fout opgetreden, probeer het later nog eens.', 'Une erreur est survenue lors du traitement de votre demande, s&#39;il vous plaît réessayer plus tard.', 'In der Bearbeitung wurde ein Fehler festgestellt. Bitte versuche es später noch einmal.', 'Un errore durante l&#039;elaborazione della richiesta, riprova più tardi.', 'Algo de errado aconteceu. Por favor, tente novamente mais tarde.', 'Обнаружена ошибка при обработке вашего запроса, пожалуйста, попробуйте еще раз', 'Un error a ocurrido procesando tu solicitud, Intenta de nuevo mas tarde.', 'İsteğiniz işlenirken hata, lütfen tekrar deneyiniz bulundu'),
(39, 'password_changed', 'Password successfully changed !', 'تم تغيير كلمة المرور بنجاح', 'Wachtwoord succesvol gewijzigd !', 'Mot de passe changé avec succès !', 'Passwort erfolgreich geändert!', 'Password cambiata con successo!', 'Senha trocada com sucesso !', 'Пароль успешно изменен!', '¡ Contrase&amp;ntilde;a modificada correctamente !', 'Şifre başarıyla değiştirildi !'),
(40, 'please_choose_correct_date', 'Please choose a correct date.', 'الرجاء أختيار تاريخ الميلاد الصحيح', 'Selecteer een geldige datum.', 'S&#39;il vous plaît choisir une date correcte.', 'Bitte gebe ein korrektes Datum an.', 'Scegliere una data corretta.', 'Selecione uma data correta.', 'Пожалуйста, выберите правильную дату.', 'Por favor elige una fecha correcta.', 'Doğru tarih seçiniz.'),
(41, 'setting_updated', 'Setting successfully updated !', 'تم تحديث المعلومات بنجاح !', 'Instellingen succesvol gewijzigd!', 'Réglage de mise à jour avec succès !', 'Einstellungen erfolgreich übernommen!', 'Impostazioni aggiornate correttamente!', 'Configura&amp;ccedil;&amp;otilde;es atualizadas !', 'Настройки успешно обновлены!', '¡ Configuraci&amp;oacute;n correctamente guardada !', 'Ayar Başarıyla Güncellendi!'),
(42, 'current_password_mismatch', 'Current password not match', 'كلمة المرور الحالية غير صحيحة', 'Huidig wachtwoord komt niet overeen', 'Mot de passe actuel ne correspond pas', 'Aktuelles Passwort stimmt nicht', 'Password corrente non corrisponde', 'Sua senha atual n&amp;atilde;o confere', 'Текущий пароль не совпадает', 'Contrase&amp;ntilde;a actual diferente', 'Mevcut şifre eşleşmiyor'),
(43, 'website_invalid_characters', 'Website is invalid.', 'صيغة الموقع الإلكتروني غير صحيحة', 'Website is niet geldig.', 'Site Web est invalide.', 'Webseite ist ungültig.', 'Sito web non è valido.', 'Site inv&amp;aacute;lido.', 'Недопустимые символы в сайте.', 'El sitio web es invalido.', 'Web sitesi geçersiz.'),
(44, 'account_deleted', 'Account successfully deleted, please wait..', 'تم حذف حسابك نهائياَ , الرجاء الإنتظار ..', 'Account is succesvol gewijzigd, een momentje..', 'Compte supprimé avec succès, s&#39;il vous plaît patienter..', 'Konto erfolgreich gelöscht, bitte warten..', 'Account cancellato con successo, si prega di attendere..', 'Conta deletada, por favor aguarde..', 'Аккаунт успешно удален, пожалуйста, подождите...', 'Cuenta eliminada correctamente, por favor espere..', 'Başarıyla silindi Hesap, lütfen bekleyin ..'),
(45, 'home', 'Home', 'الصفحة الرئيسية', 'Home', 'Domicile', 'Start', 'Home', 'In&amp;iacute;cio', 'Главная', 'Inicio', 'Ana Sayfa'),
(46, 'advanced_search', 'Advanced Search', 'البحث المتقدم', 'Uitgebreid zoeken', 'Recherche Avancée', 'Erweiterte Suche', 'Ricerca avanzata', 'Pesquisa avan&amp;ccedil;ada', 'Расширенный поиск', 'B&amp;uacute;squeda Avanzada', 'Gelişmiş Arama'),
(47, 'search_header_label', 'Search for people, pages, groups and #hashtags', 'إبحث عن أعضاء, #هاشتاغ', 'Zoek mensen, #hastags en andere dingen..', 'Recherche de personnes, et les choses #hashtags', 'Suche Personen, #hashtags und Dinge', 'Cerca per persone, cose e #hashtags', 'Pesquise #hashtags', 'Поиск людей, мест или #хэштегов', 'Buscar Otakus, #hashtags y lolis', 'Kişiler, #hashtags ve şeyler ara'),
(48, 'no_result', 'No result found', 'لم يتم العثور على أي نتائج', 'Geen resultaten gevonden', 'Aucun résultat trouvé', 'Leider keine Ergebnisse', 'Nessun risultato trovato', 'Nada encontrado', 'Не найдено ни одного результата', 'Sin resultados', 'Herhangi bir ürün bulunamadı'),
(49, 'last_seen', 'Last Seen:', 'آخر ظهور:', 'Laatst gezien:', 'Dernière Visite:', 'Zuletzt online vor:', 'Ultimo accesso:', 'Visto por &amp;uacute;ltimo:', 'Был@:', 'Hace', 'Son Görülen:'),
(50, 'accept', 'Accept', 'قبول', 'Accepteren', 'Acceptez', 'Akzeptieren', 'Accettare', 'Aceitar', 'принимать', 'Aceptar', 'Kabul etmek'),
(51, 'cancel', 'Cancel', 'إلغاء', 'Weiger', 'Annuler', 'Abbruch', 'Cancella', 'Cancelar', 'Отмена', 'Cancelar', 'Iptal'),
(52, 'delete', 'Delete', 'حذف', 'Verwijder', 'Effacer', 'Löschen', 'Ellimina', 'Deletar', 'Удалить', 'Eliminar', 'Sil'),
(53, 'my_profile', 'My Profile', 'صفحتي الشخصية', 'Mijn Profiel', 'Mon profil', 'Mein Profil', 'Mio Profilo', 'Meu Perfil', 'Мой профиль', 'Mi Perfil', 'Profilim'),
(54, 'saved_posts', 'Saved Posts', 'المنشورات المحفوظة', 'Opgeslagen berichten', 'Messages Enregistrés', 'Gespeicherte Beiträge', 'Post Salvati', 'Posts Salvos', 'Сохраненные заметки', 'Posts Guardados', 'Kayıtlı Mesajlar'),
(55, 'setting', 'Settings', 'الإعدادات', 'Instellingen', 'Réglage', 'Einstellungen', 'Impostazioni', 'Configura&amp;ccedil;&amp;otilde;es', 'Настройки', 'Configuraci&amp;oacute;n', 'Ayarlar'),
(56, 'admin_area', 'Admin Area', 'لوحة المدير', 'Beheerpaneel', 'Admin Area', 'Administration', 'Area Administratore', 'Admin', 'Админка', 'Área del Admin', 'Yönetici Alanı'),
(57, 'log_out', 'Log Out', 'تسجيل الخروج', 'Uitloggen', 'Se déconnecter', 'Abmelden', 'Esci', 'Sair', 'Выйти', 'Cerrar Sesión', 'Çıkış Yap'),
(58, 'no_new_notification', 'You do not have any notifications', 'لا يوجد إشعارات', 'Je hebt geen meldingen', 'Vous ne disposez pas de toutes les notifications', 'Derzeit keine neuen Benachrichtigungen', 'Non avete notifiche', 'Você tem 0 notificações', 'Нет новых уведомлений', 'No tienes nuevas notificaciones', 'Bildirim yok'),
(59, 'no_new_requests', 'You do not have any requests', 'لا يوجد طلبات صداقة', 'Je hebt geen verzoeken', 'Vous ne disposez pas de toutes les demandes', 'Derzeit keine neuen Anfragen', 'Non avete alcuna richiesta', 'Você tem 0 pedidos de amizade', 'Нет новых запросов', 'No tienes nuevas solicitudes', 'Istekler yok'),
(60, 'followed_you', 'followed you', 'تابعك', 'volgt je', 'je t&#039;ai suivi', 'folgt dir jetzt', 'Ti segue', 'Seguiu você', 'последовал@ за тобой', 'te ha seguido', 'Seni takip etti.'),
(61, 'comment_mention', 'mentioned you on a comment.', 'أشار لك في تعليق', 'heeft je vermeld in een reactie.', 'vous avez mentionné sur un commentaire.', 'hat dich in einem Kommentar erwähnt.', 'lei ha citato un commento.', 'mencionou você em um comentário', 'упомянул@ вас в комментарии.', 'te ha mencionado en un comentario.', 'Bir yorumum sizden bahsetti.'),
(62, 'post_mention', 'mentioned you on a post.', 'أشار لك في منشور', 'heeft je vermeld in een bericht.', 'vous avez mentionné sur un poteau.', 'hat dich in einem Beitrag erwähnt.', 'lei ha citato in un post.', 'mencionou você em um post.', 'упомянул@ вас в заметке.', 'te menciono en una publicaci&amp;oacute;.', 'Bir yayında sizden bahsetti.'),
(63, 'posted_on_timeline', 'posted on your timeline.', 'نشر على حائطك', 'heeft een krabbel op je tijdlijn geplaats.', 'posté sur votre timeline.', 'hat an deine Pinwand geschrieben.', 'pubblicato sulla timeline.', 'postou algo em sua linha do tempo.', 'Публикация на стене', 'publico en tu timeline.', 'Zaman çizelgesi Yayınlanan.'),
(64, 'profile_visted', 'visited your profile.', 'زار صفحتك الشخصية', 'heeft je profiel bezocht.', 'visité votre profil.', 'hat dein Profil besucht.', 'ha visitato il tuo profilo.', 'te visitou.', 'посетил@ ваш профиль.', 'visitó tu perfil', 'Profilinizi ziyaret etti.'),
(65, 'accepted_friend_request', 'accepted your friend request.', 'قبل طلب الصداقة', 'heeft je vriendschapsverzoek geaccepteerd.', 'accepté votre demande d&#39;ami.', 'hat deine Freundschaftsanfrage akzeptiert.', 'ha accettato la tua richiesta di amicizia.', 'aceitou seu pedido de amizade.', 'принял@ запрос о дружбе.', 'acepto tu solicitud de amistad.', 'Arkadaşlık isteğin kabul edildi.'),
(66, 'accepted_follow_request', 'accepted your follow request.', 'قبل طلب المتابعة', 'heeft je volgverzoek geaccepteerd.', 'accepté votre demande de suivi.', 'hat deine Folgenanfrage akzeptiert.', 'ha accettato la tua richiesta di follow/segumento.', 'aceitou que você siga ele.', 'принять запрос.', 'acepto tu solicitud de seguimiento.', 'Senin takip talebi kabul etti.'),
(67, 'liked_comment', 'liked your comment &quot;{comment}&quot;', 'أعجب بتعليقك &quot;{comment}&quot;', 'respecteerd je reactie &quot;{comment}&quot;', 'aimé votre commentaire &quot;{comment}&quot;', 'gefällt dein Kommentar &quot;{comment}&quot;', 'piace il tuo commento &quot;{comment}&quot;', 'curtiu seu comentário &quot;{comment}&quot;', 'нравится Ваш комментарий &quot;{comment}&quot;', 'le gusta tu comentario &quot;{comment}&quot;', 'Yorumunuzu Beğendi &quot;{comment}&quot;'),
(68, 'wondered_comment', 'wondered your comment &quot;{comment}&quot;', 'تعجب من تعليقك &quot;{comment}&quot;', 'wondered je reactie &quot;{comment}&quot;', 'demandé votre commentaire &quot;{comment}&quot;', 'wundert sich über deinen Kommentar &quot;{comment}&quot;', 'si chiedeva il tuo commento &quot;{comment}&quot;', 'não curtiu seu comentário &quot;{comment}&quot;', 'не нравится &quot;{comment}&quot;', 'le sorprendioo tu comentario &quot;{comment}&quot;', 'Yorumunuzu merak etti &quot;{comment}&quot;'),
(69, 'liked_post', 'liked your {postType} {post}', 'أعجب ب {postType} الخاص بك {post}', 'respecteerd je {postType} {post}', 'aimé votre {postType} {post}', 'gefällt dein {postType} {post}', 'piace il {postType} {post}', 'curtiu sua {postType} {post}', 'нравится {postType} {post}', 'le gusta tu {postType} {post}', 'Senin {postType} Beğendi {post}'),
(70, 'wondered_post', 'wondered your {postType} {post}', 'تعجب ب {postType} الخاص بك {post}', 'wondered je {postType} {post}', 'demandé votre {postType} {post}', 'wundert sich über deinen {postType} {post}', 'si chiedeva il tuo {postType} {post}', 'Não curtiu sua {postType} {post}', 'не нравится {postType} {post}', 'le sorprendio tu {postType} {post}', 'Senin {postType} merak etti {post}'),
(71, 'share_post', 'shared your {postType} {post}', 'شارك {postType} الخاص بك {post}', 'deelde je {postType} {post}', 'partagé votre {postType} {post}', 'hat deinen {postType} {post} geteilt', 'ha condiviso il tuo {postType} {post}', 'compartilhou {postType} {post}', 'сделал@ перепост {postType} {post}', 'ha compartido tu {postType} {post}', 'Senin {postType} paylaştı {post}'),
(72, 'commented_on_post', 'commented on your {postType} {post}', 'علق على {postType} {post}', 'reageerde op je {postType} {post}', 'commenté sur votre {postType} {post}', 'hat deinen {postType} {post} kommentiert', 'ha commentato il tuo {postType} {post}', 'comentou em sua {postType} {post}', 'прокомментировал {postType} {post}', 'comento en tu {postType} {post}', 'Senin {postType} yorumlananlar {post}'),
(73, 'activity_liked_post', 'liked {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'أعجب &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;بمنشور&lt;/a&gt; {user}.', 'respecteerd {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;bericht&lt;/a&gt;.', 'aimé {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;poster&lt;/a&gt;.', 'gefällt {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;Beitrag&lt;/a&gt;.', 'piace {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'curtiu {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'нравится &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;заметка&lt;/a&gt; {user}.', 'le gust&amp;oacute; la &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;publicaci&amp;oacute;n&lt;/a&gt; de {user} .', '{user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt; beğendi.'),
(74, 'activity_wondered_post', 'wondered {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'تعجب &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;بمنشور&lt;/a&gt; {user}.', 'wondered {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;bericht&lt;/a&gt;.', 'demandé {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;poster&lt;/a&gt;.', 'wundert sich über {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;Beitrag&lt;/a&gt;.', 'chiedeva {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'n&amp;atilde;o curtiu {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'не нравится &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;заметка&lt;/a&gt; {user}.', 'le sorprendio la &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;publicaci&amp;oacute;n&lt;/a&gt; de {user} .', 'Wondered {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.'),
(75, 'activity_share_post', 'shared {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'شارك &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;منشور&lt;/a&gt; {user}.', 'deelde {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;bericht&lt;/a&gt;.', 'partagé {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;poster&lt;/a&gt;.', 'hat {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;Beitrag&lt;/a&gt; geteilt.', 'condiviso {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'compartilhou {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'поделился &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;заметкой&lt;/a&gt; {user}.', 'compartio la &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;publicaci&amp;oacute;n&lt;/a&gt; de {user} .', 'Shared {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.'),
(76, 'activity_commented_on_post', 'commented on {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'علق على &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;منشور&lt;/a&gt; {user}.', 'reageerde op {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;bericht&lt;/a&gt;.', 'commenté sur {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;poster&lt;/a&gt;.', 'hat {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;Beitrag&lt;/a&gt; kommentiert.', 'ha commentato in {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'Comentou no {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'прокомментировал@ &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;заметку&lt;/a&gt; {user}.', 'comento en la &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;publicaci&amp;oacute;n de &lt;/a&gt;{user} .', 'Commented on {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.'),
(77, 'video_n_label', 'video.', 'الفيديو', 'video.', 'vidéo.', 'Video', 'video.', 'postagem de vídeo:', 'видео.', 'video.', 'video'),
(78, 'post_n_label', 'post.', 'المنشور', 'bericht.', 'poster.', 'Beitrag', 'post.', 'postagem:', 'пост.', 'post.', 'post'),
(79, 'photo_n_label', 'photo.', 'الصورة', 'foto.', 'photo.', 'Foto', 'imagini.', 'foto:', 'фото.', 'foto.', 'fotoğraf'),
(80, 'file_n_label', 'file.', 'الملف', 'bestand.', 'fichier.', 'Datei', 'file.', 'postagem de arquivo:', 'файл.', 'archivo.', 'dosya'),
(81, 'vine_n_label', 'vine video.', 'فيديو الفاين', 'vine video.', 'vine vidéo.', 'Vine-Video', 'vine video.', 'Vine.', 'видео.', 'vine.', 'vine video'),
(82, 'sound_n_label', 'sound.', 'الملف الصوتي', 'muziek.', 'du son.', 'Musik', 'musica.', 'postagem de áudio:', 'аудио.', 'sonido.', 'ses'),
(83, 'avatar_n_label', 'profile picture.', 'صورتك الشخصية', 'profiel foto.', 'Photo de profil.', 'Profilbild', 'imagine di profilo.', 'imagem de perfil.', 'Фото профиля', 'foto de perfil.', 'profil fotoğrafı'),
(84, 'error_not_found', '404 Error', 'خطأ 404', '404 Error', '404 Erreur', '404 Fehler', '404 Errore', 'Erro 404', 'Ошибка 404', 'Error 404', '404 Hatası'),
(85, 'sorry_page_not_found', 'Sorry, page not found!', 'عذراَ , الصفحة المطلوبة غير موجودة .', 'Sorry, pagina niet gevonden!', 'Désolé, page introuvable!', 'Entschuldigung: Seite wurde nicht gefunden!', 'la pagina non trovata!', 'Página não encontrada!', 'Извините, страница не обнаружена!', 'Gommen ne, pagina no encontrada!', 'Maalesef sayfa bulunamadı!'),
(86, 'page_not_found', 'The page you are looking for could not be found. Please check the link you followed to get here and try again.', 'الصفحة التي طلبتها غير موجودة , الرجاء فحص الرابط مرة أخرى', 'The page you are looking for could not be found. Please check the link you followed to get here and try again.', 'La page que vous recherchez n&#039;a pu être trouvée. S&#039;il vous plaît vérifier le lien que vous avez suivi pour arriver ici et essayez à nouveau.', 'Die Seite die du besuchen möchtest, wurde nicht gefunden. Bitte überprüfe den Link auf Richtigkeit und versuche es erneut.', 'La pagina che stai cercando non è stato trovato. Si prega di controllare il link che hai seguito per arrivare qui e riprovare.', 'A página que você esta procurando não foi encontrada. Confira o link e tente novamente.', 'Упс! Мы не можем найти страницу, которую вы ищете. Вы ввели неправильный адрес, или такая страница больше не существует.', 'La p&amp;aacute;gina que est&amp;aacute;s buscando no se encuentra. Por favor revisa el link y vuelve a intentar.', 'Aradığınız sayfa bulunamadı. Buraya ve tekrar denemek için izlenen linki kontrol edin.'),
(87, 'your_account_activated', 'Your account have been successfully activated!', 'لقد تم تفعيل حسابك بنجاح !', 'Je account is succesvol geactiveerd!', 'Votre compte a été activé avec succès!', 'Dein Konto wurde erfolgreich aktiviert!', 'Il tuo account è stato attivato con successo!', 'Conta ativada!', 'Ваша учетная запись была успешно активирована!', '¡Tu cuenta ha sido activada exitosamente!', 'Hesabınız başarıyla aktive edildi!'),
(88, 'free_to_login', 'You&#039;r free to &lt;a href=&quot;{site_url}&quot;&gt;{login}&lt;/a&gt; !', 'يمكنك الآن &lt;a href=&quot;http://localhost/wowonder_update&quot;&gt;{login}&lt;/a&gt; !', 'Je kan &lt;a href=&quot;http://localhost/wowonder_update&quot;&gt;{login}&lt;/a&gt; !', 'Votre libre &lt;a href=&quot;http://localhost/wowonder_update&quot;&gt;{login}&lt;/a&gt; !', 'Bitte hier &lt;a href=&quot;http://localhost/wowonder_update&quot;&gt;{login}&lt;/a&gt; !', 'Siete liberi di  &lt;a href=&quot;http://localhost/wowonder_update&quot;&gt;{login}&lt;/a&gt; !', 'Fa&amp;ccedil;a &lt;a href=&quot;http://localhost/wowonder_update&quot;&gt;{login}&lt;/a&gt; !', 'Вы&#039;r войти &lt;a href=&quot;http://localhost/wowonder_update&quot;&gt;{login}&lt;/a&gt; !', 'Eres libre de &lt;a href=&quot;http://localhost/wowonder_update&quot;&gt;{login}&lt;/a&gt; !', 'Sen serbestsin &lt;a href=&quot;http://localhost/wowonder_update&quot;&gt;{login}&lt;/a&gt; ! için'),
(89, 'general_setting', 'General Setting', 'المعلومات العامة', 'General Setting', 'Cadre général', 'Allgemeine Einstellungen', 'Impostazioni Generali', 'Configura&amp;ccedil;&amp;otilde;es gerais', 'Общие настройки', 'Configuraci&amp;oacute;n General', 'Genel Ayar'),
(90, 'login_setting', 'Login Setting', 'ملعومات الدخول', 'Login Setting', 'Connexion Cadre', 'Anmeldungseinstellungen', 'Impostazioni di accesso', 'Configura&amp;ccedil;&amp;otilde;es de login', 'Войти Настройки', 'Configuraci&amp;oactute;n de Acceso', 'Üye Girişi Ayarı'),
(91, 'manage_users', 'Manage Users', 'إدارة المستخدمين', 'Manage Users', 'gérer les utilisateurs', 'Benutzer verwalten', 'Gestisci Utenti', 'Editar usu&amp;aacute;rios', 'Управление пользователями', 'Manejar Usuarios', 'Kullanıcıları Yönet'),
(92, 'manage_posts', 'Manage Posts', 'إدارة المنشورات', 'Manage Posts', 'gérer les messages', 'Beiträge verwalten', 'Gestisci Posts', 'Editar posts', 'Управление сообщения', 'Manejar Publicaciones', 'Mesajlar Yönet'),
(93, 'manage_reports', 'Manage Reports', 'إدارة التبليغات', 'Manage Reports', 'gérer les rapports', 'Meldungen verwalten', 'Gestisci Segnalazioni', 'Vizualizar reports', 'Управление отчетами', 'Manenjar Reportes', 'Raporlar Yönet'),
(94, 'advertisement', 'Advertisement', 'الإعلانات', 'Advertisement', 'Publicité', 'Werbung', 'Publicita', 'Divulga&amp;ccedil;&amp;atilde;o', 'Реклама', 'Aviso', 'Reklâm'),
(95, 'more', 'More', 'أكثر', 'Meer', 'Plus', 'mehr', 'Più', 'Mais', 'еще', 'Más información', 'daha'),
(96, 'cache_system', 'Cache System', 'نظام الكاش', 'Cache System', 'Système de cache', 'Cachsystem', 'Cache di Systema', 'Cache', 'система кэша', 'Cache', 'Önbellek Sistemi'),
(97, 'chat_system', 'Chat System', 'نظام الدردشة', 'Chat System', 'système chat', 'Chatsystem', 'Sistema Chat', 'Sistema do chat', 'Чат системы', 'Chat', 'Sohbet Sistemi'),
(98, 'email_validation', 'Email validation', 'تأكيد الحساب عبر الايميل', 'Email validation', 'Email de validation', 'Emailbestätigung', 'Email di convalida', 'Valida&amp;ccedil;&amp;atilde;o de Email', 'E-mail валидации', 'Validaci&amp;oacute;n de correo', 'E-posta Doğrulama'),
(99, 'email_notification', 'Email notification', 'إرسال الإشعارات عبر الايميل', 'Email notification', 'Notification par courriel', 'Email-Benachrichtigungen', 'Notifiche Email', 'Notifica&amp;ccedil;&amp;atilde;o de Email', 'E-mail уведомления', 'Notificaciones', 'E-posta Bildirimi'),
(100, 'smooth_links', 'Smooth links', 'الروابط القصيرة', 'Smooth links', 'liens lisses', 'Einfache Links', 'Collegamenti Smooth', 'Links permitidos', 'Гладкие Ссылки', 'Smooth links', 'Pürüzsüz Bağlantılar'),
(101, 'seo_friendly_url', 'SEO friendly url', 'الروابط الداعة لمواقع البحث', 'SEO friendly url', 'SEO URL conviviale', 'SEO freundliche URL', 'SEO amicizie url', 'URL', 'SEO Дружественные ссылки', 'url amigable para SEO', 'SEO dost URL'),
(102, 'file_sharing', 'File sharing', 'مشاركة الملفات', 'File sharing', 'Partage de fichier', 'Datenaustausch', 'Condivisione di file', 'Compartilhar arquivo', 'обмена файлами', 'Compartir Archivos', 'Dosya paylaşımak'),
(103, 'themes', 'Themes', 'شكل الموقع', 'Themes', 'thèmes', 'Design', 'Temi', 'Temas', 'Темы', 'Temas', 'Temalar'),
(104, 'user_setting', 'User Setting', 'إعدادات المستخدم', 'User Setting', 'Cadre de l&#39;utilisateur', 'Benutzereinstellungen', 'Impostazioni utente', 'Configura&amp;ccedil;&amp;otilde;es do usu&amp;aacute;rio', 'настройки пользователя', 'Configuraci&amp;oacute;n de usuario', 'Kullanıcı Ayarı'),
(105, 'site_setting', 'Site Setting', 'إعدادات الموقع', 'Site Setting', 'site Cadre', 'Seiteneinstellungen', 'impostazioni del sito', 'Configura&amp;ccedil;&amp;otilde;es do site', 'настройка сайта', 'Configuraci&amp;oacute;n de sitio', 'Sitede Ayarı'),
(106, 'cache_setting_info', 'Enable cache system to speed up your website, Recommended more than 10,000 active users.', 'فعل نظام الكاش لتسريع الموقع, يستحسن إستخدامه لأكثر من 10 آلاف مستخدم.', 'Enable cache system to speed up your website, Recommended more than 10,000 active users.', 'Activer système de cache pour accélérer votre site, a recommandé plus de 10.000 utilisateurs actifs.', 'Aktiviere das Cachesystem um die Seiten schneller zu machen, Empfehlenswert ab 10,000 aktiven Benutzern.', 'Abilita sistema di cache per velocizzare il tuo sito web, consigliato più di 10.000 utenti attivi.', 'Ativar o cache para aumentar a velocidade do site, Recomendado se tiver mais de 10,000 usu&amp;aacute;rios ativos.', 'Включить систему кэш для ускорения вашего сайта, Рекомендуем более 10000 активных пользователей.', 'Habilitar cache para aumentar la velocidad de tu sitio, Recomendado para m&amp;aacute;s de 10,000 usuarios activos.', 'Web sitenizi hızlandırmak için önbellek sistemi etkinleştirin, 10.000 &#039;den fazla aktif kullanıcı önerilir.'),
(107, 'chat_setting_info', 'Enable chat system to chat with friends on the buttom of the page', 'فعل نظام الدردشة للدردشة مع الإصدقاء في يمين أسفل الصفحة.', 'Enable chat system to chat with friends on the buttom of the page', 'Activer système chat pour discuter avec des amis sur le fond de la page', 'Aktiviere das Chatsystem zum chatten mit Freunden', 'Attivare il sistema chat per chiacchierare con gli amici in fondo alla pagina', 'Ativar sistema de chat para conversas com seus amigos no rodap&amp;eacute; da p&amp;aacute;gina', 'Включить чат системы для общаться с друзьями на дне страницы', 'Habilitar cache de chat, para hablar con amigos al pie del sitio', 'Sayfanın altındaki arkadaşlarınızla sohbet etmek için sohbet sistemini etkinleştirme'),
(108, 'email_validation_info', 'Enable email validation to send activation link when user registred.', 'لإرسال رمز التاكيد عبر البريد الإلكتروني عندما يسجل المستخدم.', 'Enable email validation to send activation link when user registred.', 'Activer la validation de messagerie pour envoyer le lien d&#39;activation lorsque l&#39;utilisateur référencée.', 'Aktiviere Email-Aktivierung zum Senden eines Aktivierungslinks wenn sich ein Benutzer registriert.', 'Abilitare la convalida e-mail per inviare link di attivazione quando utente registrato.', 'Ativar valida&amp;ccedil;&amp;atilde;o de e-mail quando um usu&amp;aacute;rio se registrar.', 'Включить проверку электронной почты, чтобы отправить ссылку активации, когда зарегистрированный пользователь.', 'Habilitar validaci&amp;oacute;n de correo para usuarios registrados.', 'Kullanıcı kayıt sırasında aktivasyon bağlantısını göndermek için e-posta doğrulama etkinleştirin.'),
(109, 'email_notification_info', 'Enable email notification to send user notification via email.', 'لإرسال الإشعارات عبر البريد الإلكتروني.', 'Enable email notification to send user notification via email.', 'Activer la notification par e-mail pour envoyer une notification par e-mail de l&#039;utilisateur.', 'Aktiviere Email-Benachrichtigungen zum Senden von Benachrichtigungen an die Benutzer.', 'Abilita notifica e-mail per inviare via e-mail di notifica all&#039;utente.', 'Enviar notificações por e-mail.', 'Включить уведомление по электронной почте, чтобы отправить уведомление пользователя по электронной почте.', 'Habilitar notificaci&amp;oacute;n por correo para enviar al usuario.', 'E-posta yoluyla kullanıcı bildirim göndermek için e-posta bildirimi etkinleştirin.'),
(110, 'smooth_links_info', 'Enable smooth links, e.g.{site_url}/home.', 'لإستخدام الروابط القصيرة, مثال: http://localhost/wowonder_update/home.', 'Enable smooth links, e.g.http://localhost/wowonder_update/home.', 'Activer les liens lisses, e.g.http://localhost/wowonder_update/home.', 'Aktiviere Einfache Links, z.B.http://localhost/wowonder_update/start', 'Abilita collegamenti regolari, e.g.http://localhost/wowonder_update/home.', 'Ativar links limpos, exemplo.http://localhost/wowonder_update/home.', 'Включить гладкие ссылки, напримерhttp://localhost/wowonder_update/home.', 'Habilitar smooth links, e.g.http://localhost/wowonder_update/home.', 'Pürüzsüz bağlantıları etkinleştirme, e.g.http://localhost/wowonder_update/home.'),
(111, 'seo_frindly_info', 'Enable SEO friendly url, e.g.{site_url}//1_hello-how-are-you-doing.html', 'لإستخدام الروابط المساعدة لمواقع البحث, مثال: http://localhost/wowonder_update/1_hello-how-are-you-doing.html', 'Enable SEO friendly url, e.g.http://localhost/wowonder_update//1_hello-how-are-you-doing.html', 'Activer SEO URL conviviale, e.g.http://localhost/wowonder_update//1_hello-how-are-you-doing.html', 'Aktiviere SEO freundliche URL, z.B.http://localhost/wowonder_update//1_hallo-was-machst-du-gerade.html', 'Abilita SEO friendly url, e.g.http://localhost/wowonder_update//1_hello-how-are-you-doing.html', 'Ativar SEO URL, exemplo.http://localhost/wowonder_update//1_hello-how-are-you-doing.html', 'Включить SEO Дружественные ссылки, напримерhttp://localhost/wowonder_update//1_hello-how-are-you-doing.html', 'Habilitar url amigable para SEO, e.g.http://localhost/wowonder_update//1_hello-how-are-you-doing.html', 'SEO dostu url etkinleştirme, e.g.http://localhost/wowonder_update//1_hello-how-are-you-doing.html'),
(112, 'file_sharing_info', 'Enable File sharing to share &amp; upload videos,images,files,sounds, etc..', 'لمشاركة الملفات : صوت , فيديو , صورة , الخ ..', 'Enable File sharing to share &amp; upload videos,images,files,sounds, etc..', 'Activez le partage de fichiers pour partager et télécharger des vidéos, des images, des fichiers, des sons, etc...', 'Aktiviere Datenaustausch zum teilen und hochladen von: Videos, Bildern, Dateien, Musik, etc..', 'Attivare la condivisione di file per condividere e caricare video, immagini, file, suoni, ecc..', 'Ativar o compartilhamento de arquivos, para compartilhar videos,imagens,arquivos,sons,etc..', 'Включить общий доступ к файлам, чтобы разделить и загрузить видео, изображения, файлы, звуки и т.д ..', 'Habilitar compartir archivos, para subir v&amp;iacute;deos, sonido, im&amp;aacute;genes , etc..', 'Paylaşmak &amp; vb video, görüntü, dosyaları, sesler, yüklemek için Dosya paylaşımını etkinleştirin ..'),
(113, 'cache', 'Cache', 'الكاش', 'Cache', 'Cache', 'Cache', 'Cache', 'Cache', 'кэш', 'Cache', 'Önbellek'),
(114, 'site_information', 'Site Information', 'معلومات الموقع', 'Site Information', 'Informations sur le site', 'Seiteninformationen', 'Informazioni sul sito', 'Informa&amp;ccedil;&amp;otilde;es do Site', 'информация о сайте', 'Informaci&amp;oacute;n del sitio', 'Site Bilgileri'),
(115, 'site_title_name', 'Site Name &amp; Title:', 'اسم الموقع و عنوانه:', 'Site Name &amp; Title:', 'Site Nom et titre:', 'Seitenname und Titel:', 'Nome del sito &amp; Titolo:', 'Nome e título do Site:', 'Название сайта и Заголовок:', 'Nombre y t&amp;iacute;tulo del sitio:', 'Site Adı &amp; Başlık:'),
(116, 'site_name', 'Site name', 'اسم الموقع', 'Site name', 'Nom du site', 'Seitenname', 'Nome del sito', 'Nome do Site', 'Название сайта', 'Nombre del sitio', 'Site adı'),
(117, 'site_title', 'Site title', 'عنوان الموقع', 'Site title', 'Titre du site', 'Seitentitel', 'Titolo del sito', 'Título do Site', 'Заголовок сайта', 'T&amp;iacute;tulo del sitio', 'Site başlığı'),
(118, 'site_keywords_description', 'Site Keywords &amp; Description:', 'وصف الموقع والكلمات المفتاحية:', 'Site Keywords &amp; Description:', 'Site Mots-clés et description:', 'Seiten-Schlüsselwörter und Beschreibung:', 'Chave di ricerca e descrizione del sito:', 'Descri&amp;ccedil;&amp;atilde;o das palavras-chaves:', 'Сайт Ключевые слова и Описание:', 'Palabras clave y Descripci&amp;oacute;:', 'Sitede Anahtar kelimeler ve Açıklama:'),
(119, 'site_keywords', 'Site Keywords', 'كلمات مفتاحية, مثال: موقع, تواصل اجتماعي, الخ ..', 'Site Keywords (eg: social,wownder ..)', 'site Mots-clés (eg: social,wownder ..)', 'Seiten-Schlüsselwörter (z.B: social,wundern ..)', 'Parole chiave del sito (ad esempio: il mio social network, social etc ..)', 'Palavras-chaves do site', 'Ключевые слова Сайт (например: социальная, лучше ..)', 'Palabras clave (ej: social,wownder ..)', 'Site Anahtar kelimeler (eg: social,wownder ..)'),
(120, 'site_description', 'Site Description', 'وصف الموقع', 'Site Description', 'description du site', 'Seitenbeschreibung', 'Descrizione del stio', 'Descri&amp;ccedil;&amp;atilde;o do site', 'описание сайта', 'Descripci&amp;oacute;n del sitio', 'Site Açıklaması'),
(121, 'site_email', 'Site E-mail', 'بريد الموقع الإلكتروني:', 'Site E-mail', 'Site E-mail', 'System-Email', 'Indirizo email del sito', 'E-mail do site', 'Сайт E-mail', 'E-mail del sitio', 'Site E-posta'),
(122, 'site_lang', 'Default Language', 'اللغة الأساسية:', 'Default Language', 'Langage par défaut', 'Standard-Sprache', 'Lingua di default', 'Linguagem padr&amp;ccedil;o', 'Язык по умолчанию', 'Lenguaje por defecto', 'Varsayılan dil'),
(123, 'theme_setting', 'Theme Setting', 'إعدادات شكل الموقع', 'Theme Setting', 'thème Cadre', 'Design Einstellungen', 'Impostazione tema', 'Configura&amp;ccedil;&amp;otilde;es do tema', 'тема настройка', 'Configuraci&amp;oacute;n de Tema', 'Tema Ayarı'),
(124, 'activeted', 'Activated', 'مفعل', 'Activeted', 'Activeted', 'Aktiviert', 'Attiva', 'Ativado', 'активировал', 'Activado', 'Aktif'),
(125, 'version', 'Version:', 'الإصدار:', 'Version:', 'Version:', 'Version:', 'Versione:', 'Vers&amp;ccedil;o:', 'Версия:', 'Versi&amp;oacute;n:', 'Sürüm:'),
(126, 'author', 'Author:', 'المالك:', 'Author:', 'Auteur:', 'Autor:', 'Author:', 'Autor:', 'Автор:', 'Autor:', 'Yazar:'),
(127, 'user_status', 'User status', 'حالة المستخدم', 'User status', 'Le statut de l&#39;utilisateur', 'Benutzerstatus', 'Status del utente', 'Status do usu&amp;aacute;rio', 'Статус пользователь', 'Estatus de usuario', 'Kullanıcı durumu'),
(128, 'online_lastseen', '(online/last seen)', '(متصل / آخر ظهور)', '(online/last seen)', '(en ligne / vu la dernière fois)', '(online/zuletzt online)', '(Attvo/Ultima visita)', '(online/visto por &amp;uacute;ltimo)', '(онлайн / в последний раз был@)', '(En linea/visto hace)', '(çevrimiçi / son görüldüğü)'),
(129, 'enable', 'Enable', 'تفعيل', 'Enable', 'Activer', 'Aktivieren', 'Attiva', 'Ativar', 'Включить', 'Habilitado', 'Etkinleştirmek'),
(130, 'disable', 'Disable', 'إلغاء التفعيل', 'Disable', 'désactiver', 'Deaktivieren', 'Disattiva', 'Desativar', 'Отключить', 'Des habilitado', 'Devre dışı'),
(131, 'allow_users_to_delete', 'Allow users to delete their account', 'السماح للمستخدم بحذف حسابه؟', 'Allow users to delete their account:', 'Autoriser les utilisateurs à supprimer son compte:', 'Benutzern erlauben ihr Konto zu löschen:', 'Consentire agli utenti di cancellare il proprio account:', 'Permitir que usuários deletem suas contas', 'Разрешить пользователям удалять свой счет:', 'Permitir a usuarios eliminar su cuenta:', 'Kullanıcıların kendi hesabını silmek için izin verin:'),
(132, 'profile_visit_notification', 'Profile visit notification', 'تلقي الإشعارات عند زيارة الصفحة الشخصية؟', 'Profile visit notification:', 'Profil notification de visite:', 'Profilbesucher Benachrichtigung:', 'Notifiche sula visita del tuo profilo:', 'Notificação de visita no perfil', 'Профиль уведомление визит:', 'Notificaci&amp;oacute;n de visita de perfil:', 'Profil ziyaret bildirimi:'),
(133, 'display_user_age_as', 'Display user age as', 'أظهار العمر ك؟', 'Display user age as:', 'Display user d&#39;âge:', 'Zeige das Alter eines Users als:', 'Mostra eta utente come:', 'Mostrar idade como', 'Показать возраст пользователя, как:', 'Mostrar edad de usuario como:', 'Ekran kullanıcı yaşı olarak:'),
(134, 'date', 'Date', 'التاريخ', 'Date', 'Rendez-vous amoureux', 'Datum', 'Data', 'Data', 'Дата', 'Fecha', 'Tarih'),
(135, 'age', 'Age', 'العمر', 'Age', 'Âge', 'Alter', 'Eta', 'Idade', 'Возраст', 'Edad', 'Yaş'),
(136, 'connectivity_system', 'Connectivity System', 'نظام الصداقة:', 'Connectivity System:', 'Système de connectivité:', 'Communityart:', 'Connettività del Sistema:', 'Conectividade do sistema', 'Связь система:', 'Sistema de Conectividad:', 'Bağlantı Sistemi:'),
(137, 'connectivity_system_note', '&lt;span style=&quot;color:red;&quot;&gt;Note:&lt;/span&gt; If you change the system to another one, all existing followings, followers, friends will be deleted.&lt;/small&gt;', '&lt;span style=&quot;color:red;&quot;&gt;ملاحظة:&lt;/span&gt; عند تغيير نظام الصداقة كل الصداقات , المتابعات سوف تحذف من قاعدة البيانات , الرجاء الحذر !&lt;/small&gt;', '&lt;span style=&quot;color:red;&quot;&gt;Note:&lt;/span&gt; If you migrate from one system to another, all existing followings, followers, friends, memberships will be deleted to avoid issues.&lt;/small&gt;', '&lt;span style=&quot;color:red;&quot;&gt;Note:&lt;/span&gt; If you migrate from one system to another, all existing followings, followers, friends, memberships will be deleted to avoid issues.&lt;/small&gt;', '&lt;span style=&quot;color:red;&quot;&gt;Achtung:&lt;/span&gt; Wenn Du von einem System zu einem anderen migrierst, werden alle existierenden: Folger, Verfolger, Freunde, Mitgliedschaften gelöscht um Probleme zu vermeiden.&lt;/small&gt;', '&lt;span style=&quot;color:red;&quot;&gt;Nota:&lt;/span&gt; Se si esegue la migrazione da un sistema all&#039;altro, tutti i seguenti esistenti, seguaci, amici, appartenenze verranno eliminati per evitare problemi.&lt;/small&gt;', '&lt;span style=&quot;color:red;&quot;&gt;Observa&amp;ccedil;&amp;atilde;o:&lt;/span&gt; Se voc&amp;ecirc; mudar o sistema, todos aqueles que voc&amp;ecirc; segue, que te seguem e seus amigos ser&amp;ccedil;o perdidos.&lt;/small&gt;', '&lt;span style=&quot;color:red;&quot;&gt;Примечание:&lt;/span&gt;  При переходе от одной системы к другой, все существующие Подписан, последователи, друзья, членство будет удален, чтобы избежать проблем.&lt;/small&gt;', '&lt;span style=&quot;color:red;&quot;&gt;Nota:&lt;/span&gt; Si migras de un sistema a otro, Toda la informaci&amp;oacute;n existente, seguidos, seguidores, etc.., ser&amp;aacute; eliminada para evitar conflictos.&lt;/small&gt;', '&lt;span style=&quot;color:red;&quot;&gt;Not:&lt;/span&gt; Eğer bir sistemden diğerine göç, tüm mevcut followings, takipçileri, arkadaşlar, üyelikleri sorunları önlemek için silinir.&lt;/small&gt;'),
(138, 'friends_system', 'Friends system', 'نظام الصداقة مثل فيسبوك', 'Friends system', 'Système d&#39;amis', 'Freundesystem', 'Sistema Amici', 'Sistema de amigos', 'Друзья система', 'Sistema de amigos', 'Arkadaşlar Sistemi');
INSERT INTO `Wo_Langs` (`id`, `lang_key`, `english`, `arabic`, `dutch`, `french`, `german`, `italian`, `portuguese`, `russian`, `spanish`, `turkish`) VALUES
(139, 'follow_system', 'Follow system', 'نظام المتابعة مثل تويتر', 'Follow system', 'système de suivi', 'Folgensystem', 'Sistema con seguire/Follow', 'Sistema de seguidores', 'Следуйте системы', 'Sistema de seguidores', 'Takip Sistemi'),
(140, 'max_upload_size', 'Max upload size for videos, images, sounds, and files', 'الحد الأقصى لحجم الرفع:', 'Max upload size:', 'Taille maximale de téléchargement:', 'Maximale Dateigröße zum hochladen:', 'Dimensione massima di upload:', 'Tamanho m&amp;aacute;ximo de v&amp;iacute;deos, imagens, sons e arquivos', 'Максимальный размер загрузки:', 'Limite de subida:', 'Max upload size:'),
(141, 'max_characters_length', 'Max characters length', 'الحد الأقصى لعدد الأحرف:', 'Max characters length:', 'Max longueur des caractères:', 'Maximale Zeichenlänge:', 'Lunghezza massima caratteri:', 'Max characters length', 'Макс символов длиной:', 'Limite de caracteres:', 'Maksimum yükleme boyutu:'),
(142, 'allowed_extenstions', 'Allowed extenstions', 'صيغ الملفات المسومح يها:', 'Allowed extenstions:', 'extensions autorisées:', 'Erlaubte Endungen:', 'Estensioni ammessi:', 'Extens&amp;otilde;es permitidas', 'Допустимые расширения:', 'Extensiones permitidas:', 'İzin Uzantıları:'),
(143, 'reCaptcha_setting', 'reCaptcha Setting', 'إعدادات الريكابتا', 'reCaptcha Setting', 'reCaptcha Cadre', 'reCaptcha Einstellungen', 'reCaptcha Impostazioni', 'Configura&amp;ccedil;&amp;atilde;o do reCaptcha', 'настройка ReCaptcha', 'Configuraci&amp;oacute;n reCaptcha', 'Tuttum Ayarı'),
(144, 'reCaptcha_key_is_required', 'reCaptcha key is required', 'مفتاح الريكابتشا مطلوب', 'reCaptcha key is required', 'reCaptcha clé est nécessaire', 'reCaptcha schlüßel ist erforderlich', 'reCaptcha Chiave e richesta', 'a chave do reCaptcha &amp;eacute; necess&amp;aacute;ria', 'Ключ ReCaptcha требуется', 'Llave de reCaptcha es requerida', 'Tuttum anahtarı gereklidir'),
(145, 'reCaptcha_key', 'reCaptcha Key', 'مفتاح الريكابتشا:', 'reCaptcha Key :', 'reCaptcha clé :', 'reCaptcha Schlüßel :', 'reCaptcha chiave :', 'chave do reCaptcha', 'Ключ ReCaptcha :', 'Llave de reCaptcha :', 'Tuttum Anahtarı:'),
(146, 'google_analytics', 'Google Analytics', 'تحليل غوغل', 'Google Analytics', 'Google Analytics', 'Google Analytics', 'Google Analytics', 'Google Analytics', 'Гугл Аналитика', 'Google Analytics', 'Google Analiz'),
(147, 'google_analytics_code', 'Google analytics code', 'كود لتحليل غوغل', 'Google analytics code:', 'Google code Google Analytics:', 'Google Analytics Code:', 'Google analytics Codice:', 'C&amp;oacute;digo do Google analytics', 'Гугл Аналитика код:', 'Google analytics code:', 'Google Analytics Kodu:'),
(148, 'cache_setting', 'Cache Setting', 'إعدادات الكاش', 'Cache Setting', 'cache Cadre', 'Cache Einstellungen', 'Cache Impostazioni', 'Configura&amp;ccedil;&amp;atilde;o de Cache', 'настройка кэша', 'Configuraci&amp;oacute;n de Cache', 'Önbellek Ayarı'),
(149, 'cache_recomended_clear', 'It&#039;s highly recommended to clear the cache.', 'انه من المستحين أن تمسح الكاش.', 'It&#039;s highly recommended to clear the cache.', 'Il est fortement recommandé de vider le cache.', 'Es ist sehr empfehlenswert den Cache zu säubern.', 'Si raccomanda di cancellare la cache.', 'É recomendável que você limpe o cache.', 'Это рекомендуется очистить кэш.', 'Es altamente recomendado limpiar la cache.', 'Oldukça önbelleği temizlemek için tavsiye edilir.'),
(150, 'cache_size', 'Cache Size:', 'حجم الكاش:', 'Cache Size:', 'Taille du cache:', 'Cachegröße:', 'Cache Dimensioni:', 'Tamanho do cache:', 'Размер кэша:', 'Tamaño de Cache:', 'Önbellek Boyutu:'),
(151, 'clear_cache', 'Clear Cache', 'مسح الكاش', 'Clear Cache', 'Vider le cache', 'Cache säubern', 'Pulisci Cache', 'Limpar Cache', 'Очистить кэш', 'Limpiar Cache', 'Önbelleği'),
(152, 'social_login', 'Social login', 'دخول التواصل الإجتماعي', 'Social login', 'Social login', 'Social Anmeldung', 'Social login', 'Login', 'Социальный вход', 'Social login', 'Sosyal giriş'),
(153, 'social_login_api', 'Social login API&#039;S', 'ال API الخاص ب الدخول بإستخدام التواصل الإجتماعي', 'Social login API&#039;S', 'Social login API&#39;S', 'Social Anmeldung API&#039;S', 'Social login API&#039;S', 'Login API', 'Социальный вход API &#039;S', 'APIS sociales', 'Sosyal giriş APIler'),
(154, 'facebook', 'Facebook', 'الفسبوك', 'Facebook', 'Facebook', 'Facebook', 'Facebook', 'Facebook', 'Facebook', 'Facebook', 'Facebook'),
(155, 'google', 'Google+', 'غوغل بلاس', 'Google+', 'Google+', 'Google+', 'Google+', 'Google+', 'Google+', 'Google+', 'Google+'),
(156, 'twitter', 'Twitter', 'تويتر', 'Twitter', 'Twitter', 'Twitter', 'Twitter', 'Twitter', 'Твиттер', 'Twitter', 'Twitter'),
(157, 'linkedin', 'Linkedin', 'لينكد إن', 'Linkedin', 'Linkedin', 'Linkedin', 'Linkedin', 'Linkedin', 'Linkedin', 'Linkedin', 'Linkedin'),
(158, 'vkontakte', 'Vkontakte', 'في كينتاكتا', 'Vkontakte', 'Vkontakte', 'Vkontakte', 'Vkontakte', 'Vkontakte', 'Вконтакте', 'Vkontakte', 'Vkontakte'),
(159, 'facebook_api', 'Facebook API', 'فيسبوك API', 'Facebook API', 'Facebook API', 'Facebook API', 'Facebook API', 'Facebook API', 'Facebook API', 'Facebook API', 'Facebook API'),
(160, 'google_api', 'Google API', 'غوغل API', 'Google API', 'Google API', 'Google API', 'Google API', 'Google API', 'Google API', 'Google API', 'Google API'),
(161, 'twitter_api', 'Twitter API', 'تويتر API', 'Twitter API', 'Twitter API', 'Twitter API', 'Twitter API', 'Twitter API', 'Твиттер API', 'Twitter API', 'Twitter API'),
(162, 'linkedin_api', 'Linkedin API', 'لينكد إن API', 'Linkedin API', 'Linkedin API', 'Linkedin API', 'Linkedin API', 'Linkedin API', 'Linkedin API', 'Linkedin API', 'Linkedin API'),
(163, 'vkontakte_api', 'Vkontakte API', 'في كينتاكتا API', 'Vkontakte API', 'Vkontakte API', 'Vkontakte API', 'Vkontakte API', 'Vkontakte API', 'Vkontakte API', 'Vkontakte API', 'Vkontakte API'),
(164, 'app_id', 'App ID', 'رقم التطبيق', 'App ID', 'App ID', 'App ID', 'App ID', 'App ID', 'App ID', 'ID de aplicaci&amp;oacute;n', 'App Kimliği'),
(165, 'app_secret_key', 'App Secret Key', 'مفتاح التطبيق', 'App Secret Key', 'App Secret Key', 'App Geheim Schlüssel', 'Chiave segreta delle app', 'Chave Secreta APP', 'App Секретный ключ', 'Llave secreta de aplicaci&amp;oacute;n', 'App Gizli Anahtarı'),
(166, 'login_with', 'Login with', 'تسجيل الدخول عن طريق:', 'Login with', 'Connectez-vous avec', 'Anmelden mit', 'Entra con', 'Logar com', 'Войдите с', 'Ingresar con', 'İle giriş'),
(167, 'id', 'ID', 'ID', 'ID', 'ID', 'ID', 'ID', 'ID', 'ID', 'ID', 'ID'),
(168, 'source', 'Source', 'المصدر', 'Source', 'La source', 'Quelle', 'Fonte', 'Source', 'Источник', 'Fuente', 'Kaynak'),
(169, 'status', 'Status', 'الحالة', 'Status', 'statut', 'Status', 'Stato', 'Status', 'Состояние', 'Estado', 'Statü'),
(170, 'pending', 'Pending', 'قيد الإنتظار', 'Pending', 'en attendant', 'Anstehend', 'In atesa', 'Pendente', 'В ожидании', 'Pendiente', 'Bekleyen'),
(171, 'first_name', 'First name', 'الإسم', 'First name', 'Prénom', 'Vorname', 'Nome', 'Primeiro nome', 'Имя', 'Nombre', 'İsim'),
(172, 'last_name', 'Last name', 'الكنية', 'Last name', 'Nom de famille', 'Nachname', 'Cognome', '&amp;uacute;ltimo nome', 'Фамилия', 'Apellido', 'Soyadı'),
(173, 'about_me', 'About me', 'عني', 'About me', 'A propos de moi', 'Über mich', 'Su di me', 'Sobre', 'Обо мне', 'Sobre mi', 'Benim hakkımda'),
(174, 'website', 'Website', 'الموقع الإلكتروني', 'Website', 'Website', 'Webseite', 'Sito Web', 'Website', 'Веб-сайт', 'Website', 'Web Sitesi'),
(175, 'action', 'Action', 'عمل ', 'Actie', 'action', 'Aktion', 'Azione', 'Açao', 'действие', 'Acción', 'Aksiyon'),
(176, 'show_more_users', 'Show more users', 'عرض المزيد من المستخدمين', 'Show more users', 'Afficher plus d&#039;utilisateurs', 'Zeige mehr Benutzer', 'Mostra piu utenti', 'Mostrar mais usuários', 'Показать больше пользователей', 'Mostrar m&amp;aacute;s usuarios', 'Daha fazla kullanıcı göster'),
(177, 'no_more_users_to_show', 'No more users to show', 'لا يوجد المزيد', 'No more users to show', 'Pas plus aux utilisateurs d&#039;afficher', 'Keine weiteren Benutzer', 'Nessun altro utente da mostrare', 'Não há mais usuários para mostrar', 'Нет больше пользователей, чтобы показать', 'No hay m&amp;aacute;s usuarios', 'Artık kullanıcılar göstermek için'),
(178, 'user_delete_confirmation', 'Are you sure you want to delete this user?', 'هل أنت متأكد من حفذ هذا المستخدم؟', 'Are you sure you want to delete this user?', 'Êtes-vous sûr de vouloir supprimer cet utilisateur?', 'Diesen Benutzer wirklich löschen?', 'Sei sicuro di voler eliminare questo utente?', 'Gostaria mesmo de deletar esse usuário?', 'Вы уверены, что хотите удалить этого пользователя?', '¿Seguro que deseas eliminar este amigo?', 'Bu kullanıcıyı silmek istediğinizden emin misiniz?'),
(179, 'post_delete_confirmation', 'Are you sure you want to delete this post?', 'هل أنت متأكد من حفذ هذا المنشور؟', 'Are you sure you want to delete this post?', 'Es-tu sur de vouloir supprimer cette annonce?', 'Diesen Beitrag wirklich löschen?', 'Sei sicuro di voler eliminare questo post?', 'Gostaria mesmo de deletar esse post?', 'Вы уверены, что хотите удалить это сообщение?', '¿Seguro que deseas eliminar est&amp;aacute; punlicaci&amp;oacute;n?', 'Bu mesajı silmek istediğinizden emin misiniz?'),
(180, 'show_more_posts', 'Show more posts', 'عرض المزيد من المنشورات', 'Show more posts', 'Montrer plus d&#39;articles', 'Zeige mehr Beiträge', 'Mostra gli altri messaggi', 'Mostrar mais posts', 'Показать еще записи', 'Mostrar m&amp;aacute;s publicaciones', 'Daha fazla mesajları göster'),
(181, 'no_more_posts', 'No more posts', 'لا يوجد المزيد', 'No more posts', 'Pas plus de postes', 'Keine weiteren Beiträge', 'Non ci sono altri post', 'Não existem mais postagens em sua região! Convide seus amigos!', 'Нет заметок для отображения', 'No hay mas publicaciones', 'Daha mesajlar yok'),
(182, 'no_posts_found', 'No posts found', 'لا يوجد منشورات', 'No posts found', 'Aucun post trouvé', 'Keine Beiträge gefunden', 'nesun post trovato', 'Nenhum post encontrado', 'Заметки не найдены', 'Publicaci&amp;oacute;n no encontrada', 'Mesajlar yok'),
(183, 'publisher', 'Publisher', 'الناشر', 'Publisher', 'Éditeur', 'Herausgeber', 'Editore', 'Publicador', 'опубликовал@', 'Editor', 'Yayımcı'),
(184, 'there_are_not_new_posts_for_now', 'There are not new post for now', 'لا يوجد منشورات جديدة', 'There are not new post for now', 'Il n&#39;y a pas pour le moment nouveau poste', 'Derzeit keine neuen Beiträge', 'Nessun nuovo post per ora', 'Nenhum post novo', 'На данный момент нет новых сообщений', 'No hay mas publicaciones por ahora', 'Henüz okunmamış Mesaja vardır'),
(185, 'post_link', 'Post link', 'رابط المنشور', 'Post link', 'lien Poster', 'Beitragslink', 'Post link', 'Link do post', 'Заметка ссылка', 'Publicar link', 'Mesaj bağlantı'),
(186, 'time', 'Time', 'الوقت', 'Time', 'Temps', 'Zeit', 'Orario', 'Tempo', 'Время', 'Hora', 'Zaman'),
(187, 'post', 'Post', 'المنشور', 'Post', 'Poster', 'Beitrag', 'Post', 'Post', 'Заметка', 'Publicacion', 'Mesaj'),
(188, 'no_posts_yet', '{name} has not posted anything yet', '{name} لم ينشر أي منشور بعد.', '{name} has not posted anything yet', '{name} n&#39;a pas encore posté rien', '{name} hat noch keine Beiträge erstellt', '{name} non ha pubblicato ancora nulla', '{name} n&amp;atilde;o postou nada ainda', '{name} еще ничего не опубликовано', '{name} no ha publicado nada a&amp;uacute;n', '{name} bir şey yayınlamadı'),
(189, 'search', 'Search', 'بحث', 'Search', 'Recherche', 'Suche', 'Cerca', 'Procurar', 'Поиск', 'Buscar', 'Aramak'),
(190, 'on', 'On', 'تفعيل', 'On', 'Sur', 'An', 'Attivo', 'On', 'Открыт', 'Encender', 'Açık'),
(191, 'off', 'Off', 'إالغاء', 'Off', 'De', 'Aus', 'Spento', 'Off', 'Закрыт', 'Apagar', 'Kapalı'),
(192, 'save', 'Save', 'حفظ', 'Save', 'sauvegarder', 'Speichern', 'Salva', 'Salvar', 'Сохранить', 'Guardar', 'Kaydet'),
(193, 'saved', 'Saved !', 'تم الحفظ !', 'Saved !', 'Enregistré !', 'Gespeichert!', 'Salvato !', 'Salvo !', 'Сохранено!', '¡ Guardado !', 'Kaydedilen!'),
(194, 'reporter', 'Reporter', 'البالغ', 'Reporter', 'Journaliste', 'Meldungen', 'Rapporter', 'Usuário', 'Отчет', 'Reportes', 'Muhabir'),
(195, 'time_reported', 'Time Reported', 'زمن التبليغ', 'Time Reported', 'temps Rapporté', 'Meldungs Zeit', 'Tempo Segnalato', 'Hor&amp;aacute;rio', 'Время отчета', 'Hora reportada', 'Bildiren Zaman'),
(196, 'there_are_no_new_reports', 'There are no new reports for now.', 'لا يوجد تبليغات جديدة', 'There are no new reports for now.', 'Il n&#39;y a pas de nouveaux rapports pour l&#39;instant.', 'Derzeit keine neuen Meldungen.', 'Non ci sono nuovi report per ora.', 'Nenhum report novo.', 'На данный момент нет новых отчетов.', 'No hay nuevos reportes por ahora.', 'Henüz yeni raporlar vardır.'),
(197, 'open_post', 'Open Post', 'أفتح المنشور', 'Open Post', 'Ouvrir Poste', 'Beitrag öffnen', 'Apri il post', 'Abrir Post', 'Открыть заметку', 'Abrir publicaci&amp;oacute;n', 'Henüz Raporlar Vardır Yeni.'),
(198, 'mark_safe', 'Mark Safe', 'تعيين كآمن', 'Mark Safe', 'Mark Safe', 'Als sicher markieren', 'Mark sicuro', 'Marcar como seguro', 'Безопасно', 'Marcar como seguro', 'Mark Güvenli'),
(199, 'delete_post', 'Delete Post', 'حذف', 'Delete Post', 'Delete Post', 'Beitrag löschen', 'Cancella questo Post', 'Deletar Post', 'Удалить заметку', 'Eliminar publicaci&amp;oacute;n', 'Sil'),
(200, 'advertisement_setting', 'Advertisement Setting', 'إعدادات الإعلانات', 'Advertisement Setting', 'Cadre Publicité', 'Werbeeinstellungen', 'Impostazione Pubblicità', 'Configura&amp;ccedil;&amp;otilde;es de divulga&amp;ccedil;&amp;atilde;o', 'Реклама настройки', 'Configuraci&amp;oacuten; de avisos', 'Reklam Ayarı'),
(201, 'more_setting', 'More Setting', 'المزيد', 'More Setting', 'plus Cadre', 'Mehr Einstellungen', 'Piu Impostazioni', 'Mais configura&amp;ccedil;&amp;otilde;es', 'Расширенные настройки', 'M&amp;aacute;s configuraciones', 'Daha Ayar'),
(202, 'mailing_users', 'Mailing list', 'القائمة البريدية', 'Mailing list', 'Liste de diffusion', 'Mail an alle User', 'Lista di email', 'Lista dos emails', 'Список рассылки', 'Lista de correos', 'Mail listesi'),
(203, 'send', 'Send', 'إرسال', 'Send', 'Envoyer', 'Senden', 'Invia', 'Enviar', 'Отправить', 'Enviar', 'Gönder'),
(204, 'mailing_subject', 'Subject..', 'الموضوع..', 'Subject..', 'Assujettir..', 'Gegenstand..', 'Subject..', 'Título..', 'Тема..', 'Tema..', 'Konu ..'),
(205, 'mailing_message', 'Message..', 'الرسالة..', 'Message..', 'Message..', 'Nachricht..', 'Messaggio..', 'Mensagem..', 'Сообщение..', 'Mensaje..', 'Mesaj..'),
(206, 'announcement', 'Announcement', 'تصريح', 'Announcement:', 'Annonce:', 'Ankündigung:', 'Annuncio:', 'Aviso', 'Объявление:', 'Anuncio:', 'Duyuru:'),
(207, 'new_announcement', 'New announcement', 'تصريح جديد', 'New announcement ..', 'nouvelle annonce ..', 'Neue Ankündigung ..', 'Nuovo annuncio ..', 'Novo aviso', 'Новое объявление...', 'Nuevo anuncio ..', 'Yeni duyuru ..'),
(208, 'add', 'Add', 'إضافة', 'Add', 'Ajouter', 'Hinzufügen', 'Aggiungi', 'Add', 'Добавить', 'Agregar', 'Ekle'),
(209, 'views', 'Views', 'الآراء', 'Uitzichten', 'Vues', 'Ansichten', 'Visualizzazioni', 'Visualizações', 'Просмотры', 'Puntos de vista', 'Görüntüler'),
(210, 'there_are_no_active_announcements', 'There are no active announcements.', 'لا يوجد تصريحات مفعلة', 'There are no active announcements.', 'Il n&#39;y a pas d&#39;annonces actives.', 'Derzeit keine aktiven Ankündigungen.', 'Non ci sono annunci attivi.', 'Nenhum aviso novo.', 'Нет активных объявлений.', 'No hay avisos activos.', 'Aktif duyurular hiç yok.'),
(211, 'there_are_no_inactive_announcements', 'There are no inactive announcements.', 'لايوجد تصريحات غير مفعلة', 'There are no inactive announcements.', 'Il n&#39;y a aucune annonce inactifs.', 'Derzeit keine Inaktiven Ankündigungen.', 'Non ci sono annunci inattivi.', 'Nenhum aviso inativo.', 'Нет неактивных объявления.', 'No hay avisos inactivos.', 'Aktif değil duyurular hiç yok.'),
(212, 'add_friend', 'Add Friend', 'إضافة صديق', 'Voeg toe', 'Ajouter', 'Als Freund', 'Aggiungi Amico', 'Adicionar como amigo', 'Добавить друга', 'Agregar amigo', 'Arkadaş ekle'),
(213, 'follow', 'Follow', 'متابعة', 'Volgen', 'Suivre', 'folgen', 'Segui', 'Seguir', 'Следовать', 'Seguir', 'Takip et'),
(214, 'following', 'Following', 'متابَعون', 'Volgend', 'Suivant', 'folgt', 'Following', 'Seguindo', 'Следую', 'Siguiendo', 'Aşağıdaki'),
(215, 'following_btn', 'Following', 'متابع', 'Volgend', 'Suivant', 'folgt', 'Following', 'Seguindo', 'Следую', 'Siguiendo', 'Aşağıdaki'),
(216, 'followers', 'Followers', 'متابِعون', 'Volgers', 'Les adeptes', 'verfolger', 'Followers', 'Seguidor', 'Подписчики', 'Seguidores', 'İzleyiciler'),
(217, 'message', 'Message', 'رسالة', 'Stuur bericht', 'Message', 'Nachricht', 'Messaggio', 'Mensagem', 'Сообщение', 'Mensaje', 'Mesaj'),
(218, 'requested', 'Requested', 'طلب', 'Aangevraagd', 'Demandé', 'Gewünscht', 'richiesto', 'Requeridos', 'запрошенный', 'Pedido', 'İstenen'),
(219, 'friends_btn', 'Friends', 'الإصدقاء', 'Vrienden', 'Friends', 'Freunde', 'Amici', 'Amigos', 'Друзья', 'Amigos', 'Arkadaşlar'),
(220, 'online', 'Online', 'متصل', 'Online', 'Online', 'Online', 'In Linea', 'Online', 'Онлайн', 'Conectado', 'Çevrimiçi'),
(221, 'offline', 'Offline', 'غير متصل', 'Offline', 'Offline', 'Offline', 'Offline', 'Offline', 'Оффлайн', 'Desconectado', 'Çevrimdışı'),
(222, 'you_are_currently_offline', 'You are currently offline', 'غير متصل', 'Je bent momenteel offline', 'Vous êtes actuellement déconnecté', 'Du wirst als Offline angezeigt!', 'Attualmente sei offline', 'Você esta offline', 'На данный момент вы в оффлайн', 'Est&amp;aacute;s actualmente desconectado.', 'Şu anda çevrimdışı olan'),
(223, 'no_offline_users', 'No offline users.', 'لا يوجد أصدقاء غير متصلين', 'Geen offline mensen.', 'Aucun utilisateur hors ligne.', 'Freunde Offline.', 'Nessun utente in offline.', 'Nenhum usuário offline.', 'Нет пользователей оффлайн.', 'Sin usuarios desconectados.', 'Hiçbir çevrimdışı kullanıcıları.'),
(224, 'no_online_users', 'No online users.', 'لا يوجد أصدقاء متصلين', 'Geen online mensen.', 'Aucun membre en ligne.', 'Freunde Online.', 'Nessun utente in linea.', 'Nenhum usuário Online.', 'Нет пользователей онлайн.', 'Sin usuarios conectados.', 'Hiçbir online kullanıcılar.'),
(225, 'search_for_users', 'Search for users', 'إبحث عن مستخدمين', 'Zoek mensen', 'Recherche d&#039;utilisateurs', 'Freund suchen', 'Cerca per utente', 'Procurar usuários', 'Поиск пользователей', 'Buscar usuarios', 'Kullanıcıları için ara'),
(226, 'no_users_found', 'No users found.', 'لا يوجد نتائج', 'Geen mensen gevonden.', 'Aucun utilisateur trouvé.', 'Leider kein Treffer.', 'nessun utente trovato.', 'Nenhum usuário encontrado.', 'Не найдено ни одного пользователя.', 'Usuario no encontrado.', 'Kullanıcı bulunamadı.'),
(227, 'seen', 'Seen', 'تمت القراءة', 'Gezien', 'vu', 'Gesehen', 'Visto', 'Visto', 'посещений', 'Visto', 'Görülme'),
(228, 'load_more_posts', 'Load more posts', 'تحميل المزيد من المنشورات', 'Laad meer berichten', 'Chargez plus de postes', 'Mehr Beiträge laden', 'Carica piu notizie', 'Carregar mais posts', 'Загрузка заметок', 'Cargar m&amp;aacute;s publicaciones', 'Daha fazla Mesajları yükle'),
(229, 'load_more_users', 'Load more users', 'تحميل المزيد من المستخدمين', 'Laad meer mensen', 'Charger plusieurs utilisateurs', 'Mehr Benutzer laden', 'Carica piu utenti', 'Carregar mais usuários', 'Загрузить больше', 'Cargar m&amp;aacute;S usuarios', 'Daha fazla kullanıcı yükle'),
(230, 'there_are_no_tags_found', 'No results found for your query.', 'لا يوجد منشورات حول هذه التاغ', 'Geen resultaten gevonden.', 'Aucun résultat n&#39;a été trouvé pour votre recherche.', 'Keine Ergebnisse für deine Anfrage gefunden.', 'Nessun risultato corrisponde alla tua richiesta.', 'Nenhum resultado encontrado.', 'Не найдено ни одной метки.', 'Sin resultados para tu b&amp;uacute;squeda.', 'Bulunan hiçbir etiket bulunmamaktadır.'),
(231, 'there_are_no_saved_posts', 'You don&#039;t have any saved posts.', 'لا يوجد منشورات محفوظة', 'Je hebt geen opgeslagen berichten.', 'Vous ne disposez pas de messages enregistrés.', 'Keine gespeicherten Beiträge.', 'Non avete nessun post salvato.', 'Você não tem nenhum post salvo.', 'Нет сохраненных заметок.', 'No tienes ning&amp;uacute;na publicaci&amp;oacute;n guardada.', 'Kaydedilmiş bir konu bulunmuyor.'),
(232, 'messages', 'Messages', 'الرسائل', 'Berichten', 'Messages', 'Nachrichten', 'Messaggi', 'Mensagens', 'Переписка', 'Mensajes', 'Mesajlar'),
(233, 'write_something', 'Write Something ..', 'أكتب رسالة ..', 'Schrijf iets ..', 'Écrire quelque chose ..', 'Schreibe etwas..', 'Scrivi qualcosa ..', 'Escreva algo ..', 'Напишите что-нибудь...', 'Escribe algo ..', 'Bir şey yaz ..'),
(234, 'no_more_message_to_show', 'No more message', 'لا يوجد رسائل', 'Geen berichten om weer te geven', 'Pas plus un message', 'Keine weiteren Nachrichten', 'Niente più messaggi', 'Nenhuma mensagem nova', 'Нет больше сообщений', 'No hay mensajes', 'Artık mesajı'),
(235, 'view_more_messages', 'View more messages', 'تحميل المزيد من الرسائل', 'Bekijk meer berichten', 'Voir plus de messages', 'Mehr Nachrichten ansehen', 'Vedi più messaggi', 'Ver mais mensagens', 'Посмотреть больше сообщений', 'Ver m&amp;aacute;s mensajes', 'Daha fazla ileti görüntüle'),
(236, 'sorry_cant_reply', 'Sorry, you can&#039;t reply to this user.', 'عذراَ لا يمكنك إرسال رسالة لهذا المستخدم.', 'Sorry, je kan niet reageren op dit bericht.', 'Désolé, vous ne pouvez pas répondre à cet utilisateur.', 'Du kannst diesem Benutzer nicht antworten.', 'Siamo spiacenti, non è possibile rispondere a questo utente.', 'Você não pode responder este usuário', 'Извините, вы не можете ответить.', 'Disculpa, no puedes responder a este usuario.', 'Maalesef, bu kullanıcı cevap veremezsiniz.'),
(237, 'choose_one_of_your_friends', 'Choose one of your friends', 'أخنر واحداَ من أصدقائك', 'Selecteer een van je vrienden', 'Choisissez un de vos amis', 'Wähle einen deiner Freunde', 'Scegli uno dei tuoi amici', 'Escolha um de seus amigos', 'Выберите одного из ваших друзей', 'Elige uno de tus amigos', 'Arkadaşlarınızla birini seçin'),
(238, 'and_say_hello', 'And Say Hello !', 'و قل له مرحباً !', 'En zeg Hallo !', 'Et dire Bonjour !', 'und sage Hallo!', 'E dire Ciao !', 'E diga ol&amp;aacute; !', 'И скажите что-нибудь!', '¡ Y dile algo!', 'Ve Merhaba Deyin!'),
(239, 'download', 'Download', 'تحميل', 'Download', 'Télécharger', 'Herunterladen', 'Download', 'Download', 'Скачать', 'Descargar', 'İndir'),
(240, 'update_your_info', 'Update your info', 'تحديث المعلومات الخاصة بك', 'Update je informatie', 'Mettre à jour vos informations', 'Aktualisiere deine Informationen', 'Aggiorna le tue informazioni', 'Atualizar sua informa&amp;ccedil;&amp;atilde;o', 'Обновите свою информацию', 'Actualizar tu informaci&amp;oacute;n', 'Bilgilerinizi güncelleyin'),
(241, 'choose_your_username', 'Choose your username:', 'أختر اسم مستخدم خاص بك :', 'Kies een gebruikersnaam:', 'Choisissez votre nom d&#039;utilisateur:', 'Wähle deinen Benutzernamen:', 'Scegli il tuo username:', 'Escolha seu nome de usuário:', 'Выберите ваше имя пользователя:', 'Escoje tu nombre de usuario:', 'Kullanıcı adınızı seçin:'),
(242, 'create_your_new_password', 'Create your new password:', 'أنشء كملنة المرور:', 'Geef een nieuw wachtwoord op:', 'Créer votre nouveau mot de passe:', 'Erstelle dein neues Passwort:', 'Crea la tua nuova password:', 'Nova Senha:', 'Создать новый пароль:', 'Crear tu nueva contrase&amp;ntilde;a:', 'Yeni şifrenizi oluşturun:'),
(243, 'update', 'Update', 'تحديث', 'Update', 'Mettre à jour', 'Aktualisieren', 'Aggiorna', 'Atualizar', 'Обновить', 'Actualizar', 'Güncelleme'),
(244, 'delete_comment', 'Delete Comment', 'حذف التعليق', 'Verwijder reactie', 'supprimer les commentaires', 'Kommentar löschen', 'Ellimina il commento', 'Deletar comentário', 'Удалить комментарий', 'Eliminar comentario', 'Yorum Sil'),
(245, 'confirm_delete_comment', 'Are you sure that you want to delete this comment ?', 'هل أنت متاكد من حذف هذا التعليق ؟', 'Weet je zeker dat je deze reactie wil verwijderen?', 'Etes-vous sûr que vous voulez supprimer ce commentaire ?', 'Diesen Kommentar wirklich löschen ?', 'Sei sicuro di voler eliminare questo commento ?', 'Deletar comentário ?', 'Вы уверены, что хотите удалить этот комментарий?', '¿ Seguro que deseas eliminar est&amp;eacute; comentario ?', 'Bu yorumu silmek istediğinizden emin misiniz?'),
(246, 'confirm_delete_post', 'Are you sure that you want to delete this post ?', 'هل أنت متاكد من حذف هذا المنشور ؟', 'Weet je zeker dat je dit bericht wil verwijderd?', 'Etes-vous sûr que vous voulez supprimer ce message ?', 'Diesen Beitrag wirklich löschen ?', 'Sei sicuro di voler eliminare questo post?', 'Deletar post ?', 'Вы уверены, что хотите удалить эту заметку?', '¿ Seguro que deseas eliminar est&amp;aacute; publicaci&amp;oacute;n?', 'Eğer bu mesajı silmek istediğinizden emin misiniz?'),
(247, 'edit_post', 'Edit Post', 'تعديل', 'Wijzig bericht', 'Modifier le message', 'Betrag bearbeiten', 'Modifica Post', 'Editar Post', 'Редактировать заметку', 'Editar Publicaci&amp;oacute;n', 'Düzenle'),
(248, 'session_expired', 'Session Expired', 'انتهت الجلسة !', 'Sessie is verlopen', 'La session a expiré', 'Sitzung abgelaufen', 'Sessione scaduta', 'Sess&amp;ccedil;o expirada', 'Время сессии истекло', 'Sesi&amp;oacute;n Expirada', 'Oturum süresi doldu'),
(249, 'session_expired_message', 'Your Session has been expired, please login again.', 'لقد تم أنتهاء جلستك, الرجاء الدخول مرة أخرى', 'Je sessie is verlopen, log opnieuw in.', 'Votre session a expiré, s&#39;il vous plaît vous connecter à nouveau.', 'Deine Sitzung ist abgelaufen, bitte melde dich erneut an.', 'La tua sessione è scaduta, effettua il login di nuovo.', 'Sess&amp;ccedil;o expirada. Fa&amp;ccedil;a login para continuar.', 'Время сессии истекло, пожалуйста, войдите еще раз.', 'Tu sesi&amp;oacute;n ha expirado, ingresa nuevamente.', 'Sizin Oturum süresi dolmuş olması, tekrar giriş yapınız.'),
(250, 'country', 'Country', 'البلد', 'Land', 'Pays', 'Land', 'Nazione', 'Pa&amp;iacute;s', 'Страна', 'Pa&amp;iacute;s', 'Ülke'),
(251, 'all', 'All', 'الكل', 'Alle', 'Tous', 'Alle', 'Tutti', 'Todos', 'Все', 'Todo', 'Hepsi'),
(252, 'gender', 'Gender', 'الجنس', 'Geslacht', 'Genre', 'Geschlecht', 'Genere', 'Genero', 'Пол', 'Genero', 'Cinsiyet'),
(253, 'female', 'Female', 'أنثى', 'Vrouw', 'Femelle', 'Weiblich', 'Femmina', 'Mulher', 'Женский', 'Mujer', 'Dişi'),
(254, 'male', 'Male', 'ذكر', 'Man', 'Mâle', 'Männlich', 'Maschio', 'Homem', 'Мужской', 'Hombre', 'Erkek'),
(255, 'profile_picture', 'Profile Picture', 'الصورة الشخصية', 'Profielfoto', 'Photo de profil', 'Profilfoto', 'Immagine del profilo', 'Imagem de Perfil', 'Профиль фото', 'Imagen de perfil', 'Profil fotoğrafı'),
(256, 'result', 'Result', 'النتائج:', 'Resultaat:', 'Résultat:', 'Ergebnis:', 'Risultato:', 'Resultado', 'Результат:', 'Resultado:', 'Sonuç:'),
(257, 'yes', 'Yes', 'نعم', 'Ja', 'Oui', 'Ja', 'Si', 'Sim', 'Да', 'Si', 'Evet'),
(258, 'no', 'No', 'لا', 'Nee', 'Non', 'Nein', 'No', 'Não', 'Нет', 'No', 'Hayır'),
(259, 'verified_user', 'Verified User', 'حساب موثّق', 'Bekende Babster', 'vérifié utilisateur', 'Verifiziertes Mitglied', 'Utente Verificato', 'Contribuidor', 'Проверенный пользователь', 'Usuario Verificado', 'Doğrulanmış Kullanıcı'),
(260, 'change_password', 'Change Password', 'تغير كلمة المرور', 'Wijzig Wachtwoord', 'Changer le mot de passe', 'Passwort ändern', 'Cambiare la password', 'Mudar Senha', 'Изменить пароль', 'Cambiar contrase&amp;ntilde;a', 'Şifre değiştir'),
(261, 'current_password', 'Current Password', 'كلمة المرور الحالية', 'Huidig wachtwoord', 'Mot de passe actuel', 'Aktuelles Passwort', 'Password attuale', 'Senha Atual', 'Текущий пароль', 'Contrase&amp;ntilde;a actual', 'Şifreniz'),
(262, 'repeat_password', 'Repeat password', 'أعادة كلمة المرور', 'Herhaal wachtwoord', 'Répéter le mot de passe', 'Passwort wiederholen', 'Ripeti la password', 'Confirme sua senha atual', 'Повторите пароль', 'Repetir contrase&amp;ntilde;a', 'Şifreyi tekrar girin'),
(263, 'general', 'General', 'العامة', 'Algemeen', 'Général', 'Allgemein', 'Generale', 'Geral', 'Основные', 'General', 'Genel'),
(264, 'profile', 'Profile', 'الصفحة الشخصية', 'Profiel', 'Profil', 'Profil', 'Profilo', 'Perfil', 'Профиль', 'Perfil', 'Profil'),
(265, 'privacy', 'Privacy', 'الخصوصية', 'Privacy', 'Intimité', 'Privatsphäre', 'Privacy', 'Privacidade', 'Конфиденциальность', 'Privacidad', 'Gizlilik'),
(266, 'delete_account', 'Delete Account', 'حذف الحساب', 'Verwijder je account', 'Effacer le compte', 'Konto löschen', 'Ellimina Account', 'Deletar conta', 'Удалить аккаунт', 'Eliminar cuenta', 'Hesabım sil'),
(267, 'delete_account_confirm', 'Are You sure that you want to delete your account, and leave our network ?', 'هل أنت متأكد من حذف حسابك , وترك موقعنا ؟', 'Weet je zeker dat je je account voor goed wil verwijderen?', 'Etes-vous sûr que vous voulez supprimer votre compte, et de laisser notre réseau ?', 'Möchtest du Dein Konto wirklich löschen und &quot;wen-kennt-wer&quot; verlassen?', 'Sei sicuro di voler eliminare il tuo account, e lasciare la nostra rete?', 'Deletar conta e sair da nossa rede social ?', 'Вы уверены, что хотите удалить свой аккаунт, и оставить нашу сеть?', '¿ Seguro que deseas eliminar tu cuenta ?', 'Hesabınızı silmek ve ağımızı ayrılmak istediğinizden emin misiniz?'),
(268, 'delete_go_back', 'No, I&#039;ll Think', 'لا , ليس الآن.', 'Nee, liever niet', 'Non, je vais y réfléchir', 'Ich möchte nochmal eine Nacht drüber schlafen', 'No, ci penserò', 'N&amp;atilde;o, irei pensar melhor.', 'Нет, я подумаю', 'No, fue un error', 'Hayır, bence olacak'),
(269, 'verified', 'Verified', 'توثيق', 'Geverifieerd', 'vérifié', 'Verifiziert', 'Verificato', 'Verificado', 'Подтвержден', 'Verificado', 'Doğrulanmış'),
(270, 'not_verified', 'Not verified', 'غير موثّق', 'Niet Geverifieerd', 'non vérifié', 'Nicht verifiziert', 'Non Verificato', 'Não é verificado', 'Не подтвержден', 'No verificado', 'Doğrulanmadı'),
(271, 'admin', 'Admin', 'مدير', 'Admin', 'Administrateur', 'Admin', 'Administratore', 'Admin', 'Админ', 'Administrador', 'Yönetici'),
(272, 'user', 'User', 'مستخدم', 'Gebruiker', 'Utilisateur', 'Benutzer', 'Utente', 'Usuário', 'Пользователь', 'Usuario', 'Kullanıcı'),
(273, 'verification', 'Verification', 'التأكيد', 'Verificatie', 'Vérification', 'Verifizierung', 'Verifica', 'Verifica&amp;ccedil;&amp;atilde;o', 'Верификация', 'Verificaci&amp;oacute;n', 'Doğrulama'),
(274, 'type', 'Type', 'النوع', 'Type', 'Type', 'Typ', 'Tipo', 'Tipo', 'Тип', 'Tipo', 'Tip'),
(275, 'birthday', 'Birthday', 'تاريخ الميلاد', 'Geboortedatum', 'Anniversaire', 'Geburtstag', 'Compleano', 'Anivers&amp;aacute;rio', 'Дата рождения', 'Cumplea&amp;ntilde;os', 'Doğum Günü'),
(276, 'active', 'Active', 'مفعل', 'Actief', 'actif', 'Aktiv', 'Attivo', 'Ativo', 'Активный', 'Activo', 'Aktif'),
(277, 'inactive', 'inactive', 'غير مفعل', 'Inactief', 'inactif', 'Inaktiv', 'Innativo', 'Desativar Login', 'Неактивный', 'Inactivo', 'Pasif'),
(278, 'privacy_setting', 'Privacy Setting', 'إعدادات الخصوصية', 'Privacy Instellingen', 'Paramètre de confidentialité', 'Privatsphäre Einstellungen', 'Impostazione di Privacy', 'Configura&amp;ccedil;&amp;otilde;es de privacidade', 'Настройки конфиденциальности', 'Configuraci&amp;oacute;n de privacidad', 'Gizlilik ayarı'),
(279, 'follow_privacy_label', 'Who can follow me ?', 'من يستطيع متابعتي ؟', 'Wie kan mij volgen?', 'Qui peut me suivre ?', 'Wer darf mir folgen?', 'Chi può seguirmi?', 'Quem pode me seguir ?', 'Кто может следовать за мной?', '¿ Qui&amp;eacute;n puede seguirme ?', 'Kim beni takip edebilirim?'),
(280, 'everyone', 'Everyone', 'الكل', 'Iedereen', 'Toutes les personnes', 'Jeder', 'Tutti', 'Todos', 'Все', 'Todos pueden ver', 'Herkes'),
(281, 'my_friends', 'My Friends', 'أصدقائي', 'Mijn vrienden', 'Mes amis', 'Meine Freunde', 'Miei amici', 'Amigos', 'Мои друзья', 'Mis Amigos', 'Arkadaşlarım'),
(282, 'no_body', 'No body', 'لا أحد', 'Niemand', 'Personne', 'Niemand', 'Nessuno', 'Ningu&amp;eacute;m', 'Никто', 'Nadie', 'hiçbir vücut'),
(283, 'people_i_follow', 'People I Follow', 'أعضاء أتابعهم', 'Mensen die ik volg', 'Les gens que je suivre', 'Personen denen ich folge', 'Persone che Seguo', 'Pessoas que eu sigo', 'За кем я следую', 'Personas que sigo', 'İnsanlar izleyin'),
(284, 'people_follow_me', 'People Follow Me', 'أعضاء يتابعونني', 'Mensen die mij volgen', 'Les gens Follow Me', 'Persone die mir folgen', 'Persone che mi seguono', 'Pessoas que me seguem', 'Кто следует за мной', 'Personas que me sigue', 'İnsanlar beni takip etti'),
(285, 'only_me', 'Only me', 'أنا فقط', 'Alleen ik', 'Seulement moi', 'Nur ich', 'Solo Io', 'apenas eu', 'Только мне', 'Solo yo', 'Sadece ben'),
(286, 'message_privacy_label', 'Who can message me ?', 'من يستطيع إرسال رسالة لي ؟', 'Wie kan mij een bericht sturen?', 'Qui peut me message ?', 'Wer darf mir Nachrichten schreiben?', 'Chi può inviarmi i messaggi?', 'quem pode me enviar mensagem ?', 'Кто может отправлять мне сообщения?', '¿Qui&amp;eacute;n puede enviarme mensajes?', 'Kim bana mesaj olabilir?'),
(287, 'timeline_post_privacy_label', 'Who can post on my timeline ?', 'من يستطيع النشر على حائطي ؟', 'Wie kan mij krabbelen?', 'Qui peut poster sur mon calendrier ?', 'Wer darf an meine Pinwand schreiben?', 'Chi può postare su mia timeline?', 'quem pode postar na minha linha do tempo ?', 'Кто может публиковать на моей стене?', '¿Qui&amp;eacute;n puede publicar en mi perfil?', 'Benim zaman çizelgesi üzerinde kim gönderebilir?'),
(288, 'activities_privacy_label', 'Show my activities ?', 'إضهار إنشطتي', 'Laat mijn activiteiten zien?', 'Afficher mes activités ?', 'Zeige meine Aktivitäten?', 'Visualizza le mie attività?', 'Mostrar minhas atividades ?', 'Показывать мою деятельность?', '¿Mostrar mi actividad?', 'Benim faaliyetleri göster?'),
(289, 'show', 'Show', 'إظهار', 'Ja', 'Montrer', 'Zeigen', 'Mostra', 'Mostrar', 'Показать', 'Mostrar', 'Göster'),
(290, 'hide', 'Hide', 'أخفي', 'Nee', 'Cache', 'Verstecken', 'Nascondi', 'Esconder', 'Скрывать', 'Ocultar', 'Gizl'),
(291, 'confirm_request_privacy_label', 'Confirm request when someone follows you ?', 'قبول الطلب أو رفضه عندما يتابعك مستخدم ؟', 'Bevestig verzoek wanneer iemand jou volgt?', 'Confirmer la demande quand quelqu&#039;un vous suit ?', 'Anfrage bestätigen wenn mir jemand folgen möchte?', 'Confermare richiesta quando qualcuno ti segue?', 'Gostaria de escolher quem pode te seguir?', 'Подтверждать запрос когда кто-то следует за вами?', '¿Confirmar cuando alguien te sigue?', 'Birisi size izlediğinde isteği onaylayın?'),
(292, 'lastseen_privacy_label', 'Show my last seen ?', 'إظهار أخر ظهور ؟', 'Laat mijn laatst gezien zien?', 'Afficher mon dernière fois ?', 'Zeigen was ich zuletzt gesehen habe?', 'Mostra mia ultima visita?', 'Mostrar a última vez que você foi visto?', 'Показывать мой последний визит?', '¿Mostrar mi &amp;uacute;ltima conexi&amp;oacute;n?', 'Benim son görüldüğü göster?'),
(293, 'site_eg', '(e.g: http://www.siteurl.com)', '(مثال: http://www.enbrash.com)', '(e.g: http://www.siteurl.com)', '(e.g: http://www.siteurl.com)', '(z.B.: http://www.meine-seite.de)', '(Esempio: http://www.siteurl.com)', '(exemplo: http://www.siteurl.com)', '(например: http://www.siteurl.com)', '(e.j: http://www.siteurl.com)', '(örneğin: http://www.siteurl.com)'),
(294, 'profile_setting', 'Profile Setting', 'إعدادات الصفحة الشخصية', 'Profiel Instellingen', 'Profile Setting', 'Profil Einstellungen', 'Impostazioni Profilo', 'Configura&amp;ccedil;&amp;otilde;es de Perfil', 'Профиль настройки', 'Configuraci&amp;oacute;n de perfil', 'Profil Ayarı'),
(295, 'pinned_post', 'Pinned', 'مثبت', 'Vastgezete Bericht', 'épinglé', 'Angepinnt', 'Appuntato', 'Fixo', 'Важная', 'Anotado', 'Sabitlenmiş'),
(296, 'pin_post', 'Pin Post', 'تثبيت', 'Vastzetten', 'Pin Poster', 'Beitrag Anpinnen', 'Appunta un Post', 'Fixar post', 'Закрепить заметку', 'Anotar publicaci&amp;oacute;n', 'Pim'),
(297, 'unpin_post', 'Unpin Post', 'إلغاء تثبيت', 'Niet meer vastzetten', 'Détacher Poster', 'Beitrag Abpinnen', 'Rimuovi il Post Appuntato', 'Desafixar Post', 'Снять заметку', 'Des anotar publicaci&amp;oacute;n', 'Kaldırılıncaya'),
(298, 'open_post_in_new_tab', 'Open post in new tab', 'أفتح في صفحة جديدة', 'Open bericht in nieuw tapblad', 'Ouvrir dans un nouvel onglet après', 'Beitrag in neuem Fenster öffnen', 'Alberino aperto in una nuova scheda', 'Abrir post em uma nova aba', 'Открыть в новой вкладке', 'Abrir en nueva pestaña', 'Yeni sekmede aç sonrası'),
(299, 'unsave_post', 'Unsave Post', 'إلغاء حفظ المنشور', 'Verwijderen', 'unsave Poster', 'Beitrag nicht mehr speichern', 'Non salvare post', 'N&amp;atilde;o salvar post', 'Не сохранять заметку', 'No guardar publicaci&amp;oacute;n', 'Kaydetme Seçeneğini'),
(300, 'save_post', 'Save Post', 'حفظ المنشور', 'Opslaan', 'Sauvegarder Poster', 'Beitrag speichern', 'Salva Post', 'Salvar Post', 'Сохранить заметку', 'Guardar publicaci&amp;oacute;n', 'Kaydet Mesaj'),
(301, 'unreport_post', 'Unreport Post', 'إلغاء التبليغ', 'Verwijder Aangeven', 'Unreport Poster', 'Beitrag nicht mehr melden', 'Ellimina segnalazione di questo Post', 'Não reportar Post', 'Не жаловаться', 'Quitar reporte', 'Rapor sil'),
(302, 'report_post', 'Report Post', 'تبليغ المنشور', 'Bericht aangeven', 'Signaler le message', 'Beitrag melden', 'Segnala questo Post', 'Reportar Post', 'Пожаловаться', 'Reportar publicaci&amp;oacute;n', 'Rapor'),
(303, 'shared_this_post', 'Shared this post', 'شارك هذا المنشور', 'Heeft dit bericht gedeeld', 'Partagé ce post', 'hat diesen Beitrag geteilt', 'Condividi questo Post', 'Compartilhar post', 'поделился этой записью', 'Comparti&amp;oacute; est&amp;aacute; publicaci&amp;oacute;n', 'Bu yazı paylaştı'),
(304, 'changed_profile_picture_male', 'Changed his profile picture', 'غير صورته الشخصية', 'Heeft zijn profielfoto gewijzigd', 'Changé sa photo de profil', 'hat sein Profilbild geändert', 'Cambiato l&#039;immagine del profilo', 'Mudou sua imagem de perfil', 'изменил свою фотографию', 'Cambio su foto de perfil', 'Onun profil resimlerini değiştirdi'),
(305, 'changed_profile_picture_female', 'Changed her profile picture', 'غيرت صورتها الشخصية', 'Heeft haar profielfoto gewijzigd', 'A changé sa photo de profil', 'hat ihr Profilbild geändert', 'Cambiato sua immagine del profilo', 'Mudou sua imagem de perfil', 'изменила свою фотографию', 'Cambio su foto de perfil', 'Onun profil resimlerini değiştirdi'),
(306, 'post_login_requriement', 'Please log in to like, wonder, share and comment!', 'الرجاء الدخول لعمل إعجاب , تعجب , وكومنت !', 'Login om te respecteren, te reageren!', 'S&#039;il vous plaît vous connecter à aimer, étonnant, partager et commenter !', 'Bitte melde dich zuerst an!', 'Effettua il login per piacere, meraviglia, condividere e commentare!', 'Fa&amp;ccedil;a login para compartilhar, curtir, comentar, etc !', 'Пожалуйста войдите или зарегистрируйтесь, чтобы добавлять &quot;Мне нравится&quot; и комментарии!', '¡ Ingresa para dar Like, Comentar, Seguir y muchas cosas m&amp;aacute;s !', 'Merak, pay gibi ve Yorumlamak için giriş yapınız!'),
(307, 'likes', 'Likes', 'الإعجابات', 'Respects', 'Aime', 'Gefällt mir', 'Mi piace', 'Curtiu', 'Нравится', 'Me gusta', 'Beğeniler'),
(308, 'like', 'Like', 'إعجاب', 'Respect!', 'Aimer', 'Gefällt mir', 'Mi piace', '', 'Мне нравится', 'Me gusta', 'Beğen'),
(309, 'wonder', 'Wonder', 'تعجب', 'Wonder', 'Merveille', 'Wundert mich', 'Wonder', 'N&amp;atilde;o curtir', 'Удивляться!', 'Me sorprende', 'Merak et'),
(310, 'wonders', 'Wonders', 'التعجبات', 'Super Respects', 'Merveilles', 'Verwundert', 'Wonders', 'Dislikes', 'Удивляться', 'Me sorprende', 'Merakler'),
(311, 'share', 'Share', 'شارك', 'Delen', 'Partagez', 'Teilen', 'Condividi', '', 'Перепост', 'Compartir', 'Paylaş'),
(312, 'comments', 'Comments', 'التعليقات', 'Reacties', 'commentaires', 'Kommentare', 'Commenti', 'Comentários', 'Комментарии', 'Comentarios', 'Yorumlar'),
(313, 'no_likes', 'No likes yet', 'لا يوجد إعجابات', 'Geen respects', 'Aucune aime encore', 'Noch keine Gefällt mir Angaben', 'Non hai ancora un mi piace', 'Nenhuma curtida ainda', 'Пока нет &quot;Мне нравится&quot;', 'Sin Me Gusta', 'Hiç beğeniler yok'),
(314, 'no_wonders', 'No wonders yet', 'لا يوجد تعجبات', 'Geen super respects', 'Pas encore wondres', 'Noch keine Verwunderungen', 'Ancora nessun wondres', 'Nenhum dislike ainda', 'Неудивительно, пока', 'Sin Me Sorprende', 'Hiç merakler yok'),
(315, 'write_comment', 'Write a comment and press enter', 'اكتب تعليق وأضغط أنتر ..', 'Schrijf je reactie en druk dan op enter', 'Ecrire un commentaire et appuyez sur Entrée', 'Schreibe einen Kommentar und drücke Enter', 'Scrivi un commento e premere invio', 'Escreva um comentário...', 'Напишите комментарий и нажмите клавишу ВВОД', 'Escribe un comentario y presiona enter', 'Bir yorum yazın ve enter tuşuna basın ..'),
(316, 'view_more_comments', 'View more comments', 'إظهار المزيد من التعليقات', 'Bekijk meer reacties', 'Voir plus de commentaires', 'Mehr Kommentare zeigen', 'Visualizza più commenti', 'Vizualizar mais comentários', 'Посмотреть больше комментариев', 'Ver m&amp;aacute;s comentarios', 'Daha fazla yorum'),
(317, 'welcome_to_news_feed', 'Welcome to your News Feed !', 'أهلا بك في صفحة أحدث المنشورات !', 'Welkom op je tijdlijn !', 'Bienvenue à votre Nouvelles RSS!', 'Willkkommen in deinem News-Feed!', 'Benvenuto nel tuo News Feed!', 'Bem vindo as nossa notícias!', 'Добро пожаловать в ленту новостей!', '¡ Bienvenido a tu muro de noticias !', 'Hoş geldiniz!'),
(318, 'say_hello', 'Say Hello !', 'قل مرحباً !', 'Zeg snel Hallo !', 'Dis bonjour !', 'Sag Hallo!', 'Saluta !', 'Diga Ol&amp;aacute; !', 'Скажи привет!', '¡ Di hola !', 'Merhaba de !'),
(319, 'publisher_box_placeholder', 'What&#039;s going on? #Hashtag.. @Mention.. Link..', 'ما الذي يحصل الآن ؟ #هاشتاغ .. @إشارة', 'Hoe gaat het vandaag? #Hashtag.. @Vermeld..', 'Ce qui se passe? #hashtag ..@Mention..', 'Was ist los? #Hashtag.. @Erwähnen..', 'A cosa sti pensando? ..', 'Use #hashtags para descrever sua postagem', 'Что у Вас нового? #Хэштегом... @Упоминание...', '¿ Que est&amp;aacute;s pensando ? #Anime #lolis.. @Otakus..', 'Ne söylemek istersin ? #Hashtag .. @Mansiyon ..'),
(320, 'youtube_link', 'Youtube Link', 'رابط اليوتيوب', 'Youtube Link', 'Youtube Link', 'Youtube Link', 'Youtube Link', 'Youtube Link', 'Youtube Ссылка', 'Link de Youtube', 'Youtube Bağlantısık'),
(321, 'vine_link', 'Vine Link', 'رابط الفاين', 'Vine Link', 'Vine Link', 'Vine Link', 'Vine Link', 'Vine Link', 'Vine Ссылка', 'Link de Vine', 'Vine Bağlantı'),
(322, 'soundcloud_link', 'SoundCloud Link', 'رابط الساوندكلاود', 'SoundCloud Link', 'SoundCloud Link', 'SoundCloud Link', 'SoundCloud Link', 'SoundCloud Link', 'SoundCloud Ссылка', 'Link de SoundCloud', 'Soundcloud Bağlantı'),
(323, 'maps_placeholder', 'Where are you ?', 'أين أنت الآن ؟', 'Waar ben je ?', 'Où es tu?', 'Wo bist du?', 'Dove ti trovi?', 'Onde você esta?', 'Это где?', '¿ Donde est&amp;aacute;s ?', 'Neredesin ?'),
(324, 'post_label', 'Post', 'نشر', 'Plaats', 'Poster', 'LOS', 'Post', 'Post', 'Отправить', 'Publicar', 'Gönder'),
(325, 'text', 'Text', 'النصوص', 'Tekst', 'Envoyer des textos', 'Text', 'Testo', 'Texto', 'Заметки', 'Texto', 'Metin'),
(326, 'photos', 'Photos', 'الصور', 'Foto&#039;s', 'Photos', 'Fotos', 'Foto', 'Fotos', 'Фото', 'Fotos', 'Resimler'),
(327, 'sounds', 'Sounds', 'الموسيقى', 'muziek', 'Des sons', 'Musik', 'Musica', 'Sons', 'Аудио', 'Sonidos', 'Sesler'),
(328, 'videos', 'Videos', 'الفيديو', 'Video&#039;s', 'Les vidéos', 'Videos', 'Video', 'V&amp;iacute;deos', 'Видео', 'Videos', 'Videolar'),
(329, 'maps', 'Maps', 'الخرائط', 'Maps', 'Plans', 'Karten', 'Mappe', 'Mapas', 'Карты', 'Mapas', 'Haritalar'),
(330, 'files', 'Files', 'الملفات', 'Files', 'Dossiers', 'Dateien', 'File', 'Arquivos', 'Файлы', 'Archivos', 'Dosyalar'),
(331, 'not_following', 'Not following any user', 'لا يوجد متابِعين', 'Volgt nog geen mensen', 'Ne pas suivre tout utilisateur', 'folgt niemandem', 'Non seguire qualsiasi utente', 'N&amp;atilde;o segue ningu&amp;eacute;m', 'Не следовать', 'No sigues a ning&amp;uacute;n usuario', 'Herhangi kullanıcıları takip Değil'),
(332, 'no_followers', 'No followers yet', 'لا يوجد متابَعين', 'Heeft geen volgers', 'Pas encore adeptes', 'hat keine Verfolger', 'Non hai ancora nessuno che ti segue', 'Nenhum seguidor ainda', 'Пока нет последователей', 'Sin seguidores', 'Henüz takipçileri'),
(333, 'details', 'Details', 'المعلومات العامة', 'Details', 'Détails', 'Einzelheiten', 'Detagli', 'Detalhes', 'Информация', 'Detalles', 'Detaylar'),
(334, 'social_links', 'Social Links', 'الروابط الاجتماعية', 'Sociale Links', 'Liens sociaux', 'Sociallinks', 'Link Sociali', 'Redes Sociais', 'Социальные ссылки', 'Enlaces Sociales', 'Sosyal Bağlantılar'),
(335, 'online_chat', 'Chat', 'الأصدقاء المتصلين', 'Online vrienden', 'amis en ligne', 'Freunde Online', 'Utenti Attivi', 'Amigos Online', 'Друзья онлайн', 'Amigos Conectados', 'Çevrimiçi arkadaş'),
(336, 'about', 'About', 'حول', 'About', 'Sur', 'Über Uns', 'Su di noi', 'Sobre', 'О нас', 'Pin', 'Yaklaşık'),
(337, 'contact_us', 'Contact Us', 'إتصل بنا', 'Contact Us', 'Contactez nous', 'Kontaktiere uns', 'Contattaci', 'Contato', 'Контакты', 'Contacto', 'Bize Ulaşın'),
(338, 'privacy_policy', 'Privacy Policy', 'سياسة الخصوصية', 'Privacy Policy', 'politique de confidentialité', 'Datenschutz', 'Privacy Policy', 'Privacidade', 'Политика', 'Pol&amp;iacute;tica', 'Gizlilik Politikası'),
(339, 'terms_of_use', 'Terms of Use', 'شروط الاستخدام', 'Terms of Use', 'Conditions d&#39;utilisation', 'Nutzungsbedingungen', 'Condizioni d&#039;uso', 'Termos de Uso', 'Условия', 'Condiciones', 'Kullanım Şartları'),
(340, 'developers', 'Developers', 'المطورين', 'Developers', 'Développeurs', 'Entwickler', 'Sviluppatori', 'Desenvolvedores', 'Разработчикам', 'Developers', 'Geliştiriciler'),
(341, 'language', 'Language', 'اللغة', 'Language', 'Langue', 'Sprache', 'Lingua', 'Linguagem', 'Язык', 'Idioma', 'Dil'),
(342, 'copyrights', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}'),
(343, 'year', 'year', 'سنة', 'jaar', 'an', 'Jahr', 'Anno', 'ano', 'год', 'A&amp;ntilde;o', 'yıl'),
(344, 'month', 'month', 'شهر', 'maand', 'mois', 'Monat', 'mese', 'm&amp;ecirc;s', 'месяц', 'mes', 'ay'),
(345, 'day', 'day', 'يوم', 'dag', 'jour', 'Tag', 'giorno', 'dia', 'день', 'd&amp;iacute;a', 'gün'),
(346, 'hour', 'hour', 'ساعة', 'uur', 'heure', 'Stunde', 'ora', 'hora', 'час', 'hora', 'saat'),
(347, 'minute', 'minute', 'دقيقة', 'minuut', 'minute', 'Minute', 'minuto', 'minuto', 'минут', 'minuto', 'dakika'),
(348, 'second', 'second', 'ثانية', 'seconde', 'deuxième', 'Sekunde', 'secondo', 'segundo', 'секунд', 'segundo', 'saniye'),
(349, 'years', 'years', 'سنوات', 'jaren', 'années', 'Jahre', 'anni', 'anos', 'лет', 'a&amp;ntilde;os', 'yıllar'),
(350, 'months', 'months', 'اشهر', 'maanden', 'mois', 'Monate', 'messi', 'meses', 'месяцев', 'meses', 'aylar'),
(351, 'days', 'days', 'أيام', 'dagen', 'jours', 'Tage', 'giorni', 'dias', 'дней', 'd&amp;iacute;as', 'günler'),
(352, 'hours', 'hours', 'ساعات', 'uren', 'des heures', 'Stunden', 'ore', 'horas', 'часов', 'horas', 'saatler'),
(353, 'minutes', 'minutes', 'دقائق', 'minuten', 'minutes', 'Minuten', 'minuti', 'minutos', 'минут', 'minutos', 'dakika'),
(354, 'seconds', 'seconds', 'ثانية', 'seconden', 'secondes', 'Sekunden', 'secondi', 'segundos', 'секунд', 'segundos', 'saniye'),
(355, 'time_ago', 'ago', 'منذ', 'geleden', 'depuis', '', 'fa', 'atrás', 'назад', '', 'önce'),
(356, 'time_from_now', 'from now', 'من الآن', 'van nu', 'à partir de maintenant', 'ab jetzt', 'da adesso', 'agora', 'С этого момента', 'desde ahora', 'şu andan itibaren'),
(357, 'time_any_moment_now', 'any moment now', 'في أي لحظة الآن', 'een moment geleden', 'd un moment', 'jeden Moment', 'da un momento all&#039;altro', 'algum tempo atrás', 'В любой момент', 'cualquier momento', 'Şimdi her an'),
(358, 'time_just_now', 'Just now', 'ألآن', 'Net geplaats', 'Juste maintenant', 'Gerade eben', 'Proprio adesso', 'Neste momento', 'Прямо сейчас', 'Ahora', 'Şu anda'),
(359, 'time_about_a_minute_ago', 'about a minute ago', 'منذ دقيقة', 'een minuut geleden', 'Il ya environ une minute', 'Vor einer Minute', 'circa un minuto fa', 'minuto atrás', 'минуту назад', 'Hace un minuto', 'yaklaşık bir dakika önce'),
(360, 'time_minute_ago', '%d minutes ago', 'منذ %d دقائق', '%d minuten geleden', '%d il y a quelques minutes', 'vor %d Minuten', '%d minuti fa', '%d minutos atrás', '%d минут назад', 'hace %d minutos', '%d dakika önce'),
(361, 'time_about_an_hour_ago', 'about an hour ago', 'منذ ساعة', 'een uur geleden', 'il y a à peu près une heure', 'Vor einer Stunde', 'circa un&#039;ora fa', 'uma hora atrás', 'около часа назад', 'Hace una hora', 'yaklaşık bir saat önce'),
(362, 'time_hours_ago', '%d hours ago', 'منذ %d ساعات', '%d uren geleden', '%d il y a des heures', 'vor %d Stunden', '%d ore fa', '%d horas atrás', '%d часов назад', 'Hace %d horas', '%d saatler önce'),
(363, 'time_a_day_ago', 'a day ago', 'منذ يوم', 'a dag geleden', 'a Il ya jour', 'Gestern', 'a giorni fa', 'dia atrás', 'день назад', 'Hace un dia', 'bir gün önce'),
(364, 'time_a_days_ago', '%d days ago', 'منذ %d أيام', '%d dagen geleden', '%d il y a quelques jours', 'vor %d Tagen', '%d giorni fa', 'dias atrás', '%d дней назад', 'Hace %d dias', '%d gün önce'),
(365, 'time_about_a_month_ago', 'about a month ago', 'منذ شهر', 'een maand geleden', 'il y a environ un mois', 'vor einem Monat', 'circa un mese fa', 'um mês atrás', 'Около месяца назад', 'Hace un mes', 'yaklaşık bir ay önce'),
(366, 'time_months_ago', '%d months ago', 'منذ %d أشهر', '%d maanden geleden', '%d il y a des mois', 'vor %d Monaten', '%d mesi fa', '%d meses atrás', '%d месяц назад', 'Hace %d meses', '%d aylar önce'),
(367, 'time_about_a_year_ago', 'about a year ago', 'منذ سنة', 'een jaar geleden', 'Il ya environ un an', 'vor einem Jahr', 'circa un anno fa', 'um ano atr&amp;aacute;s', 'около года назад', 'Hace un año', 'yaklaşık bir yıl önce'),
(368, 'time_years_ago', '%d years ago', 'منذ %d سنوات', '%d jaar geleden', '%d il y a des années', 'vor %d Jahren', '%d anni fa', '%d anos atr&amp;aacute;s', '%d много лет назад', 'Hace %d años', '%d yıllar önce');
INSERT INTO `Wo_Langs` (`id`, `lang_key`, `english`, `arabic`, `dutch`, `french`, `german`, `italian`, `portuguese`, `russian`, `spanish`, `turkish`) VALUES
(369, 'latest_activities', 'Latest Activities', 'آخر النشاطات', 'Laatste Activiteiten', 'Dernières activités', 'Letzte Aktivitäten', 'Ultimi Attività', '&amp;uacute;ltimas atividades', 'Последнии действия', 'Actividad reciente', 'Son Aktiviteler'),
(370, 'no_activities', 'No new activities', 'لا يوجد نشاطات', 'Geen nieuwe activiteiten', 'Pas de nouvelles activités', 'Keine neuen Aktivitäten', 'Non ci sono nuove attività', 'Nenhuma atividade nova', 'Нет действий', 'No hay actividad reciente', 'Aktiviteler yok'),
(371, 'trending', 'Trending !', 'الهاشتاغات النشطة !', 'Populair!', 'Trending !', 'Im Trend!', 'Tendenza !', 'Assunto do momento !', 'Тенденции!', '¡ Popular !', 'Trend!'),
(372, 'load_more_activities', 'Load more activities', 'تحميل المزيد من النشاطات', 'Laad meer activiteiten', 'Chargez plus d&#39;activités', 'Weitere Aktivitäten laden', 'Carica altri attività', 'Carregar mais atividades', 'Загрузить больше', 'Cargar mas actividad', 'Daha fazla faaliyetleri yükle'),
(373, 'no_more_actitivties', 'No more actitivties', 'لا يوجد المزيد من النشاطات', 'Geen andere activiteiten', 'Pas plus d&#39;activités', 'Keine weiteren Aktivitäten', 'Nessun altro attività', 'Nenhuma atividade nova', 'Нет больше действий', 'No hay mas actividad', 'Daha faaliyetler yok'),
(374, 'people_you_may_know', 'People you may know', 'مستخدمين قد تعرفهم', 'Mensen die je misschien kent', 'Les gens que vous connaissez peut-être', 'Personen die Du vielleicht kennst', 'Persone che Potresti Conoscere', 'Pessoas que você talvez conheça', 'Люди, которых вы можете знать', 'Personas que tal vez conozcas', 'Tanıyor olabileceğin kişiler'),
(375, 'too_long', 'Too long', 'طويل جداَ', 'Te lang', 'Trop long', 'Zu Lang', 'Troppo lungo', 'Muito grande.', 'Слишком длинный', 'Muy largo', 'Too long'),
(376, 'too_short', 'Too short.', 'قصير جداَ', 'To kort.', 'Trop court.', 'Zu Kurz.', 'Troppo corto.', 'Muito curto.', 'Слишком короткий.', 'Muy corto.', 'Too short.'),
(377, 'available', 'Available !', 'متاح !', 'Beschikbaar!', 'Disponible !', 'Verfügbar!', 'A disposizione !', 'Available !', 'Доступный!', '¡ Disponible !', 'Available !'),
(378, 'in_use', 'In use.', 'مستخدم', 'In gebruik.', 'En service.', 'In Benutzung.', 'In uso.', 'Em uso.', 'В использовании.', 'En uso.', 'In use.'),
(379, 'username_invalid_characters_2', 'Username should not contain any special characters, symbols or spaces.', 'يجب اسم المستخدم أن لا يحتوي على أية أحرف خاصة أو رموز أو مسافات.', 'Gebruikersnaam mag geen speciale tekens bevatten.', 'Nom d&#039;utilisateur ne doit pas contenir de caractères, symboles ou espaces spéciaux.', 'Bitte keine Sonder- oder Leerzeichen verwenden.', 'Nome utente non deve contenere caratteri speciali, simboli o spazi.', 'O nome de usuário Não deve conter nenhum caractere especial, símbolo ou espaços.', 'Имя пользователя не должно содержать каких-либо специальных символов или пробелов.', 'Nombre de usuario no valido, no debe contener s&amp;iacute;mbolos, caracteres especiales o espacios.', 'Username should not contain any special characters, symbols or spaces.'),
(380, 'liked', 'Liked', 'معجب', 'Vond', 'A aimé', 'gefällt dir', 'Piacuto', 'Curtiu', 'Нравится', 'Me gusta', 'Beğendim'),
(381, 'my_pages', 'My Pages', 'صفحاتي', 'Mijn pagina&#039;s', 'Mes Pages', 'Meine Seiten', 'Mie Pagine', 'Minhas Páginas', 'Мои Страницы', 'Mis páginas', 'Benim Sayfalar'),
(382, 'liked_page', 'Liked your page ({page_name})', 'أعجب بصفحتك ({page_name})', 'Vond je pagina ({page_name})', 'Aimé votre page ({page_name})', 'gefällt Deine Seite ({page_name})', 'Piaciuto tua pagina ({page_name})', 'Curtiu sua p&amp;aacute;gina ({page_name})', 'нравится ваша страница ({page_name})', 'Me gustó tu página ({page_name})', 'Sayfanızı Beğendi ({page_name})'),
(383, 'this_week', 'This week', 'إعجابات هذا الإسبوع', 'Deze week', 'Cette semaine', 'in dieser Woche', 'Questa settimana', 'Essa semana', 'На этой неделе', 'Esta semana', 'Bu hafta'),
(384, 'posts', 'posts', 'المشاركات ', 'posts', 'des postes', 'Beiträge', 'messaggi', 'Postagens', 'сообщений', 'Mensajes', 'Mesajları'),
(385, 'located_in', 'Located in', 'موجود في', 'Gelegen in', 'Situé dans', 'Wohnt in', 'Situata in', 'Localiza&amp;ccedil;&amp;atilde;o', 'Город', 'Situado en', 'Bulunan'),
(386, 'phone_number', 'Phone', 'الهاتف', 'Telefoon', 'Téléphone', 'Telefon', 'Telefono', 'Telefone', 'Телефон', 'Teléfono', 'Telefon'),
(387, 'company', 'Company', 'شركة', 'Bedrijf', 'Compagnie', 'Unternehmen', 'Azienda', 'Empresa', 'Компания', 'Empresa', 'şirket'),
(388, 'category', 'Category', 'القسم', 'Categorie', 'Catégorie', 'Kategorie', 'Categoria', 'Categoria', 'Категория', 'Categoría', 'Kategori'),
(389, 'search_for_posts', 'Search for posts', 'إبحث عن منشورات', 'Zoeken naar berichten', 'Rechercher les messages', 'Nach Beiträgen suchen', 'Cerca messaggi', 'Procurar posts', 'Поиск заметок', 'Buscar mensajes', 'Mesajları ara'),
(390, 'create_new_page', 'Create New Page', 'إنشاء صفحة جديدة', 'Nieuwe pagina', 'Créer une page', 'Neue Seite erstellen', 'Crea nuova pagina', 'Criar uma nova p&amp;aacute;gina', 'Создать страницу', 'Crear nueva página', 'Yeni Sayfa Oluştur'),
(391, 'page_name', 'Page name', 'إسم الصفحة (الذي يظهر في رابط الصفحة)', 'Pagina naam', 'Nom de la page', 'Seitenname', 'Nome della Pagina', 'Nome da p&amp;aacute;gina', 'Название', 'Nombre de la página', 'Sayfa adı'),
(392, 'page_title', 'Page title', 'عنوان الصفحة', 'Pagina titel', 'Titre de la page', 'Seitentitel', 'Titolo della Pagina', 'Título da página', 'Заголовок', 'Título de la página', 'Sayfa başlığı'),
(393, 'your_page_title', 'Your page title', 'عنوان صفحتك', 'Uw pagina titel', 'Votre titre de la page', 'Dein Seitentitel', 'Il tuo titolo della Pagina', 'Título da sua página', 'Заголовок страницы', 'Tu página de título', 'Sizin sayfa başlık'),
(394, 'page_category', 'Page Category', 'القسم', 'Pagina Categorie', 'page Catégorie', 'Seiten-Kategorie', 'Categoria', 'Categoria da página', 'Категория', 'Página Categoría', 'Sayfa Kategori'),
(395, 'page_description', 'Page description', 'حول الصفحة', 'Pagina beschrijving', 'Description de la page', 'Seitenbeschreibung', 'Descrivi la tua pagina', 'Descri&amp;ccedil;&amp;atilde;o da p&amp;aacute;gina', 'Описание страницы', 'Descripción de la página', 'Sayfa açıklaması'),
(396, 'page_description_info', 'Your Page description, Between 10 and 200 characters max.', 'معلومات حول صفحتك', 'Uw pagina beschrijving, tussen de 10 en 200 karakters max.', 'Votre description de la page, entre 10 et 200 caractères max.', 'Deine Seitenbeschreibung, zwischen 10 und 200 Zeichen max.', 'La tua descrizione pagina, tra i 10 ei 200 caratteri massimo.', 'A descrição da sua página deve conter entre 10 e 200 caracteres.', 'Описание страницы между 10 и 200 символов макс.', 'Su descripción de página, entre 10 y 200 caracteres máx.', '10 ve 200 karakter max Arasında Sayfanız açıklama'),
(397, 'create', 'Create', 'إنشاء', 'Creëren', 'Créer', 'Erstellen', 'Crea', 'Criar', 'Создать', 'Crear', 'Yarat'),
(398, 'page_name_exists', 'Page name is already exists.', 'إسم الصفحة مستخدم بل الفعل', 'Pagina naam is al bestaat.', 'Nom de la page est existe déjà.', 'Seitenname ist bereits vorhanden.', 'Nome della pagina è esiste già.', 'O nome @ dessa página já esta sendo usado.', 'Название страницы уже существует.', 'Nombre de la página es que ya existe.', 'Sayfa adı zaten var olduğunu.'),
(399, 'page_name_characters_length', 'Page name must be between 5 / 32', 'إسم الصفحة يجب ان يكون بين 5 الى 32 حرف', 'Pagina naam moet tussen 5/32', 'Nom de la page doit être comprise entre 5/32', 'Seitenname muss zwischen 5 und 32 Zeichen bestehen', 'Nome della pagina deve essere compresa tra 5/32', 'O nome da página deve conter entre 5 / 32 caracteres', 'Название страницы должно быть между 5/32 символами', 'Nombre de la página debe estar entre 5/32', 'Sayfa adı 5/32 arasında olmalıdır'),
(400, 'page_name_invalid_characters', 'Invalid page name characters', 'صيغة اسم الصفحة خاطئة', 'Ongeldige pagina naam tekens', 'Invalides nom de la page caractères', 'Ungültige Zeichen vorhanden', 'Caratteri del nome di pagina non valida', 'Caracteres inválidos', 'Недопустимые символы в Названии страницы', 'Caracteres del nombre de la página no válidos', 'Geçersiz sayfa adı karakterleri'),
(401, 'edit', 'Edit', 'تصحيح', 'Bewerk', 'modifier', 'Bearbeiten', 'Modifica', 'Editar', 'редактировать', 'Editar', 'Düzenleme'),
(402, 'page_information', 'Page Information', 'معلومات الصفحة', 'Pagina Informatie', 'Informations sur la page', 'Seiteninformationen', 'Informazioni pagina', 'Informações da página', 'Информация о странице', 'Página de información', 'Sayfa Bilgileri'),
(403, 'delete_page', 'Delete Page', 'حذف الصفحة', 'Pagina Verwijderen', 'supprimer la page', 'Seite löschen', 'Ellimina Pagina', 'Deletar p&amp;aacute;gina', 'Удалить страницу', 'Eliminar página', 'Sayfayı Sil'),
(404, 'location', 'Location', 'موقع', 'Plaats', 'Emplacement', 'Lage', 'Posizione', 'Escreva o nome do bairro e/ou cidade', 'Расположение', 'Ubicación', 'Konum'),
(405, 'pages_you_may_like', 'Pages you may like', 'صفحات قد تعجبك', 'Pagina&#039;s die je misschien graag', 'Pages que vous aimerez', 'Seiten, die Dir gefallen', 'Pagine che potete gradire', 'Páginas que talvez você goste', 'Рекомендуемые страницы', 'Páginas que le gustará', 'Eğer gibi olabilir Sayfalar'),
(406, 'show_more_pages', 'Show more pages', 'أظهر المزيد من الثفحات', 'Toon meer pagina&#039;s', 'Voir plus de pages', 'Zeige mehr Seiten', 'Mostra più pagine', 'Mostrar mais páginas', 'Показать больше страниц', 'Mostrar más páginas', 'Daha fazla sayfa göster'),
(407, 'no_more_pages', 'No more pages to show', 'لا يوجد المزيد', 'Geen pagina te tonen', 'No more pages to show', 'Keine weiteren Seiten vorhanden,', 'Niente più pagine per mostrare', 'Nenhuma p&amp;aacute;gina nova para mostrar', 'Нет больше страниц', 'No más páginas para mostrar', 'Yok daha fazla sayfa göstermek için'),
(408, 'page_delete_confirmation', 'Are you sure you want to delete this page?', 'هل أنت متأكد أنك تريد حذف هذه الصفحة ؟', 'Bent u zeker dat u deze pagina wilt verwijderen?', 'Etes-vous sûr de vouloir supprimer cette page?', 'Bist Du sicher das Du diese Seite löschen möchtest?', 'Sei sicuro di voler cancellare questa pagina?', 'Deletar p&amp;aacute;gina?', 'Вы уверены, что хотите удалить эту страницу?', '¿Seguro que quieres borrar esta página?', 'Bu sayfayı silmek istediğinizden emin misiniz?'),
(409, 'manage_pages', 'Manage Pages', 'إدارة الصفحات', 'Pagina&#039;s beheren', 'gérer les pages', 'Seiten verwalten', 'Gestisci Pagine', 'Editar página', 'Управление страницами', 'Gestionar páginas', 'Sayfaları Yönet'),
(410, 'owner', 'Owner', 'المدير', 'Eigenaar', 'Propriétaire', 'Inhaber', 'Proprietario', 'Dono', 'Владелец', 'Propietario', 'Sahib'),
(411, 'no_pages_found', 'No pages found', 'لا يوجد صفحات', 'Geen pagina&#039;s gevonden', 'Aucune page trouvé', 'Keine Seiten gefunden', 'Nessuna pagina trovata', 'Nenhuma p&amp;aacute;gina encontrada', 'Не найдено ни одной страницы', 'No se encontraron páginas', 'Hiçbir sayfalar bulunamadı'),
(412, 'welcome_wonder', 'Wonder', 'تعجب', 'Wonder', 'Merveille', 'Wundern', 'Wonder', 'N&amp;atilde;o curtiu', 'Pintter', 'Pintter', 'şaşkınlık'),
(413, 'welcome_connect', 'Connect', 'إتصل', 'Aansluiten', 'connecter', 'Verbinden', 'Connettiti', 'Conectar', 'Подключайтесь', 'Conectar', 'Bağlamak'),
(414, 'welcome_share', 'Share', 'شارك', 'Delen', 'Partagez', 'Teilen', 'Condividi', 'Compartilhar', 'Делитесь', 'Compartir', 'Pay'),
(415, 'welcome_discover', 'Discover', 'إستكشف', 'Ontdekken', 'Découvrir', 'Entdecken', 'Scoprire', 'Descobrir', 'Знакомьтесь', 'Descubrir', 'Keşfedin'),
(416, 'welcome_find_more', 'Find more', 'جد المزيد', 'Vind meer', 'Trouve plus', 'Mehr fnden', 'Trova più', 'Procurar mais', 'Найдите больше', 'Encuentra más', 'Daha fazla bul'),
(417, 'welcome_mobile', 'Mobile Friendly', 'متناسق مع جميع الأجهزة', 'Mobile Vriendelijk', 'mobile bienvenus', '100% Mobilfreundlich', 'Mobile Friendly', 'Amigos pelo celular', 'Адаптивный дизайн', 'Mobile Amistoso', 'Mobil Dostu'),
(418, 'welcome_wonder_text', 'Wonder (NEW), ability to wonder a post if you don&#039;t like it.', 'تعجب (جديد), ميزة جديدة تستطيع من خلالها التعجب بل المنشورات.', 'Wonder (NEW), de mogelijkheid om een bericht af of je niet bevalt.', 'Wonder (NOUVEAU), la capacité à se demander un poste si vous ne l&#039;aimez pas.', '(NEU) Wundern, die Möglichkeit, einen Beitrag zu markieren, in Frage zu stellen, weil Du es nicht glaubst oder verstehst.', 'Wonder (NEW), capacità di stupirsi un post, se non ti piace.', 'Não curtir (NEW), abilidade para N&amp;atilde;o curtir um post.', 'Свободно и без ограничений, делитесь своими публикациями со всем миром.', 'Libre y sin restricciones, asombroso para compartir tus publicaciones en todo el mundo.', 'Eğer beğenmezseniz bir yazı merak (YENİ), yetenek Wonder.'),
(419, 'welcome_connect_text', 'Connect with your family and friends and share your moments.', 'تواصل مع عائلتك وأصدقائك شارك اللحظات الخاصة بك.', 'Verbinden met je familie en vrienden en deel je momenten.', 'Connectez-vous avec votre famille et vos amis et partager vos moments.', 'Ein modernes soziales Netzwerk für den Kontakt zu Deiner Familie und Freunden.', 'Connettiti con la tua famiglia e gli amici e condividere i tuoi momenti.', 'Conecte com seus amigos e fam&amp;iacute;lia, e compartilhe seus momentos.', 'Общайтесь с вашей семьей и друзьями, поделитесь своими лучшими моментами.', 'Conéctate con tu familia y amigos para compartir los mejores momentos.', 'Aileniz ve arkadaşlarınızla bağlamak ve anları paylaşmak.'),
(420, 'welcome_share_text', 'Share what&#039;s new and life moments with your friends.', 'شارك الحظات الجديدة في حياتك مع أصدقائك.', 'Deel wat nieuw is en het leven momenten met je vrienden.', 'Partager ce sont des moments de nouvelles et de la vie avec vos amis.', 'Teile mit Deinen Freunden, Nachbarn und Kollegen alles was neu ist. Zeige was Dir gefällt.', 'Condividi ciò che è nuovo e la vita momenti con i tuoi amici.', 'Compartilhe com seus amigos conteúdo relevante para sua cidade.', 'Поделитесь своим контентом с помощью Pintter и получите самое лучшее продвижение.', 'Comparte todos tus contenidos a través de Pintter y consigue la mejor promoción.', 'Arkadaşlarınızla yeni ve yaşam anları ne paylaşın.'),
(421, 'welcome_discover_text', 'Discover new people, create new connections and make new friends.', 'إكتشف أشخاص جدد، وأنشىء اتصالات جديدة وكون صداقات جديدة.', 'Ontdek nieuwe mensen, nieuwe verbindingen te maken en nieuwe vrienden maken.', 'Découvrez de nouvelles personnes, créer de nouvelles connexions et de faire de nouveaux amis.', 'Entdecke neue Leute, neue Verbindungen und neue Freunde.', 'Scoprire nuove persone, creare nuove connessioni e fare nuove amicizie.', 'Descubra novas pessoas, fa&amp;ccedil;a amigos e se divirta!', 'Откройте для себя новых людей, создавайте связи и заводите новых друзей.', 'Descubre nuevas personas, haz nuevas conexiones y nuevos contactos.', 'Yeni insanlarla keşfedin, yeni bağlantılar oluşturmak ve yeni arkadaşlar.'),
(422, 'welcome_find_more_text', 'Find more of what you&#039;re looking for with WoWonder Search.', 'أبحث عن ما تريد مع  نظام بحث واواندر', 'Vind meer van wat je zoekt met WoWonder Search.', 'Trouver plus de ce que vous n &#39;êtes à la recherche d&#39;avec WoWonder Recherche.', 'Finde viel mehr, was Du mit wen-kennt-wer-Suche suchst.', 'Trova più di quello che stai cercando con WoWonder Ricerca.', 'Veja mais do que voc&amp;ecirc; esta procurando com o WoWonder Search.', 'Узнайте больше о том, что вы ищете с помощью функции поиска Pintter.', 'Encuentras más de lo que estás buscando con el nuevo Pintter Buscador.', 'Eğer WoWonder Arama ile aradığınızı daha bulun.'),
(423, 'welcome_mobile_text', '100% screen adaptable on all tablets and smartphones.', '100% متناسق مع جميع الأجهزة من الهواتف الذكية والتابلت', '100% scherm passen op alle tablets en smartphones.', 'Écran 100% adaptable sur toutes les tablettes et les smartphones.', '100% für Dein Tablet und Smartphone angepasst.', 'Schermo100%  adattato su tutti i tablet e smartphone.', 'Tela 100% adapt&amp;aacute;vel em todos os tablets e smartphones.', '100% адаптируется к любому мобильному экрану, таблету или смарт-устройству.', '100% adaptable a cualquier pantalla móvil, tabletas o dispositivo inteligentes.', 'Tüm tabletler ve akıllı telefonlarda uyarlanabilir %100 ekran.'),
(424, 'working_at', 'Working at', 'يعمل في', 'Werken bij', 'Travailler à', 'Arbeitet bei', 'Lavorare in', 'Trabalhando em', 'Работает в', 'Trabajando en', 'Çalışmak'),
(425, 'relationship', 'Relationship', 'الحالة الإجتماعية', 'Verhouding', 'Relation', 'Beziehung', 'Relazione', 'Relacionamento', 'Отношения', 'Relación', 'ilişki'),
(426, 'none', 'None', 'غير محدد', 'Geen', 'Aucun', 'Keine', 'Nessuna', 'Nenhum', 'Не выбрано', 'Ninguno', 'Hiçbiri'),
(427, 'avatar', 'Avatar', 'الصورة الشخصية', 'Avatar', 'Avatar', 'Profilbild', 'Avatar', 'Avatar', 'Аватар', 'Avatar', 'Avatar'),
(428, 'cover', 'Cover', 'الغلاف', 'Deksel', 'Couverture', 'Titelbild', 'Immagine di copertura', 'Capa', 'Обложка', 'Fondo', 'Kapak'),
(429, 'background', 'Background', 'خلفية صفحتك الشحصية', 'Achtergrond', 'Contexte', 'Hintergrund', 'Sfondo', 'Fundo', 'Задний план', 'Fondo de Pantalla', 'Geçmiş'),
(430, 'theme', 'Theme', 'الثيم', 'Thema', 'Thème', 'Thema', 'Temi', 'Tema', 'Тема', 'Tema', 'Tema'),
(431, 'deafult', 'Default', 'الإفتراضي', 'Standaard', 'Défaut', 'Standard', 'Predefinito', 'Padr&amp;ccedil;o', 'По умолчанию', 'Defecto', 'Standart'),
(432, 'my_background', 'My Background', 'الخاص بي', 'Mijn Achtergrond', 'Mon arrière-plan', 'Mein Hintergrund', 'Mio Sfondo', 'Meu fundo', 'Мой фон', 'Mi pasado', 'Benim Arkaplan'),
(433, 'company_website', 'Company website', 'الموقع الإلكتروني للعمل', 'Bedrijfs websitee', 'Site Web de l&#39;entreprise', 'Unternehmenswebseite', 'Sito Sociaeta', 'Site da empresa', 'Веб-сайт компании', 'Página Web de la compañía', 'Şirket Web Sitesi'),
(434, 'liked_my_page', 'Liked My Page', 'معجبين بصفحتي', 'Vond Mijn pagina', 'Aimé Ma Page', 'gefällt meine Seite', 'Mi è piaciuta la mia pagina', 'Curtiu minha página', 'Понравилась моя страница', 'Me gustó mi página', 'Benim Sayfam Beğendi'),
(435, 'dislike', 'Dislike', 'عدم الإعجاب', 'Afkeer', 'Aversion', 'nicht gefallen', 'Antipatia', '&amp;nbsp &amp;nbsp &amp;nbsp &amp;nbsp &amp;nbsp &amp;nbsp &amp;nbsp &amp;nbsp', 'Не нравится', 'No me gusta', 'Beğenmeme'),
(436, 'dislikes', 'Dislikes', 'غير معجبين', 'Antipathieën', 'Dégoûts', 'Unbeliebt', 'Antipatia', '', 'Не нравится', 'No le gusta', 'Sevmedikleri'),
(437, 'disliked_post', 'disliked your {postType} {post}', 'لم يعجب {postType} {post}', 'hekel aan je {postType} {post}', 'détesté votre {postType} {post}', 'gefällt dein Beitrag {postType} {post} nicht', 'antipatia tuo {postType} {post}', 'Não curtiu sua {postType} {post}', 'не нравится {postType} {post}', 'no le gusta tu {postType} {post}', 'senin {postType} sevmiyordu {post}'),
(438, 'disliked_comment', 'disliked your comment &quot;{comment}&quot;', 'لم يعجب بتعليقك &quot;{comment}&quot;', 'hekel aan je reactie &quot;{comment}&quot;', 'détesté votre commentaire &quot;{comment}&quot;', 'gefällt dein Kommentar &quot;{comment}&quot;', 'antipatia tuo commento &quot;{comment}&quot;', 'Não curtiu seu comentário &quot;{comment}&quot;', 'не нравится ваш комментарий &quot;{comment}&quot;', 'no le gustaba su comentario &quot;{comment}&quot;', 'sevilmeyen Yorumunuzu &quot;{comment}&quot;'),
(439, 'activity_disliked_post', 'disliked {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'لم يعجب &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;بمنشور&lt;/a&gt; {user} .', 'hekel {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'détesté {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'unbeliebt {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt; Beitrag &lt;/a&gt;.', 'antipatia {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', 'Não curtiu {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;post&lt;/a&gt;.', '{user} не нравится &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt;пост&lt;/a&gt;.', 'No me gustó {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt; posterior &lt;/a&gt;.', 'Sevmediği {user} &lt;a class=&quot;main-color&quot; href=&quot;{post}&quot;&gt; yazılan &lt;/a&gt;.'),
(440, 'second_button_type', 'Second post button type', 'نوع الزر الثاني للمنشور', 'Tweede post type knop', 'Deuxième poste de type bouton', 'Zweiter Likebutton', 'Secondo palo tipo di pulsante', 'Segundo tipo de bot&amp;ccedil;o', 'Второй тип кнопки', 'Segundo mensaje de tipo botón', 'İkinci sonrası düğmesi tipi'),
(441, 'group_name', 'Group name', 'إسم المجموعة', 'Groepsnaam', 'Nom de groupe', 'Gruppenname', 'Nome del gruppo', 'Nome do grupo', 'URL группы', 'Nombre del grupo', 'Grup ismi'),
(442, 'group_title', 'Group title', 'عنوان المجموعة', 'Groep titel', 'Titre de groupe', 'Gruppentitel', 'Titolo del gruppo', 'Título do grupo', 'Название группы', 'Título del Grupo', 'Grup başlık'),
(443, 'my_groups', 'My Groups', 'مجموعاتي', 'Mijn Groepen', 'Mes Groupes', 'Meine Gruppen', 'I miei gruppi', 'Meus grupos', 'Мои группы', 'Mis grupos', 'Gruplar'),
(444, 'school', 'School', 'المدرسة', 'School', 'L&#39;école', 'Schule', 'Scuola', 'Escola', 'Школа', 'Colegio', 'Okul'),
(445, 'site_keywords_help', 'Example: social, wowonder, social site', 'Example: social, wowonder, social site', 'Example: social, wowonder, social site', 'Example: social, wowonder, social site', 'Beispiel: soziale, wen-kennt-wer, soziale Website', 'Esempio: sociale, wowonder, sito di social', 'Exemplo: social, wowonder, site social', 'Пример: социальная сеть, pintter, социальный сайт', 'Ejemplo:, wowonder, sitio social sociales', 'Örnek: Sosyal, wowonder, sosyal sitesi'),
(446, 'message_seen', 'Message Seen', 'الرسائل المقروئة', 'Bericht Seen', 'Vu message', 'Nachricht gesehen', 'Messaggio Visto', 'Mensagem lida', 'Прочитал@', 'Mensaje Seen', 'İleti Seen'),
(447, 'recommended_for_powerful', 'Recommended for powerful servers', 'مستحسن للاسيرفرات القوية', 'Aanbevolen voor krachtige servers', 'Recommandé pour les puissants serveurs', 'Empfohlen für leistungsstarke Server', 'Consigliato per potenti server', 'Recomendado para servi&amp;ccedil;os pesados', 'Рекомендуется для мощных серверов', 'Recomendado para servidores de gran alcance', 'Güçlü sunucular için önerilen'),
(448, 'message_typing', 'Chat typing system', 'نظام &quot;يكتب&quot; للشات', 'Chat typering systeem', 'Système de typage chat', 'Chat Typisierungssystem', 'Sistema di digitazione Chat', 'Sistema de digita&amp;ccedil;&amp;atilde;o', 'Набирает сообщение', 'Sistema de tipificación de Chat', 'Sohbet yazarak sistemi'),
(449, 'reCaptcha', 'reCaptcha', 'reCaptcha', 'reCaptcha', 'reCaptcha', 'reCaptcha', 'reCaptcha', 'reCaptcha', 'ReCaptcha', 'reCaptcha', 'Tuttum'),
(450, 'instagram', 'Instagram', 'الأنستاغرام', 'Instagram', 'Instagram', 'Instagram', 'Instagram', 'Instagram', 'Instagram', 'Instagram', 'Instagram'),
(451, 'profile_visit_notification_help', 'if you don&#039;t share your visit event , you won&#039;t be able to see other people visiting your profile.', 'اذا لم تفعل زيارة الصفحة , فانك لن تكون قادرا على رؤية الاخرين وهم يزورون صفحتك.', 'als u niet uw bezoek evenement te delen, zult u niet in staat zijn om andere mensen een bezoek aan uw profiel te zien.', 'si vous ne partagez pas votre événement de la visite, vous ne serez pas en mesure de voir d&#039;autres gens qui visitent votre profil.', 'Wenn Du Deine Profilbesuche nicht teilen willst, können andere nicht sehen wenn du ihr Profil besucht hast.', 'se non si condivide il vostro evento visita, non sarà in grado di vedere altre persone che visitano il tuo profilo.', 'Se você Não habilitar a notificação de perfil, você Não poderá ver quem visitou seu perfil.', 'Если отключить это уведомление, вы не будете получать уведомления о том кто посещал ваш профиль.', 'Si desactivas esta notificación tu tampoco recibirás avisos de visita de otros usuarios.', 'Eğer ziyaret olayı paylaşmak yoksa, profilinizi ziyaret eden diğer kişileri görmek mümkün olmayacaktır.'),
(452, 'account_delete', 'Delete Account', 'حذف الحساب', 'Account verwijderen', 'Effacer le compte', 'Konto löschen', 'Eliminare l&#039;account', 'Deletar conta', 'Удалить аккаунт', 'Borrar cuenta', 'Hesabı sil'),
(453, 'ip_address', 'IP Address', 'IP عنوان', 'IP Address', 'Adresse IP', 'IP Adresse', 'Indirizzo IP', 'Endere&amp;ccedil;o IP', 'Айпи адрес', 'Dirección IP', 'IP adresi'),
(454, 'manage_groups', 'Manage Groups', 'إدارة المجموعات', 'Groepen beheren', 'Gérer les groupes', 'Gruppen verwalten', 'Gestisci gruppi', 'Editar grupos', 'Управление группами', 'Administrar grupos', 'Grupları Yönet'),
(455, 'group_delete_confirmation', 'Are you sure you want to delete this group?', 'هل أنت متأكد أنك تريد حذف هذه المجموعة؟', 'Bent u zeker dat u deze groep wilt verwijderen?', 'Êtes-vous sûr de vouloir supprimer ce groupe?', 'Bist Du sicher das Du diese Gruppe löschen möchtest?', 'Sei sicuro di voler eliminare questo gruppo?', 'Deletar este grupo?', 'Вы уверены, что хотите удалить эту группу?', '¿Seguro que quieres borrar este grupo?', 'Bu grubu silmek istediğinizden emin misiniz?'),
(456, 'no_more_groups', 'No more groups to show', 'لا يوجد المزيد من المجموعات', 'Geen groepen tonen', 'Pas de plusieurs groupes pour montrer', 'Keine weiteren Gruppen,', 'Nessun altro gruppo per mostrare', 'Nenhum grupo para mostrar', 'Нет больше групп для отображения', 'No hay más grupos que mostrar', 'Artık gruplar göstermek için'),
(457, 'show_more_groups', 'Show more groups', 'عرض المزيد من المجموعات', 'Toon meer groepen', 'Montrer plus de groupes', 'Mehrere Gruppen anzeigen', 'Mostra più gruppi', 'Mostrar mais grupos', 'Показать больше групп', 'Mostrar más grupos', 'Daha fazla gruplar göster'),
(458, 'members', 'Members', 'أفراد', 'leden', 'Membres', 'Mitglieder', 'Utenti', 'Membros', 'члены', 'Miembros', 'Üyeler'),
(459, 'verifications_requests', 'Verification Requests', 'طلبات الحسابات المؤكدة', 'Verificatie Verzoeken', 'Demandes de vérification', 'Verifizierungsanfragen', 'Richieste di verifica', 'Pedidos de verifica&amp;ccedil;&amp;atilde;o', 'Запросы', 'Solicitudes verificación', 'Doğrulama İstekleri'),
(460, 'verify', 'Verify', 'تأكيد', 'Verifiëren', 'Vérifier', 'Überprüfen', 'Verificare', 'Verificar', 'Добавить', 'Verificar', 'Doğrulamak'),
(461, 'ignore', 'Ignore', 'تجاهل', 'Negeren', 'Ignorer', 'Ignorieren', 'Ignorare', 'Ignorar', 'Игнорировать', 'Ignorar', 'Ignore'),
(462, 'page', 'Page', 'صفحة', 'Pagina', 'Page', 'Seite', 'Pagina', 'Página', 'Страница', 'Página', 'Sayfa'),
(463, 'no_new_verification_requests', 'No new verification requests', 'لا يوجد طلبات جديدة للتحقق', 'Geen nieuwe verificatie aanvragen', 'Aucune nouvelle demande de vérification', 'Keine neuen Verifizierungsanfragen', 'Non ci sono nuove richieste di verifica', 'Nenhum pedido de verifica&amp;ccedil;&amp;atilde;o', 'Нет новых запросов', 'No hay nuevas solicitudes de verificación', 'Yeni doğrulama istekleri'),
(464, 'ban_user', 'Ban User', 'حظر العضو', 'Ban gebruiker', 'Ban User', 'Gesperrte Benutzer', 'Ban utente', 'Banir usuário', 'Забанить', 'Ban usuario', 'Ban User'),
(465, 'banned', 'Banned', 'المحظور', 'Verboden', 'Banned', 'Verboten', 'Vietato', 'Banido', 'Забанен', 'Banned', 'Yasaklı'),
(466, 'there_are_no_banned_ips', 'There are no banned ips.', 'لا يوجد ips محطورة', 'Er zijn geen verboden ips.', 'Il n&#39;y a pas ips interdits.', 'Es sind keine gesperrte IPs.', 'Non ci sono ips vietati.', 'Nenhum IP banido.', 'Нет забаненных IPS.', 'No hay ips prohibidas.', 'Hiçbir yasak ips bulunmamaktadır.'),
(467, 'invalid_ip', 'Invalid ip address.', 'عنوان IP غير صالح.', 'Ongeldig IP-adres.', 'Adresse IP non valide.', 'Ungültige IP-Adresse.', 'Indirizzo IP non valido.', 'IP inválido.', 'Неверный IP адрес.', 'Dirección IP no válida.', 'Geçersiz IP adresi.'),
(468, 'ip_banned', 'IP address successfully banned.', 'عنوان IP حظرت بنجاح.', 'IP-adres succesvol verbannen.', 'Adresse IP banni avec succès.', 'IP-Adresse erfolgreich verboten.', 'Indirizzo IP vietato con successo.', 'IP banido.', 'IP-адрес успешно забанен.', 'Dirección IP prohibida éxito.', 'IP adresi başarıyla yasaklandı.'),
(469, 'ip_exist', 'IP address already exist', 'عنوان IP موجودة بالفعل', 'IP-adres bestaan', 'Adresse IP existent déjà', 'Bereits vorhanden IP-Adresse', 'Indirizzo IP già esistente', 'J&amp;aacute; existe este IP', 'IP-адрес уже существует', 'Dirección IP ya existen', 'IP adresi zaten mevcut'),
(470, 'please_add_ip', 'Please add an ip address', 'يرجى إضافة عنوان IP', 'Voeg een ip-adres', 'S&#39;il vous plaît ajouter une adresse ip', 'Bitte füge eine IP-Adresse hinzu', 'Si prega di aggiungere un indirizzo IP', 'Adicione um IP', 'Пожалуйста, добавьте IP адрес', 'Por favor, añada una dirección IP', 'Bir ip adresinizi ekleyiniz'),
(471, 'ip_deleted', 'IP address successfully deleted', 'عنوان IP حذف بنجاح', 'IP-adres succesvol verwijderd', 'Adresse IP supprimé avec succès', 'IP-Adresse erfolgreich gelöscht', 'Indirizzo IP eliminato', 'IP deletado', 'IP-адрес успешно удален', 'Dirección IP eliminado correctamente', 'IP adresi başarıyla silindi'),
(472, 'email_me_when', 'Email me when', 'أرسل لي عندما', 'E-mail me als', 'Envoyez-moi lorsque', 'Email-Bnachrichtigung, wenn:', 'Inviami una email quando', 'Enviar e-mail quando algu&amp;eacute;m', 'Напишите, когда', 'Envíame un email:', 'Bana e-posta'),
(473, 'e_likes_my_posts', 'Someone liked my posts', 'شخص اعجب بمنشوري', 'Iemand hield van mijn berichten', 'Quelqu&#39;un aimait mes messages', 'Jemand wundert sich über meinen Beitrag', 'Qualcuno è piaciuto miei post', 'Curtir meus posts', 'Нравятся мои заметки', 'Cuando a alguien le gusta mis posts', 'Birisi Mesajları sevdim'),
(474, 'e_wondered_my_posts', 'Someone wondered my posts', 'شخص تعجب بمنشوري', 'Iemand vroeg me af van mijn berichten', 'Quelqu&#39;un demanda mes messages', 'Jemand mag meine Beiträge nicht', 'Qualcuno chiese miei post', 'Dar dislike em meus posts', 'Не нравятся мои заметки', 'Cuando alguien pregunta en mis posts', 'Birisi Mesajları merak'),
(475, 'e_commented_my_posts', 'Someone commented on my posts', 'شخص علق على منشوراتي', 'Iemand heeft gereageerd op mijn berichten', 'Quelqu&#39;un a commenté mes messages', 'Jemand kommentiert meine Beiträge', 'Qualcuno ha commentato i miei post', 'Comentar meus posts', 'Прокомментировали мои заметки', 'Cuando alguien comenta mis posts', 'Birisi benim mesajlar yorumladı'),
(476, 'e_shared_my_posts', 'Someone shared on my posts', 'شخص شارك منشوراتي', 'Iemand gedeeld op mijn berichten', 'Quelqu&#39;un partagé sur mes posts', 'Jemand teilt meine Beiträge', 'Qualcuno ha condiviso i miei post', 'Compartilhar meus posts', 'Поделились моими заметками', 'Cuando alguien comparte mis posts', 'Birisi benim yazılarda paylaştı'),
(477, 'e_followed_me', 'Someone followed me', 'شخص تابعني', 'Iemand volgde mij', 'Quelqu&#39;un m&#39;a suivi', 'Jemand folgt mir', 'Qualcuno mi ha seguito', 'Me seguir', 'Следуют за мной', 'Cuando alguien me sigue', 'Biri beni takip'),
(478, 'e_liked_page', 'Someone liked my pages', 'شخص أعجب بصفحة خاصة بي', 'Iemand hield van mijn pagina&#039;s', 'Quelqu&#039;un aimait mes pages', 'Jemand gefällt meine Seiten', 'Qualcuno è piaciuto mie pagine', 'Curtir minha página', 'Нравится моя страница', 'Cuando a alguien le gusta mis páginas', 'Birisi sayfalarını sevdim'),
(479, 'e_visited_my_profile', 'Someone visited my profile', 'شخص زار صفحتي الشخصية', 'Iemand bezocht mijn profiel', 'Quelqu&#39;un a visité mon profil', 'Jemand hat mein Profil besucht', 'Qualcuno ha visitato il mio profilo', 'Visitar meu perfil', 'Посетили мой профиль', 'Cuando alguien ha visitó mi perfil', 'Birisi profilimi ziyaret'),
(480, 'e_mentioned_me', 'Someone mentioned me', 'شخص ذكرني', 'Iemand noemde me', 'Quelqu&#39;un a parlé de moi', 'Jemand erwähnte mich', 'Qualcuno mi ha detto', 'Te mencionar', 'Упомянули меня', 'Cuando alguien me menciona', 'Biri bana söz'),
(481, 'e_joined_group', 'Someone joined my groups', 'شخص انضم الى مجموعاتي', 'Iemand trad mijn groepen', 'Quelqu&#39;un a rejoint mes groupes', 'Jemand ist meiner Gruppe beigetreten', 'Qualcuno è entrato miei gruppi', 'Entrar no meu grupo', 'Вступили в мою группу', 'Cuando alguien se une a mis grupos', 'Birisi grupları katıldı'),
(482, 'e_accepted', 'Someone accepted my friend/follow requset', 'شخص قبل طلب صادقتي/متابعتي', 'Iemand aanvaard mijn vriend / follow verzoek', 'Quelqu&#39;un a accepté mon ami / suivre la demande', 'Jemand akzeptiert mein Freundschaftsanfrage', 'Qualcuno ha accettato il mio amico / seguire la richiesta', 'Aceitar o meu pedido para seguir/adicionar aos amigos', 'Приняли дружбу / запрос следовать', 'Cuando alguien acepta mi petición', 'Birisi arkadaşım / takip et requset kabul'),
(483, 'e_profile_wall_post', 'Someone posted on my timeline', 'شخص نشر على صفحتي الشخصية', 'Iemand gepost op mijn timeline', 'Quelqu&#39;un a posté sur mon calendrier', 'Jemand hat etwas in mein Profil geschrieben', 'Qualcuno ha postato su mia timeline', 'Postar em sua linha do tempo', 'Публикация на стене профиля', 'Cuando alguien escribe en mi muro', 'Birisi benim zaman çizelgesi yayınlanan'),
(484, 'no_groups_found', 'No groups found', 'لا يوجد مجموعات', 'Geen groepen gevonden', 'Pas de groupes trouvés', 'Keine Gruppen gefunden', 'Nessun gruppo trovato', 'Nenhum grupo encontrado', 'Группы не найдены', 'No se encontraron grupos', 'Grup bulunamadı'),
(485, 'group_information', 'Group information', 'معلومات المجموعة', 'Groep informatie', 'L&#39;information de groupe', 'Gruppenthemen', 'Informazioni Gruppo', 'Informa&amp;ccedil;&amp;otilde;es do grupo', 'Информация о группе', 'Información del Grupo', 'Grup bilgileri'),
(486, 'delete_group', 'Delete Group', 'حذف المجموعة', 'Groep verwijderen', 'Supprimer le groupe', 'Gruppe löschen', 'Elimina gruppo', 'Deletar grupo', 'Удалить группу', 'Eliminar grupo', 'Grubu Sil'),
(487, 'group_name_exists', 'Group name is already exists.', 'اسم المجموعة موجود بالفعل.', 'Groepsnaam is al bestaat.', 'Le nom du groupe est existe déjà.', 'Gruppenname ist bereits vorhanden.', 'Il nome del gruppo è già esistente.', 'Nome do grupo j&amp;aacute; esta em uso.', 'Название группы уже существует.', 'El nombre del grupo es ya existe.', 'Grup adı zaten var olduğunu.'),
(488, 'group_name_invalid_characters', 'Invalid group name characters', 'أحرف اسم مجموعة غير صالحة', 'Ongeldige naam van de groep tekens', 'Invalides nom de groupe caractères', 'Ungültige Gruppenname Zeichen', 'Caratteri del nome del gruppo non validi', 'Caracteres inválidos', 'Недопустимые символы в URL группы', 'Caracteres del nombre de grupo no válido', 'Geçersiz grup adı karakter'),
(489, 'group_name_characters_length', 'Group name must be between 5 / 32', 'يجب أن يكون اسم المجموعة بين 5/32 حرف', 'Groepsnaam moet tussen 5/32', 'Le nom du groupe doit être comprise entre 5/32', 'Gruppenname muss zwischen 5 und 32 Zeichen bestehen', 'Il nome del gruppo deve essere compresa tra 5/32', 'O nome do grupo deve conter entre 5 / 32 caracteres', 'URL группы должен быть между 5/32 символами', 'Nombre del grupo debe estar entre 5/32', 'Grup ismi 5/32 arasında olmalıdır'),
(490, 'no_requests_found', 'No requests found!', 'لم يتم العثور على أية طلبات!', 'Geen verzoeken gevonden!', 'Aucune demande trouvée!', 'Keine Anfragen gefunden!', 'Nessuna richiesta trovata!', 'Não foram encontrados pedidos!', 'Запросов не найдено!', 'No se han encontrado solicitudes!', 'İstek bulunamadı!'),
(491, 'remove', 'Remove', 'إزالة', 'Verwijderen', 'Enlever', 'Entfernen', 'Rimuovere', 'Remover', 'Удалить', 'Eliminar', 'Kaldır'),
(492, 'no_members_found', 'No members found', 'لم يتم العثور على أي أعضاء ', 'Er zijn geen leden gevonden', 'Aucun membre trouvé', 'Keine Mitglieder gefunden', 'Nessun membro trovato', 'Nenhum membro encontrado', 'Участники не найдены', 'No se encontraron miembros', 'Üye bulunamadı'),
(493, 'group_deleted', 'Group successfully deleted', 'تم حذف المجموعة بنجاح', 'Groep succesvol verwijderd', 'Groupe supprimé avec succès', 'Gruppe erfolgreich gelöscht', 'Gruppo cancellato con successo', 'Grupo deletado', 'Группа успешно удалена', 'Grupo eliminado correctamente', 'Grup başarıyla silindi'),
(494, 'create_new_group', 'Create New Group', 'إنشاء مجموعة جديدة', 'Nieuwe groep', 'Créer un nouveau groupe', 'Neue Gruppe erstellen', 'Crea nuovo gruppo', 'Criar novo grupo', 'Создать новую группу', 'Crear nuevo grupo', 'Yeni Grup Oluştur'),
(495, 'my_games', 'My Games', 'ألعابي', 'Mijn games', 'Mes Jeux', 'Meine Spiele', 'I miei giochi', 'Meus jogos', 'Мои игры', 'Mis juegos', 'Benim Oyunlar'),
(496, 'no_games_found', 'No games found', 'لم يتم العثور على ألعاب', 'Geen games gevonden', 'Pas de jeux trouvés', 'Keine Spiele gefunden', 'Nessun gioco trovato', 'Nenhum jogo encontrado', 'Игры не найдены', 'No se han encontrado juegos', 'Hiçbir oyun bulunamadı'),
(497, 'groups', 'Groups', 'المجموعات', 'Groepen', 'Groupes', 'Gruppen', 'Gruppi', 'Grupos', 'Группы', 'Grupos', 'Gruplar'),
(498, 'no_friends', 'No friends yet', 'ليس لديه أصدقاء حتى الآن', 'Nog geen vriendent', 'Pas encore d&#39;amis', 'Noch keine Freunde', 'Non ci sono ancora amici', 'Nenhum amigo', 'Нет друзей', 'No tiene amigos todavía', 'Hiç arkadaşım yok'),
(499, 'no_groups', 'Didn&#039;t join any groups yet', 'لم ينضم أي مجموعة حتى الآن', 'Heeft een groep nog niet mee', 'N&#39;a pas encore de rejoindre les groupes', 'Hat noch keiner Gruppe beigetreten', 'Non ha ancora aderire a nessun gruppo', 'N&amp;atilde;o entrou em nenhum grupo', 'Не вступать ни в какие группы', 'No unirse a ningún grupo todavía', 'Henüz hiçbir gruba katılmadı'),
(500, 'load_more_pages', 'Load more pages', 'تحميل المزيد من الصفحات', 'Laad meer pagina&#039;s', 'Chargez plus de pages', 'Weitere Seiten laden', 'Caricare più pagine', 'Carregar mais páginas', 'Загрузить больше страниц', 'Cargar más páginas', 'Daha fazla sayfaları yük'),
(501, 'load_more_groups', 'Load more groups', 'تحميل المزيد من المجموعات', 'Laad meer groepen', 'Chargez plusieurs groupes', 'Weitere Gruppen laden', 'Carica altri gruppi', 'Carregar mais grupos', 'Загрузить больше групп', 'Cargar más grupos', 'Daha fazla gruplar yükle'),
(502, 'joined_group', 'Joined your group ({group_name})', 'إنضم الى مجموعتك ({group_name})', 'Toegetreden tot de groep ({group_name})', 'Rejoint notre groupe ({group_name})', 'ist deiner Gruppe ({group_name}) beigetreten', 'Iscritto il nostro gruppo ({group_name})', 'Entrou no seu grupo ({group_name})', 'вступил@ в ({group_name})', 'Se ha unido a tu grupo ({group_name})', 'Kayıt grup ({group_name})'),
(503, 'happy', 'Happy', 'السعادة', 'Blij', 'Heureux', 'glücklich', 'Contento', 'Feliz', 'Счастливый', 'Feliz', 'Mutlu'),
(504, 'loved', 'Loved', 'الحب', 'Hield', 'Aimé', 'begeistert', 'Amato', 'Apaixonado', 'Влюбленный', 'Me encantaron', 'Sevilen'),
(505, 'sad', 'Sad', 'الحزن', 'Verdrietig', 'Triste', 'traurig', 'Triste', 'Triste', 'Грустный', 'Triste', 'Üzücü'),
(506, 'so_sad', 'Very sad', 'الحزن الشديد', 'Zeer triest', 'Très triste', 'sehr traurig', 'Molto triste', 'Muito triste', 'Очень грустный', 'Muy triste', 'Çok üzgün'),
(507, 'angry', 'Angry', 'الغضب', 'Boos', 'En colère', 'verärgert', 'Arrabbiato', 'Bravo', 'Злой', 'Enfadado', 'Öfkeli'),
(508, 'confused', 'Confused', 'الحيرة', 'Verward', 'Confus', 'verwirrt', 'Confuso', 'Confuso', 'В замешательстве', 'Confuso', 'Şaşkın'),
(509, 'smirk', 'Hot', 'ساخن', 'Warm', 'Chaud', 'Heiß', 'Caldo', 'Sexy', 'Горячий', 'Caliente', 'Sıcak'),
(510, 'broke', 'Broken', 'الاحباط', 'Gebroken', 'Brisé', 'am Boden zerstört', 'Rotte', 'Tra&amp;iacute;do', 'Сломанный', 'Roto', 'Broken'),
(511, 'expressionless', 'expressionless', 'مستغرب', 'Wezenloos', 'Inexpressif', 'ausdruckslos', 'Inespressivo', 'Sem express&amp;atilde;o', 'Без выражений', 'inexpresivo', 'ifadesiz'),
(512, 'cool', 'Cool', 'الروعة', 'Koel', 'Bien', 'cool', 'Bene', 'Legal', 'Круто', 'Guay', 'Güzel'),
(513, 'funny', 'Funny', 'الضحك', 'Grappig', 'Amusant', 'komisch', 'Divertente', 'Divertido', 'Веселая', 'Divertido', 'Komik'),
(514, 'tired', 'Tired', 'التعب', 'Moe', 'Fatigué', 'müde', 'Stanco', 'Cansado', 'Устала', 'Cansado', 'Yorgun'),
(515, 'lovely', 'Lovely', 'محب', 'Heerlijk', 'Charmant', 'sehr verliebt', 'Bello', 'Amavel', 'Прекрасный', 'Encantador', 'Güzel'),
(516, 'blessed', 'Blessed', 'المنحة', 'Gezegend', 'Béni', 'gesegnet', 'Benedetto', 'AbeN&amp;atilde;oado', 'Благословенный', 'Bendito', 'Mübarek'),
(517, 'shocked', 'Shocked', 'الصدمة', 'Geschokt', 'Choqué', 'schockiert', 'Scioccato', 'Chocado', 'В шоке', 'Conmocionado', 'Şokta'),
(518, 'sleepy', 'Sleepy', 'النعاس', 'Slaperig', 'Somnolent', 'schläfrig', 'Assonnato', 'Dormindo', 'Сонный', 'Soñoliento', 'Uykulu'),
(519, 'pretty', 'Pretty', 'الجمال', 'Mooi', 'Assez', 'hübsch', 'Bella', 'Bonito', 'Милая', 'bastante', 'Oldukça'),
(520, 'bored', 'Bored', 'الملل', 'Verveeld', 'Ennuyé', 'gelangweilt', 'Annoiato', 'Entediado', 'Скучающий', 'aburrido', 'Bıkkın'),
(521, 'total_users', 'Total Users', 'كل المستخدمين', 'Totaal aantal leden', 'Nombre d&#039;utilisateurs', 'Benutzer insgesamt', 'Totale Utenti', 'Total de usuários', 'Всего пользователей', 'Total de usuarios', 'Toplam Kullanıcılar'),
(522, 'users', 'Users', 'المستخدمين', 'Gebruikers', 'Utilisateurs', 'Benutzer', 'Utenti', 'Usuários', 'Пользователи', 'Usuarios', 'Kullanıcılar'),
(523, 'pages', 'Pages', 'الصفحات', 'Pagina&#039;s', 'Pages', 'Seiten', 'Pagine', 'Páginas', 'Страницы', 'Páginas', 'Sayfalar'),
(524, 'games', 'Games', 'الألعاب', 'Spelen', 'Jeux', 'Spiele', 'Giochi', 'Jogos', 'Игры', 'Juegos', 'Oyunlar'),
(525, 'online_users', 'Online Users', 'المستخدمين المتصلين', 'Online Gebruikers', 'Utilisateurs en ligne', 'User online', 'Utenti Online', 'Usuários online', 'Сейчас на сайте', 'Usuarios en línea', 'Çevrimiçi Kullanıcılar'),
(526, 'recent_searches', 'Recent Searches', 'عمليات البحث الأخيرة', 'Recente zoekopdrachten', 'Recherches récentes', 'Letzte Suche', 'Ricerche recenti', 'Procuras recentes', 'Последние поисковые запросы', 'Búsquedas recientes', 'Son aramalar'),
(527, 'clear', 'Clear', 'مسح', 'Duidelijk', 'Clair', 'Klar', 'Chiaro', 'Limpar', 'Очистить', 'Claro', 'Açık'),
(528, 'search_filter', 'Search filter', 'البحث المتقدم', 'Search filter', 'Filtre de recherche', 'Suchfilter', 'Filtro di ricerca', 'Filtro de pesquisa', 'Фильтр поиска', 'Filtro de búsqueda', 'Arama filtresi'),
(529, 'keyword', 'Keyword', 'الكلمة', 'Zoekfilter', 'Mot-clé', 'Stichwort', 'Parola chiave', 'Palavra-chave', 'Ключевое слово', 'Palabra clave', 'Kelime'),
(530, 'what_are_looking_for', 'What are looking for ?', 'عن ماذا تبحث؟', 'Wat zoekt?', 'Que cherchez?', 'Was suchst du?', 'Quello che sono in cerca di ?', 'O que você esta procurando?', 'Что вы ищете?', '¿Que están buscando ?', 'Ne arıyorsun?'),
(531, 'changed_profile_cover_picture_male', 'Changed his profile cover', 'غير صورة الغلاف الخاص به', 'Veranderd zijn profiel deksel', 'Changé sa couverture de profil', 'hat sein Titelbild geändert', 'Cambiato la sua copertura del profilo', 'Trocou sua capa de perfil', 'Сменил обложку', 'Cambió su foto de perfil', 'Onun profil kapağı Değiştirildi'),
(532, 'changed_profile_cover_picture_female', 'Changed her profile cover', 'غيرت صورة الغلاف الخاصة بها', 'Veranderde haar profiel deksel', 'Changé son profil couvercle', 'hat ihr Titelbild geändert', 'Cambiato suo profilo baiar', 'Trocou sua capa de perfil', 'Сменила обложку', 'Cambió su foto de perfil', 'Onun profil kapağı Değiştirildi'),
(533, 'latest_games', 'Latest games', 'آخر الألعاب', 'Nieuwste games', 'Derniers jeux', 'Neueste Spiele', 'Ultimi giochi', 'Jogos novos', 'Последние игры', 'Últimos Juegos', 'Son Eklenen Oyunlar'),
(534, 'no_albums_found', 'No albums found', 'لا يوجد البومات', 'Geen albums gevonden', 'Aucun album n&#39;a été trouvé', 'Kein Album gefunden', 'Nessun album trovato', 'Nenhum &amp;aacute;lbum encontrado', 'Альбомов не найдено', 'No hay álbumes encontrados', 'Albüm bulunamadı'),
(535, 'create_album', 'Create album', 'إنشاء ألبوم', 'Maak een album', 'Créer un album', 'Album erstellen', 'Creare album', 'Criar &amp;aacute;lbum', 'Создать альбом', 'Crear albúm', 'Albüm oluştur'),
(536, 'my_albums', 'My Albums', 'البوماتي', 'Mijn Albums', 'Mes albums', 'Meine Alben', 'I miei album', 'Meus &amp;aacute;lbuns', 'Мои альбомы', 'Mis álbumes', 'Albümlerim'),
(537, 'album_name', 'Album name', 'اسم الالبوم', 'Albumnaam', 'Nom de l&#39;album', 'Name des Albums', 'Nome album', 'Nome do &amp;aacute;lbum', 'Название альбома', 'Nombre del álbum', 'Albüm adı'),
(538, 'upload', 'Upload', 'رفع', 'Uploaden', 'Télécharger', 'Hochladen', 'Caricare', 'Carregar', 'Загрузить', 'Subir', 'Yükleme'),
(539, 'add_photos', 'Add photos', 'إضافة صور', 'Foto&#039;s toevoegen', 'Ajouter des photos', 'Fotos hinzufügen', 'Aggiungi foto', 'Adicionar fotos', 'Добавить фотографии', 'Agregar fotos', 'Fotoğraf ekle'),
(540, 'replies', 'Replies', 'ردود', 'Antwoorden', 'Réponses', 'Antworten', 'risposte', 'Respostas', 'Ответы', 'Respuestas', 'Cevaplar'),
(541, 'reply_to_comment', 'Reply to comment', 'ردعلى التعليق', 'Reageer op reactie', 'Répondre au commentaire', 'Auf Kommentar antworten', 'Rispondi al commento', 'Responder o coment&amp;aacute;rio', 'Ответить на комментарий', 'Responder al comentario', 'Yorumu yanıtla'),
(542, 'replied_to_comment', 'replied to your comment &quot;{comment}&quot;', 'رد على تعليقك &quot;{comment}&quot;', 'beantwoord je reactie &quot;{comment}&quot;', 'répondu à votre commentaire &quot;{comment}&quot;', 'hat auf Deinen Kommentar geantwortet &quot;{comment}&quot;', 'Risposto al tuo commento &quot;{comment}&quot;', 'respondeu seu comentário  &quot;{comment}&quot;', 'ответил@ на ваш комментарий &quot;{comment}&quot;', 'respondió a tu comentario &quot;{comment}&quot;', 'Yorumlarınız yanıtladı &quot;{comment}&quot;'),
(543, 'comment_reply_mention', 'mentioned you in a reply &quot;{comment}&quot;', 'ذكرك في رد على تعليق &quot;{comment}&quot;', 'je genoemd in een antwoord &quot;{comment}&quot;', 'vous avez mentionné dans une réponse &quot;{comment}&quot;', 'hat dich in einer Antwort erwähnt &quot;{comment}&quot;', 'ti ha menzionato in una risposta &quot;{comment}&quot;', 'mencionou você em uma resposta &quot;{comment}&quot;', 'упомянул@ вас в комментарии &quot;{comment}&quot;', 'te ha mencionado en una respuesta &quot;{comment}&quot;', 'bir cevapta sizden bahsetti &quot;{comment}&quot;'),
(544, 'also_replied', 'replied to the comment &quot;{comment}&quot;', 'رد على التعليق &quot;{comment}&quot;', 'antwoordde op de reactiefeed &quot;{comment}&quot;', 'répondu au commentaire &quot;{comment}&quot;', 'hat auf den Kommentar zurück kommentiert &quot;{comment}&quot;', 'risposto al commento &quot;{comment}&quot;', 'respondeu o comentário &quot;{comment}&quot;', 'ответил@ на комментарий &quot;{comment}&quot;', 'respondió al comentario &quot;{comment}&quot;', 'yorumuna cevap verdi, &quot;{comment}&quot;'),
(545, 'liked_reply_comment', 'liked your reply &quot;{comment}&quot;', 'أعجب بردك &quot;{comment}&quot;', 'vond uw antwoord &quot;{comment}&quot;', 'aimé votre réponse &quot;{comment}&quot;', 'gefält deine Antwort &quot;{comment}&quot;', 'piaciuto vostra risposta &quot;{comment}&quot;', 'curtiu sua resposta &quot;{comment}&quot;', 'понравился ваш ответ &quot;{comment}&quot;', 'gustado su respuesta &quot;{comment}&quot;', 'Cevabınızı &quot;{comment}&quot; sevdi'),
(546, 'wondered_reply_comment', 'wondered your reply &quot;{comment}&quot;', 'تعجب بردك &quot;{comment}&quot;', 'afgevraagd uw antwoord &quot;{comment}&quot;', 'demandé votre réponse &quot;{comment}&quot;', 'wundert sich über deine Antwort &quot;{comment}&quot;', 'wondered la tua risposta &quot;{comment}&quot;', 'n&amp;atilde;o curtiu sua resposta &quot;{comment}&quot;', 'Не нравится ваш ответ &quot;{comment}&quot;', 'preguntó su respuesta &quot;{comment}&quot;', 'Cevabınızı &quot;{comment}&quot; merak'),
(547, 'disliked_reply_comment', 'disliked your reply &quot;{comment}&quot;', 'لم يعجب بردك &quot;{comment}&quot;', 'hekel aan uw antwoord &quot;{comment}&quot;', 'détesté votre réponse &quot;{comment}&quot;', 'gefällt deine Antwort &quot;{comment}&quot;', 'non amava la sua risposta &quot;{comment}&quot;', 'n&amp;atilde;o curtiu sua resposta &quot;{comment}&quot;', 'Не нравится ответ &quot;{comment}&quot;', 'no le gustaba su respuesta &quot;{comment}&quot;', 'Cevabınızı &quot;{comment}&quot; sevmiyordu'),
(548, 'profile_visit_notification_p', 'Send users a notification when i visit their profile?', 'إرسال المستخدمين إخطارا عندما أقوم بزيارة صفحته الشخصية؟', 'Stuur gebruikers een melding wanneer ik bezoek hun profiel?', 'Envoyer utilisateurs une notification lorsque je visite son profil?', 'Benutzern eine Benachrichtigung senden, wenn ich ihr Profil besucht habe?', 'Inviare agli utenti una notifica durante la mia visita il loro profilo?', 'Enviar uma notificação para outros usuários quando eu visitar o perfil deles?', 'Отправлять пользователям уведомления, когда я посещаю их профили?', '¿Enviar a los usuarios aviso de visita?', 'Ben kendi profilini ziyaret ettiğinizde kullanıcılara bir bildirim gönder?');
INSERT INTO `Wo_Langs` (`id`, `lang_key`, `english`, `arabic`, `dutch`, `french`, `german`, `italian`, `portuguese`, `russian`, `spanish`, `turkish`) VALUES
(549, 'showlastseen_help', 'if you don&#039;t share your last seen , you won&#039;t be able to see other people last seen.', 'اذا لم تشارك اخر ظهور لك , فانك لن تكون قادرا على رؤية اخر ظهور المستخدمين.', 'als je het niet eens met je voor het laatst gezien, zult u niet in staat zijn om andere mensen het laatst gezien.', 'si vous ne partagez pas votre dernière fois, vous ne serez pas en mesure de voir d&#039;autres personnes la dernière fois.', 'wenn du nicht teilen willst was du dir als letztes angesehen hast, kannst Du auch nicht sehen was andere sich angesehen haben.', 'se non si condivide il visto l&#039;ultima volta, non sarà in grado di vedere altre persone visto l&#039;ultima volta.', 'Se você não quiser mostrar para as pessoas que visitou o perfil delas, você também não poderá ver quem visitou seu perfil.', 'Если отключить это уведомление, вы не сможете видеть последнее подключение других пользователей..', 'Si desactivas esta notificación tu tampoco podrás ver la conexión de otros usuarios.', 'Eğer son görüldüğü paylaşmak yoksa, son görüldüğü diğer insanları görmek mümkün olmayacaktır.'),
(550, 'timeline_birthday_label', 'Who can see my birthday?', 'من يمكنه رؤية تاريخ ميلادي؟', 'Wie kan mijn verjaardag zien?', 'Qui peut voir mon anniversaire?', 'Wer kann mein Geburtstag sehen?', 'Chi può vedere il mio compleanno?', 'Quem pode ver a data do meu anivers&amp;aacute;rio?', 'Кто может видеть мой день рождения?', '¿Quién puede ver mi cumpleaños?', 'Kim benim doğum günüm görebilir?'),
(551, 'people_likes_this', 'people like this', 'مستخدم معجبين بهذا', 'mensen vinden dit leuk', 'personnes aiment ce', '„Gefällt mir“ Angaben', 'persone piace questo', 'pessoas gostaram disso', 'Нравится', 'Me gusta', 'Bu gibi insanlar'),
(552, 'page_inviate_label', 'Invite friends to like this Page', 'إدعو أصدقائك للإجاب بهذه الصفحة', 'Vrienden uitnodigen om deze pagina graag', 'Inviter des amis à aimer cette page', 'Freunde einladen, denen diese Seite gefallen könnte', 'Invita gli amici a piacere questa Pagina', 'Convidar pessoas para curtir essa página', 'Пригласить друзей', 'Invitar amigos', 'Sayfaya sevmeye arkadaşlarınızı davet edin'),
(553, 'invite_your_frineds', 'Invite your friends/followers', 'إدعوا اصدقائك/متابعينك', 'Nodig je vrienden / volgelingen', 'Invitez vos amis / followers', 'Laden Sie Ihre Freunde / Follower', 'Invita i tuoi amici / seguaci', 'Convidar seus amigos/seguidores', 'Пригласить друзей', 'Invita a tus amigos / seguidores para ver si les gusta esto', 'Arkadaşların / takipçileri davet'),
(554, 'not_invite', 'You don&#039;t have any more friends to invite', 'لا يوجد المزيد للدعوة', 'U hoeft niet meer vrienden uitnodigen', 'Vous ne disposez pas d&#039;autres amis à inviter', 'Du hast keine weiteren Freunde eingeladen', 'On avete più amici per invitare', 'Você já convidou todos seus amigos', 'У Вас нет друзей, чтобы пригласить', 'No tienes más amigos que invitar...', 'Davet etmek artık arkadaş yok'),
(555, 'invite', 'Invite', 'إدعو', 'Nodigen', 'Invitez', 'Einladen', 'Invitare', 'Convidar', 'Пригласить', 'Invitación', 'Davet etmek'),
(556, 'invited_page', 'invited you to like ({page_name})', 'دعاك للاعجاب بل الصفحة ({page_name})', 'u uitgenodigd om te willen ({page_name})', 'vous invite à aimer ({page_name})', 'Ich möchte dich gerne zu ({page_name}) einladen', 'invitato a piacere ({page_name})', 'Convidou você para curtir ({page_name})', 'предлагает вам отметить страницу ({page_name}) как понравившуюся', 'te invito para ver si te gusta ({page_name})', 'Hoşunuza davet etti ({page_name})'),
(557, 'accepted_invited_page', 'accepted your request to like ({page_name})', 'قبل دعوتك للإجاب ب ({page_name})', 'aanvaard uw verzoek te willen ({page_name})', 'accepté votre demande d&#39;aimer ({page_name})', 'Deine Beitrittsanfrage für die Seite ({page_name}) wurde genehmigt', 'accettato la richiesta di piacere ({page_name})', 'aceitou sua solicita&amp;ccedil;&amp;atilde;o para curtir ({page_name})', 'принял@ ваше приглашение в ({page_name})', 'acepto tu invitación a ({page_name})', 'İsteğiniz sevmeye kabul edilir ({page_name})'),
(558, 'call_to_action', 'Call to action', 'Call to action', 'Oproep tot actie', 'Appel à l&#39;action', 'Was möchtest du tun?', 'Chiamare all&#039;azione', 'Ligar a&amp;ccedil;&amp;atilde;o', 'Призыв к действию', 'Llamar a la acción', 'Eylem çağrısı'),
(559, 'call_to_action_target', 'Call to target url', 'Call to target url', 'Bellen om url richten', 'Appel à l&#39;URL cible', 'Rufe das URL-Ziel auf', 'Chiama per indirizzare url', 'Ligar a uma URL definida', 'Введите URL сайта', 'Insertar url', 'Url hedef Çağrı'),
(560, 'call_action_type_url_invalid', 'Call to action website is invalid', 'Call to action website is invalid', 'Oproep tot actie website is ongeldig', 'Appel à l&#039;action est site de invalide', 'Es besteht Handlungsbedarf, Website ist ungültig', 'Chiama per il sito di azione non è valido', 'Website inválido', 'Неправильный URL', 'Llamado a la página web de acción no es válido', 'Eylem web Çağrı geçersiz'),
(561, 'avatar_and_cover', 'Avatar &amp; Cover', 'الصورة الشخصية والغلاف', 'Avatar &amp; Cover', 'Avatar &amp; Cover', 'Profil- und Titelbild', 'Avatar &amp; Coprire', 'Avatar &amp; Capa', 'Аватар и обложка', 'Avatar y Fondo', 'Avatar &amp; Kapak'),
(562, 'online_sidebar_admin_label', 'Enable online users to show active users in sidebar.', 'مكن لإظهار المستخدمين النشطين في الشريط الجانبي.', 'Toelaten online gebruikers actieve gebruikers te laten zien in de zijbalk.', 'Permettre aux utilisateurs en ligne pour montrer aux utilisateurs actifs dans la barre latérale.', 'Aktivieren Internetuser zu aktiven Nutzern in Seitenleiste zeigen.', 'Abilita utenti per mostrare agli utenti attivi in sidebar.', 'Permitir que Usuários visualizem os Usuários ativos na barra lateral.', 'Включить онлайн-пользователей, показать активных пользователей в боковой панели.', 'Permita que los usuarios en línea para usuarios activos mostrar en la barra lateral.', 'Kenar çubuğundaki aktif kullanıcıya göstermek için çevrimiçi kullanıcıları etkinleştirin.'),
(563, 'not_active', 'Not active', 'غير فعال', 'Niet actief', 'Pas actif', 'Nicht aktiv', 'Non attivo', 'Não ativo', 'Не активен', 'No activo', 'Aktif değil'),
(564, 'females', 'Females', 'الإناث', 'Niet geactiveerd', 'Femmes', 'Frauen', 'Femmine', 'Mulheres', 'Женщины', 'Las hembras', 'Dişiler'),
(565, 'males', 'Males', 'الذكور', 'Mannetjes', 'Les mâles', 'Männlich', 'Maschi', 'Homens', 'Мужчины', 'Los machos', 'Erkekler'),
(566, 'dashboard', 'Dashboard', 'اللوحة الرئيسية', 'Dashboard', 'Tableau de bord', 'Übersicht', 'Cruscotto', 'Painel', 'Информационная панель', 'Tablero de instrumentos', 'Dashboard'),
(567, 'albums', 'Albums', 'الألبومات', 'Albums', 'Albums', 'Alben', 'Albums', '&amp;aacute;lbuns', 'Альбомы', 'Álbumes', 'Albümler'),
(568, 'name', 'Name', 'الإسم', 'Naam', 'Prénom', 'Name', 'Nome', 'Nome', 'Имя', 'Nombre', 'Isim'),
(569, 'players', 'Players', 'اللاعبين', 'Spelers', 'Des joueurs', 'Spieler', 'Giocatori', 'Jogadores', 'Игроки', 'Jugadores', 'Oyuncular'),
(570, 'add_new_game', 'Add New Game', 'إضافة لعبة جديدة', 'Voeg een nieuwe game', 'Ajouter un nouveau jeu', 'Neues Spiel hinzufügen', 'Aggiungi nuovo gioco', 'Adicionar um novo jogo', 'Добавить новую игру', 'Añadir Nuevo Juego', 'Yeni Oyun Ekle'),
(571, 'game_added', 'Game added', 'تم الإضافة بنجاح', 'Spel toegevoegd', 'jeu ajouté', 'Spiel hinzugefügt', 'Gioco aggiunto', 'Jogo adicionado', 'Игра добавлена', 'Juego añadió', 'Oyun eklendi'),
(572, 'url_not_supported_game', 'This url is not supported', 'هذا الرابط غير مدعوم', 'Deze url wordt niet ondersteund', 'Cet URL est pas pris en charge', 'Diese URL wird nicht unterstützt', 'Questo URL non è supportata', 'URL inválida', 'Этот URL-адрес не поддерживается', 'Esta url no es compatible', 'Bu url desteklenmiyor'),
(573, 'please_add_a_game', 'Please add a game url', 'يرجى إضافة رابط لعبة', 'Voeg dan een spel url', 'S&#39;il vous plaît ajouter une url de jeu', 'Bitte füge ein Spiel hinzu', 'Si prega di aggiungere un URL di gioco', 'Please add a game url', 'Пожалуйста, добавьте игру URL', 'Por favor, añada una url juego', 'Bir oyun url ekleyin'),
(574, 'active_announcements', 'Active announcements', 'إعلانات نشطة', 'actieve aankondigingen', 'Annonces actives', 'Aktive Ankündigungen', 'Annunci attivi', 'Avisos ativos', 'Активные объявления', 'Anuncios activos', 'Aktif duyurular'),
(575, 'inactive_announcements', 'Inactive announcements', 'إعلانات غير نشطة', 'inactief aankondigingen', 'Annonces inactifs', 'Inaktive Ankündigungen', 'Annunci inattivi', 'Avisos in&amp;aacute;tivos', 'Неактивные объявления', 'Anuncios inactivos', 'Etkin olmayan duyurular'),
(576, 'ban', 'Ban', 'حظر', 'Ban', 'Ban', 'Verbot', 'Bandire', 'Ban', 'Запрет', 'Prohibición', 'Yasak'),
(577, 'new_email', 'New E-mail', 'رسالة جديدة', 'Nieuwe E-mail', 'Nouveau E-mail', 'Neue Email', 'Nuova Email', 'Novo e-mail', 'Новый E-mail', 'Nuevo Email', 'Yeni e-posta'),
(578, 'html_allowed', 'Html allowed', 'ال html مسموح', 'Html toegestaan', 'HTML autorisée', 'HTML erlaubt', 'Html permesso', 'HTML permitida', 'HTML разрешено', 'Html permitido', 'Html izin'),
(579, 'send_me_to_my_email', 'Send to my email', 'ارسل الى بريدي الالكتروني', 'Stuur naar mijn e-mail', 'Envoyer à mon e-mail', 'An meine Emailadresse senden', 'Invia alla mia email', 'Enviar para o meu email', 'Отправить на мою электронную почту', 'Enviar a mi correo electrónico', 'Benim e-posta gönder'),
(580, 'test_message', 'Test message', 'جرب الراسلة أولا', 'Test bericht', 'Message test', 'Testnachricht', 'Messaggio di testo', 'Mensagem teste', 'Тестовое сообщение', 'Mensaje de prueba', 'Deney mesajı'),
(581, 'joined_members', 'Joined Members', 'الأعضاء', 'Toegetreden leden', 'Membres Inscrit', 'Registrierte Mitglieder', 'Iscritto membri', 'Membros', 'Регистрация Пользователей', 'Miembros Antigüedad', 'Katılım Üyeler'),
(582, 'join_requests', 'Join Requests', 'طلبات الإنضمام', 'Join Verzoeken', 'Rejoignez Demandes', 'Registrierte Anfragen', 'Join Richieste', 'Pedidos para entrar', 'Регистрация запросов', 'Únete Solicitudes', 'İstekler katılın'),
(583, 'verified_pages', 'Verified Pages', 'الصفحاتالؤكدة', 'Verified Pages', 'Pages Vérifié', 'Verifizierte Seiten', 'Verificato Pagine', 'Páginas verificadas', 'Официальные страницы', 'Verificado Páginas', 'Doğrulanmış Sayfalar'),
(584, 'file_sharing_extenstions', 'File sharing extensions (separated with comma,)', 'ملحقات تبادل الملفات (مفصولة بفاصلة،)', 'Sharing bestandsextensies (gescheiden met een komma,)', '', 'Daten-Transfer-Erweiterungen (mit Komma getrennt,)', 'Estensioni di file sharing (separati da una virgola,)', 'Compartilhar arquivos (separados por uma v&amp;iacute;rgula,)', 'Расширения обмена файлов (через запятую,)', 'Extensiones de intercambio de archivos (separados con comas,)', 'Dosya paylaşımı uzantıları (virgül ile ayrılmış)'),
(585, 'word_cons', 'Words to be censored, separated by a comma (,)', 'كلمات البذيئة، مفصولة بفاصلة (،)', 'Woorden worden gecensureerd, gescheiden door een komma (,)', 'Partage de fichiers extensions (séparées par des virgules,)', 'Zensierte Worte mit einem Komma trennen, (,)', 'Parole da censurati, separati da una virgola (,)', 'Palavras censuradas, separadas por v&amp;iacute;rgula (,)', 'Слова подвергаться цензуре, разделенных запятыми (,)', 'Palabras para ser censurados, separados por una coma (,)', 'Kelimeler sansür edilmesi, virgülle ayrılmış (,)'),
(586, 'join', 'Join', 'أنضم', 'Toetreden', 'Joindre', 'Beitreten', 'Aderire', 'Entrar', 'Вступить', 'Unirse', 'Katılmak'),
(587, 'joined', 'Joined', 'منضم', 'Geregistreerd', 'Inscrit', 'Beigetreten', 'Iscritto', 'Entrou', 'Выйти', 'Unido', 'Katılım'),
(588, 'request', 'Request', 'اطلب', 'Verzoek', 'Demande', 'Anfordern', 'Richiesta', 'Solicitar', 'Запрос', 'Petición', 'İstek'),
(589, 'edit_comment', 'Edit comment', 'تحرير تعليق', 'Reactie bewerken', 'Modifier commentaire', 'Kommentar bearbeiten', 'Modifica commento', 'Editar comentário', 'Редактировать комментарий', 'Editar comentario', 'Düzenleme Yorum'),
(590, 'last_play', 'Last Play:', 'آخر نشاط', 'Laatste Play:', 'Dernière lecture:', 'Letztes Spiel:', 'Ultimo Gioco:', '&amp;uacute;ltimo jogo:', 'Последняя игра:', 'Último juego:', 'Son Oyun:'),
(591, 'play', 'Play', 'العب', 'Spelen', 'Joue', 'Spielen', 'Giocare', 'Jogar', 'Играть', 'Jugar', 'Oyna'),
(592, 'confirm_request_group_privacy_label', 'Confirm request when someone joining this group ?', 'إرسال طلب عندما يقوم شخص بل الإنضمام لهذه المجموعة؟', 'Bevestigt aanvraag als iemand mee deze groep?', 'Confirmer la demande lorsque quelqu&#39;un se joindre à ce groupe?', 'Anfrage bestätigen, wenn jemand dieser Gruppe beitreten will?', 'Confermare richiesta quando qualcuno entrare in questo gruppo ?', 'Confirmar solicita&amp;ccedil;&amp;atilde;o quando algu&amp;eacute;m quiser fazer parte do grupo ?', 'Подтверждать запрос когда, кто-то хочет присоединиться к этой группе?', 'Confirmar pedido cuando alguien unirse a este grupo?', 'Birisi bu gruba katılmadan isteği onaylayın?'),
(593, 'who_can_see_group_posts', 'Who can see group&#039;s posts ?', 'Who can see group&#039;s posts ?', 'Wie kan groepen berichten zien?', 'Qui peut voir des groupes messages?', 'Wer kann Gruppenbeiträge sehen?', 'Chi può vedere gruppi di messaggi?', 'Quem pode ver os posts do grupo ?', 'Кто может видеть сообщения группы?', '¿Quién puede ver los mensajes de este grupo?', 'Kim grubun mesajları görebilirim?'),
(594, 'joined_users', 'Joined users', 'الأعشاء المنضمين', 'Geregistreerd gebruikers', 'Inscrit utilisateurs', 'Registriert Nutzer', 'Iscritto utenti', 'Usuários', 'Вступившие пользователи', 'Usuarios Antigüedad', 'Katılım kullanıcılar'),
(595, 'living_in', 'Living in', 'يسكن في', 'Leven in', 'Vivre dans', 'Lebt in', 'Residente a', 'Morando em', 'Страна', 'Viviendo en', 'Yaşayan'),
(596, 'design', 'Design', 'تصميم', 'Design', 'Design', 'Design', 'Design', 'Design', 'дизайн', 'Desiño', 'Dizayn'),
(597, 'people_you_may_want_to_meet', 'People you may want to meet', 'Pأعضاء قد ترغل في لقائهم', 'Mensen die je misschien wilt ontmoeten', 'Les personnes que vous pouvez rencontrer', 'Vielleicht kennst du', 'La gente si consiglia di rispettare', 'Pessoas que você talvez conheça', 'Люди, которых вы можете знать', 'La gente es posible que desee conocer', 'İnsanlar karşılamak isteyebilirsiniz'),
(598, 'added_new_photos_to', 'added new photos to', 'أضاف صور جديدة الى', 'Toegevoegd nieuwe foto&#039;s aan', 'ajoutés nouvelles photos à', 'hat neue Fotos hinzugefügt', 'aggiunte nuove foto', 'adicionou novas fotos', 'Добавлены новые фотографии в', 'añadido nuevas fotos a', 'eklenen yeni fotoğraf'),
(599, 'is_feeling', 'is feeling', 'يشعر ب', 'is het gevoel', 'est le sentiment', 'ist', 'è la sensazione', 'se sentindo', 'это чувство', 'es la sensación', 'duygu olduğunu'),
(600, 'is_traveling', 'is traveling to', 'يسافر إلى', 'is reizen naar', 'se rend à', 'ist auf Reisen', 'è un viaggio in', 'viajando para', 'едет в', 'está viajando a', 'için seyahat ediyor'),
(601, 'is_listening', 'is listening to', 'يستمع إلى', 'luistert naar', 'écoute', 'hört zu', 'è l&#039;ascolto', 'ouvindo', 'слушает', 'está escuchando', 'dinliyor'),
(602, 'is_playing', 'is playing', 'يلعب ب', 'speelt', 'est en train de jouer', 'spielt', 'sta giocando', 'jogando', 'играет', 'está jugando', 'oynuyor'),
(603, 'is_watching', 'is watching', 'يشاهد', 'is aan het kijken', 'regarde', 'beobachtet', 'sta guardando', 'assistindo', 'смотрит', 'esta viendo', 'izliyor'),
(604, 'feeling', 'Feeling', 'يشعر', 'Gevoel', 'Sentiment', 'Gefühl', 'Sensazione', 'Sentindo', 'Настроение', 'Sensación', 'Duygu'),
(605, 'traveling', 'Traveling to', 'يسافر', 'Reizen naar', 'Voyager à', 'Reisen', 'In viaggio verso', 'Viajando para', 'Путешествую', 'Viajando a', 'Seyahat'),
(606, 'watching', 'Watching', 'يشاهد', 'Kijken', 'En train de regarder', 'Ansehen', 'Guardando', 'Assistindo', 'Смотрю', 'Acecho', 'İzlenen'),
(607, 'playing', 'Playing', 'يلعب', 'Spelen', 'En jouant', 'Spielend', 'Giocando', 'Jogando', 'Играю', 'Jugando', 'Oynama'),
(608, 'listening', 'Listening to', 'يستمع إلى', 'Luisteren naar', 'Écouter', 'Hören', 'Ascoltare', 'ouvindo', 'Слушаю', 'Escuchar', 'Dinliyorum'),
(609, 'feeling_q', 'What are you feeling ?', 'بماذا تعشر؟', 'Wat voel je ?', 'Que ressentez vous ?', 'Was fühlst du ?', 'Cosa senti ?', 'Como esta se sentindo?', 'Что чувствуете?', 'Que estás sintiendo ?', 'Ne hissediyorsun ?'),
(610, 'traveling_q', 'Where are you traveling ?', 'الى أين تسافر', 'Waar wilt u verblijven?', 'Où êtes-vous?', 'Wohin möchtest du reisen?', 'Dove si viaggia ?', 'Para onde esta viajando ?', 'Куда едите?', 'A donde viajas ?', 'Nereye seyahat?'),
(611, 'watching_q', 'What are you watching ?', 'ماذا تشاهد؟', 'Waar ben je naar aan het kijken ?', 'Qu&#39;est-ce que vous regardez ?', 'Was schaust Du ?', 'Cosa stai guardando ?', 'O que esta assistindo ?', 'Что смотришь?', 'Qué estás viendo ?', 'Ne izliyorsun ?'),
(612, 'playing_q', 'What are you Playing ?', 'ماذا تلعب؟', 'Wat ben je aan het spelen ?', 'A quoi tu joues ?', 'Was spielst du ?', 'A cosa stai giocando ?', 'O que esta jogando ?', 'Во что играешь?', '¿A qué juegas?', 'Ne oynuyorsun ?'),
(613, 'listening_q', 'What are you Listening to ?', 'إلى ماذا تستمع؟', 'Waar ben je naar aan het luisteren ?', 'Qu&#39;écoutes-tu ?', 'Was hörst du ?', 'Cosa stai ascoltando ?', 'O que esta ouvindo ?', 'Что слушаешь?', 'Qué estás escuchando ?', 'Ne dinliyorsun ?'),
(614, 'feel_d', 'What are you doing ?', 'ماذا تغعل؟', 'Wat ben je aan het doen ?', 'Qu&#39;est-ce que tu fais ?', 'Was machst Du?', 'Che stai facendo?', 'O que esta fazendo ?', 'Что делаете?', 'Que estas haciendo ?', 'Ne yapıyorsun ?'),
(615, 'studying_at', 'Studying at', 'يدرس في', 'Studeren aan', 'Etudier à', 'Studiert an', 'Studiare a', 'Estudando em', 'Образование', 'Estudiando en', 'Öğrenim'),
(616, 'company_website_invalid', 'Company website is invalid', 'موقع الشركة غير صالح', 'Website van het bedrijf is ongeldig', 'Site de la société est invalide', 'Unternehmens-Website ist ungültig', 'Sito internet della Società non è valido', 'Site da empresa inválido', 'Веб-сайт компании является недействительным', 'Página web de la empresa no es válido', 'Şirket web sitesi geçersiz'),
(617, 'page_deleted', 'Page deleted successfully', 'الصفحة حذفت بنجاح', 'Pagina succesvol verwijderd', 'Page supprimée avec succès', 'Seite erfolgreich gelöscht', 'Pagina eliminato con successo', 'Página deletada', 'Страница успешно удалена', 'Página eliminado correctamente', 'Sayfa başarıyla silindi'),
(618, 'cover_n_label', 'cover image.', 'صورة الغلاف.', 'Bedekken afbeelding.', 'Image de couverture.', 'Titelbild.', 'immagine di copertina.', 'Capa.', 'обложка.', 'Imagen de portada.', 'Kapak resmi.'),
(619, 'suggested_groups', 'Suggested groups', 'المجموعات المقترحة', 'Suggereerde groepen', 'Suggestion de groupes', 'Empfohlene Gruppen', 'Gruppi consigliati', 'Grupos sugeridos', 'Рекомендуемые группы', 'Grupos sugeridos', 'Önerilen gruplar'),
(620, 'accepted_join_request', 'accepted your request to join ({group_name})', 'قبل طلب للإنضمام الى ({group_name})', 'aanvaard uw verzoek om lid te worden ({group_name})', 'accepté votre demande d&#39;adhésion ({group_name})', 'Deine Beitrittsanfrage wurde akzeptiert ({group_name})', 'accettato tua richiesta di iscrizione ({group_name})', 'aceitou sua solicita&amp;ccedil;&amp;atilde;o para se juntar ao ({group_name})', 'Запрос принят на вступление в ({group_name})', 'aceptó su solicitud para unirse ({group_name})', 'İsteğiniz katılmak için kabul edilir ({group_name})'),
(621, 'requested_to_join_group', 'requested to join your group', 'طلب منك الإنضمام الى مجموعتك', 'verzocht om uw groep aan te sluiten', 'demandé à rejoindre votre groupe', 'lädt dich ein, dieser Gruppe beizutreten', 'richiesto di unirsi al vostro gruppo', 'pediu para entrar no seu grupo', 'хочет присоединиться к вашей группе', 'solicitado a unirse a su grupo', 'senin gruba katılmak istedi'),
(622, 'no_one_posted', 'No one posted yet', 'لا يوجد اي منشور بعد', 'Maar niemand geplaatst', 'Personne encore posté', 'Doch niemand geschrieben', 'Nessuno ha scritto ancora', 'Nenhum post ainda', 'Еще ничего не опубликовано', 'Nadie ha escrito todavía', 'Henüz hiç kimse gönderildi'),
(623, 'add_your_frineds', 'Add your friends to this group', 'إضافة أصدقائك إلى هذه المجموعة', 'Voeg uw vrienden aan deze groep', 'Ajouter à vos amis de ce groupe', 'Füge deine Freunde zu dieser Gruppe hinzu', 'Aggiungi ai tuoi amici di questo gruppo', 'Adicionar amigos à este grupo', 'Добавить друзей в эту группу', 'Añadir amigos a este grupo', 'Bu gruba arkadaşlarınızı ekleyin'),
(624, 'added_all_friends', 'There are no friends to add them', 'لا يوجد أصدقاء للإضافة', 'Er zijn geen vrienden om ze toe te voegen', 'Il n&#39;y a aucun ami à les ajouter', 'Es gibt keine Freunde, um sie hinzuzufügen', 'Non ci sono amici da aggiungere loro', 'Nenhum amigo dispon&amp;iacute;vel para ser adicionado', 'Добавить всех друзей', 'No hay amigos para agregarlos', 'Eklemek için hiçbir arkadaş yok'),
(625, 'added_you_to_group', 'added you to the group ({group_name})', 'أضافك الى المجموعة ({group_name})', 'u hebt toegevoegd aan de groep ({group_name})', 'vous ajouté au groupe ({group_name})', 'hat dich zur Gruppe ({group_name}) hinzugefügt', 'ti ha aggiunto al gruppo ({group_name})', 'adicionado ao grupo ({group_name})', 'добавил@ вас в группу ({group_name})', 'te agrego al grupo ({group_name})', 'sizi grubuna ekledi ({group_name})'),
(626, 'group_type', 'Group type', 'نوع المجموعة', 'groepstype', 'Type de groupe', 'Gruppentyp', 'Tipo di gruppo', 'Estilo do Grupo', 'Тип группы', 'Tipo de grupo', 'Grup türü'),
(627, 'public', 'Public', 'عام', 'Openbaar', 'Public', 'Öffentlichkeit', 'Pubblico', 'P&amp;uacute;blico', 'Открытая группа', 'Público', 'Kamu'),
(628, 'private', 'Private', 'خاص', 'Private', 'Privé', 'Privat', 'Privato', 'Privado', 'Закрытая группа', 'Privado', 'Özel'),
(629, 'reports', 'Reports', 'الإبلاغات', 'Rapporten', 'Rapports', 'Meldungen', 'Rapporti', 'Reportes', 'Отчеты', 'Informes', 'Raporlar'),
(630, 'no_dislikes', 'No dislikes yet', 'لا يوجد غير معجبين', 'nog geen hekel', 'Pas encore aversions', 'Keiner dem das nicht gefällt', 'Non hai ancora un antipatie', 'Nenhum dislike ainda', 'Пока нет &quot;Не нравится&quot;', 'Sin embargo no le gusta', 'Henüz sevmeyen'),
(631, 'disliked', 'Disliked', 'غير معجب', 'Bevallen', 'N&#039;a pas aimé', 'unbeliebt', 'Malvisto', 'Não curtiu', 'Не нравится', 'No me gustó', 'Sevmediği'),
(632, 'wondered', 'Wondered', 'متعجب', 'Afgevraagd', 'Demandé', 'Verwundert', 'Si chiese', 'N&amp;atilde;o curtiu', 'Не нравится', 'Se preguntó', 'Merak eti'),
(633, 'terms', 'Terms Pages', 'صفحات الموقع', 'Algemene Pagina', 'Conditions Pages', 'Allgemeine Seiten', 'Condizioni Pagine', 'Termos', 'Правила и условия', 'Condiciones Páginas', 'Şartlar Sayfalar'),
(634, 'profile_privacy', 'User Profile Privacy', 'خصوصية الحساب الشخصي', 'User Profile Privacy', 'Profil de confidentialité', 'Benutzerprofil Datenschutz', 'Profilo Utente Privacy', 'Privacidade do perfil', 'Профиль конфиденциальности', 'Perfil de usuario de Privacidad', 'Kullanıcı Profili Gizlilik'),
(635, 'profile_privacy_info', 'Enable it to allow non logged users to view users profiles.', 'مكن هذه الميزة للسماح بعرض المستخدمين لغير المسجلين.', 'In staat stellen om niet-aangemelde gebruikers gebruikers profielen te bekijken.', 'Activer qu&#039;il permette non connecté aux utilisateurs de voir les profils des utilisateurs.', 'Aktivieren Sie es, damit nicht angemeldete Benutzer, um Benutzer Profile anzusehen.', 'Consentirle di consentire agli utenti non registrati di visualizzare profili utenti.', 'Permitir usuários que não estão registrados para ver os perfis.', 'Включите его, чтобы не являющихся зарегистрированные пользователи для просмотра профили пользователей.', 'Activar para permitir que los usuarios no iniciar sesión para ver los perfiles de los usuarios.', 'Olmayan açmış olan kullanıcılar profillerini görüntülemek için izin için etkinleştirin.'),
(636, 'video_upload', 'Video Upload', 'رفع الفيديو', 'Video uploaden', 'Video Upload', 'Video hochladen', 'Carica video', 'Carregar v&amp;iacute;deo', 'Видео Загрузить', 'Vídeo Subir', 'Video Yükleme'),
(637, 'video_upload_info', 'Enable video upload to share &amp; upload videos to the site.', 'مكن هذه الميزة  لتحميل وتبادل الفيديو على الموقع.', 'Enable video uploaden om te delen en video&#039;s uploaden naar de site.', 'Activer télécharger la vidéo pour partager et télécharger des vidéos sur le site.', 'Aktivieren Sie Video-Upload zu teilen und Videos auf der Website.', 'Abilita video upload per condividere e caricare i video al sito.', 'Carregar v&amp;iacute;deo e compartilhar ele no site.', 'Включить видео загрузки, чтобы разделить и загрузить видео на сайт.', 'Habilitar subida de vídeo para compartir y subir videos al sitio.', 'Paylaşmak ve siteye video yüklemek için video upload etkinleştirin.'),
(638, 'audio_upload', 'Audio Upload', 'رفع الموسيقى', 'Audio uploaden', 'Audio Upload', 'Audio hochladen', 'Audio Upload', 'Carregar audio', 'Аудио Загрузить', 'Audio Subir', 'Ses Yükleme'),
(639, 'audio_upload_info', 'Enable audio upload to share &amp; upload sounds to the site.', 'مكن هذه الميزة  لتحميل وتبادل الصوتيات على الموقع.', 'Enable audio uploaden om te delen en uploaden geluiden naar de site.', 'Activer audio upload pour partager et télécharger des sons sur le site.', 'Aktivieren Sie Audio-Upload zu teilen und Upload-Sounds auf der Website.', 'Abilita audio upload per condividere e caricare i suoni al sito.', 'Carregar audios e compartilhar no site.', 'Включить аудио загрузки, чтобы разделить и загрузки звучит на сайт.', 'Habilitar audio upload compartir y cargar suena al sitio.', 'Paylaşmak ses yükleme etkinleştirin ve upload sitesine geliyor.'),
(640, 'read_more', 'Read more', 'المزيد ..', 'Lees Meer..', 'En lire plus..', 'Weiterlesen', 'Leggi di più..', 'Ler mais', 'Показать полностью...', 'Lee mas..', 'Daha fazla..'),
(641, 'read_less', 'Read less', 'أخفاء ..', 'Lees Minder..', 'Lire moins..', 'Weniger lesen', 'Leggi meno..', 'Ler menos', 'Свернуть...', 'Cerrar..', 'Az Oku..'),
(642, 'add_photo', 'Add a photo.', 'أضِف صورة.', 'Voeg een foto toe.', 'Ajouter une photo.', 'Füge ein Bild hinzu.', 'Aggiungi una foto.', 'Adicionar foto.', 'Добавьте фотографию.', 'Añade una foto.', 'Bir fotoğraf ekle.'),
(643, 'add_photo_des', 'Show your unique personality and style.', 'أظهِر شخصيّتك وأسلوبك الفريد.', 'Voeg een foto toe.', 'Affichez votre personnalité et votre style uniques.', 'Zeige Deine einzigartige Persönlichkeit und Deinen Stil.', 'Mostra la tua personalità e il tuo stile.', 'Mostrar sua personalidade e estilo.', 'Продемонстрируйте свою индивидуальность и неповторимый стиль.', 'Muestra tu estilo y personalidad única.', 'Eşsiz karakterini ve tarzını yansıt.'),
(644, 'start_up_skip', 'Or Skip this step for now.', 'تخطّى هذه الخطوة الآن', 'Deze stap voor nu overslaan', 'Sauter cette étape pour le moment', 'Diesen Schritt vorerst überspringen', 'Salta questo passaggio per ora', 'Pular.', 'Пропустить этот шаг', 'Omitir este paso por ahora', 'Bu adımı şimdilik atla'),
(645, 'start_up_continue', 'Save &amp; Continue', 'المتابعة', 'Opslaan &amp; Doorgaan', 'Enregistrer &amp; Continuer', 'Speichern und weiter', 'Salva e continua', 'Salvar e continuar', 'Сохранить &amp; Продолжить', 'Guardar y Continuar', 'Kaydet ve Devam Et'),
(646, 'tell_us', 'Tell us about you.', 'أخبرنا عنك.', 'Vertel ons over jou.', 'Parlez-nous de vous.', 'Erzählen Sie uns von Ihnen.', 'Parlaci di te.', 'Fale sobre você.', 'Расскажите о себе.', 'Cuéntanos acerca de ti.', 'Senin hakkında bilgi verin.'),
(647, 'tell_us_des', 'Share your information with our community.', 'تبادل المعلومات الخاصة بك مع مجتمعنا.', 'Deel je informatie met onze gemeenschap.', 'Partager vos informations avec notre communauté.', 'Ihre Daten an unsere Community.', 'Condividere le informazioni con la nostra comunità.', 'Compartilhe informa&amp;ccedil;&amp;otilde;es.', 'Поделитесь информацией с нашим сообществом.', 'Comparta su información con nuestra comunidad.', 'Eden ile bilgilerinizi paylaşın.'),
(648, 'get_latest_activity', 'Get latest activities from our popular users.', 'الحصول على أحدث الأنشطة من أكثر المستخدمين شعبية لدينا.', 'Ontvang de meest recente activiteiten van onze populaire gebruikers.', 'Obtenir les dernières activités de nos utilisateurs populaires.', 'Holen Sie sich aktuelle Aktivitäten aus unserer beliebten Nutzer.', 'Ottenere ultime attività dei nostri utenti popolari.', 'Veja as novas informações dos usuários mais populares.', 'Следите за последними действиями популярных пользователей.', 'Obtener las últimas actividades de los usuarios populares.', 'Bizim popüler kullanıcılardan son faaliyetleri alın.'),
(649, 'follow_head', 'Follow our famous users.', 'تابع أشهر المستخدمين.', 'Volg onze beroemde gebruikers.', 'Suivez nos utilisateurs célèbres.', 'Folgen Sie unseren berühmten Nutzer.', 'Segui i nostri utenti famosi.', 'Siga os usuários mais populares.', 'Следуйте за нашими знаменитыми пользователями.', 'Siga nuestros usuarios más populares.', 'Bizim ünlü kullanıcıları izleyin.'),
(650, 'follow_num', 'Follow {number} &amp; Finish', 'تابع {number} وإستمر', 'Volg {number} &amp; Finish', 'Suivez {number} &amp; Terminer', 'Folgen Sie {number} &amp; Finish', 'Seguire {number} &amp; Finitura', 'Seguir {number} &amp; terminar', 'Следовать за {number}', 'Siga {number} y Finalizar', '{number} Takip et ve bit'),
(651, 'looks_good', 'Looks good.', 'يبدو جيّدًا.', 'Ziet er goed uit.', 'Ça a l&#39;air bien.', 'Sieht gut aus.', 'Sembra buono.', 'Parece legal.', 'Неплохо.', 'Se ve bien.', 'İyi görünüyor.'),
(652, 'looks_good_des', 'You&#039;ll be able to add more to your profile later.', 'ستكون قادرًا على إضافة المزيد لملفك الشخصيّ لاحقًا.', 'Je kunt later meer toevoegen aan je profiel.', 'Vous serez en mesure de compléter votre profil ultérieurement.', 'Du wirst später mehr zu Deinem Profil hinzufügen können.', 'Più tardi potrai aggiungere altro al tuo profilo.', 'Você poderá adicionar mais em seu perfil depois.', 'Вы сможете добавить другую информацию в свой профиль позже.', 'Podrás añadir más a tu perfil después.', 'Daha sonra profiline yeni şeyler ekleyebilirsin.'),
(653, 'upload_your_photo', 'Upload your photo', 'إرفع صورتك', 'Upload je foto', 'Téléchargez votre photo', 'Lade Dein Bild hoch', 'Carica la tua foto', 'Carregar foto', 'Загрузите вашу фотографию', 'Sube tu foto', 'Fotoğrafını yükle'),
(654, 'please_wait', 'Please wait..', 'الرجاء الإنتظار..', 'Even geduld aub..', 'S&#39;il vous plaît, attendez..', 'Warten Sie mal..', 'Attendere prego..', 'Aguarde..', 'Пожалуйста подождите..', 'Por favor espera..', 'Lütfen bekleyin..'),
(655, 'username_or_email', 'Username or E-mail', 'اسم المستخدم أو البريد الإلكتروني', 'Gebruikersnaam of E-mail', 'Nom d&#39;utilisateur ou email', 'Benutzername oder E-Mail-Adresse', 'Nome utente o E-mail', 'Nome de usuário ou E-mail', 'Никнейм или E-mail адрес', 'Usuario o correo electrónico', 'Kullanıcı adı ya da email'),
(656, 'email_setting', 'E-mail Setting', 'إعداد البريد الإلكتروني', 'E-mail instellen', 'E-mail Réglage', 'E-Mail Einstellung', 'E-mail Impostazione', 'Configuração de E-mail', 'Электронная почта Настройка', 'Ajuste de Correo', 'E-posta Ayarı'),
(657, 'years_old', 'years old', 'سنوات', 'jaar oud', 'ans', 'Jahre alt', 'Anni', 'anos', 'лет', 'años', 'yaşında'),
(658, 'friends_birthdays', 'Friends Birthdays', 'اعياد ميلاد الاصدقاء', 'Verjaardagen van vrienden', 'Annivarsaire d&#39;amis', 'Geburtstage von Freunden', 'amici Compleanni', 'Aniversários de Amigos', 'Друзья Дни рождения', 'Cumpleaños de amigos', 'Arkadaşlarının Doğumgünü'),
(659, 'sms_setting', 'SMS Setting', 'اعدادات الرسائل القصيرة', 'SMS Instellingen', 'Paramètres SMS', 'SMS Einstellungen', 'Impostazione SMS', 'Configuração de SMS', 'SMS Настройка', 'Configuración SMS', 'SMS Ayarları'),
(660, 'smooth_loading', 'Smooth Loading', 'تحمبل سلس', 'Gelijdelijk laden', 'Chargement régulier', 'Schnelles Laden', 'Smooth Caricamento', 'Carregamento Suave', 'Гладкая загрузка', 'Cargando', 'Düzgün Yükleme'),
(661, 'boosted_pages_viewable', 'Boosted pages are already viewable by all our community members.', 'الصفحات المعززة يتم مشاهدتها من قبل جميع افراد المجتمع', 'Omhoog geplaatste pagina&#039;s zijn al zichtbaar voor leden.', 'Les pages boostées sont déjà visibles par tous les membres de votre communauté', 'Hervorgehobene Seiten sind sofort für alle Mitglieder der Community Sichtbar.', 'pagine potenziato sono già visualizzabili da tutti i membri della community.', 'Páginas impulsionadas já estão visíveis por todos os membros da nossa comunidade.', 'Усиленные страницы уже доступны для просмотра всеми нашими членами сообщества.', 'Tus paginas promocionadas seran vistas en toda la comunidad.', 'Yükseltilen sayfalar tüm kullanıcılarımız tarafından görüntülenebilir.'),
(662, 'boost_page_in_same_time', 'You&#039;re a {type_name}, You&#039;re just able to boost {can_boost} pages at the same time.', 'صفحة في نفس الوقت{can_boost}بامكانك فقط تسريع ,{type_name} انت', 'Je bent {type_name}, Je kan nu  {can_boost} omhoog plaatsen.', 'Vous êtes un {type_name}, vous pouvez booster {can_boost} pages en même temps.', 'Du nutzt einen {type_name} Account, Du kannst nicht {can_boost} Seiten zur selben Zeit hervorheben.', 'Sei un {type_name}, Sei solo in grado di aumentare {can_boost} pagine in tempo stesso.', 'Você é um {type_name}, Você é apenas capaz de impulsionar {can_boost} páginas ao mesmo tempo.', 'Ты {type_name}, Вы просто в состоянии повысить {can_boost} может увеличить страницы в то же самое время.', 'Tu {type_name}, solo puedes promocionar {can_boost} paginas al mismo tiempo.', 'Sen bir {type_name}, aynı zamanda {can_boost} sadece sayfaları yükseltebilirsin.'),
(663, 'boost_posts_in_same_time', 'You&#039;re a {type_name}, You&#039;re just able to boost {can_boost} posts at the same time.', 'صفحة في نفس الوقت{can_boost}بامكانك فقط تسريع ,{type_name} انت', 'Je bent {type_name}, Je kan nu {can_boost} berichten omhoog plaatsen.', 'You&#039;re a {type_name}, vous pouvez booster {can_boost} posts en même temps.', 'Du nutzt einen {type_name} Account, Du kannst nicht  {can_boost} Beiträge zur selben Zeit hervorheben.', 'Sei un {type_name}, Sei solo in grado di aumentare {can_boost} messaggi in tempo stesso.', 'Você é um {type_name}, Você é apenas capaz de impulsionar  {can_boost}  postagens ao mesmo tempo.', 'Ты {type_name}, Вы просто в состоянии повысить {can_boost} может увеличить посты в то же самое время.', 'Tu {type_name}, solo puedes promocionar {can_boost} posts al mismo tiempo.', 'Sen bir {type_name}, aynı zamanda {can_boost} sadece mesajları yükseltebilirsin.'),
(664, 'there_are_no_boosted_pages', 'There are no boosted pages yet.', 'لا يوجد صفحات معززة الان', 'Er zijn geen omhoog geplaatste pagina&#039;s.', 'Il n&#39;y a pas encore de pages boostées.', 'Es gibt zu Zeit keine hervorgehobenen Seiten.', 'Non ci sono ancora pagine potenziati.', 'Não há páginas impulsionadas ainda.', 'Там нет Boosted страниц пока.', 'No hay paginas promocionados aún.', 'Henüz yükseltilmiş sayfa bulunmuyor.'),
(665, 'there_are_no_boosted_posts', 'There are no boosted posts yet.', 'لا يوجد صفحات معززة الان', 'Er zijn geen omhoog geplaatste berichten.', 'Il n&#39;y a pas encore de posts boostés.', 'Es gibt zur Zeit noch keine hervorgehobenen Beiträge.', 'Non ci sono ancora messaggi potenziati.', 'Não há postagens impulsionadas ainda.', 'Там нет Boosted сообщений пока.', 'No hay post promocionados aún.', 'Henüz yükseltilmiş mesaj bulunmuyor.'),
(666, 'discover_pro_types', 'Discover more features with {sitename} PRO packages !', 'اكتشاف المزيد للمحترفين من الميزات مع حزم {sitename}', 'Ontdek meer opties met {sitename} PRO!', 'Découvrez plus de fonctionnalités avec {sitename} PRO !', 'Entdecke mehr Funktionen mit dem {sitename} Pro-Paket !', 'Scopri di più caratteristiche con WoWonder pacchetti PRO !', 'Descubra mais recursos com WoWonder PRO packages !', 'Откройте для себя больше возможностей с WoWonder пакетами PRO !', 'Descubre mas {sitename} funciones con los nuevos paquetes!', '{sitename} PRO paketleri ile daha fazla özellik keşfedin !'),
(667, 'star', 'Star', 'برونزي', 'Ster', 'Etoile', 'Star', 'Star', 'Estrela', 'Star', 'Star', 'Yıldız'),
(668, 'hot', 'Hot', 'فضي', 'Heet', 'Hot', 'Hot', 'Hot', 'Quente', 'Hot', 'Hot', 'Sıcak'),
(669, 'ultima', 'Ultima', 'ذهبي', 'Ultimate', 'Ultima', 'Ultima', 'Ultima', 'Ultima', 'Ultima', 'Ultima', 'Ultima'),
(670, 'vip', 'Vip', 'ماسي', 'VIP', 'Vip', 'Vip', 'Vip', 'Vip', 'Vip', 'Vip', 'Vip'),
(671, 'featured_member', 'Featured member', 'عضو متميز', 'Aanbevolen lid', 'Membres en vedette', 'Besonderes Mitglied', 'membro In primo piano', 'Membro em destaque', 'Показанный член', 'Miembros destacados', 'Önerilen üye'),
(672, 'see_profile_visitors', 'See profile visitors', 'رأيت صفحات الشخصية للزوار', 'Bekijk profiel bezoekers', 'Voir le profil des visiteurs', 'Sehe wer dein Profil besucht hat', 'Vedi visitatori profilo', 'Veja os perfis de visitantes', 'См посетителей профиля', 'Ver visitantes en su perfil', 'Profil ziyaretçilerini gör'),
(673, 'show_hide_lastseen', 'Show / Hide last seen', 'اظهار/إخفاء أخر ظهور', 'Verberg laatst gezien', 'Voir / Cacher la dernière fois vu', 'Anzeigen oder Verstecke zuletzt gesehen', 'Mostra / Nascondi visto l&#039;ultima volta', 'Mostra / Esconder visto por último', 'Показать / Скрыть последний раз видели', 'Ver / Ocultar ultimas visitas', 'Son görünmeyi Göster / Gizle'),
(674, 'verified_badge', 'Verified badge', 'شارة التحقق', 'Vericatie Badge', 'Badge Vérifié', 'Verifiziert Abzeichen', 'distintivo verificato', 'Crachá verificado', 'Проверенные значок', 'Cuenta Verificada', 'Onaylanmış rozet'),
(675, 'post_promotion_star', 'Posts promotion&lt;br&gt;', 'نشر تريج&lt;br&gt;&lt;small&gt;(غير متاح)&lt;/small&gt;', 'Bericht promotie&lt;br&gt;&lt;small&gt;(Niet beschikbaar)&lt;/small&gt;', 'Promotion de post&lt;br&gt;&lt;small&gt;(Indisponible)&lt;/small&gt;', 'Beitrags Promotion&lt;br&gt;&lt;small&gt;(Nicht verfügbar)&lt;/small&gt;', 'la promozione Messaggio&lt;br&gt;&lt;small&gt;(Non disponibile)&lt;/small&gt;', 'Pós promoção&lt;br&gt;&lt;small&gt;(Não disponível)&lt;/small&gt;', 'продвижение сообщение&lt;br&gt;&lt;small&gt;(Недоступен)&lt;/small&gt;', 'Promocionar publicación&lt;br&gt;&lt;small&gt;(No Disponible)&lt;/small&gt;', 'Mesaj tanıtımı&lt;br&gt;&lt;small&gt;(Mevcut değil)&lt;/small&gt;'),
(676, 'page_promotion_star', 'Pages promotion&lt;br&gt;', 'صفحة الترويج&lt;br&gt;&lt;small&gt;(غير متاحة)&lt;/small&gt;', 'Pagina promotie&lt;br&gt;&lt;small&gt;(Niet beschkbaar)&lt;/small&gt;', 'Promotion de page&lt;br&gt;&lt;small&gt;(Indisponible)&lt;/small&gt;', 'Seiten Promotion&lt;br&gt;&lt;small&gt;(Nicht verfügbar)&lt;/small&gt;', 'promozione pagina&lt;br&gt;&lt;small&gt;(Non disponibile)&lt;/small&gt;', 'Pré promoção&lt;br&gt;&lt;small&gt;(Não disponível)&lt;/small&gt;', 'продвижение Page&lt;br&gt;&lt;small&gt;(Недоступен)&lt;/small&gt;', 'Promocionar pagina&lt;br&gt;&lt;small&gt;(No Disponible)&lt;/small&gt;', 'Sayfa tanıtımı&lt;br&gt;&lt;small&gt;(Mevcut değil)&lt;/small&gt;'),
(677, '0_discount', '0% discount', '0% تخفيض', '0% korting', '0% de réduction', '0% Nachlass', '0% sconto', '0% de desconto', '0% скидка', '0% descuento', '0% indirim'),
(678, '10_discount', '10% discount', '10% تخفيض', '10% korting', '10% de réduction', '10% Nachlass', '10% sconto', '10% de desconto', '10% скидка', '10% descuento', '10% indirim'),
(679, '20_discount', '20% discount', '20% تخفيض', '20% korting', '20% de réduction', '20% Nachlass', '20% dsconto', '20% de desconto', '20% скидка', '20% descuento', '20% indirim'),
(680, '60_discount', '60% discount', '60% تخفيض', '60% korting', '60% de réduction', '60% Nachlass', '60% sconto', '60% de desconto', '60% скидка', '60% descuento', '60% indirim'),
(681, 'per_week', 'per week', 'لمدة اسبوع', 'per week', 'par semaine', 'pro Woche', 'settimanale', 'por semana', 'в неделю', 'por semana', 'haftada'),
(682, 'per_month', 'per month', 'لمدة شهر', 'per maand', 'par mois', 'pro Monat', 'al mese', 'por mês', 'в месяц', 'por mes', 'ayda'),
(683, 'per_year', 'per year', 'لمدة عام', 'per jaar', 'par an', 'pro Jahr', 'per anno', 'por ano', 'в год', 'por año', 'yılda'),
(684, 'life_time', 'life time', 'مدى الحياة', 'levens lang', 'à vie', 'Lebenslang', 'tutta la vita', 'tempo de vida', 'продолжительность жизни', 'de por vida', 'ömür boyu'),
(685, 'upgrade_now', 'Upgrade Now', 'ترقية الان', 'Upgrade Nu', 'Mise à jour maintenant', 'Jetzt Upgraden', 'Aggiorna ora', 'Atualize Agora', 'Обнови сейчас', 'Actualiza ahora', 'Hemen Yükselt'),
(686, 'boosted_posts', 'Boosted Posts', 'المشاركات المعززت', 'Omhoog geplaatse berichten', 'Posts boostés', 'hervorgehobene Beiträge', 'Messaggi potenziato', 'Postagens Impulsionadas', 'Усиленные Сообщений', 'Promocionar Posts', 'yükseltılan Mesajlar'),
(687, 'boosted_pages', 'Boosted Pages', 'الصفحات المعززت', 'Omhoog geplaatsen pagina&#039;s', 'Pages boostées', 'hervorgehobene Seiten', 'Pagine potenziato', 'Páginas Impulsionadas', 'Усиленные Страницы', 'Promocionar Paginas', 'yükseltılan Sayfalar'),
(688, 'put_me_here', 'Put Me Here', 'ضعني هنا', 'Zet mij hier nier', 'Me mettre ici', 'Setze mich Hier', 'Mettimi qui', 'Me Coloque Aqui', 'Put Me Здесь', 'Poner aqui', 'buraya koy'),
(689, 'promoted', 'Promoted', 'معزز', 'Advertenties', 'Promoted', 'Promotions', 'Promossa', 'Promovido', 'Повышен', 'Promocionar', 'Tanıtılan'),
(690, 'oops_something_went_wrong', 'Oops ! Something went wrong.', 'Oops ! حدث خطأ ما', 'Oeps ! Er ging iets mis.', 'Oops ! Quelquechose s&#39;est mal passé.', 'Oops ! Irgendetwas ist schief gegangen.', 'Oops! Qualcosa è andato storto.', 'Oops! Algo deu errado.', 'К сожалению! Что-то пошло не так.', 'Oops ! Algo salio mal.', 'Hata ! Bir şeyler yanlış gitti.'),
(691, 'try_again', 'Try again', 'حاول مجددا', 'Probeer het opnieuw', 'Essayez encore', 'Versuche es erneut', 'Riprova', 'Tente novamente', 'Попробуй еще раз', 'Trata de nuevo', 'Tekrar deneyin'),
(692, 'boost_page', 'Boost Page', 'اضافة صفحة', 'Plaats pagina omhoog', 'Booster Page', 'Seite hervorheben', 'Boost Pagina', 'Página Impulsionada', 'Повышение Page', 'Promocionar Pagina', 'Sayfa yükselt'),
(693, 'page_boosted', 'Page Boosted', 'الصفحة المعززة', 'Pagina omhoog geplaatst', 'Page Boostée', 'Die Seite wurde hervorgehoben', 'pagina potenziato', 'Página Impulsionada', 'Страница Boosted', 'Pagina promocionada', 'yükseltılan Sayfa'),
(694, 'un_boost_page', 'Un-Boost Page', 'الصفحة الغير معززة', 'Verwijder omhoog plaatsing', 'Un-Boost Page', 'Seite nicht mehr hervorheben', 'Un-Boost Pagina', 'Desimpulsionar Página', 'Un-наддув Page', 'Des-promover pagina', 'Sayfayı yükseltma'),
(695, 'edit_page_settings', 'Edit Page Settings', 'تعديل إعدادات الصفحة', 'Verander pagina instellingen', 'Editer paramètres de la Page', 'Seiten Einstellungen bearbeiten', 'Modifica impostazioni pagina', 'Editar as configurações de página', 'Изменить настройки страницы', 'Editar ajustes de página', 'Sayfa Ayarlarını Düzenle'),
(696, 'blocked_users', 'Blocked Users', 'المستخدمين المحظورين', 'Geblokkerde Gebruikers', 'Utilisateurs bloqués', 'Blockierte Mitglieder', 'Gli utenti bloccati', 'Usuários Bloqueados', 'Заблокированные пользователи', 'Blockear usuario', 'Bloklu Kullanıcılar'),
(697, 'un_block', 'Un-Block', 'غير محضور', 'Deblokkeer', 'Débloquer', 'Blockierung aufheben', 'Sbloccare', 'Desbloquear', 'открыть', 'Des-blockear', 'Blok yükselt'),
(698, 'css_file', 'CSS file', 'CSS ملف', 'CSS bestand', 'fichier CSS', 'CSS Datei', 'file CSS', 'arquivo CSS', 'файл CSS', 'Archivo CCS', 'CSS dosyası'),
(699, 'css_status_default', 'Default design', 'التصميم الاولي', 'Standaard design', 'Design par défaut', 'Standart Design', 'disegno predefinito', 'Design padrão', 'дизайн По умолчанию', 'diseño por defecto', 'Varsayılan dizayn'),
(700, 'css_status_my', 'My CSS file', 'الخاص بي CSS ملف', 'Mijn CSS bestand', 'Mon fichier CSS', 'Meine CSS Datei', 'Il mio file CSS', 'Meu arquivo CSS', 'Мой файл CSS', 'Mi CSS', 'CSS dosyam'),
(701, 'css_file_info', 'You can fully design your profile by uploading your own CSS file', 'CSS الخاص بك يمكنك تصميم كامل ملف التعريف الخاص بك عن طريق تحميل ملف', 'Je kan je profiel helemaal pimpen door je eigen CSS bestand te uploaden', 'Vous pouvez modifier le design de votre profil via le téléversement de votre propre fichier CSS', 'Du kannst dein Profil komplett selbst Designen in dem du deine CSS Datei hoch lädst', 'È possibile progettare completamente il proprio profilo caricando il proprio file CSS', 'Você pode projetar totalmente o seu perfil através de upload do seu próprio arquivo CSS', 'Вы можете полностью создать свой профиль, загрузив свой собственный файл CSS', 'Ahora puedes rediseñar tu perfil con tu propio estilo (Css)', 'Kendi Css dosyanızı yükleyerek profilinizi tamamen siz tasarlayabilirsiniz.'),
(702, 'invite_your_frineds_home', 'Invite Your Friends', 'دعوة اصدقائك', 'Nodig je vrienden uit', 'Inviter vos amis', 'Lade deine Freunde ein', 'Invita i tuoi amici', 'Convidar Seus Amigos', 'Пригласить друзей', 'Invita a tus amigos', 'Arkadaşlarını Davet Et'),
(703, 'send_invitation', 'Send Invitation', 'إرسال الدعوة', 'Verstuur', 'Envoyer Invitation', 'Einladung Versenden', 'Spedire un invito', 'Enviar Convite', 'Выслать пригласительное', 'Enviar invitación', 'Davetiye gönder'),
(704, 'boost_post', 'Boost Post', 'تعزيز المنشور', 'Plaast bericht omhoog', 'Boost Post', 'Beitrag Hervorheben', 'Boost Messaggio', 'Impulsionar Postagem', 'Повысьте Post', 'Promocionar post', 'Boost Post'),
(705, 'unboost_post', 'UnBoost Post', 'عدم تعزيز المنشور', 'Verwijder', 'Un-Boost Post', 'Beitrag nicht mehr Hervorheben', 'UnBoost Messaggio', 'Desimpulsionar Postagem', 'UnBoost сообщение', 'Des-promocionar post', 'Un-Boost Post'),
(706, 'drag_to_re', 'Drag to reposition cover', 'اسحب لتعديل الصورة', 'Sleep naar de juiste positie', 'Faites glisser pour repositionner la couverture', 'Ziehe das Cover mit der Maus um es neu zu Positionieren', 'Trascinare per riposizionare la copertura', 'Arraste para reposicionar a cobertura', 'Перетащите, чтобы изменить положение крышки', 'Arrastra la portada para recortarla', 'Kapağı yeniden konumlandırmak için sürükleyin'),
(707, 'block_user', 'Block User', 'حضر المستخدم', 'Blokkeer gebruiker', 'Bloquer l&#39;utilisateur', 'Mitglied Blocken', 'Blocca utente', 'Bloquear Usuário', 'Заблокировать пользователя', 'Blockear usuario', 'Kullanıcı Blok'),
(708, 'edit_user', 'Edit User', 'تعديل حساب العضو', 'Wijzig gebruiker', 'Editer Utilisateur', 'Mitglied Bearbeiten', 'Modifica utente', 'Editar Usuário', 'Изменить пользователя', 'Editar usuario', 'Kullanıcıyı Düzenle'),
(709, 'cong', 'Congratulations ! You&#039;re now a &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}', 'مبروك! انت الان &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}', 'Gefeliciteerd ! Je bent nu een &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}', 'Félicitation ! Vous êtes maintenant un &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}', 'Herzlichen Glückwunsch! Du bist nun ein &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}', 'Complimenti ! Ora sei un &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}', 'Parabéns ! Você é agora um &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}', 'Поздравления ! Ты теперь &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}', 'Felicidades! Ahora &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}', 'Tebrikler ! Artık sen bir &lt;span style=&quot;color:{color}&quot;&gt;&lt;i class=&quot;fa fa-{icon} fa-fw&quot;&gt;&lt;/i&gt;{type_name}'),
(710, 'cong_2', 'Start browsing the new features', 'بدء تصفح الميزات الجديدة', 'Bekijk nu je nieuwe opties', 'Commencer à naviguer sur les nouvelles fonctionnalités', 'Beginne dir die neuen Funktionen anzusehen', 'Avviare la navigazione le nuove funzionalità', 'Começe a navegar os novos recursos', 'Начать просмотр новых функций', 'Comiencza a utilizar las nuevas funciones', 'Yeni özellikleri taramaya başlayın'),
(711, 'activation_oops', 'Oops, looks like your account is not activated yet.', 'Oops, .يبدو انه لم يتم تنشيط حسابك بعد', 'Oeps, het lijkt er op of je account nog niet is geactiveerd.', 'Oops, votre compte n&#39;est pas encore activé.', 'Oops, so wie es aussieht wurde dein Account Nachbericht aktiviert.', 'Spiacenti, sembra che il tuo account non è ancora attivato.', 'Oops, parece que sua conta não está ativada ainda.', 'К сожалению, похоже, ваша учетная запись еще не активирована.', 'Oops, Parece que su cuenta no está activada aún.', 'Hata, hesabınız henüz aktif edilmemiş gibi görünüyor.'),
(712, 'activation_method', 'Please choose a method below to activate your account.', '.يرجى اختيار طريقة لتفعيل حسابك أدناه', 'Selecteer een optie om je account te activeren.', 'Merci de choisir une méthode ci*dessous pour activer votre compte.', 'Bitte suche dir eine unten stehende Methode aus um dein Account zu aktivieren.', 'Si prega di scegliere un metodo seguito per attivare il tuo account.', 'Por favor escolha um método abaixo para ativar sua conta.', 'Пожалуйста, выберите метод ниже, чтобы активировать учетную запись.', 'Por favor trata con otro metodo para activar tu cuenta.', 'Hesabınızı etkinleştirmek için aşağıda ki yöntemlerden birini seçiniz.'),
(713, 'activation_email', 'Via E-mail', 'عن طريق البريد', 'Via E-mail', 'Par E-mail', 'Via E-mail', 'Via posta elettronica', 'Via E-mail', 'По электронной почте', 'Via E-mail', 'E-mail ile');
INSERT INTO `Wo_Langs` (`id`, `lang_key`, `english`, `arabic`, `dutch`, `french`, `german`, `italian`, `portuguese`, `russian`, `spanish`, `turkish`) VALUES
(714, 'activation_phone', 'Via Phone Number', 'عن طريق الهاتف', 'Via Telefoonnummer', 'Par téléphone', 'Via Telefonnummer', 'Via Numero di telefono', 'Via Número de Telefone', 'Via номеру телефона', 'Via SMS', 'Telefon Numarası ile'),
(715, 'activation_or', 'Or', 'أو', 'Of', 'Ou', 'Oder', 'O', 'Ou', 'Или', 'O', 'yada'),
(716, 'activation_send_code', 'Send Confirmation Code', 'إرسال رمز التأكيد', 'Stuur activatie code', 'Envoyer le code confirmation', 'Sende uns deinen Bestätigungs Code Manuell', 'Invia codice di conferma', 'Enviar Confirmação do Código', 'Отправить код подтверждения', 'Enviar código de activación', 'Onay Kodu Gönder'),
(717, 'activation_get_code_again', 'Didn&#039;t get the code?', 'لم يتم استقبال الرمز؟', 'Code niet ontvangen?', 'Didn&#39;t get the code?', 'Du hast keinen Code erhalten?', 'Non avere il codice?', 'Não obteve o código?', 'Не получить код?', 'No he recivido código?', 'Onay kodunu almadınız mı?'),
(718, 'activation_resend', 'Resend', 'اعادت ارسال', 'Verstuur opnieuw', 'Renvoyer', 'Erneut Senden', 'inviare di nuovo', 'Re-enviar', 'Отправить', 'Re-enviar', 'Tekrar gönder'),
(719, 'activation_should_receive', 'You should receive the code within', 'يجب استقبال الرمز في مدة', 'Je zult de code ontvangen', 'You should receive the code within', 'Du solltest den Code in Kürze erhalten.', 'Si dovrebbe ricevere il codice all&#039;interno', 'Você deve receber o código dentro de', 'Вы должны получить код внутри', 'Debería recibir el código dentro de', 'içinde kodu almalısınız'),
(720, 'confirm', 'Confirm', 'تأكيد', 'Bevestig', 'Confirmer', 'Bestätigen', 'Confermare', 'Confirmar', 'подтвердить', 'Confirmar', 'Onayla'),
(721, 'phone_num_ex', 'Phone number (eg. +905...)', '(eg. +905...) رقم الهاتف', 'Telefoonnumer (bijv. +31...)', 'Numéro de téléphone (eg. +33...)', 'Telefonnummer  (z.b +49...)', 'Numero di telefono (eg. +905...)', 'Número de telefone (ex. +905...)', 'Номер телефона (eg. +905...)', 'Numero de Telefono (eg. +001...)', 'Telefon Numarası (örn. +905...)'),
(722, 'error_while_activating', 'Error while activating your account.', '.خطأ أثناء تفعيل حسابك', 'Error tijdens het activeren van uw account.', 'Error while activating your account.', 'Fehler beim aktivieren deines Accounts.', 'Errore durante l&#039;attivazione dell&#039;account.', 'Erro ao ativar a sua conta.', 'Ошибка при активации учетной записи.', 'Error al activar su cuenta.', 'Hesabınızı onaylarken hata oluştu.'),
(723, 'wrong_confirmation_code', 'Wrong confirmation code.', '.خطأ في رمز التأكيد', 'Ongeldige code.', 'Code de confirmation erroné.', 'Falscher bestätigungs Code.', 'codice di conferma errato.', 'Código de confirmação incorreto.', 'Неправильный код подтверждения.', 'Este código no es valido.', 'Yanlış onay kodu.'),
(724, 'failed_to_send_code', 'Failed to send the confirmation code.', '.فشل في إرسال رمز التأكيد', 'Het is niet gelukt de code te verzenden.', 'Impossible d&#39;envoyer le code de confirmation.', 'Fehler beim senden des bestätigungs Code.', 'Impossibile inviare il codice di conferma.', 'Não foi possível enviar o código de confirmação.', 'Не удалось отправить код подтверждения.', 'No se pudo enviar código de activación.', 'Onay kodu gönderilirken hata oluştu.'),
(725, 'worng_phone_number', 'Wrong phone number.', '.رقم الهاتف خاطئ', 'Geen geldige telefoonnummer.', 'Numéro de téléphone erroné.', 'Falsche Telefonnummer.', 'numero di telefono sbagliato.', 'Número de telefone incorreto.', 'Неправильный номер телефона.', 'Numero incorrecto.', 'Yanlış telefon numarası.'),
(726, 'phone_already_used', 'Phone number already used.', '.رقم الهاتف موجود', 'Telefoonnummer is al in gebruik.', 'Numéro de téléphone déjà utilisé.', 'Die angebene Telefonnummer wird bereits verwendet.', 'Numero di telefono già in uso.', 'Número de telefone já em uso.', 'Номер телефона уже используется.', 'Este nuemero ya a sido usado.', 'Telefon numarası kullanılıyor.'),
(727, 'sms_has_been_sent', 'SMS has been sent successfully.', '.تم ارسا الرسالة النصية بنجاح', 'SMS is succsesvol verzonden.', 'SMS envoyé avec succès.', 'Die SMS wurde erfolgreich versendet.', 'SMS è stato inviato con successo.', 'SMS foi enviado com sucesso.', 'SMS было отправлено успешно.', 'El código de activación a sido enviado.', 'SMS başarıyla gönderildi.'),
(728, 'error_while_sending_sms', 'Error while sending the SMS, please try another number.', '.خطأ أثناء إرسال الرسالة القصيرة، يرجى المحاولة باستخدام رقم آخر', 'We konden de SMS niet versturen, probeer een ander nummer.', 'Erreur lors de l&#39;envooi du SMS, merci d&#39;essayer un autre numéro de téléphone.', 'Fehler beim Versenden der SMS, bitte versuche eine andere Telefonnummer.', 'Errore durante l&#039;invio del SMS, prova un altro numero.', 'Erro ao enviar o SMS, por favor tente outro número.', 'Ошибка при отправке SMS, пожалуйста, попробуйте другой номер.', 'Error al enviar código de activacion, por favor trata con otro numero .', 'SMS gönderilemiyor, lütfen farklı numara deneyiniz.'),
(729, 'failed_to_send_code_fill', 'Failed to send the confirmation code, please select one of the activation methods.', '.فشل في إرسال رمز التأكيد، يرجى ملء إحدى خانات التنشيط', 'Het is niet gelukt de code te versturen, probeer een andere methoda.', 'Impossible d&#39;envoyer le code de confirmation, essayez une des méthodes d&#39;activation.', 'Fehler beim Versenden des bestätigungs Code, bitte wählen eine andere aktivierungs Methode.', 'Impossibile inviare il codice di conferma, selezionare uno dei metodi di attivazione.', 'Não foi possível enviar o código de confirmação, por favor preencha um dos métodos de ativação.', 'Не удалось отправить код подтверждения, пожалуйста, выберите один из предложенных способов активации.', 'Error al enviar código de activacion, por favor trata con otro metodo.', 'Onay kodu gönderilemiyor, lütfen aktivasyon yöntemlerinden birini seçiniz.'),
(730, 'email_sent_successfully', 'E-mail has been sent successfully, please check your inbox or spam folder for the activation link.', '.تم إرسال البريد الإلكتروني بنجاح، يرجى مراجعة مجلد البريد الوارد أو الرسائل غير المرغوب فيها لرابط التفعيل', 'E-mail is succesvol verzonden, kijk ook in uw spam/ongewenste inbox.', 'E-mail envoyé avec succès, merci de vérifier votre boite de réception et dossier spam pour le lien d&#39;activation.', 'Es wurde dir eine Email gesendet, bitte überprüfe deinen Postfach und gegebenfalls auch den Spam Ordner.', 'E-mail è stata inviata con successo, controllare la cartella Posta in arrivo o spam per il link di attivazione.', 'E-mail foi enviado com sucesso, verifique a sua pasta caixa de entrada ou de spam para o link de ativação.', 'Электронная почта была успешно отправлена, пожалуйста, проверьте свой почтовый ящик или спам папку для ссылки активации.', 'El correo a sido enviado, por favor check your inbox or spam folder for the activation link.', 'E-mail gönderildi, aktivasyon linki için lütfen mesaj kutunuzu yada spam kutusunu kontrol ediniz.'),
(731, 'limit_exceeded', 'Limit exceeded, please try again later.', '.لقد تجاوزت الحد المسموح به، يرجى المحاولة مرة أخرى في وقت لاحق', 'Te vaak geprobeerd, probeer het later nog eens.', 'Limite dépassé, merci de réessayer plus tard.', 'Anzahl an versuche überschritten , bitte versuche es später nochmal..', 'Limite superato, riprova più tardi.', 'Limite excedido, tente novamente mais tarde.', 'Превышен лимит, пожалуйста, повторите попытку позже.', 'Límite excedido, por favor trata mas tarde.', 'Limit aşıldı, lütfen daha sonra tekrar deneyin.'),
(732, 'failed_to_send_code_email', 'Error while sending the SMS, please try another number or activate your account via email by logging into your account.', '.خطأ أثناء إرسال الرسائل القصيرة، يرجى المحاولة رقم آخر أو تفعيل حسابك عبر البريد الإلكتروني عن طريق الدخول في حسابك', 'Probeer je account te verifiëren via de e-mail, we konden geen sms sturen.', 'Erreur lors de l&#39;envoi du SMS, merci d&#39;essayer un autre numéro ou activer votre compte par e-mail en vous connectant à votre compte.', 'Fehler beim Versenden der SMS, bitte benutze eine andere Telefonnummer  oder aktiviere deinen Account via Email, indem  du dich mit deinem Account Anmeldest.', 'Errore durante l&#039;invio del SMS, prova un altro numero o attivare il tuo account tramite e-mail accedendo al proprio conto.', 'Erro ao enviar o SMS, tente outro número ou ativar sua conta via e-mail, entrando em sua conta.', 'Ошибка при отправке SMS, пожалуйста, попробуйте другой номер или активировать свою учетную запись через электронную почту, войдя в свой аккаунт.', 'Error al enviar código de activacion, por favor trata con otro numero o activa tu cuenta via email accediendo a tu cuenta .', 'SMS gönderilemiyor, lütfen başka bir numara deneyiniz yada hesabınıza giriş yaparak hesabınızı mail ile etkinleştiriniz.'),
(733, 'free_member', 'Free Member', 'عضو عادي', 'Gratis Lid', 'Free member', 'Kostenlose Mitgliedschaft', 'Free Member', 'Membro grátis', 'Free Member', 'Usuario gratis', 'Ücretsiz üye'),
(734, 'star_member', 'Star Member', 'عضو برونزي', 'Ster Lid', 'Star Member', 'Star Mitgliedschaft', 'Star Member', 'Membro estrela', 'Star Member', 'Usuario star', 'Yıldız üye'),
(735, 'hot_member', 'Hot Member', 'عضو فضي', 'Hot Lid', 'Hot Member', 'Hot Mitgliedschaft', 'Hot Member', 'Membro Quente', 'Hot Member', 'Usuario hot', 'Sıcak Üye'),
(736, 'ultima_member', 'Ultima Member', 'عضو ذهبي', 'Ultimate Lid', 'Ultima Member', 'Ultima Mitgliedschaft', 'Ultima Member', 'Ultima Member', 'Ultima Member', 'Usuario ultima', 'Ultima Üye'),
(737, 'vip_member', 'Vip Member', 'عضو ماسي', 'VIP Lid', 'Vip Member', 'Vip Mitgliedschaft', 'Vip Member', 'Membro Vip', 'Vip Member', 'Usuario VIP', 'Vip Üye'),
(738, 'moderator', 'Moderator', 'مشرف', 'Moderator', 'Modérateur', 'Moderator', 'Moderator', 'Moderador', 'Moderator', 'Moderador', 'Moderator'),
(739, 'member_type', 'Member Type', 'نوع العضوية', 'Member soort', 'Type de membres', 'Mitglieds Typ', 'Member Type', 'Tipo de Membro', 'Member Type', 'Tipo de menbresia', 'Üye Türü'),
(740, 'membership', 'Membership', 'العضوية', 'Membership', 'Membership', 'Mitgliedschaft', 'membri', 'Filiação', 'членство', 'Membresia', 'Üyelik'),
(741, 'upgrade', 'Upgrade', 'الترقية', 'Upgrade', 'Mise à jour', 'Upgrade', 'aggiornamento', 'Atualização', 'Обновить', 'Actualización', 'Yükselt'),
(742, 'error_please_try_again', 'Error, Please try again later.', '.خطئ, يرجى المحاولة لاحقا', 'Error, probeer het later opnieuw.', 'Erreur, merci de réessayer plus tard.', 'Fehler, bitte versuche es später nochmal.', 'Errore, riprova più tardi.', 'Erro, Por favor tente novamente.', 'Ошибка, пожалуйста, повторите попытку позже.', 'Error, trata de nuevo.', 'Hata, Lütfen daha sonra tekrar deneyin.'),
(743, 'upgrade_to_pro', 'Upgrade To Pro', 'لترقية الى مزايا أكثر', 'Upgraden naar Pro', 'Passer à Pro', 'Upgrade auf Pro', 'Aggiornamento a Pro', 'Upgrade To Pro', 'Обновление до Pro', 'Para actualizar Pro', 'Pro&#039;ya yükselt'),
(744, 'no_answer', 'No answer', 'لا يوجد رد', 'Geen antwoord', 'Pas de réponse', 'Keine Antwort', 'Nessuna risposta', 'Sem resposta', 'Нет ответа', 'Sin respuesta', 'Cevap yok'),
(745, 'please_try_again_later', 'Please try again later.', 'الرجاء المحاولة لاحقا.', 'Probeer het later opnieuw.', 'Veuillez réessayer plus tard.', 'Bitte versuchen Sie es später noch einmal.', 'Per favore riprova più tardi.', 'Por favor, tente novamente mais tarde.', 'Пожалуйста, повторите попытку позже.', 'Por favor, inténtelo de nuevo más tarde.', 'Lütfen daha sonra tekrar deneyiniz.'),
(746, 'answered', 'Answered !', 'تم الرد !', 'Beantwoord !', 'répondre !', 'Beantwortet !', 'Risposte !', 'Respondidas !', 'Ответил !', 'Contestada !', 'Yanıtlanan !'),
(747, 'call_declined', 'Call declined', 'تم فصل الإتصال من قبل المستخدم', 'Call gedaald', 'Appel refusé', 'Anruf abgelehnt', 'chiamata rifiutato', 'chamada diminuiu', 'Вызов отказался', 'Llamar declinó', 'çağrı reddedildi'),
(748, 'call_declined_desc', 'The recipient has declined the call, please try again later.', 'تم فصل الإتصال من قبل المستخدم, الرجاء المحاولة لاحقا.', 'De ontvanger heeft de oproep geweigerd, probeer het later opnieuw.', 'Le destinataire a refusé l&#39;appel, s&#39;il vous plaît essayer à nouveau plus tard.', 'Der Empfänger hat den Anruf abgelehnt, bitte versuchen Sie es später noch einmal.', 'Il destinatario ha rifiutato la chiamata, si prega di riprovare più tardi.', 'O destinatário recusou a chamada, por favor tente novamente mais tarde.', 'Получатель отклонил вызов, пожалуйста, повторите попытку позже.', 'El receptor ha rechazado la llamada, por favor intente de nuevo más tarde.', 'Alıcı çağrıyı reddetti, daha sonra tekrar deneyin.'),
(749, 'new_video_call', 'New video call', 'إتصال فيديو', 'Nieuwe video-oproep', 'Nouvel appel vidéo', 'Neue Videoanruf', 'Nuovo video chiamata', 'chamada de vídeo novo', 'Новое видео вызова', 'Nueva llamada de video', 'Yeni video görüşmesi'),
(750, 'new_video_call_desc', 'wants to video chat with you.', 'يريد ان يحدثك عن طريق الفيديو.', 'wil video chatten met je.', 'veut le chat vidéo avec vous.', 'möchte mit Ihnen Video-Chat.', 'vuole chat video con te.', 'quer vídeo chat com você.', 'хочет видео-чат с вами.', 'quiere chatear con video con usted.', 'Sizinle görüntülü sohbet etmek istiyor.'),
(751, 'decline', 'Decline', 'فصل', 'Afwijzen', 'Déclin', 'Ablehnen', 'Declino', 'Declínio', 'снижение', 'Disminución', 'düşüş'),
(752, 'accept_and_start', 'Accept &amp; Start', 'القبول &amp; البدأ', 'Accepteer &amp; Start', 'Accepter &amp; Start', 'Akzeptieren &amp; Start', 'Accetta &amp; Start', 'Aceitar &amp; Start', 'принимать', 'Aceptar &amp; Start', 'Kabul Et ve Başlaı'),
(753, 'calling', 'Calling', 'يتم الإتصال', 'Roeping', 'Appel', 'Berufung', 'chiamata', 'chamada', 'призвание', 'Vocación', 'çağrı'),
(754, 'calling_desc', 'Please wait for your friend answer.', 'الرجاء الإنتظار لحين يتم الرد من قبل المستخدم.', 'Wacht tot je vriend antwoord op de video chat starten.', 'S&#39;il vous plaît attendre votre ami répondre à démarrer le chat vidéo.', 'Bitte warten Sie, Ihr Freund das Video-Chat zu starten beantworten.', 'Si prega di attendere per il vostro amico a rispondere per avviare la chat video.', 'Por favor aguarde o amigo responder para iniciar o bate-papo de vídeo.', 'Пожалуйста, подождите, ваш друг ответить, чтобы начать видео чат.', 'Por favor, espere a que su amigo responde a iniciar el chat de vídeo.', 'Arkadaşınız, video sohbet başlatmak için cevap için bekleyin.'),
(755, 'your_friends_chat', 'You&#039;re friends on {site_name}', 'أنتم أصدقاء في {site_name}', 'Je bent vrienden op {site_name}', 'Vous êtes amis sur {site_name}', 'Sie sind freunde auf {site_name}', 'Sei amici su {site_name}', 'Você é amigos {site_name}', 'Вы друзья на {site_name}', 'Eres amigos en {site_name}', 'Üzerinde dostuz {site_name}'),
(756, 'your_following', 'You&#039;re following', 'أنت تتابع', 'Je volgt', 'Vous suivez', 'Sie folgen', 'Stai seguendo', 'Você está seguindo', 'Вы следующие', 'Usted está siguiendo', 'İzlediğiniz'),
(757, 'see_all', 'See all', 'الكل', 'alles zien', 'Voir tout', 'Alles sehen', 'Vedi tutti', 'Ver todos', 'Увидеть все', 'Ver todo', 'Hepsini gör'),
(758, 'me', 'Me', 'أنا', 'Me', 'Moi', 'Mich', 'Me', 'Mim', 'меня', 'Yo', 'Ben'),
(759, 'post_promotion_hot', 'Boost up to {monthly_boosts} posts&lt;br&gt;', 'نشر اكثر من {monthly_boosts} منشورات &lt;br&gt;&lt;small&gt;({monthly_boosts} في نفس الوقت 7/24)&lt;/small&gt;', '{monthly_boosts} berichten omhoog plaatsen&lt;br&gt;&lt;small&gt;({monthly_boosts} tegelijk 7/24)&lt;/small&gt;', 'Boost up to {monthly_boosts} posts&lt;br&gt;&lt;small&gt;({monthly_boosts} in same time 7/24)&lt;/small&gt;', 'Bis zu {monthly_boosts} Beiträge hervorheben&lt;br&gt;&lt;small&gt;({monthly_boosts} Beiträge gleichen Zeit 7/24)&lt;/small&gt;', 'Boost fino a {monthly_boosts} posti&lt;br&gt;&lt;small&gt;({monthly_boosts} nel contempo 7/24)&lt;/small&gt;', 'Impulsionar até {monthly_boosts} postagens&lt;br&gt;&lt;small&gt;({monthly_boosts} ao mesmo tempo)&lt;/small&gt;', 'Повышение до {monthly_boosts} сообщений&lt;br&gt;&lt;small&gt;({monthly_boosts} в то же время 7/24)&lt;/small&gt;', 'Promociona asta {monthly_boosts} posts&lt;br&gt;&lt;small&gt;({monthly_boosts} al mismo tiempo 7/24)&lt;/small&gt;', '{monthly_boosts} mesaj yükselt&lt;br&gt;&lt;small&gt;({monthly_boosts} aynı zamanda 7/24)&lt;/small&gt;'),
(760, 'page_promotion_hot', 'Boost up to {monthly_boosts} pages&lt;br&gt;', 'نشر اكثر من {monthly_boosts} صفحات&lt;br&gt;&lt;small&gt;({monthly_boosts} في نفس الوقت 7/24)&lt;/small&gt;', '{monthly_boosts} pagina&#039;s omhoog plaatsen&lt;br&gt;&lt;small&gt;({monthly_boosts} tegelijk 7/24)&lt;/small&gt;', 'Boost up to {monthly_boosts} pages&lt;br&gt;&lt;small&gt;({monthly_boosts} in same time 7/24)&lt;/small&gt;', 'Bis zu {monthly_boosts} Seiten hervorheben&lt;br&gt;&lt;small&gt;({monthly_boosts} Seiten zur gleichen Zeit 7/24)&lt;/small&gt;', 'Boost fino a {monthly_boosts} pagine&lt;br&gt;&lt;small&gt;({monthly_boosts} nel contempo 7/24)&lt;/small&gt;', 'Impulsionar até {monthly_boosts} páginas&lt;br&gt;&lt;small&gt;({monthly_boosts} ao mesmo tempo)&lt;/small&gt;', 'Повышение до {monthly_boosts} страниц&lt;br&gt;&lt;small&gt;({monthly_boosts} в то же время 7/24)&lt;/small&gt;', 'Promociona asta {monthly_boosts} paginas&lt;br&gt;&lt;small&gt;({monthly_boosts} al mismo tiempo 7/24)&lt;/small&gt;', '{monthly_boosts} sayfa yükselt&lt;br&gt;&lt;small&gt;({monthly_boosts} aynı zamanda 7/24)&lt;/small&gt;'),
(761, 'post_promotion_ultima', 'Boost up to {yearly_boosts} posts&lt;br&gt;', 'نشر اكثر من {yearly_boosts} منشورات&lt;br&gt;&lt;small&gt;({yearly_boosts} في نفس الوقت 7/24)&lt;/small&gt;', '{yearly_boosts} berichten omhoog plaatsen&lt;br&gt;&lt;small&gt;({yearly_boosts} tegelijk 7/24)&lt;/small&gt;', 'Boost up to {yearly_boosts} posts&lt;br&gt;&lt;small&gt;({yearly_boosts} in same time 7/24)&lt;/small&gt;', 'Bis zu {yearly_boosts} Beiträge hervorheben&lt;br&gt;&lt;small&gt;({yearly_boosts} Beiträge zur gleichen Zeit 7/24)&lt;/small&gt;', 'Boost fino a {yearly_boosts} posti&lt;br&gt;&lt;small&gt;({yearly_boosts} nel contempo 7/24)&lt;/small&gt;', 'Impulsionar até {yearly_boosts} postagens&lt;br&gt;&lt;small&gt;({yearly_boosts} ao mesmo tempo)&lt;/small&gt;', 'Повысить до {yearly_boosts} должностей&lt;br&gt;&lt;small&gt;({yearly_boosts} в то же время 7/24)&lt;/small&gt;', 'Promociona asta {yearly_boosts} posts&lt;br&gt;&lt;small&gt;({yearly_boosts} al mismo tiempo 7/24)&lt;/small&gt;', '{yearly_boosts} mesaj yükselt&lt;br&gt;&lt;small&gt;({yearly_boosts} aynı zamanda 7/24)&lt;/small&gt;'),
(762, 'page_promotion_ultima', 'Boost up to {yearly_boosts} pages&lt;br&gt;', 'نشر اكثر من {yearly_boosts} صفحات&lt;br&gt;&lt;small&gt;({yearly_boosts} في نفس الوقت 7/24)&lt;/small&gt;', '{yearly_boosts} pagina&#039;s omhoog plaatsen&lt;br&gt;&lt;small&gt;({yearly_boosts} tegelijk 7/24)&lt;/small&gt;', 'Boost up to {yearly_boosts} pages&lt;br&gt;&lt;small&gt;({yearly_boosts} in same time 7/24)&lt;/small&gt;', 'Bis zu {yearly_boosts} Seiten hervorheben&lt;br&gt;&lt;small&gt;({yearly_boosts} Seiten zur gleichen Zeit 7/24)&lt;/small&gt;', 'Boost fino a {yearly_boosts} pagine&lt;br&gt;&lt;small&gt;({yearly_boosts} nel contempo 7/24)&lt;/small&gt;', 'Impulsionar até {yearly_boosts} páginas&lt;br&gt;&lt;small&gt;({yearly_boosts} ao mesmo tempo)&lt;/small&gt;', 'Повышение до {yearly_boosts} страниц&lt;br&gt;&lt;small&gt;({yearly_boosts} в то же время 7/24)&lt;/small&gt;', 'Promociona asta {yearly_boosts} paginas&lt;br&gt;&lt;small&gt;({yearly_boosts} al mismo tiempo 7/24)&lt;/small&gt;', '{yearly_boosts} sayfa yükselt&lt;br&gt;&lt;small&gt;({yearly_boosts} aynı zamanda 7/24)&lt;/small&gt;'),
(763, 'post_promotion_vip', 'Boost up to {lifetime_boosts} posts&lt;br&gt;', 'نشر اكثر من {lifetime_boosts} منشورات&lt;br&gt;&lt;small&gt;({lifetime_boosts} في نفس الوقت 7/24)&lt;/small&gt;', 'Boost up to {lifetime_boosts} posts&lt;br&gt;&lt;small&gt;({lifetime_boosts} in same time 7/24)&lt;/small&gt;', 'Boost up to {lifetime_boosts} posts&lt;br&gt;&lt;small&gt;({lifetime_boosts} in same time 7/24)&lt;/small&gt;', 'Bis zu {lifetime_boosts} Beiträge hervorheben&lt;br&gt;&lt;small&gt;({lifetime_boosts} Beiträge zur gleichen Zeit 7/24)&lt;/small&gt;', 'Boost fino a {lifetime_boosts} posti&lt;br&gt;&lt;small&gt;({lifetime_boosts} nel contempo 7/24)&lt;/small&gt;', 'Impulsionar até {lifetime_boosts} postagens&lt;br&gt;&lt;small&gt;({lifetime_boosts} ao mesmo tempo)&lt;/small&gt;', 'Повысить до {lifetime_boosts} должностей&lt;br&gt;&lt;small&gt;({lifetime_boosts} in same time 7/24)&lt;/small&gt;', 'Promociona asta {lifetime_boosts} posts&lt;br&gt;&lt;small&gt;({lifetime_boosts} al mismo tiempo 7/24)&lt;/small&gt;', '{lifetime_boosts} mesaj yükselt&lt;br&gt;&lt;small&gt;({lifetime_boosts} aynı zamanda 7/24)&lt;/small&gt;'),
(764, 'page_promotion_vip', 'Boost up to {lifetime_boosts} pages&lt;br&gt;', 'نشر اكثر من {lifetime_boosts} صفحات&lt;br&gt;&lt;small&gt;({lifetime_boosts} في نفس الوقت 7/24)&lt;/small&gt;', 'Boost up to {lifetime_boosts} pages&lt;br&gt;&lt;small&gt;({lifetime_boosts} in same time 7/24)&lt;/small&gt;', 'Boost up to {lifetime_boosts} pages&lt;br&gt;&lt;small&gt;({lifetime_boosts} in same time 7/24)&lt;/small&gt;', 'Bis zu {lifetime_boosts} Seiten hervorheben&lt;br&gt;&lt;small&gt;({lifetime_boosts} Seiten zur gleichen Zeit 7/24)&lt;/small&gt;', 'Boost fino a {lifetime_boosts} pagine&lt;br&gt;&lt;small&gt;({lifetime_boosts} nel contempo 7/24)&lt;/small&gt;', 'Impulsionar até {lifetime_boosts} páginas&lt;br&gt;&lt;small&gt;({lifetime_boosts} ao mesmo tempo)&lt;/small&gt;', 'Повышение до {lifetime_boosts} страниц&lt;br&gt;&lt;small&gt;({lifetime_boosts} в то же время 7/24)&lt;/small&gt;', 'Promociona asta {lifetime_boosts} paginas&lt;br&gt;&lt;small&gt;({lifetime_boosts} al mismo tiempo 7/24)&lt;/small&gt;', '{lifetime_boosts} sayfa yükselt&lt;br&gt;&lt;small&gt;({lifetime_boosts} aynı zamanda 7/24)&lt;/small&gt;'),
(765, 'sign_up', 'Sign up', 'التسجيل', 'Aanmelden', 'S&#39;inscrire', 'Anmelden', 'Registrazione', 'inscrever-se', 'зарегистрироваться', 'Regístrate', 'Kaydol'),
(766, 'youtube', 'YouTube', 'يوتيوب', 'YouTube', 'YouTube', 'YouTube', 'YouTube', 'Youtube', 'YouTube', 'Youtube', 'YouTube'),
(767, 'my_products', 'My Products', 'منتجاتي', 'mijn producten', 'Mes produits', 'Meine Produkte', 'I miei prodotti', 'meus produtos', 'Мои продукты', 'Mis productos', 'Ürünlerim'),
(768, 'choose_a_payment_method', 'Choose a payment method', 'اختر طريقة الدفع', 'Kies een betaalmethode', 'Choisissez une méthode de paiement', 'Wählen Sie eine Zahlungsmethode', 'Scegliere un metodo di pagamento', 'Escolha um método de pagamento', 'Выберите способ оплаты', 'Elija un método de pago', 'Bir ödeme yöntemi seçin'),
(769, 'paypal', 'PayPal', 'باي بال', 'PayPal', 'PayPal', 'PayPal', 'PayPal', 'PayPal', 'PayPal', 'PayPal', 'PayPal'),
(770, 'credit_card', 'Credit Card', 'بطاقة ائتمان', 'Credit Card', 'Credit Card', 'Kreditkarte', 'Carta di credito', 'Cartão de crédito', 'Кредитная карта', 'Tarjeta de crédito', 'Kredi Kartı'),
(771, 'bitcoin', 'Bitcoin', 'بيتكوين', 'Bitcoin', 'Bitcoin', 'Bitcoin', 'Bitcoin', 'Bitcoin', 'Bitcoin', 'Bitcoin', 'Bitcoin'),
(772, 'categories', 'Categories', 'الإقسام', 'Categorieën', 'Catégories', 'Kategorien', 'Categorie', 'Categorias', 'категории', 'Categorías', 'Kategoriler'),
(773, 'latest_products', 'Latest Products', 'آخر المنتجات', 'nieuwste producten', 'Derniers produits', 'Neueste Produkte', 'Gli ultimi prodotti', 'Produtos Mais recentes', 'Последние поступления', 'últimos productos', 'Yeni ürünler'),
(774, 'search_for_products_main', 'Search for products', 'إبحث عن منتج', 'Zoeken naar producten', 'Recherche de produits', 'Suche nach Produkten', 'Ricerca di prodotti', 'Pesquisa de produtos', 'Поиск продукции', 'Búsqueda de productos', 'Ürün ara'),
(775, 'search_for_products', 'Search for products in {category_name}', 'بحث عن منتج في {category_name}', 'Zoeken naar producten in {category_name}', 'Recherche de produits dans {category_name}', 'Suche nach Produkten im {category_name}', 'Ricerca di prodotti in {category_name}', 'Pesquisa para os produtos em {category_name}', 'Поиск продукции в {category_name}', 'Búsqueda de productos en {category_name}', 'Ürünlerde ara {category_name}'),
(776, 'no_available_products', 'No available products to show.', 'لا توجد منتجات متاحة.', 'Geen beschikbare tonend.', 'Pas de produits disponibles pour afficher.', 'Keine verfügbaren Produkte zu zeigen.', 'Non ci sono prodotti disponibili da mostrare.', 'Não há produtos disponíveis para mostrar.', 'Нет доступных продуктов для отображения.', 'No hay productos disponibles para mostrar.', 'Kullanılabilir bir ürün bulunamadı'),
(777, 'load_more_products', 'Load more products', 'تحميل المزيد من المنتجات', 'Laad meer producten', 'Chargez plus de produits', 'Laden Sie weitere Produkte', 'Caricare più prodotti', 'Carregar mais produtos', 'Загрузить больше продуктов', 'Cargar más productos', 'Daha fazla ürün göster'),
(778, 'sell_new_product', 'Sell new product', 'بيع منتج جديد', 'Verkoop een nieuw product', 'Vente nouveau produit', 'Verkauf neuer Produkte', 'Vendita nuovo prodotto', 'Vender novo produto', 'Продаем новый продукт', 'Vender nuevos productos', 'Yeni bir ürün sat'),
(779, 'description', 'Description', 'الوصف', 'Beschrijving', 'La description', 'Beschreibung', 'Descrizione', 'Descrição', 'Описание', 'Descripción', 'Açıklama'),
(780, 'please_describe_your_product', 'Please describe your product.', 'يرجى وصف المنتج الخاص بك.', 'Beschrijf uw product.', 'S&#39;il vous plaît décrire votre produit.', 'Bitte beschreiben Sie Ihr Produkt.', 'Si prega di descrivere il tuo prodotto.', 'Por favor, descreva o seu produto.', 'Пожалуйста, опишите ваш продукт.', 'Por favor describa su producto.', 'Ürününüzü açıklayın'),
(781, 'used', 'Used', 'مستعمل', 'Gebruikt', 'Utilisé', 'Benutzt', 'Usato', 'Usava', 'Используемый', 'Usado', 'Kullanılan'),
(782, 'new', 'New', 'جديد', 'Nieuwe', 'Nouveau', 'Neu', 'Nuovo', 'Novo', 'новый', 'Nuevo', 'Yeni'),
(783, 'price', 'Price', 'السعر', 'Prijs', 'Prix', 'Preis', 'Prezzo', 'Preço', 'Цена', 'Precio', 'Fiyat'),
(784, 'your_product_price', 'Your product price in USD currency ($), e.g (10.99)', 'سعر المنتج في الدولار ($), مثال (10.99)', 'Uw product prijs in USD valuta ($), e.g (10.99)', 'Votre prix du produit en monnaie USD ($), e.g (10.99)', 'Ihr Produktpreis in USD ($), e.g (10.99)', 'Il prezzo del prodotto in valuta USD ($), e.g (10.99)', 'Seu preço do produto em USD ($), por exemplo (10,99)', 'Ваша цена продукта в USD валюте ($) области, например (10,99)', 'Su precio del producto en USD ($), por ejemplo (10.99)', 'Ürün fiyatı dolar para birimi cinsinden ($), ör: (10.99)'),
(785, 'edit_product', 'Edit product', 'تحرير المنتج', 'Product bewerken', 'Modifier le produit', 'Bearbeiten Produkt', 'Modifica del prodotto', 'Editar produto', 'Изменить продукт', 'Editar producto', 'Ürün düzenle'),
(786, 'publish', 'Publish', 'نشر', 'Publiceren', 'Publier', 'Veröffentlichen', 'Pubblicare', 'Publicar', 'Публиковать', 'Publicar', 'Yayınla'),
(787, 'more_info', 'More info', 'المزيد', 'Meer informatie', 'More info', 'Mehr Infos', 'Ulteriori informazioni', 'Mais informações', 'Больше информации', 'Más información', 'Daha fazla bilgi'),
(788, 'contact_seller', 'Contact seller', 'تواصل مع البائع', 'De aanbieders contacteren', 'Contacter le vendeur', 'Verkäufer kontaktieren', 'Contatta il venditore', 'Contactar fornecedor', 'Связаться с продавцом', 'Contacte al vendedor', 'Satıcı olmak için başvurun'),
(789, 'by_product', 'By &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, posted {product_time}, in &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;', 'بواسطة &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, نشر {product_time}, في &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;', 'Door &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, gepost {product_time}, in &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;', 'Par &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, posté {product_time}, dans &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;', 'Durch &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, gesendet {product_time}, im &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;', 'Di &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, postato {product_time}, in &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;', 'Por &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, postou {product_time}, em &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;', 'По &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, размещенном {product_time}, в &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;', 'Por &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, publicado {product_time}, en &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;', 'Satışda olan ürün: &lt;a href=&quot;{product_url}&quot;&gt;{product_name}&lt;/a&gt;, Satışa başladığı zaman: {product_time}, Satış kategorisi: &lt;a href=&quot;{product_category}&quot;&gt;{product_category_name}&lt;/a&gt;'),
(790, 'payment_declined', 'Payment declined, please try again later.', 'حدثت مشكلة ، يرجى المحاولة مرة أخرى في وقت لاحق.', 'Betaling geweigerd, probeer het later opnieuw.', 'Paiement refusé, s&#39;il vous plaît essayer à nouveau plus tard.', 'Zahlung abgelehnt, bitte versuchen Sie es später noch einmal.', 'Pagamento rifiutato, riprova più tardi.', 'Pagamento recusado, por favor tente novamente mais tarde.', 'Платеж отклонен, пожалуйста, повторите попытку позже.', 'Pago rechazado, por favor intente de nuevo más tarde.', 'Ödeme reddedildi, lütfen daha sonra tekrar deneyin.'),
(791, 'c_payment', 'Confirming payment, please wait..', 'تأكيد الدفع، يرجى الانتظار ..', 'Bevestiging van de betaling, even geduld aub ..', 'paiement confirmant, s&#39;il vous plaît patienter ..', 'Bestätigen Zahlung, bitte warten ..', 'Confermando il pagamento, si prega di attendere ..', 'Confirmação do pagamento, aguarde por favor ..', 'Подтверждение оплаты, пожалуйста, подождите ..', 'Confirmar el pago, por favor espere ..', 'Ödeme kontrol ediliyor, lütfen bekleyin.'),
(792, 'earn_users', 'Earn up to ${amount} for each user your refer to us !', 'إكسب ما يصل الى ${amount} لكل مستخدم يسجل من جانبك !', 'Verdien tot ${amount} voor elke gebruiker je verwijzen naar ons!', 'Gagnez jusqu&#39;à ${amount} pour chaque utilisateur de votre référence à nous!', 'Verdienen Sie bis zu ${amount} Für jeden Benutzer beziehen Ihr uns!', 'Guadagna fino a ${amount} per ogni utente il vostro si riferiscono a noi!', 'Ganhe até ${amount} para cada usuário sua referem-se a nós!', 'Заработайте до ${amount} для каждого пользователя вашего обратитесь к нам!', 'Gane hasta ${amount} para cada usuario su refieren a nosotros!', 'Her kullanıcıdan ${amount} daha fazla kazanmak için bizi izleyin.'),
(793, 'earn_users_pro', 'Earn up to ${amount} for each user your refer to us and will subscribe to any of our pro packages.', 'إكسب ما يصل الى ${amount} لكل مستخدم يسجل من جانبك ويشترك باحدى عروضنا', 'Verdien tot ${amount} voor elke gebruiker je verwijzen naar ons en zal zich abonneren op een van onze propakketten.', 'Gagnez jusqu&#39;à ${amount} pour chaque utilisateur de votre référence à nous et souscrira à un de nos forfaits pro.', 'Verdienen Sie bis zu ${amount} Für jeden Benutzer beziehen Ihr für uns und wird zu einem unserer Pro-Pakete abonnieren.', 'Guadagna fino a ${amount} per ogni utente il vostro si riferiscono a noi e sottoscriverà uno qualsiasi dei nostri pacchetti pro.', 'Ganhe até ${amount} para cada usuário sua referem-se a nós e vai inscrever-se a qualquer um dos nossos profissionais pacotes.', 'Заработайте до ${amount} для каждого пользователя вашего обратитесь к нам и подписаться на любой из наших профессиональных пакетов.', 'Gane hasta ${amount} para cada usuario su refieren a nosotros y suscribirse a cualquiera de nuestros redactores paquetes.', 'Her kullanıcıdan ${amount} kazanmak için daha fazla pro paketlerimize abone olacak.'),
(794, 'my_affiliates', 'My Affiliates', 'دعوة الأصدقاء بالمكافأة', 'Mijn Affiliates', 'Mes Affiliés', 'Meine Affiliates', 'I miei affiliati', 'meus Afiliados', 'Мои Филиалы', 'Mis Afiliados', 'Benim referanslarım'),
(795, 'my_balance', 'My balance', 'رصيدي', 'Mijn balans', 'Mon solde', 'Mein Gleichgewicht', 'Il mio equilibrio', 'Meu saldo', 'Мой баланс', 'Mi balance', 'Bakiyem'),
(796, 'your_ref_link', 'Your affiliate link is', 'اللينك الخاص بك هو', 'Uw affiliate link is', 'Votre lien d&#39;affiliation est', 'Ihre Affiliate Link ist', 'Il tuo link:', 'Sua ligação da filial é', 'Ваша партнерская ссылка', 'Su red de afiliados es', 'Referans adresi'),
(797, 'your_balance', 'Your balance is ${balance}, minimum withdrawal request is ${m_withdrawal}', 'رصيدك هو ${balance}, الحد الأدنى لطلب السحب ${m_withdrawal}', 'Uw saldo is ${balance}, minimum een verzoek tot uitbetaling ${m_withdrawal}', 'Votre solde est ${balance}, demande de retrait minimum est ${m_withdrawal}', 'Ihre Waage ist ${balance}, minimum withdrawal request is ${m_withdrawal}', 'La bilancia è ${balance}, richiesta di prelievo minimo è ${m_withdrawal}', 'Seu saldo é de R ${balance} equilíbrio, o pedido de retirada mínima é de R ${m_withdrawal}', 'Ваш баланс составляет ${balance} баланс, минимальный запрос на вывод средств составляет ${m_withdrawal}', 'Su saldo es de ${balance} equilibrio, la solicitud de retiro mínimo es ${m_withdrawal}', 'Bakiyeniz ${balance}, minimum çekebileceğiniz tutar ${m_withdrawal}'),
(798, 'your_balance_is', 'Your balance is', 'رصيدك هو', 'Uw saldo is', 'Votre solde est', 'Ihre waage ist', 'La bilancia è', 'Seu saldo é', 'Ваш баланс', 'Su saldo es', 'Bakiyeniz'),
(799, 'paypal_email', 'PayPal email', 'أيميل البايبال الخاص بك', 'PayPal email', 'PayPal email', 'PayPal email', 'PayPal email', 'Email do Paypal', 'PayPal по электронной почте', 'E-mail de Paypal', 'PayPal e-posta adresi'),
(800, 'amount_usd', 'Amount (USD)', 'القيمة (دولار امريكي)', 'Bedrag (USD)', 'Montant (USD)', 'Menge (USD)', 'Quantità (USD)', 'Montante (USD)', 'Сумма (USD)', 'Monto (USD)', 'Tutar (USD)'),
(801, 'request_withdrawal', 'Request withdrawal', 'اسحب الرصيد', 'verzoek tot uitbetaling', 'Demande de retrait', 'Antrag rückzug', 'richiesta di prelievo', 'pedido de retirada', 'Запрос вывода', 'solicitud de retiro', 'Para çekme talebi'),
(802, 'payment_history', 'Payment History', 'تاريخ الدفع', 'Betaalgeschiedenis', 'Historique de paiement', 'Zahlungshistorie', 'Storico dei pagamenti', 'Histórico de pagamento', 'История платежей', 'historial de pagos', 'Ödeme tarihi'),
(803, 'amount', 'Amount', 'القيمة', 'Bedrag', 'Montant', 'Menge', 'Quantità', 'Quantidade', 'Количество', 'Cantidad', 'Tutar'),
(804, 'declined', 'Declined', 'تم الرفض', 'Afgewezen', 'Refusée', 'Abgelehnt', 'Rifiutato', 'Recusado', 'Отклонено', 'disminuido', 'Reddedildi'),
(805, 'approved', 'Approved', 'م القبول', 'Aangenomen', 'A approuvé', 'Genehmigt', 'Approvato', 'aprovado', 'утвержденный', 'Aprobado', 'Onaylandı'),
(806, 'total_votes', 'Total votes', 'مجموع الأصوات', 'Totaal aantal stemmen', 'Des votes', 'Anzahl der Kundenbewertungen', 'Totale voti', 'total de votos', 'Всего голосов', 'Total de votos', 'Toplam oy'),
(807, 'mark_as_sold', 'Mark Product As Sold', 'تم بيع المنتج', 'Mark Product zoals verkocht', 'Marque produit vendu', 'Mark erhältliche Erzeugnis', 'Mark prodotto commerciale', 'Mark produto comercializado', 'Маркировка продукта Как Продано', 'Marca de producto comercializado', 'Satılan ürün'),
(808, 'added_new_product_for_sell', 'added new product for sell.', 'ضاف منتج جديد للبيع.', 'toegevoegd nieuw product voor verkoopt.', 'nouveau produit ajouté pour vendre.', 'hinzugefügt neues Produkt zu verkaufen.', 'nuovo prodotto aggiunto per vendere.', 'adicionou novo produto para vender.', 'добавлен новый продукт для продажи.', 'añadido nuevo producto para la venta.', 'Yeni ürün satış için eklendi'),
(809, 'product_name', 'Product Name', 'اسم المنتج', 'productnaam', 'Nom du produit', 'Produktname', 'nome del prodotto', 'Nome do Produto', 'наименование товара', 'nombre del producto', 'Ürün adı'),
(810, 'in_stock', 'In stock', 'متاح', 'Op voorraad', 'en magasin', 'Auf Lager', 'Disponibile', 'Em estoque', 'В наличии', 'En stock', 'Stokda kaç adet var'),
(811, 'sold', 'Sold', 'تم البيع', 'Uitverkocht', 'Vendu', 'Verkauft', 'Venduto', 'Vendido', 'Продан', 'Vendido', 'Satılan'),
(812, 'answer', 'Answer', 'الجواب', 'Antwoord', 'Répondre', 'Antworten', 'Risposta', 'Responda', 'Ответ', 'Responder', 'Cevap'),
(813, 'add_answer', 'Add answer', 'إضافة جواب', 'Antwoord toevoegen', 'Ajouter une réponse', 'In Antwort', 'Aggiungi risposta', 'Adicionar resposta', 'Добавить ответ', 'Añadir respuesta', 'Cevap ekle'),
(814, 'authenticating', 'Authenticating', 'جاري تدقيق المعلومات', 'Authentiserende', 'Authentifier', 'Authentifizieren', 'autenticazione', 'autenticação', 'удостовер', 'de autenticación', 'Kimlik doğrulama'),
(815, 'welcome_back', 'Welcome back!', 'أهلا بك!', 'Welkom terug!', 'Nous saluons le retour!', 'Willkommen zurück!', 'Ben tornato!', 'Bem vindo de volta!', 'Добро пожаловать!', '¡Dar una buena acogida!', 'Tekrar hoşgeldiniz!'),
(816, 'welcome_', 'Welcome!', 'أهلا بك!', 'Welkom!', 'Bienvenue!', 'Willkommen!', 'Benvenuto!', 'Bem vindo!', 'Добро пожаловать!', '¡Bienvenido!', 'Hoşgediniz!'),
(817, 'connect_with_people', 'Connect with people.', 'تواصل مع الناس.', 'Contact maken met mensen.', 'Communiquer avec les gens.', 'Verbinden Sie sich mit Menschen.', 'Connettiti con persone.', 'Conectar com as pessoas.', 'Общайтесь с людьми.', 'Conectar con la gente.', 'İnsanlarla sürekli bağlantıda ol.'),
(818, 'make_new_friends', 'Make new friends.', 'كون صداقات جديدة.', 'Maak nieuwe vrienden.', 'Se faire de nouveaux amis.', 'Neue Freunde finden.', 'Fare nuovi amici.', 'Fazer novos amigos.', 'Завести новых друзей.', 'Hacer nuevos amigos.', 'Yeni arkadaşlar edin.'),
(819, 'share_your_memories', 'Share your memories.', 'شارك ذكرياتك.', 'Deel je herinneringen.', 'Partagez vos souvenirs.', 'Teilen Sie Ihre Erinnerungen.', 'Condividi i tuoi ricordi.', 'Partilhar as suas memórias.', 'Поделитесь своими воспоминаниями.', 'Compartir sus recuerdos.', 'Anılarını paylaş.'),
(820, 'create_new_relationships', 'Create new relationships.', 'أنشىء علاقات جديدة.', 'Maak nieuwe relaties.', 'Créer de nouvelles relations.', 'Erstellen Sie neue Beziehungen.', 'Crea nuove relazioni.', 'Criar novos relacionamentos.', 'Создание новых отношений.', 'Crear nuevas relaciones.', 'Yeni bir ilişki oluştur.'),
(821, 'discover_new_places', 'Discover new places.', 'إكتشف أماكن جديدة.', 'Ontdek nieuwe plaatsen.', 'Découvrez de nouveaux endroits.', 'Entdecken Sie neue Orte.', 'Scoprire posti nuovi.', 'Descubra novos lugares.', 'Откройте для себя новые места.', 'Descubrir nuevos lugares.', 'Yeni yerler keşfet.'),
(822, 'forgot_your_password', 'Forgot your password?', 'هل نسيت كلمة المرور?', 'Je wachtwoord vergeten?', 'Mot de passe oublié?', 'Haben Sie Ihr Passwort vergessen', 'Hai dimenticato la password?', 'Esqueceu sua senha?', 'Забыли пароль?', '¿Olvidaste tu contraseña?', 'Şifreni mi unuttun?'),
(823, 'invalid_markup', 'Invalid markup, please try to reset your password again', 'العلامة غير صالحة، يرجى المحاولة لإعادة تعيين كلمة المرور الخاصة بك مرة أخرى', 'Ongeldige markup, dan kunt u proberen om uw wachtwoord opnieuw in te resetten', 'balisage non valide, s&#39;il vous plaît essayez de réinitialiser votre mot de passe', 'Ungültige Markup, versuchen Sie Ihr Passwort wieder zurücksetzen', 'markup non valido, provare a reimpostare nuovamente la password', 'marcação inválida, por favor, tente redefinir sua senha novamente', 'Недопустимая разметка, пожалуйста, попробуйте сбросить пароль еще раз', 'marcado no válido, intenta restablecer la contraseña de nuevo', 'Geçersiz karakter kullandınız lütfen tekrar deneyin.'),
(824, 'go_back', 'Go back', 'الرجوع', 'Go back', 'Go back', 'Geh zurück', 'Go back', 'Volte', 'Возвращаться', 'Regresa', 'Geri git'),
(825, 'terms_agreement', 'By creating your account, you agree to our', 'قبل إنشاء الحساب الخاص بك، فإنك توافق على', 'Door het maken van uw account, gaat u akkoord met onze', 'En créant votre compte, vous acceptez nos', 'Durch die Erstellung Ihres Kontos stimmen Sie unseren', 'Creando il tuo account, accettate la nostra', 'Ao criar sua conta, você concorda com o nosso', 'При создании учетной записи, вы согласны с нашими', 'Al crear su cuenta, usted está de acuerdo con nuestra', 'Hesabınızı oluşturduğunuzda gizlilik şartlarımızı kabul etmiş sayılırsınız.'),
(826, 'please_choose_price', 'Please choose a price for your product', 'الرجاء اختيار سعر المنتج الخاص بك', 'Kies een prijs voor uw product', 'S&#39;il vous plaît choisir un prix pour votre produit', 'Bitte wählen Sie einen Preis für Ihr Produkt', 'Scegliere un prezzo per il prodotto', 'Por favor, escolha um preço para seu produto', 'Пожалуйста, выберите цену для вашего продукта', 'Por favor, elija un precio para su producto', 'Lütfen dürtmek için bir fiyat seçiniz'),
(827, 'please_choose_c_price', 'Please choose a correct value for your price', 'الرجاء اختيار القيمة الصحيحة للسعر الخاص بك', 'Kies een juiste waarde voor uw prijs', 'S&#39;il vous plaît choisir une valeur correcte pour votre prix', 'Bitte wählen Sie einen korrekten Wert für Ihr Preis', 'Scegliere un valore corretto per il vostro prezzo', 'Por favor, escolha um valor correto para o seu preço', 'Пожалуйста, выберите правильное значение для вашей цене', 'Por favor, elija un valor correcto para el precio', 'Lütfen fiyatı güncellerken bir değer giriniz'),
(828, 'please_upload_image', 'Please upload at least 1 photo', 'يرجى تحميل صورة واحد كحد ادنى', 'Upload ten minste 1 foto', 'S&#39;il vous plaît télécharger au moins 1 photo', 'Bitte laden Sie mindestens 1 Foto', 'Carica almeno 1 foto', 'Faça o upload de pelo menos 1 foto', 'Пожалуйста, загрузите по крайней мере 1 фото', 'Sube al menos 1 foto', 'Lütfen en az bir fotoğraf yükleyin'),
(829, 'you_have_already_voted', 'You have already voted this poll.', 'لقد قمت بالتصويت بالفعل لهذا الإستطلاع.', 'Je hebt al deze poll gestemd.', 'Vous avez déjà voté ce sondage.', 'Sie haben bereits abgestimmt diese Umfrage.', 'Hai già votato questo sondaggio.', 'Você já votou nesta enquete.', 'Вы уже голосовали этот опрос.', 'Ya ha votado esta encuesta.', 'Zaten bu ankete oy kullandın'),
(830, 'you_have_pending_request', 'You have already a pending request.', 'لديك بالفعل طلب معلق.', 'U heeft al een aanvraag in behandeling.', 'Vous avez déjà une demande en attente.', 'Sie haben bereits eine ausstehende Anforderung.', 'Hai già una richiesta in sospeso.', 'Você já tem um pedido pendente.', 'У вас есть уже отложенный запрос.', 'Ya tiene una solicitud pendiente.', 'Bekleyen bir isteğin var'),
(831, 'invalid_amount_value', 'Invalid amount value', 'قيمة غير صالحة', 'Ongeldig bedrag waarde', 'valeur de quantité non valide', 'Ungültige Betragswert', 'valore di importo non valido', 'valor montante inválido', 'Неверное значение суммы', 'valor de la cantidad no válida', 'Geçersiz bir miktar yazdın'),
(832, 'invalid_amount_value_your', 'Invalid amount value, your amount is:', 'قيمة غير صالحة, رصيدك هو:', 'Ongeldig bedrag waarde, uw bedrag is:', 'Valeur de quantité non valide, le montant est:', 'Ungültige Menge Wert, Ihre Menge ist:', 'valore di importo non valido, l&#039;importo è:', 'valor montante inválido, o valor é:', 'Неверное значение суммы, ваша сумма:', 'valor de la cantidad no válida, su cantidad es:', 'Geçersiz bir miktar yazdınız, bu tutar geöerli değildir:'),
(833, 'invalid_amount_value_withdrawal', 'Invalid amount value, minimum withdrawal request is:', 'قيمة غير صالحة, الحد الأدنى لطلب السحب:', 'Ongeldig bedrag waarde, minimum een verzoek tot uitbetaling:', 'valeur de quantité non valide, demande de retrait minimum est de:', 'Ungültige Betragswert , mindestauszahlungs anforderung ist:', 'Invalid amount value, minimum withdrawal request is:', 'valor montante inválido, o pedido de retirada mínima é:', 'Неверное значение суммы, минимальный запрос на вывод средств является:', 'valor de la cantidad no válida, la solicitud de retiro mínimo es:', 'Geçersiz tutar yazdınız minimum para çekme talebi:'),
(834, 'you_request_sent', 'Your request has been sent, you&#039;ll receive an email regarding the payment details soon.', 'تم إرسال طلبك، سوف تتلقى رسالة بريد إلكتروني حول تفاصيل المبلغ في وقت قريب.', 'Uw aanvraag is verzonden, zult u een e-mail met betrekking tot de betalingsgegevens binnenkort.', 'Votre demande a été envoyée, vous recevrez un e-mail concernant les détails de paiement bientôt.', 'Ihre Anfrage gesendet wurde, erhalten Sie eine E-Mail in Bezug auf die Zahlungsdetails erhalten bald.', 'La vostra richiesta è stata inviata, riceverai una e-mail per quanto riguarda i dati di pagamento al più presto.', 'O seu pedido foi enviado, você receberá um e-mail sobre os detalhes de pagamento em breve.', 'Ваш запрос был отправлен, вы получите по электронной почте о деталях платежа в ближайшее время.', 'Su solicitud ha sido enviado, recibirá un correo electrónico con respecto a los datos de pago pronto.', 'Para çekme isteğiniz başarı bir şekilde bize ulaştı yakında bununla ilgili bir e-posta göndereceğiz.'),
(835, 'turn_off_notification', 'Turn off notification sound', 'إيقاف صوت الإعلام', 'Schakel meldingsgeluid', 'Désactiver la notification sonore', 'Schalten Sie eine Benachrichtigung Sound', 'Disattiva suono di notifica', 'Desligar o som de notificação', 'Выключите звук уведомления', 'Desactivar el sonido de notificación', 'Bildirim sesini kapat'),
(836, 'turn_on_notification', 'Turn on notification sound', 'تشغيل صوت الإعلام', 'Zet meldingsgeluid', 'Activer la notification sonore', 'Schalten Sie eine Benachrichtigung Sound', 'Accendere il suono di notifica', 'Ligar som de notificação', 'Включите звук уведомления', 'Activar el sonido de notificación', 'Bildirim sesini aç'),
(837, 'view_more_posts', 'View {count} new posts', 'إظهار {count} منشور جديد', 'Uitzicht {count} nieuwe berichten', 'Vue {count} de nouveaux messages', 'Aussicht {count} neuen beiträge', 'Vista {count} nuovo messaggio', 'Veja {count} novas mensagens', 'Просмотр {count} новых сообщений', 'Ver {count} mensajes nuevos', 'Yeni mesajları görüntüle {count}'),
(838, 'store_posts_by', 'Store posts by', 'صنف المنشورات', 'Store berichten van', 'postes de magasins par', 'Zeige Beiträge', 'Visualizza i messaggi di', '&amp;nbsp Ver publicações de', 'Магазин сообщения от', 'almacenar mensajes de', 'Mağazada paylaşan'),
(839, 'new_audio_call', 'New audio call', 'إتصال جديد', 'Nieuwe audiogesprek', 'Nouveau appel audio', 'Neuer Audioanruf', 'Nuova chiamata audio', 'Nova chamada de áudio', 'Новый аудио вызов', 'Nueva llamada de audio', 'Yeni sesli çağrı'),
(840, 'new_audio_call_desc', 'wants to talk with you.', 'يريد التحدث معك.', 'wil met je praten.', 'Veut parler avec vous', 'Möchte mit Ihnen sprechen.', 'vuole parlare con te.', 'Quer falar com você', 'хочет поговорить с вами.', 'Quiere hablar contigo', 'Seninle konuşmak istiyor.'),
(841, 'audio_call', 'Audio call', 'مكالمة صوتية', 'audio oproep', 'Appel audio', 'Audioanruf', 'chiamata audio', 'Chamada de áudio', 'Аудиовызов', 'llamada de audio', 'Sesli arama'),
(842, 'audio_call_desc', 'talking with', 'يتحدث مع', 'praten met', 'parler avec', 'sprechen mit', 'parlando con', 'conversando com', 'говорить с', 'Hablando con', 'ile konuşmak'),
(843, 'market', 'Market', 'السوق', 'Markt', 'Marché', 'Markt', 'Mercato', 'Mercado', 'рынок', 'Mercado', 'Piyasa'),
(844, 'comment_post_label', 'Comment', 'علق', 'Kommentar', 'Commentaire', 'Kommentar', 'Commento', '', 'Комментарий', 'Comentario', 'Yorum Yap'),
(846, 'by', 'By', 'بواسطة', 'Door', 'Par', 'Durch', 'Di', 'De', 'От', 'Por', 'Tarafından'),
(847, 'load_more_blogs', 'Load more articles', 'تحميل المزيد من المقالات', 'Laad meer artikelen', NULL, 'Laden Sie weitere Artikel', 'Carica altri articoli', 'Carregar mais artigos', 'Загрузить другие статьи', 'Cargar más artículos', 'Daha fazla makale yükle'),
(848, 'blog', 'Blog', 'مدونة', 'blog', 'Blog', 'Blog', 'blog', 'Portal de notícias', 'Блог', 'Blog', 'Blog'),
(849, 'no_blogs_found', 'No articles found', 'لم يتم العثور على أية مقالات', 'Geen artikelen gevonden', 'Aucun article trouvé', 'Keine Artikel gefunden', 'Nessun articolo trovato', 'Nenhum artigo encontrado', 'Статьи не найдены', 'No se encontraron artículos', 'Makale bulunamadı'),
(850, 'most_recent_art', 'Most recent articles', 'أحدث المقالات', 'Meest recente artikelen', 'Articles les plus récents', 'Die neuesten Artikel', 'Articoli più recenti', 'Artigos mais recentes', 'Последние статьи', 'Artículos más recientes', 'En yeni makaleler'),
(851, 'create_new_article', 'Create new article', 'إنشاء مقالة جديدة', 'Nieuwe artikel', 'Créer un nouvel article', 'Erstellen Sie einen neuen Artikel', 'Crea un nuovo articolo', 'Criar novo artigo', 'Создать новую статью', 'Crear un nuevo artículo', 'Yeni makale oluştur'),
(852, 'my_articles', 'My articles', 'مقالاتي', 'mijn artikelen', 'Mes articles', 'Meine Artikel', 'I miei articoli', 'Meus artigos', 'Мои статьи', 'Mis artículos', 'Makalelerim'),
(853, 'title', 'Title', 'عنوان', 'Titel', 'Titre', 'Titel', 'Titolo', 'Título', 'заглавие', 'Título', 'Başlık'),
(854, 'content', 'Content', 'يحتوى', 'Inhoud', 'Contenu', 'Inhalt', 'Soddisfare', 'Conteúdo', 'Содержание', 'Contenido', 'İçerik'),
(855, 'select', 'Select', 'تحديد', 'kiezen', 'Sélectionner', 'Wählen', 'Selezionare', 'Selecionar', 'Выбрать', 'Seleccionar', 'Seç'),
(856, 'tags', 'Tags', 'العلامات', 'Tags', 'Mots clés', 'Tags', 'tag', 'Tag', 'Теги', 'Etiquetas', 'Etiketler'),
(857, 'thumbnail', 'Thumbnail', 'صورة مصغرة', 'thumbnail', 'La vignette', 'Miniaturansicht', 'Thumbnail', 'Miniatura', 'Значок видео', 'Miniatura', 'Küçük resim');
INSERT INTO `Wo_Langs` (`id`, `lang_key`, `english`, `arabic`, `dutch`, `french`, `german`, `italian`, `portuguese`, `russian`, `spanish`, `turkish`) VALUES
(858, 'published', 'Published', 'نشرت', 'Gepubliceerd', 'Publié', 'Veröffentlicht', 'Pubblicato', 'Publicados', 'Опубликовано', 'Publicado', 'Yayınlanan'),
(859, 'views', 'Views', 'الآراء', 'Uitzichten', 'Vues', 'Ansichten', 'Visualizzazioni', 'Visualizações', 'Просмотры', 'Puntos de vista', 'Görüntüler'),
(860, 'article_updated', 'Your article has been successfully updated', 'تم تحديث مقالتك بنجاح', 'Uw artikel is bijgewerkt', 'Votre article a été mis à jour avec succès', 'Ihr Artikel wurde erfolgreich aktualisiert', 'Il tuo articolo è stato aggiornato con successo', 'Seu artigo foi atualizado com sucesso', 'Ваша статья успешно обновлена', 'Su artículo ha sido actualizado con éxito', 'Makaleniz başarıyla güncellendi'),
(861, 'article_added', 'Your article has been successfully added', 'تمت إضافة مقالتك بنجاح', 'Uw artikel is succesvol toegevoegd', 'Votre article a été ajouté avec succès', 'Ihr Artikel wurde erfolgreich hinzugefügt', 'Il tuo articolo è stato aggiunto', 'Seu artigo foi adicionado com êxito', 'Ваша статья успешно добавлена', 'Su artículo ha sido añadido correctamente', 'Makalen başarıyla eklendi'),
(862, 'title_more_than10', 'Title should be more than 10 characters', 'يجب أن يكون العنوان أكثر من 10 أحرف', 'Titel moet meer zijn dan 10 tekens', 'Le titre doit comporter plus de 10 caractères', 'Titel sollte mehr als 10 Zeichen sein', 'Il titolo dovrebbe essere più di 10 caratteri', 'O título deve ter mais de 10 caracteres', 'Заголовок должен содержать более 10 символов.', 'El título debe tener más de 10 caracteres', 'Başlık en fazla 10 karakter olmalıdır'),
(863, 'desc_more_than32', 'Description should be more than 32 characters', 'يجب أن يكون الوصف أكثر من 32 حرفا', 'Beschrijving moet meer zijn dan 32 tekens', 'La description doit comporter plus de 32 caractères', 'Beschreibung sollte mehr als 32 Zeichen sein', 'Descrizione dovrebbe essere più di 32 caratteri', 'A descrição deve ter mais de 32 caracteres', 'Описание должно содержать более 32 символов.', 'La descripción debe tener más de 32 caracteres', 'Açıklama 32 karakterden uzun olmalı'),
(864, 'please_fill_tags', 'Please fill the tags field', 'يرجى ملء حقل العلامات', 'Vul het veld labels', 'Remplissez le champ tags', 'Bitte füllen Sie das Etikettenfeld aus', 'Si prega di compilare il campo tag', 'Preencha o campo de tags', 'Пожалуйста, заполните поле тегов', 'Por favor rellene el campo de etiquetas', 'Lütfen etiketler alanını doldurun'),
(865, 'error_found', 'Error found, please try again later', 'حدث خطأ، يرجى إعادة المحاولة لاحقا', 'Fout gevonden, probeer het later opnieuw', 'Une erreur a été trouvée, réessayez plus tard', 'Fehler gefunden, bitte später nochmal versuchen', 'Errore trovato, si prega di riprovare più tardi', 'Ocorreu um erro, tente novamente mais tarde', 'Ошибка найдена. Повторите попытку позже.', 'Error encontrado. Vuelve a intentarlo más tarde.', 'Hata bulundu, lütfen daha sonra tekrar deneyin.'),
(866, 'posted_on_blog', 'Posted {BLOG_TIME} in {CATEGORY_NAME}.', 'نشر {BLOG_TIME} في {CATEGORY_NAME}.', 'Posted {BLOG_TIME} in {CATEGORY_NAME}.', 'Posted {BLOG_TIME} in {CATEGORY_NAME}.', 'Posted {BLOG_TIME} in {CATEGORY_NAME}.', 'Posted {BLOG_TIME} in {CATEGORY_NAME}.', 'Posted {BLOG_TIME} in {CATEGORY_NAME}.', 'Posted {BLOG_TIME} in {CATEGORY_NAME}.', 'Posted {BLOG_TIME} in {CATEGORY_NAME}.', 'Yayınlanan {BLOG_TIME} {CATEGORY_NAME} da.'),
(867, 'created_new_blog', 'created new article', 'إنشاء مقالة جديدة', 'creëerde nieuwe artikel', 'Nouvel article créé', 'Erstellt neuen Artikel', 'nuovo articolo creato', 'Criou um novo artigo', 'Создал новую статью', 'Creó nuevo artículo', 'Yeni makale yazdı'),
(868, 'forum', 'Forum', 'منتدى', 'Forum', 'Forum', 'Forum', 'Forum', 'Fórum', 'Форум', 'Foro', 'forum'),
(869, 'replies', 'Replies', 'ردود', 'Antwoorden', 'Réponses', 'Antworten', 'risposte', 'Respostas', 'Ответы', 'Respuestas', 'Cevaplar'),
(870, 'last_post', 'Last Post', 'آخر مشاركة', 'Laatste bericht', 'Dernier commentaire', 'Letzter Beitrag', 'Ultimo messaggio', 'Última postagem', 'Последний пост', 'Ultima publicación', 'Son Posta'),
(871, 'topic', 'topic', 'موضوع', 'onderwerp', 'sujet', 'Thema', 'argomento', 'tema', 'тема', 'tema', 'konu'),
(872, 'thread_search', 'Threads search', 'بحث المواضي ', 'Zoek naar discussies', 'Recherche de threads', 'Threads suchen', 'Ricerca di thread', 'Pesquisa de Threads', 'Поиск по темам', 'Búsqueda de hilos', 'Konular arama'),
(873, 'create_new_topic', 'Create new topic', 'إنشاء موضوع جديد ', 'Maak een nieuw onderwerp aan', 'Créer un nouveau sujet', 'Neues Thema erstellen', 'Crea nuovo argomento', 'Criar novo tópico', 'Создать новую тему', 'Crear nuevo tema', 'Yeni konu oluştur'),
(874, 'jump_to', 'Jump to', 'انتقل الى ', 'Spring naar', 'Sauter à', 'Springen zu', 'Salta a', 'Pule para', 'Перейти к', 'Salta a', 'Atlamak'),
(875, 'my_threads', 'My threads', 'المواضيع ', 'Mijn draden', 'Mes fils', 'Meine Fäden', 'I miei fili', 'Meus tópicos', 'Мои темы', 'Mis hilos', 'Konuları ekle'),
(876, 'my_messages', 'My Messages', 'رسائلي ', 'Mijn berichten', 'Mes messages', 'Meine Nachrichten', 'I miei messaggi', 'Minhas mensagens', 'Мои сообщения', 'Mis mensajes', 'Mesajlarım'),
(877, 'headline', 'Headline', 'العنوان ', 'opschrift', 'Gros titre', 'Überschrift', 'Titolo', 'Título', 'Заголовок', 'Titular', 'manşet'),
(878, 'your_post', 'Your post', 'منشورك ', 'Uw bericht', 'Votre publication', 'Deine Post', 'Il tuo post', 'Sua postagem', 'Ваше сообщение', 'Tu mensaje', 'Senin gönderin'),
(879, 'reply', 'Reply', 'الرد ', 'Antwoord', 'Répondre', 'Antworten', 'rispondere', 'Resposta', 'Ответить', 'Respuesta', 'cevap'),
(880, 'started_by', 'Started by', 'بدأ ب ', 'Begonnen door', 'Commencé par', 'Angefangen von', 'Iniziato da', 'Começado por', 'Начато', 'Comenzado por', 'Başlatan'),
(881, 'site_admin', 'Site Admin', 'مسؤول الموقع ', 'Site Admin', 'Administrateur du site', 'Site Admin', 'Amministrazione del sito', 'Administrador do Site', 'Администратор сайта', 'Administrador del sitio', 'Site Yöneticisi'),
(882, 'registered', 'Registered', 'مسجل ', 'Geregistreerd', 'Inscrit', 'Eingetragen', 'Registrato', 'Registrado', 'зарегистрированный', 'Registrado', 'Kayıtlı'),
(883, 'posts', 'posts', 'المشاركات ', 'posts', 'des postes', 'Beiträge', 'messaggi', 'Postagens', 'сообщений', 'Mensajes', 'Mesajları'),
(884, 'reply_to_topic', 'Reply to this topic', 'الرد على هذا الموضوع ', 'Antwoord op dit onderwerp', 'Répondre à ce sujet', 'Antwort auf dieses Thema', 'Rispondi a questo argomento', 'Responder a este tópico', 'Ответить в эту тему Открыть новую тему', 'Responder a este tema', 'Bu konuyu cevapla'),
(885, 'topic_review', 'Topic review', 'مراجعة الموضوع ', 'Onderwerp review', 'Examen de sujet', 'Thema Bewertung', 'Revisione degli argomenti', 'Revisão do tópico', 'Обзор темы', 'Revisión de temas', 'Konu incelemesi'),
(886, 'your_reply', 'Your Reply', 'ردك ', 'Uw reactie', 'Votre réponse', 'Deine Antwort', 'La tua risposta', 'Sua resposta', 'Ваш ответ', 'Tu respuesta', 'Cevabınızı'),
(887, 'list_of_users', 'List of users', 'قائمة المستخدمين ', 'Lijst van gebruikers', 'Liste des utilisateurs', 'Liste der Benutzer', 'Elenco degli utenti', 'Lista de usuários', 'Список пользователей', 'Lista de usuarios', 'Kullanıcı listesi'),
(888, 'post_count', 'Posts count', 'عدد المشاركات ', 'Berichten tellen', 'Nombre de postes', 'Beiträge zählen', 'I numeri contano', 'Posts count', 'Количество сообщений', 'Los posts cuentan', 'Mesaj sayısı'),
(889, 'referrals', 'Referrals', 'الإحالات ', 'Verwijzingen', 'Renvois', 'Verweise', 'Referenti', 'Referências', 'Переходов', 'Referencias', 'Tavsiye'),
(890, 'last_visit', 'Last visit', 'الزيارة الأخيرة ', 'Laatste bezoek', 'Derniere visite', 'Letzter Besuch', 'Lultima visita', 'Ultima visita', 'Последнее посещение', 'Última visita', 'Son ziyaret'),
(891, 'general_search_terms', 'General search terms', 'عبارات البحث العامة ', 'Algemene zoektermen', 'Conditions générales de recherche', 'Allgemeine Suchbegriffe', 'Termini di ricerca generali', 'Termos gerais de pesquisa', 'Общие условия поиска', 'Términos generales de búsqueda', 'Genel arama terimleri'),
(892, 'search_for_term', 'Search for term', 'البحث عن مصطلح ', 'Zoek naar term', 'Rechercher un terme', 'Suche nach Begriff', 'Cerca termine', 'Pesquisar termo', 'Поиск термина', 'Buscar término', 'Terimi ara'),
(893, 'search_in', 'Search in', 'بحث في ', 'Zoek in', 'Rechercher dans', 'Suchen in', 'Cerca nel', 'Procure em', 'Поиск в', 'Busca en', 'Araştır'),
(894, 'search_in_forums', 'Search Forums', 'البحث في المنتديات ', 'Zoeken in forums', 'Rechercher dans les forums', 'Foren durchsuchen', 'Cerca i forum', 'Pesquisar Fóruns', 'Поиск по форуму Главная страница форума Форум', 'Buscar en los foros', 'Forumlarda Ara'),
(895, 'search_in_threads', 'Search in threads', 'البحث في المواضيع ', 'Zoek in discussies', 'Rechercher dans les discussions', 'Suche in Threads', 'Cerca nei thread', 'Pesquisar nos tópicos', 'Искать в темах', 'Buscar en temas', 'Konuları ara'),
(896, 'search_in_messages', 'Search in messages', 'البحث في الرسائل ', 'Zoek in berichten', 'Rechercher dans les messages', 'Suche in Nachrichten', 'Cerca nei messaggi', 'Pesquisar em mensagens', 'Искать в сообщениях', 'Buscar en mensajes', 'Mesajlarda ara'),
(897, 'search_subject_only', 'Search subject only', 'موضوع البحث فقط ', 'Zoek alleen onderwerp', 'Rechercher uniquement sur le sujet', 'Nur Suchbegriff suchen', 'Cerca solo il soggetto', 'Procurar somente assunto', 'Только поиск', 'Solo tema de búsqueda', 'Sadece konu ara'),
(898, 'threads', 'threads', 'الخيوط ', 'threads', 'Fils', 'Fäden', 'fili', 'tópicos', 'потоки', 'trapos', 'ipler'),
(899, 'action', 'Action', 'عمل ', 'Actie', 'action', 'Aktion', 'Azione', 'Açao', 'действие', 'Acción', 'Aksiyon'),
(900, 'posted', 'Posted', 'تم النشر ', 'Geplaatst', 'Publié', 'Gesendet', 'Pubblicato', 'Postou', 'Сообщение', 'Al corriente', 'Gönderildi'),
(901, 'no_forums_found', 'No forums found', 'لم يتم العثور على منتديات ', 'Geen forums gevonden', 'Aucun forum trouvé', 'Keine Foren gefunden', 'Nessun forum trovato', 'Nenhum fórum encontrado', 'Форум не найден', 'No se encontraron foros', 'Hiçbir forum bulunamadı'),
(902, 'never', 'Never', 'أبدا ', 'Nooit', 'Jamais', 'Nie', 'Mai', 'Nunca', 'Никогда', 'Nunca', 'Asla'),
(903, 'no_replies_found', 'No replies found', 'لم يتم العثور على أية ردود ', 'Geen antwoorden gevonden', 'Aucune réponse trouvée', 'Keine Antworten gefunden', 'Nessuna risposta trovata', 'Nenhuma resposta encontrada', 'Нет ответов', 'No se encontraron respuestas', 'Yanıt bulunamadı'),
(904, 'no_threads_found', 'No threads found', 'لم يتم العثور على سلاسل محادثات ', 'Geen discussies gevonden', 'Aucun sujet trouvé', 'Keine Fäden gefunden', 'Non sono stati trovati thread', 'Nenhum tópico encontrado', 'Темы не найдены', 'No se encontraron hilos', 'Konu bulunamadı'),
(905, 'no_members_found', 'No members found', 'لم يتم العثور على أي أعضاء ', 'Er zijn geen leden gevonden', 'Aucun membre trouvé', 'Keine Mitglieder gefunden', 'Nessun membro trovato', 'Nenhum membro encontrado', 'Участники не найдены', 'No se encontraron miembros', 'Üye bulunamadı'),
(906, 'no_sections_found', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(907, 'wrote', 'wrote', 'كتب ', 'schreef', 'a écrit', 'schrieb', 'ha scritto', 'escrevi', 'писал', 'Escribió', 'yazdı'),
(908, 'edit', 'Edit', 'تصحيح', 'Bewerk', 'modifier', 'Bearbeiten', 'Modifica', 'Editar', 'редактировать', 'Editar', 'Düzenleme'),
(909, 'edit_topic', 'Edit topic', 'تعديل الموضوع ', 'Bewerk onderwerp', 'Modifier le sujet', 'Thema bearbeiten', 'Modifica argomento', 'Editar tópico', 'Изменить тему', 'Editar tema', 'Konuyu düzenle'),
(910, 'reply_saved', 'Your reply was successfully saved', 'تم حفظ ردك بنجاح ', 'Uw antwoord is succesvol opgeslagen', 'Votre réponse a été enregistrée avec succès', 'Ihre Antwort wurde erfolgreich gespeichert', 'La tua risposta è stata salvata correttamente', 'Sua resposta foi salva com êxito', 'Ваш ответ был успешно сохранен', 'Tu respuesta se ha guardado correctamente.', 'Yanıtınız başarıyla kaydedildi'),
(911, 'reply_added', 'Your reply was successfully added', 'تمت إضافة ردك بنجاح', 'Je antwoord is succesvol toegevoegd', 'Votre réponse a été ajoutée avec succès', 'Ihre Antwort wurde erfolgreich hinzugefügt', 'La tua risposta è stata aggiunta con successo', 'Sua resposta foi adicionada com êxito', 'Ваш ответ был успешно добавлен', 'Tu respuesta se ha agregado correctamente.', 'Yanıtınız başarıyla eklendi'),
(912, 'forum_post_added', 'Your forum has been successfully added', 'تمت إضافة مشاركة المنتدى بنجاح ', 'Uw forum is succesvol toegevoegd', 'Votre forum a été ajouté avec succès', 'Dein Forum wurde erfolgreich hinzugefügt', 'Il tuo forum è stato aggiunto con successo', 'Seu fórum foi adicionado com sucesso', 'Ваш форум успешно добавлен', 'Tu foro se ha agregado correctamente', 'Forumunuz başarıyla eklendi'),
(913, 'members', 'Members', 'أفراد', 'leden', 'Membres', 'Mitglieder', 'Utenti', 'Membros', 'члены', 'Miembros', 'Üyeler'),
(914, 'help', 'Help', 'مساعدة ', 'Helpen', 'Aidez-moi', 'Hilfe', 'Aiuto', 'Socorro', 'Помощь', 'Ayuda', 'yardım et'),
(915, 'search_terms_more4', 'Type in one or more search terms, each must be at least 4 characters', 'اكتب عبارة بحث واحدة أو أكثر، ويجب ألا يقل عدد الأحرف عن 4 أحرف ', 'Typ één of meer zoektermen, ieder moet minstens 4 karakters', 'Tapez un ou plusieurs termes de recherche, chacun doit être dau moins 4 caractères', 'Geben Sie einen oder mehrere Suchbegriffe ein, die jeweils muss mindestens 4 Zeichen lang sein', 'Geben Sie einen oder mehrere Suchbegriffe ein, die jeweils muss mindestens 4 Zeichen lang sein', 'Digite um ou mais termos de pesquisa, cada um deve ter pelo menos 4 caracteres', 'Введите одно или несколько поисковых терминов, каждый из них должен быть не менее 4-х символов', 'Tipo de uno o más términos de búsqueda, cada uno debe tener al menos 4 caracteres', 'Bir veya daha fazla arama terimi girin, her En Az 4 karakter olmalıdır'),
(916, 'events', 'Events', 'أحداث', 'Evenementen', 'Événements', 'Veranstaltungen', 'eventi', 'Eventos', 'Мероприятия', 'Eventos', 'Olaylar'),
(917, 'going', 'Going', 'ذاهب', 'gaand', 'Aller', 'Gehen', 'Andando', 'Indo', 'Пойду', 'Yendo', 'gidiş'),
(918, 'interested', 'Interested', 'يستفد', 'Geïnteresseerd', 'Intéressé', 'Interessiert', 'Interessato', 'Interessado', 'интересное', 'Interesado', 'ilgili'),
(919, 'past', 'Pastor', 'الماضي', 'Verleden', 'Passé', 'Vergangenheit', 'Passato', 'Passado', 'прошлые', 'Pasado', 'geçmiş'),
(920, 'invited', 'invited', 'دعوة', 'Uitgenodigd', 'Invité', 'Eingeladen', 'Invitato', 'Convidamos', 'приглашенни', 'Invitado', 'Davetli'),
(921, 'you_are_going', 'You are going', 'انت ذاهب', 'Jij gaat', 'Vous allez', 'Du gehst', 'Stai andando', 'Você está indo', 'Вы собираетесь', 'Usted va', 'Gidiyorsun'),
(922, 'you_are_interested', 'You are interested', 'كنت مهتما', 'Je bent geïnteresseerd', 'Tu es intéressé', 'Sie sind interessiert', 'Sei interessato', 'Você está interessado', 'Вы заинтересованы', 'Tú estás interesado', 'İlgilisin'),
(923, 'start_date', 'Start date', 'تاريخ البدء', 'Begin datum', 'Date de début', 'Anfangsdatum', 'Data dinizio', 'Data de início', 'Дата начала', 'Fecha de inicio', 'Başlangıç tarihi'),
(924, 'end_date', 'End date', 'تاريخ الانتهاء', 'Einddatum', 'Date de fin', 'Enddatum', 'Data di fine', 'Data final', 'Дата окончания', 'Fecha final', 'Bitiş tarihi'),
(925, 'location', 'Location', 'موقع', 'Plaats', 'Emplacement', 'Lage', 'Posizione', 'Escreva o nome do bairro e/ou cidade', 'Расположение', 'Ubicación', 'Konum'),
(926, 'event', 'Event', 'هدف', 'Evenement', 'un événement', 'Event', 'Evento', 'Evento', 'Мероприятие', 'Evento', 'Olay'),
(927, 'no_events_found', 'No events found', 'لم يتم العثور على أية أحداث', 'Geen evenementen gevonden', 'Aucun événement trouvé', 'Keine Veranstaltungen gefunden', 'Nessun evento trovato', 'Nenhum evento encontrado', 'События не найдены', 'No se han encontrado eventos', 'Etkinlik bulunamadı'),
(928, 'event_you_may_like', 'Events you may like', 'الأحداث التي قد تعجبك', 'Evenementen die je misschien leuk vindt', 'Événements que vous aimeriez', 'Veranstaltungen, die Sie mögen können', 'Eventi che ti piacciono', 'Eventos que você pode gostar', 'Мероприятия, которые могут вам понравиться', 'Eventos que te pueden gustar', 'Beğeneceğiniz etkinlikler'),
(929, 'going_people', 'Going people', 'الذهاب الناس', 'Mensen gaan', 'Aller aux gens', 'Leute gehen', 'Andando gente', 'Indo as pessoas', 'Идущие люди', 'Personas que van', 'İnsanlara gidiyor'),
(930, 'interested_people', 'Interested people', 'الناس المهتمين', 'Geïnteresseerde mensen', 'Les personnes intéressées', 'Interessierte Leute', 'Persone interessate', 'Pessoas interessadas', 'Заинтересованные лица', 'Personas interesadas', 'İlgilenen insanlar'),
(931, 'invited_people', 'Invited people', 'الأشخاص المدعوون', 'Uitgenodigde mensen', 'Personnes invitées', 'Eingeladene Leute', 'Persone invitate', 'Pessoas convidadas', 'Приглашенные люди', 'Personas invitadas', 'Davet edilenler'),
(932, 'event_added', 'Your event was successfully added', 'تمت إضافة هذا الحدث الخاص بك بنجاح', 'Uw evenement is toegevoegd', 'Votre événement a été ajouté avec succès', 'Ihre Veranstaltung wurde erfolgreich hinzugefügt', 'Il vostro evento è stato aggiunto', 'Seu evento foi adicionado com sucesso', 'Ваше мероприятие успешно добавлено', 'Su caso se ha añadido con éxito', 'Etkinliğiniz başarıyla eklendi'),
(933, 'event_saved', 'Your event was successfully saved.', 'تم حفظ هذا الحدث الخاص بك', 'Uw evenement is opgeslagen', 'Votre événement a été enregistré', 'Ihre Veranstaltung wurde gespeichert', 'Il vostro evento è stato salvato', 'Seu evento foi salvo', 'Ваше мероприятие успешно сохранено', 'Su caso se ha guardado', 'Etkinlik kaydedildi'),
(934, 'confirm_delete_event', 'You are sure that you want to delete this event', 'كنت متأكدا من أنك تريد حذف هذا الحدث', 'Bent u zeker dat u wilt dit evenement verwijderen', 'Vous êtes sûr que vous voulez supprimer cet événement', 'Sie sind sicher, dass Sie dieses Ereignis löschen möchten', 'Sei sicuro di voler eliminare questo evento', 'Você tem certeza de que deseja excluir este evento', 'Вы уверены что Вы хотите удалить это событие', 'Está seguro de que desea eliminar este evento', 'Sen bu etkinliği silmek istediğinizden emin misiniz'),
(935, 'load_more', 'Load more', 'تحميل أكثر', 'Meer laden', 'Chargez plus', 'laden Sie mehr', 'caricare più', 'Coloque mais', 'Показать еще', 'Cargar más', 'daha fazla yükle'),
(936, 'subject', 'Subject', 'موضوع', 'Onderwerpen', 'Assujettir', 'Fach', 'Soggetto', 'Sujeito', 'Предмет', 'Tema', 'konu'),
(937, 'go', 'Go', 'اذهب', 'Gaan', 'Aller', 'Gehen', 'Partire', 'Ir', 'Идти', 'Ir', 'Gitmek'),
(938, 'created_new_event', 'created new event', 'حدث جديد', 'Aangemaakt nieuw evenement', 'Nouvel événement créé', 'Neue Veranstaltung erstellt', 'Ha creato un nuovo evento', 'Criou um novo evento', 'Создано новое мероприятие', 'Creó nuevo evento', 'Yeni bir etkinlik yarattı'),
(939, 'my_events', 'My events', 'أحداثي', 'Mijn gebeurtenissen', 'Mes événements', 'Meine ereignisse', 'I miei eventi', 'Meus eventos', 'Мои мероприятия', 'Mis eventos', 'Etkinliklerim'),
(940, 'is_interested', 'is interested on your event \"{event_name}\"', 'مهتم بحدثك \"{event_name}\"', 'Is geïnteresseerd in je evenement \"{event_name}\"', 'Est intéressé par votre événement \"{event_name}\"', 'Interessiert sich für deine Veranstaltung \"{event_name}\"', 'È interessato al tuo evento \"{event_name}\"', 'Está interessado no seu evento \"{event_name}\"', 'Заинтересовано в вашем мероприятии \"{event_name}\"', 'Está interesado en su evento \"{event_name}\"', '\"{Event_name}\" etkinliğinizle ilgileniyor.'),
(941, 'is_going', 'is going to your event \"{event_name}\"', 'هو الذهاب إلى الحدث \"{event_name}\"', 'Gaat naar je evenement \"{event_name}\"', 'Va à votre événement \"{event_name}\"', 'Geht zu deiner Veranstaltung \"{event_name}\"', 'Sta andando al tuo evento \"{event_name}\"', 'Está indo para o seu evento \"{event_name}\"', 'Идет на ваше мероприятие \"{event_name}\"', 'Va a su evento \"{event_name}\"', '\"{Event_name}\" etkinliğine gidiyor'),
(942, 'invited_you_event', 'invited you to go the event \"{event_name}\"', 'دعاك إلى الذهاب إلى الحدث \"{event_name}\"', 'Heeft u uitgenodigd om het evenement te gaan \"{event_name}\"', NULL, 'Lud dich ein, die Veranstaltung zu starten \"{event_name}\"', NULL, 'Convidou você para ir ao evento \"{event_name}\"', 'Приглашает вас на мероприятие \"{event_name}\"', 'Te invitó a ir al evento \"{event_name}\"', 'Sizi \"{event_name}\" etkinliğine davet etti.'),
(943, 'replied_to_topic', 'replied to your topic', 'أجاب على الموضوع', 'Antwoordde op je onderwerp', 'A répondu à votre sujet', 'Antwortete auf dein Thema', 'Ha risposto al tuo argomento', 'Respondeu ao seu tópico', 'Ответил на вашу тему', 'Respondió a su tema', 'Cevabınız cevaplandı'),
(944, 'movies', 'Movies', 'أفلام', 'Dioscoop', 'Films', 'Kino', 'Film', 'Filmes', 'Кино', 'Películas', 'Filmler'),
(945, 'translate', 'Translate', 'ترجم', 'Vertalen', 'Traduire', 'übersetzen', 'Tradurre', 'Traduzir', 'перевести', 'Traducciones', 'çevirmek'),
(946, 'genre', 'Genre', 'نوع أدبي', 'Genre', 'Genre', 'Genre', 'Genere', 'Gênero', 'Жанр', 'Genre', 'tarz'),
(947, 'recommended', 'Recommended', 'موصى به', 'Aanbevolen', 'Recommandé', 'empfohlen', 'Raccomandato', 'Recomendado', 'Рекомендуемые', 'Se recomienda', 'Tavsiye'),
(948, 'most_watched', 'Most watched', 'الأكثر مشاهدة', 'Meest bekeken', 'Le plus regardé', 'Die meisten angeschaut ', 'Più visto', 'Mais visto', 'Понравившиеся', 'Más información', 'En çok izlenen'),
(949, 'stars', 'Stars', 'نجوم', 'Stars', 'Etoiles', 'Sterne', 'Stars', 'Estrelas', 'Звезды', 'Estrellas', 'yıldız'),
(950, 'more', 'More', 'أكثر', 'Meer', 'Plus', 'mehr', 'Più', 'Mais', 'еще', 'Más información', 'daha'),
(951, 'no_movies_found', 'No movies found', 'لم يتم العثور على الأفلام', 'Geen films gevonden', 'Pas de films trouvés', 'Keine Filme gefunden', 'Nessun film trovato', 'Não há filmes encontrados', 'Фильмы не найдены', 'No movies found', 'Filmlerde Bulunan'),
(952, 'producer', 'Producer', 'منتج', 'Producent', 'Producteur', 'Produzent', 'Produttore', 'Produtor', 'Продюсер', 'Producer', 'yapımcı'),
(953, 'release', 'Release', 'إطلاق', 'Vrijlating', 'Sortie', 'Veröffentlichung', 'Rilascio', 'Lançamento', 'Релиз', 'Versión', 'salıverme'),
(954, 'quality', 'Quality', 'جودة', 'Kwaliteit', 'Qualité', 'Qualität', 'Qualità', 'Qualidade', 'Качество', 'Calidad', 'kalite'),
(955, 'more_like_this', 'More like this', 'أكثر من هذا القبيل', 'Meer in deze trant', 'Plus darticles', 'Ähnliche Titel', 'Altri risultati simili', 'Mais como este', 'Похожие фильмы', 'Más información', 'Buna benzer'),
(956, 'wallet', 'Wallet', 'محفظة نقود', 'Portemonnee', 'Portefeuille', 'Brieftasche', 'Portafoglio', 'Carteira', 'Бумажник', 'Billetera', 'Cüzdan'),
(957, 'company', 'Company', 'شركة', 'Bedrijf', 'Compagnie', 'Unternehmen', 'Azienda', 'Empresa', 'Компания', 'Empresa', 'şirket'),
(958, 'bidding', 'Bidding', 'مزايدة', 'bod', 'Enchère', 'Bieten', 'offerta', 'Licitação', 'торги', 'Ofertas', 'teklif verme'),
(959, 'clicks', 'Clicks', 'نقرات', 'klikken', 'Clics', 'Klicks', 'clic', 'Cliques', 'щелчки', 'Clics', 'Tıklanma'),
(960, 'url', 'Url', 'رابط', 'url', 'Url', 'Url', 'url', 'Url', 'Веб-сайт', 'Url', 'URL'),
(961, 'audience', 'Audience', 'جمهور', 'Publiek', 'Public', 'Publikum', 'Pubblico', 'Público', 'Аудитория', 'Audiencia', 'seyirci'),
(962, 'select_image', 'Select an image', 'حدد صورة', 'Selecteer een afbeelding', 'Sélectionnez une image', 'Wählen Sie ein Bild aus', 'Selezionare unimmagine', 'Selecione uma imagem', 'Выберите изображение', 'Seleccione una imagen', 'Bir resim seçin'),
(963, 'my_balance', 'My balance', 'رصيدي', 'Mijn balans', 'Mon solde', 'Mein Gleichgewicht', 'Il mio equilibrio', 'Meu saldo', 'Мой баланс', 'Mi balance', 'Bakiyem'),
(964, 'replenish_my_balance', 'Replenish my balance', 'تجديد رصيد بلدي', 'Herstel mijn saldo', 'Récupérer mon solde', 'Fülle meine Balance auf', 'Riempire il mio equilibrio', 'Reabasteça meu saldo', 'Пополнить баланс', 'Reponer mi saldo', 'Bakiyemi yenile'),
(965, 'continue', 'Continue', 'استمر', 'voortzetten', 'Continuer', 'Fortsetzen', 'Continua', 'Continuar', 'Продолжить', 'Continuar', 'Devam et'),
(966, 'replenishment_notif', 'Your balance has been replenished by', 'تمت إعادة تجديد رصيدك بواسطة', 'Uw saldo is aangevuld door', 'Votre solde a été reconstitué par', 'Ihr Gleichgewicht wurde ergänzt durch', 'Il tuo equilibrio è stato riempito da', 'Seu saldo foi reabastecido por', 'Ваш баланс был пополнен', 'Tu saldo ha sido reabastecido por', 'Bakiyeniz, tarafından yeniden dolduruldu.'),
(967, 'ads', 'Advertising', 'إعلان', 'Reclame', 'Publicité', 'Werbung', 'Pubblicità', 'Publicidade', 'Реклама', 'Publicidad', 'Ilan'),
(968, 'confirm_delete_ad', 'Are you sure you want to delete this ad', 'هل أنت متأكد أنك تريد حذف هذا الإعلان', 'Weet u zeker dat u deze advertentie wilt verwijderen', 'Êtes-vous sûr de vouloir supprimer cette annonce?', 'Möchten Sie diese Anzeige wirklich löschen?', 'Sei sicuro di voler cancellare questo annuncio', 'Tem certeza de que quer apagar este anúncio', 'Вы уверены, что хотите удалить эту рекламу', 'Estás seguro de que quieres eliminar esta publicidad', 'Bu reklamı silmek istediğinizden emin misiniz'),
(969, 'delete_ad', 'Delete ad', 'حذف الإعلان', 'Verwijder advertentie', 'Supprimer lannonce', 'Anzeige löschen', 'Elimina annuncio', 'Eliminar anúncio', 'Удалить объявление', 'Eliminar anuncio', 'Reklamı sil'),
(970, 'no_ads_found', 'No ads found', 'لم يتم العثور على أية إعلانات', 'Geen advertenties gevonden', 'Aucune annonce na été trouvée', 'Keine Anzeigen gefunden', 'Nessun annuncio trovato', 'Nenhum anúncio encontrado', 'Объявления не найдены', 'No se han encontrado anuncios', 'Hiç ilan bulunamadı'),
(971, 'not_active', 'Not active', 'غير فعال', 'Niet actief', 'Pas actif', 'Nicht aktiv', 'Non attivo', 'Não ativo', 'Не активен', 'No activo', 'Aktif değil'),
(972, 'appears', 'Placement', 'تحديد مستوى', 'Plaatsing', 'Placement', 'Platzierung', 'Posizionamento', 'Colocação', 'размещение', 'Colocación', 'Yerleştirme'),
(973, 'sidebar', 'Sidebar', 'الشريط الجانبي', 'sidebar', 'Barre latérale', 'Seitenleiste', 'Sidebar', 'Barra Lateral', 'Боковая панель', 'Barra lateral', 'Kenar çubuğu'),
(974, 'select_a_page_or_link', 'Select a page or enter a link to your site', 'حدد صفحة أو أدخل رابطا إلى موقعك', 'Selecteer een pagina of voer een link in op uw site', 'Sélectionnez une page ou entrez un lien vers votre site', 'Wählen Sie eine Seite aus oder geben Sie einen Link zu Ihrer Website ein', 'Seleziona una pagina o inserisci un link al tuo sito', 'Selecione uma página ou insira um link para o seu site', 'Выберите страницу или введите ссылку на свой сайт', 'Seleccione una página o ingrese un enlace a su sitio', 'Bir sayfa seçin veya sitenize bir bağlantı girin'),
(975, 'story', 'Story', 'قصة', 'Verhaal', 'Récit', 'Geschichte', 'Storia', 'História', 'История', 'Historia', 'Öykü'),
(976, 'max_number_status', 'The maximum number can not exceed 20 files at a time!', 'الحد الأقصى لعدد لا يمكن أن يتجاوز 20 ملفات في وقت واحد!', 'Het maximaal aantal kan niet meer dan 20 bestanden tegelijkertijd overschrijden!', 'Le nombre maximal ne peut pas dépasser 20 fichiers à la fois!', 'Die maximale Anzahl darf maximal 20 Dateien nicht überschreiten!', 'Il numero massimo non può superare 20 file alla volta!', 'O número máximo não pode exceder 20 arquivos de cada vez!', 'Максимальное число не может превышать 20 файлов за раз!', '¡El número máximo no puede superar los 20 archivos a la vez!', 'Maksimum sayı, aynı anda 20 dosya aşamaz!'),
(977, 'status_added', 'Your status has been successfully added!', 'تمت إضافة حالتك بنجاح!', 'Uw status is succesvol toegevoegd!', 'Votre statut a été ajouté avec succès!', 'Ihr Status wurde erfolgreich hinzugefügt!', 'Il tuo stato è stato aggiunto con successo!', 'Seu status foi adicionado com sucesso!', 'Ваш статус успешно добавлен!', '¡Tu estado se ha agregado correctamente!', 'Durumunuz başarıyla eklendi!'),
(978, 'create_new_status', 'Create New Status', 'إنشاء حالة جديدة', 'Maak een nieuwe status aan', 'Créer un nouvel état', 'Neuen Status anlegen', 'Crea nuovo stato', 'Criar novo status', 'Создать новый статус', 'Crear nuevo estado', 'Yeni Durum Oluştur'),
(979, 'sponsored', 'Sponsored', 'برعاية', 'Sponsored', 'Sponsorisé', 'Gefördert', 'sponsorizzati', 'Patrocinadas', 'Рекламные', 'Patrocinado', 'Sponsor'),
(980, 'notification_sent', 'Your notification has been sent successfully', 'تم إرسال الإشعار بنجاح', 'Uw melding is succesvol verzonden', 'Votre notification a été envoyée avec succès', 'Ihre Benachrichtigung wurde erfolgreich gesendet', 'La tua notifica è stata inviata correttamente', 'Sua notificação foi enviada com sucesso', 'Ваше уведомление отправлено успешно', 'Tu notificación se ha enviado correctamente', 'Bildiriminiz başarıyla gönderildi'),
(981, 'hide_post', 'Hide post', 'آخر اخفاء', 'Verberg post', 'Masquer la publication', 'Beitrag ausblenden', 'Nascondi post', 'Ocultar postagem', 'Скрыть сообщение', 'Esconder la publicación', 'Postayı gizle'),
(982, 'verification_sent', 'Your verification request  soon will be considered!', 'سيتم النظر في طلب التحقق قريبا!', 'Uw verificatieaanvraag zal binnenkort worden overwogen!', 'Votre demande de vérification sera bientôt prise en considération!', 'Ihre Bestätigungsanforderung wird bald berücksichtigt!', 'La tua richiesta di verifica sarà presto presa in considerazione!', 'Seu pedido de verificação em breve será considerado!', 'Ваш запрос на подтверждение скоро будет рассмотрен!', 'Su solicitud de verificación pronto será considerada!', 'Doğrulama isteğiniz yakında değerlendirilecek!'),
(983, 'profile_verification', 'Verification of the profile!', 'التحقق من الملف الشخصي!', 'Verificatie van het profiel!', 'Vérification du profil!', 'Überprüfung des Profils!', 'Verifica del profilo!', 'Verificação do perfil!', 'Проверка профиля!', 'Verificación del perfil!', 'Profilin doğrulanması!'),
(984, 'verification_complete', 'Congratulations your profile is verified!', 'تهانينا تم التحقق من ملفك الشخصي!', 'Gefeliciteerd, uw profiel is geverifieerd!', 'Félicitations, votre profil est vérifié!', 'Herzlichen Glückwunsch, Ihr Profil wird bestätigt!', 'Complimenti il ​​tuo profilo è verificato!', 'Parabéns seu perfil está verificado!', 'Поздравляем Ваш профиль проверен!', '¡Felicidades tu perfil está verificado!', 'Tebrikler, profiliniz doğrulandı!'),
(985, 'upload_docs', 'Upload documents', 'تحميل المستندات', 'Documenten uploaden', 'Télécharger des documents', 'Dokumente hochladen', 'Carica i documenti', 'Carregar documentos', 'Загрузить документы', 'Subir documentos', 'Belgeleri yükle'),
(986, 'select_verif_images', 'Please upload a photo with your passport / ID  & your distinct photo', 'يرجى تحميل صورة مع جواز سفرك / إد & أمب؛ صورتك المميزة', 'Upload een foto met uw paspoort / ID & amp; Jouw eigen foto', 'Veuillez télécharger une photo avec votre passeport / ID & amp; Votre photo distincte', 'Bitte laden Sie ein Foto mit Ihrem Pass / ID & amp; Ihr eigenes Foto', 'Carica una foto con il tuo passaporto / ID & amp; La tua foto distinta', 'Carregue uma foto com seu passaporte / ID & amp; Sua foto distinta', 'Пожалуйста, загрузите фотографию с вашим паспортом / ID и amp; Твоя отличная фотография', 'Cargue una foto con su pasaporte / ID & amp; Tu foto distinta', 'Lütfen pasaportunuzla bir fotoğraf yükleyin / kimliği ve amp; Farklı fotoğrafın'),
(987, 'passport_id', 'Passport / id card', 'جواز السفر / بطاقة الهوية', 'Paspoort / ID kaart', 'Passeport / carte didentité', 'Pass / ID-Karte', 'Passaporto / id carta', 'Passaporte / cartão de identificação', 'Паспорт / удостоверение личности', 'Pasaporte / tarjeta de identificación', 'Pasaport / kimlik kartı'),
(988, 'your_photo', 'Your photo', 'صورتك', 'Je foto', 'Ta photo', 'Dein Foto', 'La tua foto', 'Sua foto', 'Твое фото', 'Tu foto', 'Senin resmin'),
(989, 'please_select_passport_id', 'Please select your passport/id and photo!', 'يرجى تحديد جواز السفر / معرف والصورة!', 'Selecteer alstublieft uw paspoort / id en foto!', 'Sélectionnez votre passeport / id et photo!', 'Bitte wählen Sie Ihren Pass / id und Foto!', 'Seleziona il tuo passaporto / id e foto!', 'Selecione seu passaporte / id e foto!', 'Выберите свой паспорт / удостоверение личности и фото!', 'Por favor, seleccione su pasaporte / identificación y foto!', 'Lütfen pasaportunuzun / kimlik numaranızı ve fotoğrafınızı seçin!'),
(990, 'passport_id_invalid', 'The passport/id picture must be an image', 'يجب أن تكون صورة الجواز / الصورة صورة', 'De paspoort / id foto moet een afbeelding zijn', 'Limage passeport / id doit être une image', 'Das Pass / id Bild muss ein Bild sein', 'Limmagine del passaporto / id deve essere unimmagine', 'A imagem de passaporte / id deve ser uma imagem', 'Паспорт / идентификатор должен быть изображением', 'La imagen del pasaporte / id debe ser una imagen', 'Pasaport / id resmi bir resim olmalıdır'),
(991, 'user_picture_invalid', 'The user picture must be an image', 'يجب أن تكون صورة المستخدم صورة', 'De gebruikersfoto moet een afbeelding zijn', 'Limage utilisateur doit être une image', 'Das Benutzerbild muss ein Bild sein', 'Limmagine dellutente deve essere unimmagine', 'A imagem do usuário deve ser uma imagem', 'Изображение пользователя должно быть изображением', 'La imagen del usuario debe ser una imagen', 'Kullanıcı resmi bir resim olmalıdır'),
(992, 'verification_request_sent', 'Your request was successfully sent, in the very near future we will consider it!', 'تم إرسال طلبك بنجاح، في المستقبل القريب جدا سوف ننظر فيه!', 'Uw aanvraag is succesvol verzonden, in de nabije toekomst zullen we het overwegen!', 'Votre demande a été envoyée avec succès, dans un avenir très proche, nous lexaminerons!', 'Ihre Anfrage wurde erfolgreich gesendet, in naher Zukunft werden wir es betrachten!', 'La tua richiesta è stata inviata con successo, nel prossimo futuro lo considereremo!', 'Seu pedido foi enviado com sucesso, no futuro muito próximo, vamos considerá-lo!', 'Ваш запрос был успешно отправлен, в самом ближайшем будущем мы это рассмотрим!', 'Su solicitud fue enviada con éxito, en un futuro muy próximo lo consideraremos!', 'İsteğiniz başarıyla gönderildi, çok yakın bir zamanda bunu düşünüyoruz!'),
(993, 'shared', 'shared', 'مشترك', 'gedeelde', 'partagé', 'Geteilt', 'diviso', 'Compartilhado', 'Поделился', 'Compartido', 'Paylaşılan'),
(994, 'post_shared', 'Post was successfully added to your timeline!', 'تمت إضافة المشاركة بنجاح إلى المخطط الزمني!', 'Post is succesvol toegevoegd aan je tijdlijn!', 'Le message a été ajouté avec succès à votre calendrier!', 'Post wurde erfolgreich zu deinem Zeitplan hinzugefügt!', 'Lalberino è stato aggiunto con successo alla tua timeline!', 'O post foi adicionado com sucesso à sua linha de tempo!', 'Сообщение было успешно добавлено на ваш график!', '¡Se ha agregado el mensaje a tu línea de tiempo!', 'Mesaj, zaman çizelgesine başarıyla eklendi!'),
(995, 'important', 'Important!', 'مهم!', 'Belangrijk!', 'Important!', 'Wichtig!', 'Importante!', 'Importante!', 'Важно!', '¡Importante!', 'Önemli!'),
(996, 'unverify', 'Please note that if you change the username you will lose verification', 'يرجى ملاحظة أنه في حالة تغيير اسم المستخدم، فستفقد التحقق', 'Houd er rekening mee dat als u de gebruikersnaam wijzigt, u de verificatie verliest', 'Veuillez noter que si vous modifiez le nom dutilisateur, vous allez perdre la vérification', 'Bitte beachten Sie, dass Sie bei der Änderung des Benutzernamens die Bestätigung verlieren', 'Si prega di notare che se si modifica il nome utente perderà la verifica', 'Observe que se você alterar o nome de usuário, você perderá a verificação', 'Обратите внимание, что если вы измените имя пользователя, вы потеряете подтверждение', 'Tenga en cuenta que si cambia el nombre de usuario, perderá la verificación', 'Kullanıcı adını değiştirirseniz doğrulamayı kaybedeceğinizi lütfen unutmayın'),
(997, 'friend_privacy', 'Who can see my friends?', 'الذين يمكن أن نرى أصدقائي', 'Wie kan mijn vrienden zien', 'Qui peut voir mes amis', 'Wer kann meine Freunde sehen', 'Chi può vedere i miei amici', 'Quem pode ver meus amigos', 'Кто может видеть моих друзей', '¿Quién puede ver a mis amigos?', 'Arkadaşlarımı kim görebilir?'),
(998, 'added_group_admin', 'added you group admin', 'أضافك مشرف المجموعة', 'Toegevoegd je groep admin', 'Ajoute ton administrateur de groupe', 'Fügte Ihnen gruppe admin hinzu', NULL, 'Adicionou você administrador do grupo', 'Добавлен администратор группы', 'Agregó tu grupo de administración', 'Grup yöneticisi ekledi'),
(999, 'added_page_admin', 'added you page admin', 'أضافك مشرف الصفحة', 'Toegevoegd u pagina admin', 'A ajouté votre page admin', 'Fügte Ihnen die Seite admin hinzu', NULL, 'Adicionou você admin da página', 'Добавлено администратором страницы', 'Agregó tu página admin', 'Size sayfa admin ekledi'),
(1000, 'no_messages', 'No messages yet here.', 'لا توجد رسائل حتى الآن.', 'Nog geen berichten hier.', NULL, 'Noch keine Nachrichten.', 'Non ci sono ancora messaggi qui.', 'Ainda não há mensagens aqui.', 'Пока сообщений нет.', 'Aún no hay mensajes.', 'Henüz mesaj yok.'),
(1001, 'conversation_deleted', 'Conversation has been deleted!', 'تم حذف المحادثة!', 'Conversatie is verwijderd!', 'La conversation a été supprimée!', 'Konversation wurde gelöscht!', 'La conversazione è stata cancellata!', 'A conversa foi excluída!', 'Разговор удален!', '¡Se ha eliminado la conversación!', 'İleti dizisi silindi!'),
(1002, 'close', 'Close', 'قريب', 'Dichtbij', 'Fermer', 'Schließen', 'Vicino', 'Fechar', 'Закрыть', 'Cerca', 'Kapat'),
(1003, 'members', 'Members', 'أفراد', 'leden', 'Membres', 'Mitglieder', 'Utenti', 'Membros', 'члены', 'Miembros', 'Üyeler'),
(1004, 'exit_group', 'Exit group', 'خروج من المجموعة', 'Exitgroep', 'Groupe de sortie', 'Exit-Gruppe', 'Esci dal gruppo', 'Grupo de saída', 'Группа выхода', 'Salir del grupo', 'Grubundan çık'),
(1005, 'clear_history', 'Clear history', 'تاريخ واضح', 'Geschiedenis wissen', 'Histoire claire', 'Geschichte löschen', 'Cancellare la cronologia', 'Apagar o histórico', 'Удалить переписку', 'Borrar historial', 'Geçmişi temizle'),
(1006, 'group_members', 'Group members', 'أعضاء المجموعة', 'Groepsleden', 'Les membres du groupe', 'Gruppenmitglieder', 'Membri del gruppo', 'Membros do grupo', 'Участники группы', 'Miembros del grupo', 'Grup üyeleri'),
(1007, 'add_parts', 'Add participant', 'إضافة مشارك', 'Voeg deelnemer toe', 'Ajouter un participant', 'Teilnehmer hinzufügen', 'Aggiungi partecipante', 'Adicionar participante', 'Добавить участника', 'Añada participante', 'Katılımcı ekle'),
(1008, 'unreport', 'Cancel Report', 'إلغاء التقرير', 'Annuleren rapport', 'Annuler le rapport', 'Bericht abbrechen', 'Annulla rapporto', 'Cancelar relatório', 'Отменить отчет', 'Cancelar informe', 'Raporu İptal Et'),
(1009, 'report_user', 'Report this User', 'الإبلاغ عن هذا المستخدم', 'Rapporteer deze gebruiker', 'Signaler cet utilisateur', 'Diesen Nutzer melden', 'Segnala questo utente', 'Denunciar este usuário', 'Сообщить об этом пользователе', 'Reportar a este usuario', 'Bu kullanıcıyı rapor et'),
(1010, 'report_page', 'Report this Page', 'الإبلاغ عن هذه الصفحة', 'Meld deze pagina aan', 'Signaler cette page', 'Diese Seite melden', 'Segnala questa pagina', 'Informe esta página', 'Сообщить об этой странице', 'Informar sobre esta página', 'Bu sayfayı bildir'),
(1011, 'report_group', 'Report this Group', 'الإبلاغ عن هذه المجموعة', 'Meld deze groep aan', 'Signaler ce groupe', 'Diese Gruppe melden', 'Segnala questo gruppo', 'Denunciar este grupo', 'Сообщить об этой группе', 'Reportar este grupo', 'Bu Grubu Rapor Et'),
(1012, 'page_rated', 'You have already rated this page!', 'لقد قيمت هذه الصفحة من قبل!', 'U heeft deze pagina al beoordeeld!', 'Vous avez déjà noté cette page!', 'Sie haben diese Seite bereits bewertet!', 'Hai già valutato questa pagina!', 'Você já avaliou esta página!', 'Вы уже оценили эту страницу!', '¡Ya has calificado esta página!', 'Bu sayfaya zaten puan verdiniz!'),
(1013, 'rating', 'Rating', 'تقييم', 'Beoordeling', 'Évaluation', 'Bewertung', 'Valutazione', 'Avaliação', 'Рейтинг', 'Clasificación', 'Değerlendirme'),
(1014, 'reviews', 'Reviews', 'التعليقات', 'beoordelingen', 'Avis', 'Bewertungen', 'Recensioni', 'Rever', 'Отзывы', 'Comentarios', 'yorumlar'),
(1015, 'rate', 'Rate', 'معدل', 'tarief', 'Taux', 'Preis', 'Vota', 'Taxa', 'Ставка', 'Tarifa', 'oran'),
(1016, 'your_review', 'Write your review.', 'اكتب مراجعتك.', 'Schrijf je beoordeling.', 'Donnez votre avis.', 'Schreiben Sie eine Bewertung.', 'Scrivi la tua recensione.', 'Escreva sua revisão.', 'Напишите свой отзыв.', 'Escribe tu reseña.', 'Yorumunuzu yazın.'),
(1017, 'ad_saved', 'Your ad has been successfully saved!', 'تم حفظ إعلانك بنجاح!', 'Uw advertentie is succesvol opgeslagen!', 'Votre annonce a été enregistrée avec succès!', 'Ihre Anzeige wurde erfolgreich gespeichert!', 'Il tuo annuncio è stato salvato con successo!', 'Seu anúncio foi salvo com sucesso!', 'Ваше объявление успешно сохранено!', 'Tu anuncio se ha guardado correctamente.', 'Reklamınız başarıyla kaydedildi!'),
(1018, 'ad_added', 'Your ad has been successfully added!', 'تمت إضافة إعلانك بنجاح!', 'Uw advertentie is succesvol toegevoegd!', 'Votre annonce a été ajoutée avec succès!', 'Ihre Anzeige wurde erfolgreich hinzugefügt!', 'Il tuo annuncio è stato aggiunto con successo!', 'Seu anúncio foi adicionado com sucesso!', 'Ваше объявление было успешно добавлено!', 'Su anuncio se ha agregado correctamente.', 'Reklamınız başarıyla eklendi!'),
(1019, 'invalid_ad_picture', 'The ads picture must be an image!', 'يجب أن تكون صورة الإعلانات صورة!', 'De advertentie foto moet een afbeelding zijn!', NULL, 'Das Anzeigenbild muss ein Bild sein!', NULL, 'A imagem dos anúncios deve ser uma imagem!', 'Изображение объявления должно быть изображением!', '¡La imagen de los anuncios debe ser una imagen!', 'Reklam resimleri bir resim olmalıdır!'),
(1020, 'enter_valid_desc', 'Please enter a valid description!', 'الرجاء إدخال وصف صالح!', 'Vul alstublieft een geldige omschrijving in!', 'Entrez une description valable!', 'Bitte geben Sie eine gültige Beschreibung ein!', 'Inserisci una descrizione valida!', 'Digite uma descrição válida!', 'Введите действительное описание!', 'Por favor ingrese una descripción válida!', 'Lütfen geçerli bir açıklama girin!'),
(1021, 'enter_valid_titile', 'Please enter a valid title!', 'يرجى إدخال عنوان صالح!', 'Vul alstublieft een geldige titel in!', 'Entrez un titre valide!', 'Bitte geben Sie einen gültigen Titel ein!', 'Si prega di inserire un titolo valido!', 'Digite um título válido!', 'Введите действительный заголовок!', '¡Por favor ingrese un título válido!', 'Lütfen geçerli bir başlık girin!'),
(1022, 'enter_valid_url', 'Please enter a valid link!', 'الرجاء إدخال رابط صالح!', 'Vul alstublieft een geldige link in!', 'Veuillez entrer un lien valide!', 'Bitte geben Sie einen gültigen Link ein!', 'Inserisci un link valido!', 'Digite um link válido!', 'Пожалуйста, введите действующую ссылку!', 'Ingrese un enlace válido!', 'Lütfen geçerli bir bağlantı girin!'),
(1023, 'invalid_company_name', 'Please enter a valid company name!', 'الرجاء إدخال اسم شركة صالح!', 'Vul alstublieft een geldige bedrijfsnaam in!', NULL, 'Bitte geben Sie einen gültigen Firmennamen ein!', 'Inserisci un nome aziendale valido!', 'Digite um nome válido da empresa!', 'Укажите действительное название компании!', 'Introduzca un nombre de empresa válido!', 'Lütfen geçerli bir şirket adı girin!'),
(1024, 'mother', 'Mother', 'أم', 'Moeder', 'Mère', 'Mutter', 'Madre', 'Mãe', 'Мама', 'Madre', 'anne'),
(1025, 'father', 'Father', 'الآب', 'Vader', 'Père', 'Vater', 'Padre', 'Pai', 'Отец', 'Padre', 'baba'),
(1026, 'daughter', 'Daughter', 'ابنة', 'Dochter', 'Fille', 'Tochter', 'Figlia', 'Filha', 'Дочь', 'Hija', 'Kız evlat'),
(1027, 'son', 'Son', 'ابن', 'Zoon', 'Fils', 'Sohn', 'Figlio', 'Filho', 'Сын', 'Hijo', 'Oğul'),
(1028, 'sister', 'Sister', 'أخت', 'Zus', 'Sœur', 'Schwester', 'Sorella', 'Irmã', 'Сестра', 'Hermana', 'Kız kardeş'),
(1029, 'brother', 'Brother', 'شقيق', 'Broer', 'Frère', 'Bruder', 'Fratello', 'Irmão', 'Брат', 'Hermano', 'Erkek kardeş'),
(1030, 'auntie', 'Auntie', 'عمة', 'Tante', 'Tata', 'Tante', 'Auntie', 'Tia', 'тетушка', 'Tía', 'teyzeciğim'),
(1031, 'uncle', 'Uncle', 'اخو الام', 'Oom', 'Oncle', 'Onkel', 'Zio', 'Tio', 'Дядя', 'Tío', 'Amca dayı'),
(1032, 'niece', 'Niece', 'ابنة الاخ', 'Nicht', 'Nièce', 'Nichte', 'Nipote', 'Sobrinha', 'Племянница', 'Sobrina', 'Yeğen'),
(1033, 'nephew', 'Nephew', 'ابن أخ', 'Neef', 'Neveu', 'Neffe', 'Nipote', 'Sobrinho', 'Племянник', 'Sobrino', 'Erkek yeğen'),
(1034, 'cousin_female', 'Cousin (female)', 'ابن عم (أنثى)', 'Neef (vrouwelijk)', 'Cousine)', 'Cousine)', 'Cugina)', 'Prima)', 'Двоюродная сестра)', 'Prima)', 'Kuzenim (kadın)'),
(1035, 'cousin_male', 'Cousin (male)', 'ابن عم (ذكور)', 'Neef)', 'Cousin Male)', 'Cousin)', 'Cugino maschio)', 'Primo)', 'Двоюродный брат)', 'Primo varón)', 'Erkek kuzen)'),
(1036, 'grandmother', 'Grandmother', 'جدة', 'Grootmoeder', 'Grand-mère', 'Oma', 'Nonna', 'Avó', 'Бабушка', 'Abuela', 'büyükanne'),
(1037, 'grandfather', 'Grandfather', 'جد', 'Grootvader', 'Grand-père', 'Großvater', 'Nonno', 'Avô', 'Дед', 'Abuelo', 'Büyük baba'),
(1038, 'granddaughter', 'Granddaughter', 'حفيدة', 'Kleindochter', 'Petite fille', 'Enkelin', 'Nipotina', 'Neta', 'Внучка', 'Nieta', 'Kız torun'),
(1039, 'grandson', 'Grandson', 'حفيد', 'Kleinzoon', 'Petit fils', 'Enkel', 'Nipote', 'Neto', 'Внук', 'Nieto', 'Erkek torun'),
(1040, 'stepsister', 'Stepsister', 'مثل اختي', 'Stiefzuster', 'Demi-soeur', 'Stiefschwester', 'Sorellastra', 'Meia-irmã', 'Сводная сестра', 'Hermanastra', 'Üvey kızkardeş'),
(1041, 'stepbrother', 'Stepbrother', 'أخ غير شقيق', 'stiefbroeder', 'Beau-frère', 'Stiefbruder', 'Fratellastro', 'Meio-irmão', 'Сводный брат', 'Hermanastro', 'Üvey erkek kardeş'),
(1042, 'stepmother', 'Stepmother', 'زوجة الأب', 'Stiefmoeder', 'Stepmother', 'Stiefmutter', 'Matrigna', 'Madrasta', 'Мачеха', 'Madrastra', 'üvey anne'),
(1043, 'stepfather', 'Stepfather', 'زوج الأم', 'Stiefvader', 'Beau-père', 'Stiefvater', 'Patrigno', 'Padrasto', 'Отчим', 'Padrastro', 'üvey baba'),
(1044, 'stepdaughter', 'Stepdaughter', 'ربيبة', 'Stiefdochter', 'Belle fille', 'Stieftochter', 'Figliastra', 'Enteada', 'Падчерица', 'Hijastra', 'üvey kız'),
(1045, 'sister_in_law', 'Sister-in-law', 'أخت الزوج أو اخت الزوجة', 'Schoonzuster', 'Belle-soeur', 'Schwägerin', 'Cognata', 'Cunhada', 'Золовка', 'Cuñada', 'Baldız'),
(1046, 'brother_in_law', 'Brother-in-law', 'شقيق الزوج', 'Zwager', 'Beau-frère', 'Schwager', 'Cognato', 'Cunhado', 'Шурин', 'Cuñado', 'Kayınbirader'),
(1047, 'mother_in_law', 'Mother-in-law', 'حماة \" أم الزوج أو أم الزوجة', 'Schoonmoeder', 'Belle-mère', 'Schwiegermutter', 'Suocera', 'Sogra', 'Свекровь', 'Suegra', 'Kayınvalide'),
(1048, 'father_in_law', 'Father-in-law', 'ووالد بالتبنى', 'Schoonvader', 'Beau-père', 'Schwiegervater', 'Suocero', 'Sogro', 'Тесть', 'Suegro', 'Kayınpeder'),
(1049, 'daughter_in_law', 'Daughter-in-law', 'ابنة بالنسب', 'Schoondochter', 'Belle-fille', 'Schwiegertochter', 'Nuora', 'Nora', 'Невестка', 'Hijastra', 'Gelin'),
(1050, 'son_in_law', 'Son-in-law', 'ابنه قانونياً', 'Schoonzoon', 'Beau fils', 'Schwiegersohn', 'Genero', 'Genro', 'Зять', 'Yerno', 'Damat'),
(1051, 'sibling_gender_neutral', 'Sibling (gender neutral)', 'الأخوة (محايدة جنسانيا)', 'Broers en zussen (geslachtsneutraal)', 'Sibling (genre neutre)', 'Geschwister (geschlechtsneutral)', 'Fidanzamento (genere neutro)', 'Irmão (neutro em termos de gênero)', 'Сиблинг (гендерно нейтральный)', 'Hermano (neutral de género)', 'Kardeşlik (cinsiyete dayalı)'),
(1052, 'parent_gender_neutral', 'Parent (gender neutral)', 'الوالد (محايد جنسانيا)', 'Ouder (geslachtsneutraal)', 'Parent (genre neutre)', 'Elternteil (geschlechtsneutral)', 'Genitore (genere neutro)', 'Pais (neutro em termos de gênero)', 'Родитель (гендерно нейтральный)', 'Padre (neutral de género)', 'Ebeveyn (cinsiyete dayalı)'),
(1053, 'child_gender_neutral', 'Child (gender neutral)', 'الطفل (محايد جنسانيا)', 'Kind (geslachtsneutraal)', 'Enfant (genre neutre)', 'Kind (geschlechtsneutral)', 'Bambino (sesso neutro)', 'Criança (neutro em termos de gênero)', 'Ребенок (гендерно нейтральный)', 'Niño (neutral de género)', 'Çocuk (cinsiyete dayalı)'),
(1054, 'sibling_of_parent_gender_neutral', 'Sibling of Parent (gender neutral)', 'شقيق الوالد (محايد جنسانيا)', 'Broers en zussen van ouder (geslachtsneutraal)', 'Sibling of Parent (genre neutre)', 'Geschwister der Eltern (geschlechtsneutral)', 'Fratellanza del genitore (neutralità di genere)', 'Sibling of Parent (neutro em termos de gênero)', 'Братство родителей (гендерно нейтральный)', 'Hermano de padre (neutral de género)', 'Ebeveynin Kardeşliği (cinsiyete dayalı)'),
(1055, 'child_of_sibling_gender_neutral', 'Child of Sibling (gender neutral)', 'طفل الأخوة (محايد جنسانيا)', 'Kind van broer en zus (geslachtsneutraal)', 'Enfant de fratrie (genre neutre)', 'Kind des Geschwisters (geschlechtsneutral)', 'Bambino di fratelli (neutralità di genere)', 'Criança do irmão (neutro em termos de gênero)', 'Ребенок Сиблинга (гендерно нейтральный)', 'Hijo de hermano (neutral de género)', 'Kardeşlik çocuğu (cinsiyete dayalı tarafsız)'),
(1056, 'cousin_gender_neutral', 'Cousin (gender neutral)', 'ابن عم (محايدة جنسانيا)', 'Neef (geslachtsneutraal)', 'Cousin (genre neutre)', 'Cousin (geschlechtsneutral)', 'Cugino (neutralità di genere)', 'Primo (neutro em termos de gênero)', 'Кузен (гендерно нейтральный)', 'Primo (neutral de género)', 'Kuzenim (cinsiyete aykırı)'),
(1057, 'grandparent_gender_neutral', 'Grandparent (gender neutral)', 'الجد (محايد جنسانيا)', 'Grootouder (geslachtsneutraal)', 'Grandparent (genre neutre)', 'Großeltern (geschlechtsneutral)', 'Nonno (neutralità di genere)', 'Avós (neutro em termos de gênero)', 'Бабушка и дедушка (гендерный нейтраль)', 'Abuelo (neutral de género)', 'Büyükbaba (cinsiyet eşitliği yok)');
INSERT INTO `Wo_Langs` (`id`, `lang_key`, `english`, `arabic`, `dutch`, `french`, `german`, `italian`, `portuguese`, `russian`, `spanish`, `turkish`) VALUES
(1058, 'grandchild_gender_neutral', 'Grandchild (gender neutral)', 'حفيد (محايد جنسانيا)', 'Grootkind (geslachtsneutraal)', 'Petit-fils (genre neutre)', 'Enkelkind (geschlechtsneutral)', 'Nipote (neutralità di genere)', 'Neto (neutro em termos de gênero)', 'Внуки (гендерно нейтральные)', 'Nieto (neutral de género)', 'Torun (cinsiyete dayalı)'),
(1059, 'step_sibling_gender_neutral', 'Step-sibling (gender neutral)', 'أخوة الخطوة (محايدة جنسانيا)', 'Step-sibling (gender neutraal)', 'Échec-frère (genre neutre)', 'Schritt-Geschwister (geschlechtsneutral)', 'Step-sibling (gender neutral)', 'Irmão-irmão (neutro em termos de gênero)', 'Step-sibling (гендерно нейтральный)', 'Hermanastro (neutral de género)', 'Adım kardeş (cinsiyete dayalı)'),
(1060, 'step_parent_gender_neutral', 'Step-parent (gender neutral)', 'الخطوة الوالد (محايدة جنسانيا)', 'Step-parent (gender neutraal)', 'Step-parent (genre neutre)', 'Schritt-Elternteil (geschlechtsneutral)', 'Step-parent (neutralità di genere)', 'Etapa-pai (neutro em termos de gênero)', 'Пошаговый (гендерно нейтральный)', 'El padrastro (neutral de género)', 'Veli-ebeveyn (cinsiyete dayalı)'),
(1061, 'stepchild_gender_neutral', 'Stepchild (gender neutral)', 'ستيبشيلد (محايد جنسانيا)', 'Stepchild (gender neutraal)', 'Stepchild (genre neutre)', 'Stepchild (geschlechtsneutral)', 'Stepchild (genere neutro)', 'Stepchild (neutro em termos de gênero)', 'Stepchild (гендерно нейтральный)', 'Stepchild (neutral de género)', 'Üvey çocuk (cinsiyete aykırı)'),
(1062, 'sibling_in_law_gender_neutral', 'Sibling-in-law (gender neutral)', 'شقيق الزوج (محايد جنسانيا)', 'Sibling-in-law (gender neutraal)', 'Sage-frère (genre neutre)', 'Schwangerschaft (geschlechtsneutral)', 'Sibling-in-law (gender neutral)', 'Irmão-irmão (neutro em termos de gênero)', 'Сиблинг в законе (гендерно нейтральный)', 'Cuñados (neutral de género)', 'Kayın üstü (cinsiyete dayalı)'),
(1063, 'parent_in_law_gender_neutral', 'Parent-in-law (gender neutral)', 'الوالد (محايد جنسانيا)', 'Schoonmoeder (geslachtsneutraal)', 'Parent-en-loi (neutre pour le genre)', 'Schwiegertochter (geschlechtsneutral)', 'Genitore di sesso (neutralità di genere)', 'Sogro (neutro em termos de gênero)', 'Зять (гендерно нейтральный)', 'Suegro (neutral de género)', 'Kayınvalides (cinsiyet eşitli değil)'),
(1064, 'child_in_law_gender_neutral', 'Child-in-law (gender neutral)', 'صهر الطفل (محايد جنسانيا)', 'Schoonzoon (geslachtsneutraal)', 'Bien-être (genre neutre)', 'Schwiegervater (geschlechtsneutral)', 'Suono (neutrale di genere)', 'Nora (neutro em termos de gênero)', 'Тед (гендерно нейтральный)', 'Niño (s) (género neutral)', 'Kayın-kuşun (cinsiyet eşitli)'),
(1065, 'add_to_family', 'Add to family', 'إضافة إلى الأسرة', 'Voeg toe aan familie', 'Ajouter à la famille', 'Zu Familie hinzufügen', 'Aggiungi alla famiglia', 'Adicionar à família', 'Добавить в подборку', 'Añadir a la familia', 'Ailenize ekleyin'),
(1066, 'accept', 'Accept', 'قبول', 'Accepteren', 'Acceptez', 'Akzeptieren', 'Accettare', 'Aceitar', 'принимать', 'Aceptar', 'Kabul etmek'),
(1067, 'family_member', 'Family Member', 'عضو الأسرة', 'Familielid', 'Membre de la famille', 'Familienmitglied', 'Membro della famiglia', 'Membro da família', 'Член семьи', 'Miembro de la familia', 'Aile üyesi'),
(1068, 'family_members', 'Family members', 'أفراد الأسرة', 'Familieleden', 'Membres de la famille', 'Familienmitglieder', 'Membri della famiglia', 'Membros da família', 'Члены семьи', 'Miembros de la familia', 'Aile üyeleri'),
(1069, 'add_as', 'Add as', 'أضفه ك', 'Toevoegen als', 'Ajouter comme', 'Hinzufügen Als', 'Aggiungi come', 'Adicionar como', 'Добавить как', 'Agregar como', 'Olarak ekle'),
(1070, 'confirm_remove_family_member', 'Are you sure that you want to remove this member from your family?', 'هل تريد بالتأكيد إزالة هذا العضو من عائلتك؟', 'Weet u zeker dat u dit lid van uw familie wilt verwijderen?', 'Êtes-vous sûr de vouloir supprimer ce membre de votre famille?', 'Sind Sie sicher, dass Sie dieses Mitglied aus Ihrer Familie entfernen möchten?', 'Sei sicuro di voler rimuovere questo membro dalla tua famiglia?', 'Tem certeza de que deseja remover esse membro da sua família?', 'Вы уверены, что хотите удалить этого участника из своей семьи?', '¿Estás seguro de que deseas eliminar este miembro de tu familia?', 'Bu üyeyi ailenden kaldırmak istediğinizden emin misiniz?'),
(1071, 'family_member_added', 'New member was successfully added to your family list!', 'تمت إضافة عضو جديد بنجاح إلى قائمة عائلتك!', 'Nieuw lid is succesvol toegevoegd aan je familielijst!', 'Un nouveau membre a été ajouté avec succès à votre liste de famille!', 'Neues Mitglied wurde erfolgreich zu Ihrer Familienliste hinzugefügt!', 'Nuovo membro è stato aggiunto con successo alla tua lista di famiglia!', 'Novo membro foi adicionado com sucesso à sua lista de família!', 'Новый член был успешно добавлен в список ваших семей!', '¡El nuevo miembro se agregó a su lista de familia!', 'Yeni üye, aileniz listesine başarıyla eklendi!'),
(1072, 'request_sent', 'Your request was successfully sent!', 'تم إرسال طلبك بنجاح!', 'Uw verzoek is succesvol verzonden!', 'Votre demande a été envoyée avec succès!', 'Ihre Anfrage wurde erfolgreich gesendet!', 'La tua richiesta è stata inviata con successo!', 'Seu pedido foi enviado com sucesso!', 'Ваш запрос был успешно отправлен!', '¡Su solicitud ha sido enviada correctamente!', 'Talebiniz başarıyla gönderildi!'),
(1073, 'request_accepted', 'Accepted your request to be your @', 'قبلت طلبك ليكون الخاص بك @', 'Geaccepteerd uw verzoek om uw @', 'A accepté votre demande pour être votre @', 'Akzeptiert Ihre Anfrage zu Ihrem @', 'Accettato la tua richiesta per essere il tuo @', 'Aceitou seu pedido para ser seu @', 'Принял ваш запрос как ваш @', 'Aceptado su solicitud para ser su @', '@ Olmak isteğinizi kabul ettiniz'),
(1074, 'sent_u_request', 'Listed you as his @', 'المدرجة لك كما له @', 'Heb je gezien als zijn @', NULL, 'Listed Sie als seine @', 'Ti ha elencato come suo @', 'Listou você como seu @', 'Перечислил вас как его @', 'Listado como su @', 'Seni onun @'),
(1075, 'requests', 'Requests', 'طلبات', 'verzoeken', 'Demandes', 'Anfragen', 'richieste', 'solicitações de', 'Запросы', 'Peticiones', 'İstekler'),
(1076, 'no_requests_found', 'No requests found!', 'لم يتم العثور على أية طلبات!', 'Geen verzoeken gevonden!', 'Aucune demande trouvée!', 'Keine Anfragen gefunden!', 'Nessuna richiesta trovata!', 'Não foram encontrados pedidos!', 'Запросов не найдено!', 'No se han encontrado solicitudes!', 'İstek bulunamadı!'),
(1077, 'relation_with', 'In relations with ', 'في العلاقات مع', 'In relaties met', 'En relation avec', 'Im Zusammenhang mit', 'Nelle relazioni con', 'Em relação com', 'В отношениях с', 'En las relaciones con', 'Ile ilişkilerinde'),
(1078, 'married_to', 'Married to ', 'متزوج من', 'Getrouwd met', 'Marié à', 'Verheiratet mit', 'Sposato con', 'Casado com', 'В браке с', 'Casado con', 'Evli'),
(1079, 'engaged_to', 'Engaged to ', 'مخطوب ل', 'verloofd met', 'Fiancé à', 'verlobt mit', 'fidanzato con', 'noivo de', 'Помолвлены с', 'comprometido con', 'Etkileşim kurdu'),
(1080, 'relationship_status', 'Relationship Status ', 'الحالة الاجتماعية', 'Relatie status', 'Statut de la relation', 'Beziehungsstatus', 'stato delle relazioni', 'status de relacionamento', 'Семейное положение', 'estado civil', 'ilişki durumu'),
(1081, 'relationship_request', 'Relationship request ', 'طلب العلاقة', 'Verzoek om relatie', 'Demande de relation', 'Beziehungsanfrage', 'Richiesta di relazione', 'Pedido de relacionamento', 'Запрос на отношении', 'Solicitud de relación', 'Ilişki talebi'),
(1082, 'relhip_request_accepted', 'Accepted your request @ ', 'قبل طلبك @', 'Geaccepteerd uw aanvraag @', 'A accepté votre demande @', 'Akzeptiert Ihre Anfrage @', 'Accettato la tua richiesta @', 'Aceitou seu pedido @', 'Принял(а) ваш запрос @', 'Aceptado su solicitud @', 'İsteğiniz kabul edildi @'),
(1083, 'relation_rejected', 'rejected your relation request @', 'رفض طلب علاقتك @', 'Heeft uw relatieverzoek geweigerd @', 'Rejeté votre demande de relation @', 'Abgelehnt Ihre Beziehung Anfrage @', 'Ha respinto la tua richiesta di relazione @', 'Rejeitou sua solicitação de relacionamento @', 'Отклонил(a) ваш запрос отношения @', 'Rechazó su solicitud de relación @', 'Ilişki isteğini reddetti @'),
(1084, 'file_too_big', 'File size error: The file exceeds allowed the limit ({file_size}) and can not be uploaded.', 'خطأ في حجم الملف: يتجاوز الملف الحد المسموح به ({file_size}) ولا يمكن تحميله.', 'Bestandsgrootte fout: Het bestand overschrijdt de limiet toegestaan ​​({file_size}) en kan niet worden geüpload.', 'Erreur de taille de fichier: le fichier dépasse autorisé la limite ({image_fichier}) et ne peut pas être téléchargé.', 'Dateigrößenfehler: Die Datei überschreitet die Begrenzung ({file_size}) und kann nicht hochgeladen werden.', 'Errore di dimensione del file: il file supera il limite consentito ({file_size}) e non può essere caricato.', 'Erro de tamanho de arquivo: o arquivo excede permitido o limite ({file_size}) e não pode ser carregado.', 'Ошибка размера файла: файл превышает допустимый предел ({file_size}) и не может быть загружен.', 'Error de tamaño de archivo: El archivo excede el límite permitido ({file_size}) y no se puede cargar.', 'Dosya boyutu hatası: Dosya limiti aştı ({file_size}) ve yüklenemiyor.'),
(1085, 'file_not_supported', 'Unable to upload a file: This file type is not supported.', 'تعذر تحميل ملف: نوع الملف هذا غير متوافق.', 'Kan een bestand niet uploaden: dit bestandstype wordt niet ondersteund.', NULL, 'Kann eine Datei nicht hochladen: Dieser Dateityp wird nicht unterstützt.', 'Impossibile caricare un file: questo tipo di file non è supportato.', 'Não é possível carregar um arquivo: esse tipo de arquivo não é suportado.', 'Не удалось загрузить файл. Этот тип файла не поддерживается.', 'No se puede cargar un archivo: este tipo de archivo no es compatible.', 'Dosya yüklenemiyor: Bu dosya türü desteklenmiyor.'),
(1086, 'years_old', 'years old', 'سنوات', 'jaar oud', 'ans', 'Jahre alt', 'Anni', 'anos', 'лет', 'años', 'yaşında'),
(1087, 'find_friends_nearby', 'Find friends', 'البحث عن أصدقاء', 'Zoek vrienden', 'Retrouver des amis', 'Freunde finden', 'Trova amici', 'Encontrar amigos', 'Найти друзей', 'Encontrar amigos', 'Arkadaşları bul'),
(1088, 'location_dist', 'Location distance', 'مسافة الموقع', 'Locatie afstand', 'Distance demplacement', 'Standortabstand', 'Distanza della posizione', 'Distância de localização', 'Месторасположение', 'Ubicación distancia', 'Yer mesafesi'),
(1089, 'close_to_u', 'close to you', 'قريب منك', 'dicht bij jou', 'près de vous', 'nah bei dir', 'vicino a te', 'perto de você', 'близко к тебе', 'cerca de usted', 'sana yakın'),
(1090, 'find_friends', 'Find friends', 'البحث عن أصدقاء', 'Zoek vrienden', 'Trouver des amis', 'Freunde finden', 'Trova amici', 'Encontrar amigos', 'Найти друзей', 'Encontrar amigos', 'Arkadaşları bul'),
(1091, 'distance', 'distance', 'مسافه: بعد', 'afstand', 'distance', 'Entfernung', 'distanza', 'distância', 'расстояние', 'distancia', 'mesafe'),
(1092, 'distance_from_u', 'distance from you', 'المسافة منك', 'Afstand van jou', 'Distance de vous', 'Entfernung von Ihnen', 'Distanza da te', 'Distância de você', 'Расстояние от вас', 'Distancia de ti', 'Senden uzaklık'),
(1093, 'show_location', 'Show location', 'إظهار الموقع', 'Toon locatie', NULL, 'Lage anzeigen', 'Mostra la posizione', 'Mostrar localização', 'Показать на карте', 'Mostrar ubicación', 'Yeri göster'),
(1094, 'share_my_location', 'Share my location with public?', 'هل تريد مشاركة موقعي مع الجمهور؟', 'Deel mijn locatie met publiek?', 'Partagez mon emplacement avec le public?', 'Teilen Sie meinen Standort mit der Öffentlichkeit?', 'Condividi la mia posizione con il pubblico?', 'Compartilhe minha localização com o público?', 'Поделитесь своим местоположением с публикой?', 'Compartir mi ubicación con público?', 'Konumumu halkla paylaşmak mı istiyorsunuz?'),
(1095, 'enter_valid_title', 'Please enter a valid title', 'يرجى إدخال عنوان صالح', 'Vul alstublieft een geldige titel in', 'Entrez un titre valide', 'Bitte geben Sie einen gültigen Titel ein', 'Si prega di inserire un titolo valido', 'Insira um título válido', 'Введите действительное название', 'Ingrese un título válido', 'Lütfen geçerli bir başlık girin'),
(1096, 'pay_per_click', 'Pay Per Click (${{PRICE}})', 'الدفع لكل نقرة (${{PRICE}})', 'Betaal per klik (${{PRICE}})', 'Pay Per Click (${{PRICE}})', 'Pay Per Click (${{PRICE}})', 'Pay Per Click (${{PRICE}})', 'Pay Per Click (${{PRICE}})', 'Платить за клик (${{PRICE}})', 'Pago por clic (${{PRICE}})', 'Tıklama başına Öde (${{PRICE}})'),
(1097, 'pay_per_imprssion', 'Pay Per Impression (${{PRICE}})', 'الدفع لكل ظهور (${{PRICE}})', 'Betaal per indruk (${{PRICE}})', 'Pay per Impression (${{PRICE}})', 'Pay per Impression (${{PRICE}})', 'Pay Per Impression (${{PRICE}})', 'Pague por impressão (${{PRICE}})', 'Платить за показ (${{PRICE}})', 'Pago por impresión (${{PRICE}})', 'Gösterim Başına Ödeme (${{PRICE}})'),
(1098, 'top_up', 'Top up', 'فوق حتى', 'Top up', 'Remplir', 'Nachfüllen', 'Riempire', 'Completar', 'Пополнить', 'Completar', 'Ekleyin'),
(1099, 'balance_is_0', 'Your current wallet balance is: 0, please top up your wallet to continue.', 'رصيد المحفظة الحالي هو: 0، يرجى متابعة محفظتك للمتابعة.', 'Uw huidige portemonneebalans is: 0, vul uw portemonnee aan om door te gaan.', 'Votre solde de portefeuille actuel est: 0, veuillez compléter votre portefeuille pour continuer.', 'Ihre aktuelle Brieftasche Gleichgewicht ist: 0, bitte nach oben Ihre Brieftasche, um fortzufahren.', 'Il tuo saldo attuale del portafoglio è: 0, ti preghiamo di riempire il portafoglio per continuare.', 'Seu saldo de carteira atual é: 0, por favor, complete sua carteira para continuar.', 'Ваш текущий баланс кошелька: 0, пожалуйста, пополните свой кошелек, чтобы продолжить.', 'Su saldo de cartera actual es: 0, por favor, recargue su cartera para continuar.', 'Mevcut cüzdan bakiyeniz: 0, devam etmek için lütfen cüzdanınızı doldurun.'),
(1100, 'messages_delete_confirmation', 'Are you sure you want to delete this conversation?', 'هل تريد بالتأكيد حذف هذه المحادثة؟', 'Weet u zeker dat u dit gesprek wilt verwijderen?', 'Êtes-vous sûr de vouloir supprimer cette conversation?', 'Sind Sie sicher, dass Sie diese Konversation löschen möchten?', 'Sei sicuro di voler eliminare questa conversazione?', 'Tem certeza de que deseja excluir esta conversa?', 'Вы действительно хотите удалить этот разговор?', '¿Seguro que quieres eliminar esta conversación?', 'Bu sohbeti silmek istediğinizden emin misiniz?'),
(1101, 'currency', 'Currency', 'دقة', 'Valuta', 'Devise', 'Währung', 'Moneta', 'Moeda', 'валюта', 'Moneda', 'Para birimi'),
(1102, 'friends_stories', 'Friends Stories', 'قصص الأصدقاء', 'Vriendenverhalen', NULL, 'Freunde Geschichten', 'Storie di amici', 'Histórias de amigos', 'Истории друзей', 'Historias de amigos', 'Arkadaş Hikayeleri'),
(1103, 'no_messages_here_yet', 'No messages yet here.', 'لا توجد رسائل حتى الآن.', 'Nog geen berichten hier.', NULL, 'Noch keine Mitteilungen.', 'Non ci sono ancora messaggi qui.', 'Ainda não há mensagens aqui.', 'Пока сообщений нет.', 'Aún no hay mensajes.', 'Henüz mesaj yok.'),
(1104, 'conver_deleted', 'Conversation has been deleted.', 'تم حذف المحادثة.', 'Conversatie is verwijderd.', 'La conversation a été supprimée.', 'Konversation wurde gelöscht.', 'La conversazione è stata eliminata.', 'A conversa foi excluída.', 'Разговор удален.', 'Se ha eliminado la conversación.', 'Sohbet silindi.'),
(1105, 'group_name_limit', 'Group name must be 4/15 characters', 'يجب أن يكون اسم المجموعة 4/15 حرفا', 'De groepsnaam moet 4/15 karakters zijn', 'Le nom du groupe doit comporter 4/15 caractères', 'Der Gruppenname muss 4/15 Zeichen lang sein', 'Il nome del gruppo deve essere di 4/15 caratteri', 'O nome do grupo deve ser de 4/15 caracteres', 'Имя группы должно быть 4/15 символов', 'El nombre del grupo debe tener 4/15 caracteres', 'Grup adı 4/15 karakter olmalıdır'),
(1106, 'group_avatar_image', 'Group avatar must be an image', 'يجب أن تكون الصورة الرمزية للمجموعة صورة', 'Groep avatar moet een afbeelding zijn', NULL, 'Gruppen-Avatar muss ein Bild sein', NULL, 'O avatar do grupo deve ser uma imagem', 'Групповой аватар должен быть изображением', 'El avatar del grupo debe ser una imagen', 'Grup avatar bir resim olmalı'),
(1107, 'explore', 'Explore', 'إستكشاف', 'Onderzoeken', 'Explorer', 'Erforschen', 'Esplorare', 'Explorar', 'Исследовать', 'Explorar', 'Keşfetmek'),
(1108, 'format_image', 'File Format image', 'تنسيق ملف الصورة', 'Bestandsformaat afbeelding', 'Image au format du fichier', 'Dateiformat Bild', 'Immagine del formato file', 'Imagem do formato do arquivo', 'Изображение формата файла', 'Imagen de formato de archivo', 'Dosya Biçimi resmi'),
(1109, 'format_video', 'File Format video', 'تنسيق ملف الفيديو', 'Bestandsformaat video', 'Format de fichier vidéo', 'Dateiformatvideo', 'Formato file video', 'Vídeo do formato do arquivo', 'Формат файла видео', 'Formato de archivo video', 'Dosya Biçimi videosu'),
(1110, 'video', 'Video', 'فيديو', 'Video', 'Vidéo', 'Video', 'video', 'Vídeo', 'видео', 'Vídeo', 'Video'),
(1111, 'video_player', 'VideoPlayer', 'مشغل فديوهات', 'Video speler', 'Lecteur vidéo', 'Videoplayer', 'Lettore video', 'VideoPlayer', 'Видео проигрыватель', 'Reproductor de video', 'Video oynatıcı'),
(1112, 'no_file_chosen', 'No file chosen', 'لم تقم باختيار ملف', 'Geen bestand gekozen', 'Aucun fichier choisi', 'Keine Datei ausgewählt', 'Nessun file scelto', 'Nenhum arquivo selecionado', 'Файл не выбран', 'Ningún archivo elegido', 'Dosya seçili değil'),
(1113, 'choose_file', 'Choose File', 'اختر ملف', 'Kies bestand', 'Choisir le fichier', 'Datei wählen', 'Scegli il file', 'Escolher arquivo', 'Выберите файл', 'Elija el archivo', 'Dosya seçin'),
(1114, 'media', 'Media File', 'ملف وسائط', 'Media bestand', 'Fichier multimédia', 'Mediendatei', 'File multimediale', 'Arquivo de mídia', 'Файл мультимедиа', 'Archivo multimedia', 'Medya dosyası'),
(1115, 'select_valid_img_vid', 'Media file is invalid. Please select a valid image or video', 'ملف الوسائط غير صالح. الرجاء تحديد صورة أو فيديو صالحين', 'Mediabestand is ongeldig. Selecteer een geldige afbeelding of video', 'Le fichier multimédia est invalide. Veuillez sélectionner une image ou une vidéo valide', 'Mediendatei ist ungültig. Bitte wählen Sie ein gültiges Bild oder Video aus', 'Il file multimediale non è valido. Si prega di selezionare unimmagine o un video valido', 'O arquivo de mídia é inválido. Selecione uma imagem ou vídeo válido', 'Недопустимый файл мультимедиа. Выберите действительное изображение или видео', 'El archivo multimedia no es válido. Seleccione una imagen o video válido', 'Medya dosyası geçersiz. Lütfen geçerli bir resim veya video seçin'),
(1116, 'select_valid_img', 'Media file is invalid. Please select a valid image', 'ملف الوسائط غير صالح. الرجاء تحديد صورة صالحة', 'Mediabestand is ongeldig. Selecteer een geldige afbeelding', 'Le fichier multimédia est invalide. Veuillez sélectionner une image valide', 'Mediendatei ist ungültig. Bitte wählen Sie ein gültiges Bild', 'Il file multimediale non è valido. Si prega di selezionare unimmagine valida', 'O arquivo de mídia é inválido. Selecione uma imagem válida', 'Недопустимый файл мультимедиа. Выберите действительное изображение', 'El archivo multimedia no es válido. Seleccione una imagen válida', 'Medya dosyası geçersiz. Lütfen geçerli bir resim seçin'),
(1117, 'select_valid_vid', 'Media file is invalid. Please select a valid video', 'ملف الوسائط غير صالح. الرجاء تحديد فيديو صالح', 'Mediabestand is ongeldig. Selecteer een geldige video', 'Le fichier multimédia est invalide. Veuillez sélectionner une vidéo valide', 'Mediendatei ist ungültig. Bitte wählen Sie ein gültiges Video aus', 'Il file multimediale non è valido. Si prega di selezionare un video valido', 'O arquivo de mídia é inválido. Selecione um vídeo válido', 'Недопустимый файл мультимедиа. Выберите действительное видео', 'El archivo multimedia no es válido. Seleccione un video válido', 'Medya dosyası geçersiz. Lütfen geçerli bir video seçin'),
(1118, 'drop_img_here', 'Drop Image Here', 'إسقاط الصورة هنا', 'Zet hier een afbeelding neer', 'Déposer limage ici', 'Bild hier ablegen', 'Rilascia limmagine qui', 'Largue a imagem aqui', 'Отбросьте изображение здесь', 'Dejar caer la imagen aquí', 'Buraya Resim Aç'),
(1119, 'or', 'OR', 'أو', 'OF', 'OU', 'ODER', 'O', 'OU', 'ИЛИ', 'O', 'VEYA'),
(1120, 'browse_to_upload', 'Browse To Upload', 'تصفح لتحميل', 'Blader naar uploaden', 'Parcourir pour télécharger', 'Durchsuchen zum Hochladen', 'Sfoglia per caricare', 'Navegue para carregar', 'Просмотр загрузки', 'Buscar para cargar', 'Yüklemeye Gözat'),
(1121, 'pr_completion', 'Profile Completion', 'الملف الشخصي الانتهاء', 'Profiel voltooiing', 'Achèvement du profil', 'Profil Fertigstellung', 'Completamento del profilo', 'Conclusão do perfil', 'Завершение профиля', 'Terminación del perfil', 'Profil Tamamlama'),
(1122, 'ad_pr_picture', 'Add your profile picture', 'إضافة صورة ملفك الشخصي', 'Voeg je profielfoto toe', 'Ajouter votre photo de profil', 'Fügen Sie Ihr Profilbild hinzu', 'Aggiungi la tua immagine del profilo', 'Adicione sua foto de perfil', 'Добавьте свое фото профиля', 'Agrega tu foto de perfil', 'Profil resmini ekle'),
(1123, 'add_ur_name', 'Add your name', 'أضف اسمك', 'Voeg je naam toe', 'Ajoutez votre nom', 'Fügen Sie Ihren Namen hinzu', 'Aggiungi il tuo nome', 'Adicione seu nome', 'Добавьте свое имя', 'Agrega tu nombre', 'Adınızı ekleyin'),
(1124, 'ad_ur_workplace', 'Add your workplace', 'أضف مكان عملك', 'Voeg uw werkplek toe', 'Ajoutez votre lieu de travail', 'Fügen Sie Ihren Arbeitsplatz hinzu', 'Aggiungi il tuo posto di lavoro', 'Adicione seu local de trabalho', 'Добавьте свое рабочее место', 'Agregue su lugar de trabajo', 'İş yerinizi ekleyin'),
(1125, 'ad_ur_country', 'Add your country', 'أضف بلدك', 'Voeg uw land toe', 'Ajoutez votre pays', 'Fügen Sie Ihr Land hinzu', 'Aggiungi il tuo paese', 'Adicione seu país', 'Добавьте свою страну', 'Agrega tu país', 'Ülkenizi ekle'),
(1126, 'ad_ur_address', 'Add your address', 'أضف عنوانك', 'Voeg uw adres toe', 'Ajoutez votre adresse', 'Fügen Sie Ihre Adresse hinzu', 'Aggiungi il tuo indirizzo', 'Adicione seu endereço', 'Добавьте свой адрес', 'Agrega tu dirección', 'Adresinizi ekleyin'),
(1127, 'e_sent_msg', 'Someone Send me a message', 'شخص أرسل لي رسالة', 'Iemand Stuur mij een bericht', 'Quelquun Envoyez moi un message', 'Jemand Senden Sie mir eine Nachricht', 'Qualcuno Inviami un messaggio', 'Alguém Envie-me uma mensagem', 'Кто-нибудь Послать мне сообщение', 'Alguien me envía un mensaje', 'Birisi bana bir mesaj gönder'),
(1128, 'send_money', 'Send money', 'إرسال الأموال', 'Stuur geld', 'Envoyer de largent', 'Geld schicken', 'Inviare soldi', 'Enviar dinheiro', 'Отправлять деньги', 'Enviar dinero', 'Para göndermek'),
(1129, 'u_send_money', 'You can send money to your friends, acquaintances or anyone', 'يمكنك إرسال الأموال إلى أصدقائك، معارفك أو أي شخص', 'Je kunt geld sturen naar je vrienden, kennissen of wie dan ook', 'Vous pouvez envoyer de largent à vos amis, connaissances ou nimporte qui', 'Sie können Geld an Ihre Freunde, Bekannten oder irgendjemanden senden', 'Puoi inviare denaro ai tuoi amici, conoscenti o chiunque altro', 'Você pode enviar dinheiro para seus amigos, conhecidos ou qualquer um', 'Вы можете отправлять деньги своим друзьям, знакомым или кому-либо', 'Puede enviar dinero a sus amigos, conocidos o cualquier persona', 'Arkadaşlarınız, tanıdıklarınız veya herhangi birisine para gönderebilirsiniz.'),
(1130, 'available_balance', 'Available balance', 'الرصيد المتوفر', 'Beschikbaar saldo', 'Solde disponible', 'Verfügbares Guthaben', 'Saldo disponibile', 'Saldo disponível', 'Доступные средства', 'Saldo disponible', 'Kalan bakiye'),
(1131, 'send_to', 'Send To', 'ارسل إلى', 'Verzenden naar', 'Envoyer à', 'Senden an', 'Inviare a', 'Enviar para', 'Отправить', 'Enviar a', 'Gönderildi'),
(1132, 'search_name_or_email', 'Search for user name, e-mail', 'البحث عن اسم المستخدم، والبريد الإلكتروني', 'Zoeken naar gebruikersnaam, e-mail', 'Recherche de nom dutilisateur, e-mail', 'Suchen Sie nach Benutzername, E-Mail', 'Cerca nome utente, e-mail', 'Procure por nome de usuário, e-mail', 'Поиск имени пользователя, электронной почты', 'Buscar nombre de usuario, correo electrónico', 'Kullanıcı adını, e-postasını ara'),
(1133, 'money_sent_to', 'Your money was successfully sent to', 'تم إرسال أموالك بنجاح إلى', 'Uw geld is succesvol verzonden naar', 'Votre argent a été envoyé avec succès à', 'Ihr Geld wurde erfolgreich an gesendet', 'Il tuo denaro è stato inviato con successo', 'Seu dinheiro foi enviado com sucesso para', 'Ваши деньги были успешно отправлены', 'Su dinero fue enviado exitosamente a', 'Paranız başarıyla gönderildi'),
(1134, 'sent_you', 'Sent you', 'ارسلت لك', 'Stuurde je', 'Vous a envoyé', 'Hat dich geschickt', 'Ti ho inviato', 'Enviei a você', 'Отправлено Вам', 'Enviado', 'Seni gönderdi'),
(1135, 'amount_exceded', 'The amount exceded your current balance!', 'المبلغ المستحق رصيدك الحالي!', 'Het bedrag overschreed je huidige saldo!', 'Le montant a dépassé votre solde actuel!', 'Der Betrag übertraf Ihr aktuelles Guthaben!', 'Limporto ha superato il tuo saldo attuale!', 'O valor excedeu o seu saldo atual!', 'Сумма превысила ваш текущий баланс!', '¡La cantidad excedió su saldo actual!', 'Miktar, şu anki bakiyenizi aştı!'),
(1136, 'custom_thumbnail', 'Custom Thumbnail', 'صورة مصغرة مخصصة', 'Aangepaste miniatuur', 'Miniature personnalisée', 'Benutzerdefiniertes Miniaturbild', 'Miniatura personalizzata', 'Miniatura personalizada', 'Пользовательская миниатюра', 'Miniatura personalizada', 'Özel Küçük Boy'),
(1137, 'cntc_limit_reached', 'You have reached your limit of {{CNTC_LIMIT}} friends!', 'لقد بلغت الحد المسموح به لعدد {{CNTC_LIMIT}} من الأصدقاء!', 'U heeft uw limiet van {{CNTC_LIMIT}} vrienden bereikt!', 'Vous avez atteint la limite de vos amis {{CNTC_LIMIT}}!', 'Du hast dein Limit von {{CNTC_LIMIT}} Freunden erreicht!', 'Hai raggiunto il limite di {{CNTC_LIMIT}} amici!', 'Você atingiu seu limite de amigos {{CNTC_LIMIT}}!', 'Вы достигли своего предела {{CNTC_LIMIT}} друзей!', '¡Has alcanzado el límite de {{CNTC_LIMIT}} amigos!', '{{CNTC_LIMIT}} arkadaşınızla ilgili sınırınıza ulaştınız!'),
(1150, 'people', 'People', 'اشخاص', 'Mensen', 'Gens', 'Menschen', 'Persone', 'Ludzie', 'люди', 'Gente', 'İnsanlar'),
(1151, 'nature', 'Nature', 'طبيعة', 'Natuur', 'La nature', 'Natur', 'Natura', 'Natura', 'Природа', 'Naturaleza', 'Doğa'),
(1152, 'share_to', 'Share to', 'مشاركة ل', 'Delen naar', 'Partager à', 'Teilen mit', 'Condividere a', 'Compartilhar no', 'Поделиться с', 'Compartir a', 'Ile paylaş'),
(1153, 'timeline', 'Timeline', 'الجدول الزمني', 'Tijdlijn', 'Chronologie', 'Zeitleiste', 'Sequenza temporale', 'Perfil', 'График', 'Cronología', 'Zaman çizelgesi'),
(1154, 'pinterest', 'Pinterest', 'موقع Pinterest', 'Pinterest', 'Pinterest', 'Pinterest', 'Pinterest', 'Pinterest', 'Pinterest', 'Pinterest', 'pinterest'),
(1155, 'group', 'Group', 'مجموعة', 'Groep', 'Groupe', 'Gruppe', 'Gruppo', 'Grupa', 'группа', 'Grupo', 'grup'),
(1156, 'pro_members', 'Pro Members', 'الأعضاء المحترفون', 'Pro-leden', 'Membres Pro', 'Pro Mitglieder', 'Membri Pro', 'Usuários Pro', 'Пользователи', 'Miembros Pro', 'Profesyonel Üyeler'),
(1157, 'copyrights', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}', '© {date} {site_name}'),
(1158, 'popular_posts', 'Popular Posts', 'منشورات شائعة', 'populaire posts', 'Messages populaires', 'Beliebte Beiträge', 'Post popolari', 'popularne posty', 'популярные посты', 'entradas populares', 'popüler gönderiler'),
(1159, 'duration', 'Duration', 'المدة الزمنية', 'Looptijd', 'Durée', 'Dauer', 'Durata', 'Trwanie', 'продолжительность', 'Duración', 'süre'),
(1160, 'pro_feature_control_profile', 'Pro features give you complete control over your profile.', 'تمنحك الميزات الاحترافية تحكمًا كاملاً في ملفك الشخصي.', 'Pro-functies geven u volledige controle over uw profiel.', 'Les fonctionnalités Pro vous donnent un contrôle total sur votre profil.', 'Pro-Funktionen geben Ihnen die vollständige Kontrolle über Ihr Profil.', 'Le funzionalità Pro ti danno il controllo completo sul tuo profilo.', 'Funkcje Pro zapewniają pełną kontrolę nad Twoim profilem.', 'Функции Pro дают вам полный контроль над вашим профилем.', 'Las funciones Pro le brindan un control total sobre su perfil.', 'Pro özellikleri profilinizde tam kontrol sağlar.'),
(1161, 'why_choose_pro', 'Why Choose PRO?', 'لماذا اخترت للمحترفين؟', 'Waarom kiezen voor PRO?', 'Pourquoi choisir PRO?', 'Warum wählen Sie PRO?', 'Perché scegliere PRO?', 'Dlaczego warto wybrać PRO?', 'Почему выбирают PRO?', '¿Por qué elegir PRO?', 'PRO Neden Tercih Edilir?'),
(1162, 'whatsapp', 'WhatsApp', 'ال WhatsApp', 'WhatsApp', 'WhatsApp', 'WhatsApp', 'WhatsApp', 'Whatsapp', 'WhatsApp', 'WhatsApp', 'Naber'),
(1163, 'whatsapp', 'WhatsApp', 'ال WhatsApp', 'WhatsApp', 'WhatsApp', 'WhatsApp', 'WhatsApp', 'Whatsapp', 'WhatsApp', 'WhatsApp', 'Naber'),
(1164, 'post_login_requriement_dislike', 'Please log in to like, dislike, share and comment!', 'الرجاء تسجيل الدخول لإبداء الإعجاب ، وعدم الإعجاب ، والمشاركة والتعليق!', 'Meld u aan om leuk te vinden, niet leuk te vinden, te delen en te reageren!', 'Veuillez vous connecter pour aimer, ne pas aimer, partager et commenter!', 'Bitte einloggen um zu mögen, nicht mögen, teilen und kommentieren!', 'Effettua il login per piacere, non mi piace, condividi e commenta!', 'Por favor, faça o login para curtir, não gostar, compartilhar e comentar!', 'Пожалуйста, войдите в систему, чтобы не любить, делиться и комментировать!', 'Por favor inicie sesión para gustar, no me gusta, compartir y comentar!', 'Lütfen beğenmek, beğenmemek, paylaşmak ve yorum yapmak için giriş yapın!'),
(1165, 'post_login_requriement_none', 'Please log in to like, share and comment!', 'يرجى تسجيل الدخول لإبداء الإعجاب والمشاركة والتعليق!', 'Log in om leuk te vinden, delen en reageren!', 'Veuillez vous connecter pour aimer, partager et commenter!', 'Bitte einloggen um zu liken, teilen und kommentieren!', 'Effettua il login per piacere, condividere e commentare!', 'Por favor, faça o login para curtir, compartilhar e comentar!', 'Войдите, чтобы добавить, поделиться и прокомментировать!', 'Por favor inicie sesión para gustar, compartir y comentar!', 'Lütfen beğenmek, paylaşmak ve yorum yapmak için giriş yapın!'),
(1166, 'e_disliked_my_posts', 'Someone disliked my posts', 'شخص لم يعجبني مشاركاتي', 'Iemand vond mijn berichten niet leuk', NULL, 'Jemand hat meine Beiträge nicht gemocht', 'A qualcuno non sono piaciuti i miei post', 'Alguém não gostou de minhas postagens', 'Кто-то не любил мои сообщения', 'A alguien no le gustó mis publicaciones', 'Birisi yayınlarımı beğenmedi'),
(1167, 'edit_ads', 'Edit ads', 'تحرير الإعلانات', 'Bewerk advertenties', 'Modifier les annonces', 'Anzeigen bearbeiten', 'Modifica annunci', 'Editar anúncios', 'Редактировать объявления', 'Editar anuncios', 'Reklamları düzenle'),
(1168, 'manage_ads', 'Manage ads', 'إدارة الإعلانات', 'Advertenties beheren', 'Gerer annonces', 'Anzeigen verwalten', 'Gestisci annunci', 'Gerenciar anúncios', 'Управление объявлениями', 'Administrar anuncios', 'Reklamları yönet'),
(1169, 'create_new_ads', 'Create new ad', 'إنشاء إعلان جديد', 'Maak een nieuwe advertentie', 'Créer une nouvelle annonce', 'Erstellen Sie eine neue Anzeige', 'Crea un nuovo annuncio', 'Crie um novo anúncio', 'Создать новое объявление', 'Crear nuevo anuncio', 'Yeni reklam oluştur'),
(1170, 'create_events', 'Craete new event', 'حدث جديد Craete', 'Craete nieuw evenement', 'Craete nouvel événement', 'Craete neues Ereignis', 'Craete nuovo evento', 'Novo evento Craete', 'Новое событие Craete', 'Nuevo evento de Craete', 'Craete yeni etkinlik'),
(1171, 'edit_event', 'Edit event', 'تحرير الحدث', 'Gebeurtenis bewerken', 'Modifier l&#039;événement', 'Veranstaltung bearbeiten', 'Modifica evento', 'Editar evento', 'Изменить событие', 'Editar evento', 'Etkinliği düzenle'),
(1172, 'event_going', 'Events Going', 'أحداث الذهاب', 'Evenementen gaan', 'Evénements', 'Veranstaltungen gehen', 'Eventi in corso', 'Eventos indo', 'События', 'Eventos en marcha', 'Olaylar Gidiyor'),
(1173, 'event_intersted', 'Events Interested', 'الأحداث المهتمة', 'Evenementen Geïnteresseerd', 'Événements intéressés', 'Veranstaltungen interessiert', 'Eventi interessati', 'Eventos Interessados', 'События', 'Eventos Interesados', 'İlgi Alanları'),
(1174, 'event_invited', 'Invited', 'دعوة', 'Uitgenodigd', 'Invité', 'Eingeladen', 'Invitato', 'Convidamos', 'приглашенный', 'Invitado', 'Davetli'),
(1175, 'events_past', 'Past Events', 'الأحداث الماضية', 'Vorige evenementen', 'Événements passés', 'Vergangene Ereignisse', 'Eventi passati', 'Eventos passados', 'Прошедшие события', 'Eventos pasados', 'Geçmiş Etkinlikler'),
(1176, 'events_upcoming', 'Upcoming Events', 'الأحداث القادمة', 'aankomende evenementen', 'évènements à venir', 'Kommende Veranstaltungen', 'Prossimi eventi', 'próximos eventos', 'Предстоящие События', 'Próximos Eventos', 'Yaklaşan Etkinlikler'),
(1177, 'crop_your_avatar', 'Crop your avatar', 'اقتصاص الصورة الرمزية الخاصة بك', 'Snijd je avatar bij', 'Recadrez votre avatar', 'Beschneide deinen Avatar', 'Ritaglia il tuo avatar', 'Recorte seu avatar', 'Обрезать аватар', 'Recorta tu avatar', 'Avatarını kırp'),
(1178, 'cookie_message', 'This website uses cookies to ensure you get the best experience on our website.', 'يستخدم موقع الويب هذا ملفات تعريف الارتباط لضمان حصولك على أفضل تجربة على موقعنا.', 'Deze website maakt gebruik van cookies om ervoor te zorgen dat u de beste ervaring op onze website krijgt.', 'Ce site utilise des cookies pour vous assurer la meilleure expérience sur notre site.', 'Diese Website verwendet Cookies, um sicherzustellen, dass Sie die beste Erfahrung auf unserer Website erhalten.', 'Questo sito Web utilizza i cookie per assicurarti di ottenere la migliore esperienza sul nostro sito web.', 'Nas próximas notificações SEMPRE clique em Permitir. ( Recomendado )', 'На этом веб-сайте используются файлы cookie, чтобы вы могли получить лучший опыт на нашем веб-сайте.', 'Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestro sitio web.', 'Bu web sitesi, web sitemizde en iyi deneyimi yaşamanızı sağlamak için çerezleri kullanır.'),
(1179, 'cookie_dismiss', 'Got It!', 'فهمتك!', 'Begrepen!', 'Je l&#039;ai!', 'Ich habs!', 'Fatto!', 'Ok, entendi!', 'Понял!', '¡Lo tengo!', 'Anladım!'),
(1180, 'cookie_link', 'Learn More', 'أعرف أكثر', 'Kom meer te weten', 'Apprendre encore plus', 'Erfahren Sie mehr', 'Per saperne di più', 'Saber mais', 'Выучить больше', 'Aprende más', 'Daha fazla bilgi edin'),
(1181, 'terms_accept', 'Please agree to the Terms of use & Privacy Policy', 'يرجى الموافقة على شروط الاستخدام وسياسة الخصوصية', 'Ga akkoord met de gebruiksvoorwaarden en het privacybeleid', 'Veuillez accepter les conditions d&#039;utilisation et la politique de confidentialité', 'Bitte stimme den Nutzungsbedingungen und Datenschutzrichtlinien zu', 'Si prega di accettare i Termini d&#039;uso e l&#039;informativa sulla privacy', 'Por favor, aceite os Termos de Uso e Política de Privacidade', 'Пожалуйста, соглашайтесь с Условиями использования и Политикой конфиденциальности', 'Acepta los Términos de uso y la Política de privacidad', 'Lütfen Kullanım Koşulları ve Gizlilik Politikasını kabul edin'),
(1182, 'good_morning', 'Good morning', 'صباح الخير', 'Goedemorgen', 'Bonjour', 'Guten Morgen', 'Buongiorno', 'Bom Dia, espero que seu dia seja maravilhoso', 'Доброе утро', 'Buenos días', 'Günaydın'),
(1183, 'good_afternoon', 'Good afternoon', 'طاب مسائك', 'Goedenmiddag', 'bonne après-midi', 'guten Tag', 'Buon pomeriggio', 'Boa tarde, mostre para sua região algo relevante ou interessante', 'Добрый день', 'Buenas tardes', 'Tünaydın'),
(1184, 'good_evening', 'Good evening', 'مساء الخير', 'Goedenavond', 'Bonsoir', 'Guten Abend', 'Buonasera', 'Boa noite, durma bem', 'Добрый вечер', 'Buenas tardes', 'İyi akşamlar'),
(1185, 'create_ads', 'Create advertisement', 'إنشاء الإعلان', 'Maak advertentie', 'Créer une publicité', 'Erstellen Sie Werbung', 'Crea pubblicità', 'Criar anúncio', 'Создать рекламу', 'Crear publicidad', 'Reklam oluştur'),
(1186, 'find_friends_nearby', 'Find friends', 'البحث عن أصدقاء', 'Zoek vrienden', 'Retrouver des amis', 'Freunde finden', 'Trova amici', 'Encontrar amigos', 'Найти друзей', 'Encontrar amigos', 'Arkadaşları bul');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Likes`
--

CREATE TABLE `Wo_Likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `post_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Likes`
--

INSERT INTO `Wo_Likes` (`id`, `user_id`, `post_id`) VALUES
(121, 1, 263),
(123, 1, 270),
(126, 1, 279),
(135, 1, 295),
(141, 1, 321),
(142, 1, 335),
(145, 85, 356),
(146, 85, 360),
(147, 85, 348),
(148, 1, 363),
(159, 1, 366),
(165, 102, 369),
(167, 104, 366),
(168, 104, 356),
(169, 104, 353),
(175, 102, 408),
(176, 1, 406),
(178, 102, 410),
(179, 1, 408),
(180, 1, 356),
(181, 85, 410);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Messages`
--

CREATE TABLE `Wo_Messages` (
  `id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `to_id` int(11) NOT NULL DEFAULT 0,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media` varchar(255) NOT NULL DEFAULT '',
  `mediaFileName` varchar(200) NOT NULL DEFAULT '',
  `mediaFileNames` varchar(200) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0,
  `seen` int(11) NOT NULL DEFAULT 0,
  `deleted_one` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_two` enum('0','1') NOT NULL DEFAULT '0',
  `sent_push` int(11) NOT NULL DEFAULT 0,
  `notification_id` varchar(50) NOT NULL DEFAULT '',
  `type_two` varchar(32) NOT NULL DEFAULT '',
  `stickers` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Messages`
--

INSERT INTO `Wo_Messages` (`id`, `from_id`, `group_id`, `to_id`, `text`, `media`, `mediaFileName`, `mediaFileNames`, `time`, `seen`, `deleted_one`, `deleted_two`, `sent_push`, `notification_id`, `type_two`, `stickers`) VALUES
(18, 1, 0, 33, 'Oiii Daniela seja bem vinda!', '', '', '', 1538966604, 0, '0', '0', 1, '', '', NULL),
(19, 1, 0, 33, '', 'upload/photos/2018/10/y1BGxU7Pp2jpvI6k2uXX_08_4c68eec352e89bec774a80d6fa96d332_image.jpg', '20181006_141038.jpg', '', 1538967064, 0, '0', '0', 1, '', '', NULL),
(20, 1, 0, 33, '', 'upload/photos/2018/10/bV2DzeYMvg4mwP8HLt4x_08_441e35a7e972b63317d2add3f13a361c_image.jpg', '20181006_141038.jpg', '', 1538967081, 0, '0', '0', 1, '', '', NULL),
(37, 1, 0, 33, '.', '', '', '', 1539440864, 0, '0', '0', 1, '', '', NULL),
(45, 1, 1, 0, 'eqd', '', '', '', 1539750414, 0, '0', '0', 1, '', '', NULL),
(46, 1, 1, 0, 'dq', '', '', '', 1539750453, 0, '0', '0', 1, '', '', NULL),
(50, 1, 1, 0, 'c', '', '', '', 1539751478, 0, '1', '0', 1, '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_MovieCommentReplies`
--

CREATE TABLE `Wo_MovieCommentReplies` (
  `id` int(11) NOT NULL,
  `comm_id` int(11) NOT NULL DEFAULT 0,
  `movie_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `text` text DEFAULT NULL,
  `likes` int(11) NOT NULL DEFAULT 0,
  `posted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_MovieComments`
--

CREATE TABLE `Wo_MovieComments` (
  `id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Movies`
--

CREATE TABLE `Wo_Movies` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `genre` varchar(50) NOT NULL DEFAULT '',
  `stars` varchar(300) NOT NULL DEFAULT '',
  `producer` varchar(100) NOT NULL DEFAULT '',
  `country` varchar(50) NOT NULL DEFAULT '',
  `release` year(4) DEFAULT NULL,
  `quality` varchar(10) DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT 0,
  `description` text DEFAULT NULL,
  `cover` varchar(500) NOT NULL DEFAULT 'upload/photos/d-film.jpg',
  `source` varchar(1000) NOT NULL DEFAULT '',
  `iframe` varchar(1000) NOT NULL DEFAULT '',
  `video` varchar(3000) NOT NULL DEFAULT '',
  `views` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Notifications`
--

CREATE TABLE `Wo_Notifications` (
  `id` int(255) NOT NULL,
  `notifier_id` int(11) NOT NULL DEFAULT 0,
  `recipient_id` int(11) NOT NULL DEFAULT 0,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `page_id` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `event_id` int(11) NOT NULL DEFAULT 0,
  `thread_id` int(11) NOT NULL DEFAULT 0,
  `seen_pop` int(11) NOT NULL DEFAULT 0,
  `type` varchar(255) NOT NULL DEFAULT '',
  `type2` varchar(32) NOT NULL DEFAULT '',
  `text` text DEFAULT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `full_link` varchar(1000) NOT NULL DEFAULT '',
  `seen` int(11) NOT NULL DEFAULT 0,
  `sent_push` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Notifications`
--

INSERT INTO `Wo_Notifications` (`id`, `notifier_id`, `recipient_id`, `post_id`, `page_id`, `group_id`, `event_id`, `thread_id`, `seen_pop`, `type`, `type2`, `text`, `url`, `full_link`, `seen`, `sent_push`, `time`) VALUES
(82, 1, 33, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Redelive', '', 0, 1, 1538966561),
(154, 37, 36, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=sacjamm', '', 0, 1, 1539338793),
(155, 37, 35, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=sacjamm', '', 0, 1, 1539338794),
(157, 37, 33, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=sacjamm', '', 0, 1, 1539338796),
(228, 1, 46, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Redelive', '', 0, 1, 1539731397),
(232, 1, 33, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Redelive', '', 0, 1, 1539748495),
(551, 85, 82, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=marcosmessias', '', 0, 1, 1540945084),
(599, 102, 1, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=testeteste', '', 1542760477, 1, 1542754596),
(600, 102, 1, 369, 0, 0, 0, 0, 0, 'liked_post', '', 'Test..', 'index.php?link1=post&id=369', '', 1542760477, 1, 1542754625),
(601, 103, 1, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=0b5161d4a', '', 1542781567, 1, 1542777226),
(604, 104, 1, 384, 0, 0, 0, 0, 1542781894, 'wondered_post', '', '', 'index.php?link1=post&id=384', '', 1542781942, 1, 1542781893),
(605, 104, 1, 369, 0, 0, 0, 0, 1542781900, 'wondered_post', '', 'Test..', 'index.php?link1=post&id=369', '', 1542781942, 1, 1542781897),
(607, 104, 1, 356, 0, 0, 0, 0, 1542781906, 'liked_post', 'post_image', '', 'index.php?link1=post&id=356', '', 1542781942, 1, 1542781905),
(608, 104, 1, 355, 0, 0, 0, 0, 1542781912, 'wondered_post', '', 'A..', 'index.php?link1=post&id=355', '', 1542781942, 1, 1542781910),
(609, 104, 1, 353, 0, 0, 0, 0, 1542781953, 'liked_post', '', 'ww..', 'index.php?link1=post&id=353', '', 1542782181, 1, 1542781951),
(610, 104, 1, 354, 0, 0, 0, 0, 1542781953, 'wondered_post', '', 'Testando..', 'index.php?link1=post&id=354', '', 1542782181, 1, 1542781952),
(621, 104, 1, 394, 0, 0, 0, 0, 1542808311, 'comment', 'post_image', '', 'index.php?link1=post&id=394&ref=396', '', 1542852128, 1, 1542808308),
(622, 104, 1, 394, 0, 0, 0, 0, 1542808311, 'wondered_post', 'post_image', '', 'index.php?link1=post&id=394', '', 1542852128, 1, 1542808309),
(630, 107, 103, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=dbnascimento', '', 0, 1, 1542818821),
(631, 107, 105, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=dbnascimento', '', 0, 1, 1542818821),
(632, 107, 98, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=dbnascimento', '', 1542865578, 1, 1542818821),
(633, 107, 1, 0, 0, 0, 0, 0, 1542818825, 'following', '', '', 'index.php?link1=timeline&u=dbnascimento', '', 1542852128, 1, 1542818821),
(634, 108, 105, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Ezequiel', '', 0, 1, 1542838665),
(635, 108, 1, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Ezequiel', '', 1542852128, 1, 1542838665),
(636, 108, 98, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Ezequiel', '', 1542865578, 1, 1542838666),
(637, 108, 103, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Ezequiel', '', 0, 1, 1542838666),
(643, 1, 108, 406, 0, 0, 0, 0, 0, 'liked_post', 'post_cover', '', 'index.php?link1=post&id=406', '', 1543064452, 1, 1543021981),
(646, 1, 102, 408, 0, 0, 0, 0, 1543098106, 'comment', '', 'Boa noite..', 'index.php?link1=post&id=408&ref=404', '', 0, 1, 1543098052),
(648, 1, 102, 408, 0, 0, 0, 0, 0, 'liked_post', '', 'Boa noite..', 'index.php?link1=post&id=408', '', 0, 1, 1543109670),
(649, 1, 104, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Redelive', '', 0, 1, 1543109790),
(650, 1, 104, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Redelive', '', 0, 1, 1543109793),
(651, 1, 85, 0, 0, 0, 0, 0, 0, 'following', '', '', 'index.php?link1=timeline&u=Redelive', '', 1543152677, 1, 1543109845),
(652, 85, 1, 410, 0, 0, 0, 0, 0, 'liked_post', 'post_image', '', 'index.php?link1=post&id=410', '', 0, 1, 1543152854);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_PageAdmins`
--

CREATE TABLE `Wo_PageAdmins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `page_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_PageRating`
--

CREATE TABLE `Wo_PageRating` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `page_id` int(11) NOT NULL DEFAULT 0,
  `valuation` int(11) DEFAULT 0,
  `review` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Pages`
--

CREATE TABLE `Wo_Pages` (
  `page_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `page_name` varchar(32) NOT NULL DEFAULT '',
  `page_title` varchar(32) NOT NULL DEFAULT '',
  `page_description` varchar(1000) NOT NULL DEFAULT '',
  `avatar` varchar(255) NOT NULL DEFAULT 'upload/photos/d-page.jpg',
  `cover` varchar(255) NOT NULL DEFAULT 'upload/photos/d-cover.jpg',
  `page_category` int(11) NOT NULL DEFAULT 1,
  `website` varchar(255) NOT NULL DEFAULT '',
  `facebook` varchar(32) NOT NULL DEFAULT '',
  `google` varchar(32) NOT NULL DEFAULT '',
  `vk` varchar(32) NOT NULL DEFAULT '',
  `twitter` varchar(32) NOT NULL DEFAULT '',
  `linkedin` varchar(32) NOT NULL DEFAULT '',
  `company` varchar(32) NOT NULL DEFAULT '',
  `phone` varchar(32) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `call_action_type` int(11) NOT NULL DEFAULT 0,
  `call_action_type_url` varchar(255) NOT NULL DEFAULT '',
  `background_image` varchar(200) NOT NULL DEFAULT '',
  `background_image_status` int(11) NOT NULL DEFAULT 0,
  `instgram` varchar(32) NOT NULL DEFAULT '',
  `youtube` varchar(100) NOT NULL DEFAULT '',
  `verified` enum('0','1') NOT NULL DEFAULT '0',
  `active` enum('0','1') NOT NULL DEFAULT '0',
  `registered` varchar(32) NOT NULL DEFAULT '0/0000',
  `boosted` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Pages_Invites`
--

CREATE TABLE `Wo_Pages_Invites` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL DEFAULT 0,
  `inviter_id` int(11) NOT NULL DEFAULT 0,
  `invited_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Pages_Likes`
--

CREATE TABLE `Wo_Pages_Likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `page_id` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0,
  `active` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Payments`
--

CREATE TABLE `Wo_Payments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `amount` int(11) NOT NULL DEFAULT 0,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `Wo_Payments`
--

INSERT INTO `Wo_Payments` (`id`, `user_id`, `amount`, `type`, `date`) VALUES
(1, 83, 5, 'weekly', '10/2018'),
(2, 80, 5, 'weekly', '10/2018');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_PinnedPosts`
--

CREATE TABLE `Wo_PinnedPosts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `page_id` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `event_id` int(11) NOT NULL DEFAULT 0,
  `active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Polls`
--

CREATE TABLE `Wo_Polls` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `text` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Posts`
--

CREATE TABLE `Wo_Posts` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `recipient_id` int(11) NOT NULL DEFAULT 0,
  `postText` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` int(11) NOT NULL DEFAULT 0,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `event_id` int(11) NOT NULL DEFAULT 0,
  `page_event_id` int(11) NOT NULL DEFAULT 0,
  `postLink` varchar(1000) NOT NULL DEFAULT '',
  `postLinkTitle` text DEFAULT NULL,
  `postLinkImage` varchar(100) NOT NULL DEFAULT '',
  `postLinkContent` varchar(1000) NOT NULL DEFAULT '',
  `postVimeo` varchar(100) NOT NULL DEFAULT '',
  `postDailymotion` varchar(100) NOT NULL DEFAULT '',
  `postFacebook` varchar(100) NOT NULL DEFAULT '',
  `postFile` varchar(255) NOT NULL DEFAULT '',
  `postFileName` varchar(200) NOT NULL DEFAULT '',
  `postFileThumb` varchar(3000) NOT NULL DEFAULT '',
  `postYoutube` varchar(255) NOT NULL DEFAULT '',
  `postVine` varchar(32) NOT NULL DEFAULT '',
  `postSoundCloud` varchar(255) NOT NULL DEFAULT '',
  `postPlaytube` varchar(500) NOT NULL DEFAULT '',
  `postMap` varchar(255) NOT NULL DEFAULT '',
  `postShare` int(11) NOT NULL DEFAULT 0,
  `postPrivacy` enum('0','1','2','3') NOT NULL DEFAULT '1',
  `postType` varchar(30) NOT NULL DEFAULT '',
  `postFeeling` varchar(255) NOT NULL DEFAULT '',
  `postListening` varchar(255) NOT NULL DEFAULT '',
  `postTraveling` varchar(255) NOT NULL DEFAULT '',
  `postWatching` varchar(255) NOT NULL DEFAULT '',
  `postPlaying` varchar(255) NOT NULL DEFAULT '',
  `postPhoto` varchar(3000) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT 0,
  `registered` varchar(32) NOT NULL DEFAULT '0/0000',
  `album_name` varchar(52) NOT NULL DEFAULT '',
  `multi_image` enum('0','1') NOT NULL DEFAULT '0',
  `boosted` int(11) NOT NULL DEFAULT 0,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `poll_id` int(11) NOT NULL DEFAULT 0,
  `blog_id` int(11) NOT NULL DEFAULT 0,
  `videoViews` int(11) NOT NULL DEFAULT 0,
  `postRecord` varchar(3000) NOT NULL DEFAULT '',
  `postSticker` text DEFAULT NULL,
  `shared_from` int(15) NOT NULL DEFAULT 0,
  `post_url` text DEFAULT NULL,
  `parent_id` int(15) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Posts`
--

INSERT INTO `Wo_Posts` (`id`, `post_id`, `user_id`, `recipient_id`, `postText`, `page_id`, `group_id`, `event_id`, `page_event_id`, `postLink`, `postLinkTitle`, `postLinkImage`, `postLinkContent`, `postVimeo`, `postDailymotion`, `postFacebook`, `postFile`, `postFileName`, `postFileThumb`, `postYoutube`, `postVine`, `postSoundCloud`, `postPlaytube`, `postMap`, `postShare`, `postPrivacy`, `postType`, `postFeeling`, `postListening`, `postTraveling`, `postWatching`, `postPlaying`, `postPhoto`, `time`, `registered`, `album_name`, `multi_image`, `boosted`, `product_id`, `poll_id`, `blog_id`, `videoViews`, `postRecord`, `postSticker`, `shared_from`, `post_url`, `parent_id`) VALUES
(40, 40, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/09/wkLvPpCwoHNrjcrbSzqM_26_c1de932c313035b8bd70af63b72b066d_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1537936613, '9/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(41, 41, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/09/GSbsUyGIfF6qtPYCDFPb_26_3eccf18522462c408beb2b98aac32006_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1537936719, '9/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(42, 42, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/09/RdoGcSztA98VSUHtXoFO_26_8301331b9043ca3f1329e874fc345e16_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1537936759, '9/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(43, 43, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/09/Gg5lPRzjFaGamQZNhaR9_26_bfd2729b6b3015a134b836ff47d8e67e_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1537936808, '9/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(46, 46, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/09/JwAPDY9Kj9wMcPx53mO1_26_ee5d32e7339135e75b63d3b6018f16fa_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1537985631, '9/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(47, 47, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/09/oPnR6brPfz3sODbyBxuy_26_4d12fbdd62e569a1a015571d09a424fc_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1537985648, '9/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(48, 48, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/09/TTNSsrma4z8lfLIDjUSS_26_2fc380a112821cc9c13776a79683c466_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1537988582, '9/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(79, 79, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/qCB3i6VJ5dgEuq2X88K2_01_a70f079a9bf9ad14c6e951593133efe7_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1538370858, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(137, 137, 33, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/wdLdCMXzM3Nl8w5qVVBV_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1538965575, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(162, 162, 34, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/M4EeRZw461uDkST31oRS_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539065926, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(164, 164, 35, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/VZZtEMjjAa4TjXttGUr3_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539143976, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(172, 172, 36, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/lOUb4jaqKBIxqPWjTYje_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539231638, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(176, 176, 37, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/OXdoM97Iy9RFbdG65YtO_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539302792, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(181, 181, 37, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Salvador Bahia, Seabra - BA, Brasil', 0, '0', 'post', '', '', '', '', '', '', 1539338832, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(182, 182, 38, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/sUPhmgTEhJWxcKGqZgkJ_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539347504, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(185, 185, 38, 0, 'O outono é sempre igual', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '3', 'post', '', '', '', '', '', '', 1539358322, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(197, 197, 42, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/w45pkupIuFoekACxrRz4_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539569497, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(202, 202, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/PK2j9P4D4IHZqgNqafDW_15_8d511b43a78b4bf9b53941b79b568ad3_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539645785, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(203, 203, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/HvHUSQdkL4smseTkllCe_16_964bffb6fae7bce7660d74fc8932aea0_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539648535, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(204, 204, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/bvg8ncjUTIcfLkmOEmoR_16_564da173a3abb53eb2181e0acf26077f_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539649447, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(205, 205, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/oH2clb6pykmIyI4xQbGj_16_fea2b58d5fbb88727147ca2ce65c91fa_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539649605, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(206, 206, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/JhQ7TynVEZKZGjmyZgnD_16_a9469f6286783873194e935dd673dbf6_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539649648, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(207, 207, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/SjOurpLBEDQtUnq6yD7t_16_cba0c087f00310385b6cdb177ec205a4_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539649818, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(208, 208, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/GllOACOHQYvShArBZphH_16_6e2debf29cbf13f7f3f313d35ac31fb4_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539649915, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(209, 209, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/itjhbrh4pKfobEJhPoR7_16_77bd72b6a5c57abc6dae71fcd4ed9d6a_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539650087, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(210, 210, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/vUD9S6g1amIJxmTqu7sf_16_dfa67102193f33f21ce8642d90b58015_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539650160, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(211, 211, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/8BXvMSoUltZovz8UmcEb_16_0fc02c0dc0a76b6f87586e7138a84f94_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539650189, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(212, 212, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/GsyB3oVmYHYF2Fm5owSw_16_5eb6da49fd56a6e973f8655a43d23d5e_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539650747, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(215, 215, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/LPUYPUfc8UNQeNz4olx8_16_c37a1e0a48209e78eb37038fded1a2f4_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539664015, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(219, 219, 46, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/Wn7r2hCcqZcCeOSiPO4z_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539731041, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(220, 220, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/tIdJfvnf6OpFA7bnH8JV_16_9f08bfb22b19188df24197696421c066_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539731557, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(221, 221, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/cfr7fcORXUvauT79peSI_16_6534dc47e5865d27beda82f6c0f7e2c7_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539731682, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(222, 222, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/bbdqWKznWjoncmQwrUiV_16_66b09a5ac9c4cb5b687d2c4888bd080e_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539731779, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(223, 223, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/LxO9bIeeVulrh6QtJ77K_16_d20c20407cf95de88629caf90249781e_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539731832, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(224, 224, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/a5CTMK1wtmTFZUHQtCTY_16_5da62eb583f232ae8a6f399980571c41_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539731856, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(225, 225, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/zxgpB9ZUSsstDn8SjSXm_16_6f11bc9f13e64c024a1a46f16e5ce181_cover_full.jpeg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539731889, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(226, 226, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/oeeY7KHPeiQCHOPIT9Hi_16_eb9c5af11aa33b00dfa34d6405fe7b3a_cover_full.jpeg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539731939, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(227, 227, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/HQuWgwASK79G185enVlD_16_c84db9fcda0e5a5af7ef29a26f32f3cd_cover_full.jpeg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539731970, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(228, 228, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/TaMnbMPECQT1W7NY2uLF_16_1369b163e7eea164d47d1dc14fa887a6_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539732034, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(229, 229, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/IzgNFACSQ1AazZsh21vC_16_3912191da3591621ddd31187e0fabfb6_cover_full.jpeg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539732069, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(230, 230, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/BnoYiUDPFanbCcE93iEr_16_6b57e892630aa9f628c0865ebcb05ee0_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539732409, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(231, 231, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/QaHOTyUnjgK1uVcIbMnE_16_370893c0f3093774784f083d43d30566_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539732500, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(232, 232, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/QereFvpkuBkN2W7MXXBm_16_d5d64d616bbedc64ebef9650311014f2_cover_full.jpeg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539733532, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(233, 233, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/3XN8OujIozd8k7hlCq6I_16_4834ee9888075f27f2fe060de9595a00_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539733557, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(234, 234, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/yssr3N3a4HF1eWO5URcB_16_7f9fe23e8dfe5da85b08b5febac37d72_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539733850, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(235, 235, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/TjxGZrfDwj9mqxIOb2Z9_16_ac9728c20bddba96332d3f6fa95719fa_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539734016, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(244, 244, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/W43ClcrlCZYhDfnxHJEM_17_c9cecdeca2b6eae8c9640f8b5ec2052f_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539737618, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(254, 254, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/6cbmFU3GC6lA9Q2gIoXa_17_6787c7c686bc87942cb279de6fee6aa9_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539738275, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(255, 255, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/wT1VDYTGoAx7y8DLLEeI_17_ec093e3e328495b425791d109403898e_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539739292, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(256, 256, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/WWRsioHYftbxQ1TqqOUM_17_519cf83905678058aa0f1b9c2671c51d_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539740238, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(257, 257, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/MVbendP1weXIwbtYXlPI_17_64789af2e42eeb5e186c6799b7dd18a6_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539740861, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(258, 258, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/J4e8CDClmv1wEK2mEOOM_17_866506ed90f8e07584ef8af870f562c7_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539740950, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(259, 259, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/l3esJeQEwl4aLlYWX9rY_17_2a1b71a01cc2def258220e6eafd4977b_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539741409, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(260, 260, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/2Wc3NF1hm8qhNs3MDRyr_17_f637953fd310bd74210363e0394f60be_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539741841, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(261, 261, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/8nWD7mJt81kzgHXspLWN_17_ed4dc5fbb1cb49447aea139e46b87d5f_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539742004, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(262, 262, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/V3oM8qkAERHfpqd6E2DF_17_0bbbb247204134a4c55a656b18ab3d05_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539742085, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(263, 263, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/Khs9WGkQVEwNSrWE7dET_17_956e7af19ca181306d47a69f219c8f3a_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539742266, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(270, 270, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/6yAwJTbek2ZWzo1IXFZG_17_906ec8342b92ac3426d1e7cf6c72d738_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1539760003, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(279, 279, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/1k6TF1vKnG3LiEULaRqC_25_e3cd89849fd2eed2abbd6bb97d6a91dc_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1540506519, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(280, 280, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/wwiAAuvKIlCnfJsmOK3A_25_c98376013977b76398525f94e18a73b3_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1540507500, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(287, 287, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/dqFtta2HTBLChOZYiLfD_29_fc0a301a7153be159480c3720ec5b8fe_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1540774245, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(288, 288, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/8gSnAtcXy2Ut7jTZ4mxp_29_72fa3994be9d0af582f0c25407c6f1c0_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1540805312, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(293, 293, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/tSVedwZpIXmhpFQwWGwH_29_6092a0f82ecd6fac6acf2f88d28fd6e6_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1540825265, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(294, 294, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/wMBcW4T9ZdfEXTrkTdCv_29_991662e870fbe1e966947b43d8e19c9a_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1540851675, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(295, 295, 1, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Mooca, São Paulo - SP, Brasil', 0, '0', 'post', '', '', '', '', '', '', 1540857545, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(301, 301, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/A5U9oxZMj5TSATSqbg5Q_30_6789487c7264826b40f8db6232c562fd_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture', '', '', '', '', '', '', 1540872154, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(305, 305, 1, 0, 'efweqfqwe f wweqrgerwgerwgwergrewg ergrewgerwg rewg rew gergerwg gwrgg g r gwrg regre ggregerwgrgrg ewrgreg rgregrewg rewgrgwregre regwrgrewgrg rgwrg wrgrg wergregrew rg  gwgw  gwgr rg we grewrew rw rg re wgre erg rege grew gr w gw er greg r r gerg wegwwgre gergregre rgr  ergrg grg e wew wg erg regwer  g regere g  gw w grggrwg r g  wrgregrew g rewgrg  rwe gwgg  ge g eg rewg wrg reg wreg g rgre er reg rre erg rew greg r gewg rgeg wergwgr wg rg rgwe re rger gergegw w g ewr gerwgr ggrwg rgrewgrgrgwrgewg w   wrgrggw gr grgrew wegwger grgr ggg rg r g ggrgr gre ggreg grgr ergergrewgg grw grwgr gerw re gg   ggrew gwreregwr egrg wer gr wgwe', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1540927491, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(306, 306, 1, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Zero Grau Mooca - Rua Madre de Deus - Mooca, São Paulo - SP, Brasil', 0, '1', 'post', '', '', '', '', '', '', 1540928065, '10/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(315, 315, 82, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/10/9Jpnwo54s2VXkJKteR5w_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1540932764, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(318, 318, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/XKJa7YeFoIwe3VxsbDou_02_0ccf498605309536006c4e872f346bdc_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1541138291, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(319, 319, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/KsvsMc95SBBVNjW8fmg1_02_de723c8aa789e05557f41a691193851a_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1541138669, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(320, 320, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/wkvypJy5VA3BVmDEmpLq_02_b29cc2b9c82b8376a0753945e866c5ed_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_cover_picture', '', '', '', '', '', '', 1541139254, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(321, 321, 1, 0, 'T', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541222929, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(324, 324, 1, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Barra da Tijuca, Rio de Janeiro - RJ, Brasil', 0, '0', 'post', '', '', '', '', '', '', 1541408137, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(328, 328, 1, 0, 'r', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541524444, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(331, 331, 1, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', 'upload/photos/2018/11/UtAZySofKJJnlWKwvbgc_06_06b25832767c8865998e88f5c885a4cb_image.png', 'fwrewefrewfwer.png', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541545790, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(332, 332, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/knFJ3f6ju7HPWHQve4T7_07_afe49a119f08028789d8906f054f768f_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1541565005, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(333, 333, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/vajpJSj6YjStscLmSWNp_07_83525b8e5814d4871a6c88af79979522_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1541565063, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(334, 334, 1, 0, '#[3] a', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541567907, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(335, 335, 1, 0, '#[3] #[11]', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541567938, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(342, 342, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/PfQXzpl3pn9c6dsbbGEN_07_46fc35739953b521dd1a5b641508c4f0_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_cover_picture', '', '', '', '', '', '', 1541631983, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(343, 343, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/RXFNllpXDy7YyMh1j5pv_07_987c0f2754fa08d3944f120f9d6d616b_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_cover_picture', '', '', '', '', '', '', 1541632757, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(344, 344, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/5QSDouF79hRCF7zMh8sR_07_e95055ac1d5df5d641513b78ec7ce719_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture', '', '', '', '', '', '', 1541632852, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(345, 345, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/jyFEKOD5aDGdlZN2r1ev_07_af0007114db27ca734aa435f5c470b08_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture', '', '', '', '', '', '', 1541632917, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(346, 346, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/KZ3wNXzDiLy1Uc9CZzRF_07_ce240bcfd324f644996f9b8a24b1fb25_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_cover_picture', '', '', '', '', '', '', 1541632932, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(348, 348, 1, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', 'upload/photos/2018/11/St7m2NIjJPwR53BNtFji_08_56730696a526f3b2f912bc1f5d58717b_image.jpg', '20181027_181057.jpg', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541651402, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(352, 352, 1, 0, 'testando', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541748964, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(353, 353, 1, 0, 'ww', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541749142, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(354, 354, 1, 0, 'Testando', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541749979, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(355, 355, 1, 0, 'A', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541750052, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(356, 356, 1, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', 'upload/photos/2018/11/cZMBRoOBZklk4mpw3pD8_09_c67d7e936656762d857ca49902c1afb5_image.png', 'corrr.png', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541752133, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(360, 360, 85, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', 'upload/photos/2018/11/cZMBRoOBZklk4mpw3pD8_09_c67d7e936656762d857ca49902c1afb5_image.png', 'corrr.png', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541781668, '11/2018', '', '0', 0, 0, 0, 0, 0, '', '', 1, 'https://www.redelive.com/post/356', 356),
(361, 361, 85, 0, 'Estou testando', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541781841, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(362, 362, 85, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', 'upload/photos/2018/11/xpPb8mChTRv54PcqfH7G_09_c183eddee7211be0745ceecf13fdafe5_image.jpg', '1541781855491-1593873144.jpg', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541781878, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(363, 363, 1, 0, 'test', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '3', 'post', '', '', '', '', '', '', 1541782087, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(366, 366, 1, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', 'upload/videos/2018/11/96NK7uNYDN54hNfHNTQp_09_abcbcf81c08e019c4facfc7715edb3e4_video.mp4', '40148035_2062117410705892_5955059776398320110_n.mp4', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541784842, '11/2018', '', '0', 0, 0, 0, 0, 19, '', NULL, 0, NULL, 0),
(367, 367, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/3uGaZ9xChhhtc9b3r88B_09_77ad2cb617133d63bd30848a5b7d3919_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1541805581, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(369, 369, 1, 0, 'Test', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1541861906, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(374, 374, 85, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Minas Gerais, Brazil', 0, '0', 'post', '', '', '', '', '', '', 1541898268, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(375, 375, 85, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'São Paulo, SP, Brasil', 0, '0', 'post', '', '', '', '', '', '', 1541899951, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(383, 383, 98, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/B9pAnQdxx9q3jQGeEPBp_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542080642, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(384, 384, 1, 0, '#[3]', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1542136909, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(388, 388, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/IxHDc9qSxqSvcSnGiyj7_15_628cae7facf66bedaa01f8b5ee678a22_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542266029, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(389, 389, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/SkakXifJGOQ5eHee222d_15_74b21293296cd662d2ae46187b1cafee_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542266122, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(390, 390, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/5xcvaiitemh5buhzKYtY_19_a28fe7514c73590523f23d9a1e02763d_avatar_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542646039, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(391, 391, 98, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/VWNRlIPPXbW4nH4zxD7R_19_e7745e1448741cb6dbbfcdf8aee49665_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542647088, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(392, 392, 98, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/2NXbKqqiCKMeWqtqHHLR_19_1d2546e5acd3d1716b0fa2c5fccd9c82_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542647109, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(393, 393, 98, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/9vLmUzV4R9PnxIlsoFon_19_cf8d7f4884bafdad1ca399ee3895acf5_cover_full.png', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542647427, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(394, 394, 1, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', 'upload/photos/2018/11/cZMBRoOBZklk4mpw3pD8_09_c67d7e936656762d857ca49902c1afb5_image.png', 'corrr.png', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1542670500, '11/2018', '', '0', 0, 0, 0, 0, 0, '', '', 85, 'https://www.redelive.com/post/360', 360),
(395, 395, 1, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/a3xQwoWMRPHvfEq1mDRP_20_d7a8a5b3d8b1e9f911ac32405df0aeef_cover_full.gif', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542683333, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(396, 396, 103, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/aMrlUKKLpwysyi5FG9pr_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542777182, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(397, 397, 104, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Mooca Plaza Shopping - Rua Capitão Pacheco e Chaves - Mooca, São Paulo - SP, Brasil', 0, '0', 'post', '', '', '', '', '', '', 1542782005, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(398, 398, 105, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/eqfpRXsPWTPenoBANjof_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542798235, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(402, 402, 108, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/UZmxa98bPw9MgeMbNUBt_avatar_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_picture_deleted', '', '', '', '', '', '', 1542838651, '0/0000', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(404, 404, 108, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Curitiba, PR, Brasil', 0, '0', 'post', '', '', '', '', '', '', 1542901057, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(405, 405, 108, 0, 'Ubuntu é um sistema operacional (português brasileiro) ou sistema operativo (português europeu) de código aberto, construído a partir do núcleo Linux, baseado no Debian e utiliza GNOME como ambiente de desktop de sua mais recente versão com suporte de longo prazo (LTS). É desenvolvido pela Canonical Ltd.  <br>  <br>Geralmente é executado em computadores pessoais e também é popular em servidores de rede, geralmente executando a versão Ubuntu Server, com recursos de classe empresarial. O Ubuntu também está disponível para tablets e smartphones, com a edição Ubuntu Touch.  <br>  <br>A proposta do Ubuntu é oferecer um sistema que qualquer pessoa possa utilizar sem dificuldades, independentemente de nacionalidade, nível de conhecimento ou limitações físicas. O sistema deve ser constituído principalmente por software livre e deve também ser isento de qualquer taxa.', 0, 0, 0, 0, '', '', '', '', '', '', '', 'upload/photos/2018/11/xrzGRmGoWp3YHfsfZXiN_22_174e42a1e41e4d6ebce17004065a3bd9_image.jpg', 'thumb-1920-320424.jpg', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1542901136, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(406, 406, 108, 0, NULL, 0, 0, 0, 0, '', NULL, '', '', '', '', '', 'upload/photos/2018/11/saadVB7SZjIzb6sFJ269_22_e83697e541e7f97ae722493247f74192_cover_full.jpg', '', '', '', '', '', '', '', 0, '0', 'profile_cover_picture', '', '', '', '', '', '', 1542901302, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(408, 408, 102, 0, 'Boa noite', 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1542941372, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0),
(410, 410, 1, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', '', 'upload/photos/2018/11/APg9NamV25lkNhviZG98_24_eb5954682d944d96f0ba6085d61a8cc0_image.jpg', 'FB_IMG_1543075992009.jpg', '', '', '', '', '', '', 0, '0', 'post', '', '', '', '', '', '', 1543076087, '11/2018', '', '0', 0, 0, 0, 0, 0, '', NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Products`
--

CREATE TABLE `Wo_Products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` int(11) NOT NULL DEFAULT 0,
  `price` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0.00',
  `location` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `type` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USD',
  `time` int(11) NOT NULL DEFAULT 0,
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Products_Media`
--

CREATE TABLE `Wo_Products_Media` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT 0,
  `image` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_ProfileFields`
--

CREATE TABLE `Wo_ProfileFields` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `length` int(11) NOT NULL DEFAULT 0,
  `placement` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'profile',
  `registration_page` int(11) NOT NULL DEFAULT 0,
  `profile_page` int(11) NOT NULL DEFAULT 0,
  `select_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_RecentSearches`
--

CREATE TABLE `Wo_RecentSearches` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `search_id` int(11) NOT NULL DEFAULT 0,
  `search_type` varchar(32) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Wo_RecentSearches`
--

INSERT INTO `Wo_RecentSearches` (`id`, `user_id`, `search_id`, `search_type`) VALUES
(47, 1, 33, 'user'),
(48, 34, 108, 'user');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Relationship`
--

CREATE TABLE `Wo_Relationship` (
  `id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL DEFAULT 0,
  `to_id` int(11) NOT NULL DEFAULT 0,
  `relationship` int(11) NOT NULL DEFAULT 0,
  `active` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Reports`
--

CREATE TABLE `Wo_Reports` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `profile_id` int(11) NOT NULL DEFAULT 0,
  `page_id` int(15) NOT NULL DEFAULT 0,
  `group_id` int(15) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `text` text DEFAULT NULL,
  `seen` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_SavedPosts`
--

CREATE TABLE `Wo_SavedPosts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `post_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Terms`
--

CREATE TABLE `Wo_Terms` (
  `id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT '',
  `text` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Terms`
--

INSERT INTO `Wo_Terms` (`id`, `type`, `text`) VALUES
(1, 'terms_of_use', '<h4>1- Não é permitido postar pornografia.</h4>    \n <h4>2- Não é permitido expor ao ridículo, ofender ou ameaçar outros usuários.</h4>\n<h4>3- Não é permitido postar conteúdo nazista ou comunista.</h4>\n'),
(2, 'privacy_policy', '<h2>Nunca venderemos seus dados pessoais para empresas!</h2>'),
(3, 'about', '<h4>1- Nosso propósito é construir uma rede social sem a famosa “bolha de informação” e com o diferencial de proteger o consumidor de manipulação.</h4>\n\n<h4>Por ser uma rede social baseada na distancia e localização dos usuários o conteúdo gerado será muito mais relevante paras as pessoas próximas do que qualquer outra rede social e alem disso ela terá o diferencial de ajudar com muito mais eficiência e rapidez a encontrar pessoas desaparecidas, pois pessoas próximas irão ficar sabendo do ocorrido podendo ajudar a encontrar ou alertar todos da região. Diferente das outras redes onde o usuário só pode compartilhar conteúdo com amigos e não com quem está próximo.</h4>\n\n<br />\n<figure class=\"_2cuy _4nuy _2vxa _3tvj\" style=\"background-color: white; box-sizing: border-box; color: #1d2129; direction: ltr; font-family: Georgia, serif; font-size: 17px; margin: 0px auto; overflow-wrap: break-word; white-space: pre-wrap; width: 700px;\"><div class=\"_h2x _4lh3\" style=\"clear: both; float: left; font-family: inherit; font-size: 14px; line-height: 20px; margin: 0px 40px 40px 0px; text-align: center;\">\n<img alt=\"\" class=\"_h2z _297z _4lh5 img\" height=\"320\" id=\"u_3c_3\" src=\"https://scontent.fcgh8-1.fna.fbcdn.net/v/t1.0-9/34689290_453314318451767_6094043244193120256_n.png?_nc_cat=0&_nc_eui2=AeENai_SPFo5AQ8clQ7SyBR1UaF4XFIS4J__ErieJRFHZzpe1F8OVyLq4MXUPTAYjeYidmBXLVjyImNVz3b1CtjvexfSP82Ki_PhRxUb1WjWUg&oh=d19ed781aec47f66da56ba7575dddb85&oe=5C62862F\" style=\"border: 0px; max-height: 400px; max-width: 400px;\" width=\"320\" /></div>\n</figure><div class=\"_2cuy _3dgx _2vxa\" style=\"background-color: white; box-sizing: border-box; color: #1d2129; direction: ltr; font-family: Georgia, serif; font-size: 17px; margin: 0px auto 28px; overflow-wrap: break-word; white-space: pre-wrap; width: 700px;\">\nFuturamente pretendemos criar um sistema de pagamento entre os usuários e lojas com cripitomoedas totalmente decentralizado</div>\n');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Tokens`
--

CREATE TABLE `Wo_Tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `app_id` int(11) NOT NULL DEFAULT 0,
  `token` varchar(200) NOT NULL DEFAULT '',
  `time` int(32) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_UserAds`
--

CREATE TABLE `Wo_UserAds` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(3000) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `headline` varchar(200) NOT NULL DEFAULT '',
  `description` text DEFAULT NULL,
  `location` varchar(1000) NOT NULL DEFAULT 'us',
  `audience` longtext DEFAULT NULL,
  `ad_media` varchar(3000) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `gender` varchar(15) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT 'all',
  `bidding` varchar(15) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `clicks` int(15) NOT NULL DEFAULT 0,
  `views` int(15) NOT NULL DEFAULT 0,
  `posted` varchar(15) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT 1,
  `appears` varchar(10) NOT NULL DEFAULT 'post',
  `user_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_UserFields`
--

CREATE TABLE `Wo_UserFields` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `Wo_UserFields`
--

INSERT INTO `Wo_UserFields` (`id`, `user_id`) VALUES
(1, 2),
(2, 3),
(3, 4),
(4, 5),
(5, 6),
(6, 7),
(7, 8),
(8, 9),
(9, 10),
(10, 11),
(11, 12),
(12, 13),
(13, 14),
(14, 15),
(15, 16),
(16, 17),
(17, 18),
(18, 19),
(19, 20),
(20, 21),
(21, 22),
(22, 23),
(23, 24),
(24, 25),
(25, 26),
(26, 27),
(27, 28),
(28, 29),
(29, 30),
(30, 31),
(31, 32),
(32, 33),
(33, 34),
(34, 35),
(35, 36),
(36, 37),
(37, 38),
(38, 39),
(39, 40),
(40, 41),
(41, 42),
(42, 43),
(43, 44),
(44, 45),
(45, 46),
(46, 47),
(47, 48),
(48, 49),
(49, 50),
(50, 51),
(51, 52),
(52, 53),
(53, 54),
(54, 55),
(55, 56),
(56, 57),
(57, 58),
(58, 59),
(59, 60),
(60, 61),
(61, 62),
(62, 63),
(63, 64),
(64, 65),
(65, 66),
(66, 67),
(67, 68),
(68, 69),
(69, 70),
(70, 71),
(71, 72),
(72, 73),
(73, 74),
(74, 75),
(75, 76),
(76, 77),
(77, 78),
(78, 79),
(79, 80),
(80, 81),
(81, 82),
(82, 83),
(83, 84),
(84, 85),
(85, 86),
(86, 87),
(87, 88),
(88, 89),
(89, 90),
(90, 91),
(91, 92),
(92, 93),
(93, 94),
(94, 95),
(95, 96),
(96, 97),
(97, 98),
(98, 99),
(99, 100),
(100, 101),
(101, 102),
(102, 103),
(103, 104),
(104, 105),
(105, 106),
(106, 107),
(107, 108);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Users`
--

CREATE TABLE `Wo_Users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `first_name` varchar(60) NOT NULL DEFAULT '',
  `last_name` varchar(32) NOT NULL DEFAULT '',
  `avatar` varchar(100) NOT NULL DEFAULT 'upload/photos/d-avatar.jpg',
  `cover` varchar(100) NOT NULL DEFAULT 'upload/photos/d-cover.jpg',
  `background_image` varchar(100) NOT NULL DEFAULT '',
  `background_image_status` enum('0','1') NOT NULL DEFAULT '0',
  `relationship_id` int(11) NOT NULL DEFAULT 0,
  `address` varchar(100) NOT NULL DEFAULT '',
  `working` varchar(32) NOT NULL DEFAULT '',
  `working_link` varchar(32) NOT NULL DEFAULT '',
  `about` text DEFAULT NULL,
  `school` varchar(32) NOT NULL DEFAULT '',
  `gender` varchar(32) NOT NULL DEFAULT 'male',
  `birthday` varchar(50) NOT NULL DEFAULT '0000-00-00',
  `country_id` int(11) NOT NULL DEFAULT 0,
  `website` varchar(50) NOT NULL DEFAULT '',
  `facebook` varchar(50) NOT NULL DEFAULT '',
  `google` varchar(50) NOT NULL DEFAULT '',
  `twitter` varchar(50) NOT NULL DEFAULT '',
  `linkedin` varchar(32) NOT NULL DEFAULT '',
  `youtube` varchar(100) NOT NULL DEFAULT '',
  `vk` varchar(32) NOT NULL DEFAULT '',
  `instagram` varchar(32) NOT NULL DEFAULT '',
  `language` varchar(31) NOT NULL DEFAULT 'english',
  `email_code` varchar(32) NOT NULL DEFAULT '',
  `src` varchar(32) NOT NULL DEFAULT 'Undefined',
  `ip_address` varchar(32) DEFAULT '',
  `follow_privacy` enum('1','0') NOT NULL DEFAULT '0',
  `friend_privacy` enum('0','1','2','3') NOT NULL DEFAULT '0',
  `post_privacy` varchar(255) NOT NULL DEFAULT 'ifollow',
  `message_privacy` enum('1','0') NOT NULL DEFAULT '0',
  `confirm_followers` enum('1','0') NOT NULL DEFAULT '0',
  `show_activities_privacy` enum('0','1') NOT NULL DEFAULT '1',
  `birth_privacy` enum('0','1','2') NOT NULL DEFAULT '0',
  `visit_privacy` enum('0','1') NOT NULL DEFAULT '0',
  `verified` enum('1','0') NOT NULL DEFAULT '0',
  `lastseen` int(32) NOT NULL DEFAULT 0,
  `showlastseen` enum('1','0') NOT NULL DEFAULT '1',
  `emailNotification` enum('1','0') NOT NULL DEFAULT '1',
  `e_liked` enum('0','1') NOT NULL DEFAULT '1',
  `e_wondered` enum('0','1') NOT NULL DEFAULT '1',
  `e_shared` enum('0','1') NOT NULL DEFAULT '1',
  `e_followed` enum('0','1') NOT NULL DEFAULT '1',
  `e_commented` enum('0','1') NOT NULL DEFAULT '1',
  `e_visited` enum('0','1') NOT NULL DEFAULT '1',
  `e_liked_page` enum('0','1') NOT NULL DEFAULT '1',
  `e_mentioned` enum('0','1') NOT NULL DEFAULT '1',
  `e_joined_group` enum('0','1') NOT NULL DEFAULT '1',
  `e_accepted` enum('0','1') NOT NULL DEFAULT '1',
  `e_profile_wall_post` enum('0','1') NOT NULL DEFAULT '1',
  `e_sentme_msg` enum('0','1') NOT NULL DEFAULT '0',
  `e_last_notif` varchar(50) NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `active` enum('0','1','2') NOT NULL DEFAULT '0',
  `admin` enum('0','1','2') NOT NULL DEFAULT '0',
  `type` varchar(11) NOT NULL DEFAULT 'user',
  `registered` varchar(32) NOT NULL DEFAULT '0/0000',
  `start_up` enum('0','1') NOT NULL DEFAULT '0',
  `start_up_info` enum('0','1') NOT NULL DEFAULT '0',
  `startup_follow` enum('0','1') NOT NULL DEFAULT '0',
  `startup_image` enum('0','1') NOT NULL DEFAULT '0',
  `last_email_sent` int(32) NOT NULL DEFAULT 0,
  `phone_number` varchar(32) NOT NULL DEFAULT '',
  `sms_code` int(11) NOT NULL DEFAULT 0,
  `is_pro` enum('0','1') NOT NULL DEFAULT '0',
  `pro_time` int(11) NOT NULL DEFAULT 0,
  `pro_type` enum('0','1','2','3','4') NOT NULL DEFAULT '0',
  `joined` int(11) NOT NULL DEFAULT 0,
  `css_file` varchar(100) NOT NULL DEFAULT '',
  `timezone` varchar(50) NOT NULL DEFAULT '',
  `referrer` int(11) NOT NULL DEFAULT 0,
  `balance` varchar(100) NOT NULL DEFAULT '0',
  `paypal_email` varchar(100) NOT NULL DEFAULT '',
  `notifications_sound` enum('0','1') NOT NULL DEFAULT '0',
  `order_posts_by` enum('0','1','2','3','4','5') NOT NULL DEFAULT '1',
  `social_login` enum('0','1') NOT NULL DEFAULT '0',
  `device_id` varchar(50) NOT NULL DEFAULT '',
  `web_device_id` varchar(100) NOT NULL DEFAULT '',
  `wallet` varchar(20) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0.00',
  `lat` varchar(200) NOT NULL DEFAULT '0',
  `lng` varchar(200) NOT NULL DEFAULT '0',
  `last_location_update` varchar(30) NOT NULL DEFAULT '0',
  `share_my_location` int(11) NOT NULL DEFAULT 1,
  `last_data_update` int(11) NOT NULL DEFAULT 0,
  `details` varchar(300) NOT NULL DEFAULT 'a:6:{s:10:"post_count";i:0;s:11:"album_count";i:0;s:15:"following_count";i:0;s:15:"followers_count";i:0;s:12:"groups_count";i:0;s:11:"likes_count";i:0;}',
  `sidebar_data` text DEFAULT NULL,
  `last_avatar_mod` int(11) NOT NULL DEFAULT 0,
  `last_cover_mod` int(11) NOT NULL DEFAULT 0,
  `login_location_neighborhood` varchar(255) DEFAULT NULL,
  `login_location_city` varchar(255) DEFAULT NULL,
  `login_location_state` varchar(255) DEFAULT NULL,
  `login_location_country` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Users`
--

INSERT INTO `Wo_Users` (`user_id`, `username`, `email`, `password`, `first_name`, `last_name`, `avatar`, `cover`, `background_image`, `background_image_status`, `relationship_id`, `address`, `working`, `working_link`, `about`, `school`, `gender`, `birthday`, `country_id`, `website`, `facebook`, `google`, `twitter`, `linkedin`, `youtube`, `vk`, `instagram`, `language`, `email_code`, `src`, `ip_address`, `follow_privacy`, `friend_privacy`, `post_privacy`, `message_privacy`, `confirm_followers`, `show_activities_privacy`, `birth_privacy`, `visit_privacy`, `verified`, `lastseen`, `showlastseen`, `emailNotification`, `e_liked`, `e_wondered`, `e_shared`, `e_followed`, `e_commented`, `e_visited`, `e_liked_page`, `e_mentioned`, `e_joined_group`, `e_accepted`, `e_profile_wall_post`, `e_sentme_msg`, `e_last_notif`, `status`, `active`, `admin`, `type`, `registered`, `start_up`, `start_up_info`, `startup_follow`, `startup_image`, `last_email_sent`, `phone_number`, `sms_code`, `is_pro`, `pro_time`, `pro_type`, `joined`, `css_file`, `timezone`, `referrer`, `balance`, `paypal_email`, `notifications_sound`, `order_posts_by`, `social_login`, `device_id`, `web_device_id`, `wallet`, `lat`, `lng`, `last_location_update`, `share_my_location`, `last_data_update`, `details`, `sidebar_data`, `last_avatar_mod`, `last_cover_mod`, `login_location_neighborhood`, `login_location_city`, `login_location_state`, `login_location_country`) VALUES
(1, 'Redelive', 'contato@curtizando.com', '78a5e2e45e7bff3320d5eeb7ac45c80510a59be3', '', '', 'upload/photos/2018/11/5xcvaiitemh5buhzKYtY_19_a28fe7514c73590523f23d9a1e02763d_avatar.png', 'upload/photos/2018/11/a3xQwoWMRPHvfEq1mDRP_20_d7a8a5b3d8b1e9f911ac32405df0aeef_cover.gif', 'upload/photos/2018/09/gA4Bq7A52ArCGKNkGWj9_23_b955a774020897012537f7482436f9fc_background_image.png', '0', 0, 'mooca rua padre raposo 1372 ap172', 'Brasil', 'https://www.redelive.com', 'Sou uma rede social sem a famosa “bolha de informação” e com o diferencial de proteger o consumidor de manipulação.', '', 'male', '0000-00-00', 1, 'https://www.redelive.com', '', '', '', '', '', '', '', 'portuguese', '', 'Undefined', '177.33.202.89', '0', '0', 'nobody', '0', '0', '1', '2', '0', '1', 1543158028, '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '1', 'user', '00/0000', '1', '1', '1', '1', 0, '', 0, '1', 1541784437, '4', 1537451865, '', '', 0, '0', '', '0', '1', '0', '', '1a11a18c-3033-497d-ba77-0abb7820cfa5', '0.79', '-23.5655279', '-46.5903711', '1543456810', 1, 1543110799, 'a:6:{s:10:\"post_count\";s:2:\"92\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"3\";s:15:\"followers_count\";s:2:\"11\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:3:{i:0;s:3:\"104\";i:1;s:2:\"85\";i:2;s:2:\"33\";}s:14:\"followers_data\";a:9:{i:0;s:3:\"108\";i:1;s:3:\"107\";i:2;s:3:\"103\";i:3;s:3:\"102\";i:4;s:2:\"98\";i:5;s:2:\"85\";i:6;s:2:\"82\";i:7;s:2:\"42\";i:8;s:2:\"37\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 1542646073, 1542266098, 'Tv. Miriam Batucada - Mooca', 'São Paulo', 'SP', 'BR'),
(33, 'f694ae20d', 'danielamirandaveiga@gmail.com', '53c6bc1ef8a442e1e69ffdc9147ae812e2847bcb', 'Daniela', 'Veiga', 'upload/photos/2018/10/qIyCbsPcLrCOqDwONlxW_11_69a7dfa486bf1fca3baa68c8e7530a6a_avatar.jpg', 'upload/photos/2018/10/V6PJQ9zXhyp5xdqsFYnM_09_1a4b92ce36d836c7cdad829b8bbf77b0_cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '1994-10-7', 32, '', '', '', '', '', '', '', '', 'portuguese', '0b93b7903f56a1a7bc5f650da25fcc0c', 'Facebook', '177.33.200.29', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '1', 1538966331, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1538965575, '', '', 0, '0', '', '0', '0', '1', '', '', '0.00', '0', '0', '0', 1, 1543157819, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"1\";s:15:\"followers_count\";s:1:\"2\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:1:{i:0;s:1:\"1\";}s:14:\"followers_data\";a:2:{i:0;s:2:\"37\";i:1;s:1:\"1\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 1539291603, 1539748526, NULL, NULL, NULL, NULL),
(34, 'c2bb0baee', 'joao.souzi@gmail.com', '7694efd662302e3b2682fbf3745f58f86e3d38ca', 'João', 'Souzi', 'upload/photos/2018/10/M4EeRZw461uDkST31oRS_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '00-00-0000', 32, '', '', '', '', '', '', '', '', 'portuguese', 'a8e1c23964210e11869c88dfb46885e3', 'Facebook', '187.111.197.203', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1542945038, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '0', '1', '0', '1', 0, '', 0, '0', 0, '0', 1539065925, '', '', 0, '0', '', '0', '0', '1', '', '', '0.00', '0', '0', '0', 1, 1543120303, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"0\";s:15:\"followers_count\";s:1:\"1\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:0:{}s:14:\"followers_data\";a:1:{i:0;s:2:\"37\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(35, '6a796eee1', 'gabi.ghab@gmail.com', '5b5cd905b7a9929d4b77c8c519635c5fb37e2d2e', 'Gabi', 'Ghab', 'upload/photos/2018/10/VZZtEMjjAa4TjXttGUr3_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '00-00-0000', 1, '', '', '', '', '', '', '', '', 'portuguese', 'fff1ad7275747b4c6be5160112566b98', 'Facebook', '187.111.197.89', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1539144072, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '0', '1', '0', '1', 0, '', 0, '0', 0, '0', 1539143976, '', '', 0, '0', '', '0', '0', '1', '', '', '0.00', '0', '0', '0', 1, 1543162237, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"0\";s:15:\"followers_count\";s:1:\"1\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:0:{}s:14:\"followers_data\";a:1:{i:0;s:2:\"37\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(36, 'Paula', 'anapaulabeckmann@hotmail.com', '841a713386f17c64aa0f465124f40f5ddfa558f3', 'Paula', 'Beckmann', 'upload/photos/2018/10/lOUb4jaqKBIxqPWjTYje_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '1980-6-24', 32, '', '', '', '', '', '', '', '', 'portuguese', '0f5971bd3a74946ad5452ab6d9b1fe14', 'Facebook', '177.65.177.38', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1539231911, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1539231638, '', '', 0, '0', '', '0', '0', '0', '', '', '0.00', '-1.4570144', '-48.4722282', '1539836455', 1, 1543120303, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"1\";s:15:\"followers_count\";s:1:\"1\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:1:{i:0;s:1:\"1\";}s:14:\"followers_data\";a:1:{i:0;s:2:\"37\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(37, 'sacjamm', 'sac.jamm@gmail.com', '0451c4136aeb18d16e299fe1cc6d365420b2959f', 'Alisson', 'Maciel', 'upload/photos/2018/10/OXdoM97Iy9RFbdG65YtO_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '1989-4-15', 32, '', '', '', '', '', '', '', '', 'portuguese', '7ae9bd5bb98b1c5581c1dc3b3bc9f134', 'Facebook', '177.136.44.4', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1540822217, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '0', '1', '0', '1', 0, '', 0, '0', 0, '0', 1539302792, '', '', 0, '0', '', '0', '0', '0', '', '', '0.00', '0', '0', '0', 1, 1543168843, 'a:6:{s:10:\"post_count\";s:1:\"2\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"6\";s:15:\"followers_count\";s:1:\"0\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:6:{i:0;s:2:\"38\";i:1;s:2:\"36\";i:2;s:2:\"35\";i:3;s:2:\"34\";i:4;s:2:\"33\";i:5;s:1:\"1\";}s:14:\"followers_data\";a:0:{}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(38, '99cb98d40', 'lipsmega@gmail.com', '320efaccc60638c7f0dae6f9999a4d9fb63dcb7d', 'Felipe', 'Teixeira', 'upload/photos/2018/10/sUPhmgTEhJWxcKGqZgkJ_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 1, '', 'Apple', '', '', '', 'male', '0000-00-00', 0, '', '', '', '', '', '', '', '', 'portuguese', '3a30df00953015c6deb21abb69d7cec1', 'Facebook', '177.106.69.145', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1539653374, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '0', '0', '0', '1', 0, '', 0, '0', 0, '0', 1539347504, '', '', 0, '0', '', '0', '0', '1', '', '', '0.00', '0', '0', '0', 1, 1543120303, 'a:6:{s:10:\"post_count\";s:1:\"2\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"0\";s:15:\"followers_count\";s:1:\"1\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:0:{}s:14:\"followers_data\";a:1:{i:0;s:2:\"37\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(42, '4787326b8', 'claudemirsilvaramos@gmail.com', '3a2fe0ecad4c2cc1e65e82743a60bd5b680ea5d5', 'Claudemir', 'Ramos', 'upload/photos/2018/10/w45pkupIuFoekACxrRz4_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '1979-10-28', 1, '', '', '', '', '', '', '', '', 'portuguese', 'e96c26f141a1aacc03d1ac59fd1b33db', 'Facebook', '201.13.88.109', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1539833500, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1539569497, '', '', 0, '0', '', '0', '0', '1', '', '', '0.00', '-22.6443264', '-47.053209599999995', '1540174306', 1, 1543026121, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"1\";s:15:\"followers_count\";s:1:\"0\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:1:{i:0;s:1:\"1\";}s:14:\"followers_data\";a:0:{}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(46, '700bfd6a1', 'mariajosealvessilva94@gmail.com', '72d8ed921e0369b5b372af078523e74ec78da216', 'Maria', 'José', 'upload/photos/2018/10/Wn7r2hCcqZcCeOSiPO4z_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '0000-00-00', 0, '', '', '', '', '', '', '', '', 'portuguese', '8205820e97cbc79ac44d6052dc64d638', 'Facebook', '138.99.29.6', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1539731055, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '0', '0', '0', '1', 0, '', 0, '0', 0, '0', 1539731041, '', '', 0, '0', '', '0', '0', '1', '', '', '0.00', '0', '0', '0', 1, 1543120304, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"0\";s:15:\"followers_count\";s:1:\"0\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:0:{}s:14:\"followers_data\";a:0:{}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(82, '92998975f', 'thiagogaldiano@gmail.com', 'c4a3dd8c9cbd1426c540cad5cc134457446cc274', 'Thiago', 'Galdiano', 'upload/photos/2018/10/9Jpnwo54s2VXkJKteR5w_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '00-00-0000', 1, '', '', '', '', '', '', '', '', 'portuguese', 'd3ce24362842d9c435f6a2faca1bae13', 'Facebook', '177.106.93.133', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1540932957, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1540932764, '', '', 0, '0', '', '0', '0', '1', '', '', '0.00', '0', '0', '0', 1, 1543124027, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"1\";s:15:\"followers_count\";s:1:\"1\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:1:{i:0;s:1:\"1\";}s:14:\"followers_data\";a:1:{i:0;s:2:\"85\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(85, 'marcosmessias', 'marcosmessiasdev@gmail.com', '5e9e94c59c13697d8322ba7943ea806e39ad0181', 'Marcos', 'Messias', 'upload/photos/d-avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', '', '', 'male', '1982-11-6', 32, '', '', '', '', '', '', '', '', 'portuguese', 'e9646acfad972b5ae6a0c6d037e5f9ed', 'site', '189.17.45.12', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1543157032, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '10/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1540944986, '', '', 0, '0', '', '0', '1', '0', '', '', '0.00', '-20.7500419', '-42.8839906', '1543757439', 1, 1543164138, 'a:6:{s:10:\"post_count\";s:1:\"5\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"2\";s:15:\"followers_count\";s:1:\"1\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:2:{i:0;s:2:\"82\";i:1;s:1:\"1\";}s:14:\"followers_data\";a:1:{i:0;s:1:\"1\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, 'São Sebastiao', 'Viçosa', 'MG', 'BR'),
(89, 'testeapp', 'filipehenriquester@gmail.com', '2be695f13be000793c79177b1a874f159a8eae3b', '', '', 'upload/photos/d-avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '0000-00-00', 0, '', '', '', '', '', '', '', '', 'portuguese', '903686faf2fbec97f3d729db1acd5266', 'Phone', '131.221.240.226', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1541507160, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '11/2018', '0', '0', '0', '0', 0, '', 0, '0', 0, '0', 1541507160, '', 'UTC', 0, '0', '', '0', '1', '0', '', '', '0.00', '0', '0', '0', 1, 1542966375, 'a:6:{s:10:\"post_count\";s:1:\"0\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"0\";s:15:\"followers_count\";s:1:\"0\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:0:{}s:14:\"followers_data\";a:0:{}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(98, 'c51562f63', '10curtizando@gmail.com', '1a1100119f8dd1030347430652a5965457ad8506', 'Lucas', 'Marcolini', 'upload/photos/2018/11/B9pAnQdxx9q3jQGeEPBp_avatar.jpg', 'upload/photos/2018/11/9vLmUzV4R9PnxIlsoFon_19_cf8d7f4884bafdad1ca399ee3895acf5_cover.png', '', '0', 0, '', '', '', NULL, '', 'male', '00-00-0000', 1, '', '', '', '', '', '', '', '', 'portuguese', '8e390ea46eef1bb1358e0d5d5543e337', 'Facebook', '177.33.202.89', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1543161152, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '11/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1542080642, '', '', 0, '0', '', '0', '0', '1', '', '', '0.00', '-23.5655208', '-46.5903507', '1543414889', 1, 1543120303, 'a:6:{s:10:\"post_count\";s:1:\"4\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"1\";s:15:\"followers_count\";s:1:\"2\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:1:{i:0;s:1:\"1\";}s:14:\"followers_data\";a:2:{i:0;s:3:\"108\";i:1;s:3:\"107\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 1542647458, 'R. do Oratório - Mooca', 'São Paulo', 'SP', 'BR'),
(101, 'asfasf', 'sadasdas@adasdas.com', 'c7a9f84bb5ac28e434238294999c298637e77cce', '', '', 'upload/photos/d-avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '0000-00-00', 0, '', '', '', '', '', '', '', '', 'portuguese', '0a040ec34abbfb7f3030345244a913c9', 'Phone', '170.150.92.194', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1542756199, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '11/2018', '0', '0', '0', '0', 0, '', 0, '0', 0, '0', 1542566906, '', 'UTC', 0, '0', '', '0', '1', '0', '', '1a11a18c-3033-497d-ba77-0abb7820cfa5', '0.00', '-22.2585712', '-45.6933887', '1543174145', 1, 1542901024, 'a:6:{s:10:\"post_count\";s:1:\"0\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"0\";s:15:\"followers_count\";s:1:\"0\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:0:{}s:14:\"followers_data\";a:0:{}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, 'Novo Horizonte', 'Santa Rita do Sapucaí', 'MG', 'BR'),
(102, 'testeteste', 'sgegegwgw@wgeheh.com', '64e41586b9608df4f7be38cc33bce7072bd29a05', 'Teste', 'Teste', 'upload/photos/d-avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '00-00-0000', 1, '', '', '', '', '', '', '', '', 'portuguese', 'f6fd1939bdf31481d27ac4344a2aab58', 'site', '170.150.92.194', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1543098196, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '11/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1542754516, '', '', 0, '0', '', '0', '1', '0', '', '', '0.00', '0', '0', '0', 1, 1543108847, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"1\";s:15:\"followers_count\";s:1:\"0\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:1:{i:0;s:1:\"1\";}s:14:\"followers_data\";a:0:{}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(103, '0b5161d4a', 'curtizando@curtizando.com', 'bdeb58ea30be575b8f1fb329f639fa563539aeb1', 'Lucas', 'Otaviani Marcolini', 'upload/photos/2018/11/aMrlUKKLpwysyi5FG9pr_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '1989-11-28', 32, '', '', '', '', '', '', '', '', 'portuguese', '3b187b3dcf4bef6544ca47e93a03b2b8', 'Facebook', '177.33.202.89', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1542781930, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '11/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1542777182, '', '', 0, '0', '', '0', '3', '1', '', '', '0.00', '-23.565415800000004', '-46.590318599999996', '1543381984', 1, 1543165379, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"1\";s:15:\"followers_count\";s:1:\"2\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:1:{i:0;s:1:\"1\";}s:14:\"followers_data\";a:2:{i:0;s:3:\"108\";i:1;s:3:\"107\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, 'Mooca', 'São Paulo', 'SP', 'BR'),
(104, 'abcde', '234ef3wq@grg34.com', '78a5e2e45e7bff3320d5eeb7ac45c80510a59be3', '', '', 'upload/photos/d-avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', '', '', 'male', '0000-00-00', 0, '', '', '', '', '', '', '', '', 'portuguese', 'ab56b4d92b40713acc5af89985d4b786', 'site', '177.33.202.89', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1542825114, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '11/2018', '0', '0', '0', '0', 0, '', 0, '0', 0, '0', 1542781760, '', '', 0, '0', '', '0', '2', '0', '', '', '0.00', '-23.565438200000003', '-46.5903363', '1543390947', 1, 1543007335, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"0\";s:15:\"followers_count\";s:1:\"0\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:0:{}s:14:\"followers_data\";a:0:{}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, 'Tv. Miriam Batucada - Mooca', 'São Paulo', 'SP', 'BR'),
(105, '4b65ba86e', 'juniorids1@hotmail.com', '5927140e7502deb5acefe2c71b3e0173b70f6c0c', 'Junior', 'Santos', 'upload/photos/2018/11/eqfpRXsPWTPenoBANjof_avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', '', '', 'male', '0000-00-00', 0, '', '', '', '', '', '', '', '', 'portuguese', '4b83b006d5b030e730ddcf9e250e864d', 'Facebook', '179.198.215.29', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1542798581, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '11/2018', '0', '0', '0', '1', 0, '', 0, '0', 0, '0', 1542798235, '', '', 0, '0', '', '0', '1', '1', '', '', '0.00', '-23.82134752472392', '-46.10893613341676', '0', 1, 1542928059, 'a:6:{s:10:\"post_count\";s:1:\"1\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"0\";s:15:\"followers_count\";s:1:\"2\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:0:{}s:14:\"followers_data\";a:2:{i:0;s:3:\"108\";i:1;s:3:\"107\";}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, 'Brasil', '11250-000', 'SP', 'BR'),
(107, 'dbnascimento', 'nielbn@yahoo.com.br', 'd6cfe5e76c8347bc803168fe861f69fcc69cc79c', '', '', 'upload/photos/d-avatar.jpg', 'upload/photos/d-cover.jpg', '', '0', 0, '', '', '', NULL, '', 'male', '0000-00-00', 0, '', '', '', '', '', '', '', '', 'portuguese', 'd35e2b6fdb922ee11a9d1b5406d7fdd4', 'site', '200.128.32.233', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1542818826, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '11/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1542818770, '', '', 0, '0', '', '0', '1', '0', '', '', '0.00', '0', '0', '0', 1, 1542951307, 'a:6:{s:10:\"post_count\";s:1:\"0\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"4\";s:15:\"followers_count\";s:1:\"0\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:4:{i:0;s:3:\"105\";i:1;s:3:\"103\";i:2;s:2:\"98\";i:3;s:1:\"1\";}s:14:\"followers_data\";a:0:{}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 0, NULL, NULL, NULL, NULL),
(108, 'Ezequiel', 'workwebcc@gmail.com', 'c8693fc4630a27e0369966ca62776aabdb962d82', 'Ezequiel', 'AC', 'upload/photos/2018/11/UZmxa98bPw9MgeMbNUBt_avatar.jpg', 'upload/photos/2018/11/saadVB7SZjIzb6sFJ269_22_e83697e541e7f97ae722493247f74192_cover.jpg', '', '0', 0, '', '', '', '', '', 'male', '00-00-0000', 1, '', '', '', '', '', '', '', '', 'portuguese', 'dd9907778eb25b6e63f7e8fb1de5c4e8', 'Facebook', '201.21.187.147', '0', '0', 'ifollow', '0', '0', '1', '0', '0', '0', 1543169137, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '0', 'user', '11/2018', '1', '1', '1', '1', 0, '', 0, '0', 0, '0', 1542838651, '', '', 0, '0', '', '0', '3', '0', '', '', '0.00', '-25.3981714', '-49.24507450000001', '1543505530', 1, 1543162535, 'a:6:{s:10:\"post_count\";s:1:\"4\";s:11:\"album_count\";s:1:\"0\";s:15:\"following_count\";s:1:\"4\";s:15:\"followers_count\";s:1:\"0\";s:12:\"groups_count\";s:1:\"0\";s:11:\"likes_count\";s:1:\"0\";}', 'a:4:{s:14:\"following_data\";a:4:{i:0;s:3:\"105\";i:1;s:3:\"103\";i:2;s:2:\"98\";i:3;s:1:\"1\";}s:14:\"followers_data\";a:0:{}s:10:\"likes_data\";a:0:{}s:11:\"groups_data\";a:0:{}}', 0, 1542901456, 'Av. Paraná', 'Curitiba', 'PR', 'BR');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_UsersChat`
--

CREATE TABLE `Wo_UsersChat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `conversation_user_id` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0,
  `color` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Wo_UsersChat`
--

INSERT INTO `Wo_UsersChat` (`id`, `user_id`, `conversation_user_id`, `time`, `color`) VALUES
(9, 1, 33, 1539440864, ''),
(10, 33, 1, 1539440864, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_UserStory`
--

CREATE TABLE `Wo_UserStory` (
  `id` int(11) NOT NULL,
  `user_id` int(50) NOT NULL DEFAULT 0,
  `title` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(300) NOT NULL DEFAULT '',
  `posted` varchar(50) NOT NULL DEFAULT '',
  `expire` date DEFAULT NULL,
  `thumbnail` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_UserStoryMedia`
--

CREATE TABLE `Wo_UserStoryMedia` (
  `id` int(11) NOT NULL,
  `story_id` int(30) NOT NULL DEFAULT 0,
  `type` varchar(30) NOT NULL DEFAULT '',
  `filename` text DEFAULT NULL,
  `expire` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Verification_Requests`
--

CREATE TABLE `Wo_Verification_Requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `page_id` int(11) NOT NULL DEFAULT 0,
  `message` text CHARACTER SET utf8 DEFAULT NULL,
  `user_name` varchar(150) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `passport` varchar(3000) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `photo` varchar(3000) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `type` varchar(32) NOT NULL DEFAULT '',
  `seen` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_VideoCalles`
--

CREATE TABLE `Wo_VideoCalles` (
  `id` int(11) NOT NULL,
  `access_token` text DEFAULT NULL,
  `access_token_2` text DEFAULT NULL,
  `from_id` int(11) NOT NULL DEFAULT 0,
  `to_id` int(11) NOT NULL DEFAULT 0,
  `room_name` varchar(50) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT 0,
  `called` int(11) NOT NULL DEFAULT 0,
  `time` int(11) NOT NULL DEFAULT 0,
  `declined` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `Wo_VideoCalles`
--

INSERT INTO `Wo_VideoCalles` (`id`, `access_token`, `access_token_2`, `from_id`, `to_id`, `room_name`, `active`, `called`, `time`, `declined`) VALUES
(2, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJBZG1pbjF4LTE1Mzc4MjU2ODQiLCJpc3MiOiJBZG1pbjF4Iiwic3ViIjoiIiwiZXhwIjoxNTM3ODI5Mjg0LCJncmFudHMiOnsiaWRlbnRpdHkiOiI0ZTM0YTY3ZDFmYWRjYjgiLCJ2aWRlbyI6eyJyb29tIjoiOTdkMzQyM2QwOGJlN2FmNzYyODhkMjNhZTFlMTc0ZjU5ZGQxZDU0MiJ9fX0.pXxDBauRQCeWlYQR3uI5Gwz0EIVzAIs_G2Wca0-THp4', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJBZG1pbjF4LTE1Mzc4MjU2ODQiLCJpc3MiOiJBZG1pbjF4Iiwic3ViIjoiIiwiZXhwIjoxNTM3ODI5Mjg0LCJncmFudHMiOnsiaWRlbnRpdHkiOiJiM2ViNjU2NDdmMmQzMmMiLCJ2aWRlbyI6eyJyb29tIjoiOTdkMzQyM2QwOGJlN2FmNzYyODhkMjNhZTFlMTc0ZjU5ZGQxZDU0MiJ9fX0.dbGPd62HP2LRfMyh1mH7UOhWMfwBPDE_b0hCsqvJRmM', 1, 2, '97d3423d08be7af76288d23ae1e174f59dd1d542', 1, 1, 1537825684, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Votes`
--

CREATE TABLE `Wo_Votes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `option_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Wo_Wonders`
--

CREATE TABLE `Wo_Wonders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `post_id` int(11) NOT NULL DEFAULT 0,
  `type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Wo_Wonders`
--

INSERT INTO `Wo_Wonders` (`id`, `user_id`, `post_id`, `type`) VALUES
(76, 1, 320, 0),
(78, 85, 346, 0),
(79, 85, 362, 0),
(85, 1, 305, 0),
(90, 104, 384, 0),
(91, 104, 369, 0),
(92, 104, 355, 0),
(93, 104, 354, 0),
(94, 104, 394, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `MAM_City`
--
ALTER TABLE `MAM_City`
  ADD PRIMARY KEY (`CT_ID`);

--
-- Indexes for table `MAM_Country`
--
ALTER TABLE `MAM_Country`
  ADD PRIMARY KEY (`SL_ID`);

--
-- Indexes for table `MAM_State`
--
ALTER TABLE `MAM_State`
  ADD PRIMARY KEY (`UF_ID`);

--
-- Indexes for table `Wo_Activities`
--
ALTER TABLE `Wo_Activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `activity_type` (`activity_type`),
  ADD KEY `order1` (`user_id`,`id`),
  ADD KEY `order2` (`post_id`,`id`);

--
-- Indexes for table `Wo_AdminInvitations`
--
ALTER TABLE `Wo_AdminInvitations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`(255));

--
-- Indexes for table `Wo_Ads`
--
ALTER TABLE `Wo_Ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active` (`active`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `Wo_Affiliates_Requests`
--
ALTER TABLE `Wo_Affiliates_Requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `time` (`time`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `Wo_AgoraVideoCall`
--
ALTER TABLE `Wo_AgoraVideoCall`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `type` (`type`),
  ADD KEY `room_name` (`room_name`),
  ADD KEY `time` (`time`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `Wo_Albums_Media`
--
ALTER TABLE `Wo_Albums_Media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `order1` (`post_id`,`id`);

--
-- Indexes for table `Wo_Announcement`
--
ALTER TABLE `Wo_Announcement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `Wo_Announcement_Views`
--
ALTER TABLE `Wo_Announcement_Views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `announcement_id` (`announcement_id`);

--
-- Indexes for table `Wo_Apps`
--
ALTER TABLE `Wo_Apps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Wo_AppsSessions`
--
ALTER TABLE `Wo_AppsSessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `session_id` (`session_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `platform` (`platform`);

--
-- Indexes for table `Wo_Apps_Hash`
--
ALTER TABLE `Wo_Apps_Hash`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hash_id` (`hash_id`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `Wo_Apps_Permission`
--
ALTER TABLE `Wo_Apps_Permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`app_id`);

--
-- Indexes for table `Wo_AudioCalls`
--
ALTER TABLE `Wo_AudioCalls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `call_id` (`call_id`),
  ADD KEY `called` (`called`),
  ADD KEY `declined` (`declined`);

--
-- Indexes for table `Wo_Banned_Ip`
--
ALTER TABLE `Wo_Banned_Ip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip_address` (`ip_address`);

--
-- Indexes for table `Wo_Blocks`
--
ALTER TABLE `Wo_Blocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blocker` (`blocker`),
  ADD KEY `blocked` (`blocked`);

--
-- Indexes for table `Wo_Blog`
--
ALTER TABLE `Wo_Blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `title` (`title`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `Wo_BlogCommentReplies`
--
ALTER TABLE `Wo_BlogCommentReplies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comm_id` (`comm_id`),
  ADD KEY `blog_id` (`blog_id`),
  ADD KEY `order1` (`comm_id`,`id`),
  ADD KEY `order2` (`blog_id`,`id`),
  ADD KEY `order3` (`user_id`,`id`);

--
-- Indexes for table `Wo_BlogComments`
--
ALTER TABLE `Wo_BlogComments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_id` (`blog_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_BlogMovieDisLikes`
--
ALTER TABLE `Wo_BlogMovieDisLikes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_comm_id` (`blog_comm_id`),
  ADD KEY `movie_comm_id` (`movie_comm_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `blog_commreply_id` (`blog_commreply_id`),
  ADD KEY `movie_commreply_id` (`movie_commreply_id`),
  ADD KEY `blog_id` (`blog_id`),
  ADD KEY `movie_id` (`movie_id`);

--
-- Indexes for table `Wo_BlogMovieLikes`
--
ALTER TABLE `Wo_BlogMovieLikes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_id` (`blog_comm_id`),
  ADD KEY `movie_id` (`movie_comm_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `blog_commreply_id` (`blog_commreply_id`),
  ADD KEY `movie_commreply_id` (`movie_commreply_id`),
  ADD KEY `blog_id_2` (`blog_id`),
  ADD KEY `movie_id_2` (`movie_id`);

--
-- Indexes for table `Wo_Codes`
--
ALTER TABLE `Wo_Codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `Wo_CommentLikes`
--
ALTER TABLE `Wo_CommentLikes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `Wo_Comments`
--
ALTER TABLE `Wo_Comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `order1` (`user_id`,`id`),
  ADD KEY `order2` (`page_id`,`id`),
  ADD KEY `order3` (`post_id`,`id`),
  ADD KEY `order4` (`user_id`,`id`),
  ADD KEY `order5` (`post_id`,`id`);

--
-- Indexes for table `Wo_CommentWonders`
--
ALTER TABLE `Wo_CommentWonders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Comment_Replies`
--
ALTER TABLE `Wo_Comment_Replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `user_id` (`user_id`,`page_id`);

--
-- Indexes for table `Wo_Comment_Replies_Likes`
--
ALTER TABLE `Wo_Comment_Replies_Likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reply_id` (`reply_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Comment_Replies_Wonders`
--
ALTER TABLE `Wo_Comment_Replies_Wonders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reply_id` (`reply_id`,`user_id`);

--
-- Indexes for table `Wo_Config`
--
ALTER TABLE `Wo_Config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `Wo_CustomPages`
--
ALTER TABLE `Wo_CustomPages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Wo_Egoing`
--
ALTER TABLE `Wo_Egoing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Einterested`
--
ALTER TABLE `Wo_Einterested`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Einvited`
--
ALTER TABLE `Wo_Einvited`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `inviter_id` (`invited_id`);

--
-- Indexes for table `Wo_Emails`
--
ALTER TABLE `Wo_Emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Events`
--
ALTER TABLE `Wo_Events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `poster_id` (`poster_id`),
  ADD KEY `name` (`name`),
  ADD KEY `start_date` (`start_date`),
  ADD KEY `start_time` (`start_time`),
  ADD KEY `end_time` (`end_time`),
  ADD KEY `end_date` (`end_date`),
  ADD KEY `order1` (`poster_id`,`id`),
  ADD KEY `order2` (`id`);

--
-- Indexes for table `Wo_Family`
--
ALTER TABLE `Wo_Family`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `Wo_Followers`
--
ALTER TABLE `Wo_Followers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `following_id` (`following_id`),
  ADD KEY `follower_id` (`follower_id`),
  ADD KEY `active` (`active`),
  ADD KEY `order1` (`following_id`,`id`),
  ADD KEY `order2` (`follower_id`,`id`);

--
-- Indexes for table `Wo_Forums`
--
ALTER TABLE `Wo_Forums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `description` (`description`(255)),
  ADD KEY `posts` (`posts`);

--
-- Indexes for table `Wo_ForumThreadReplies`
--
ALTER TABLE `Wo_ForumThreadReplies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thread_id` (`thread_id`),
  ADD KEY `forum_id` (`forum_id`),
  ADD KEY `poster_id` (`poster_id`),
  ADD KEY `post_subject` (`post_subject`(255)),
  ADD KEY `post_quoted` (`post_quoted`),
  ADD KEY `posted_time` (`posted_time`);

--
-- Indexes for table `Wo_Forum_Sections`
--
ALTER TABLE `Wo_Forum_Sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section_name` (`section_name`),
  ADD KEY `description` (`description`(255));

--
-- Indexes for table `Wo_Forum_Threads`
--
ALTER TABLE `Wo_Forum_Threads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `views` (`views`),
  ADD KEY `posted` (`posted`),
  ADD KEY `headline` (`headline`(255)),
  ADD KEY `forum` (`forum`);

--
-- Indexes for table `Wo_Games`
--
ALTER TABLE `Wo_Games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Wo_Games_Players`
--
ALTER TABLE `Wo_Games_Players`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`game_id`,`active`);

--
-- Indexes for table `Wo_GroupAdmins`
--
ALTER TABLE `Wo_GroupAdmins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `Wo_GroupChat`
--
ALTER TABLE `Wo_GroupChat`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_GroupChatUsers`
--
ALTER TABLE `Wo_GroupChatUsers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `Wo_Groups`
--
ALTER TABLE `Wo_Groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `privacy` (`privacy`);

--
-- Indexes for table `Wo_Group_Members`
--
ALTER TABLE `Wo_Group_Members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`group_id`);

--
-- Indexes for table `Wo_Hashtags`
--
ALTER TABLE `Wo_Hashtags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `last_trend_time` (`last_trend_time`),
  ADD KEY `trend_use_num` (`trend_use_num`),
  ADD KEY `tag` (`tag`),
  ADD KEY `expire` (`expire`);

--
-- Indexes for table `Wo_HiddenPosts`
--
ALTER TABLE `Wo_HiddenPosts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Langs`
--
ALTER TABLE `Wo_Langs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang_key` (`lang_key`);

--
-- Indexes for table `Wo_Likes`
--
ALTER TABLE `Wo_Likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Messages`
--
ALTER TABLE `Wo_Messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `seen` (`seen`),
  ADD KEY `time` (`time`),
  ADD KEY `deleted_two` (`deleted_two`),
  ADD KEY `deleted_one` (`deleted_one`),
  ADD KEY `sent_push` (`sent_push`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `order1` (`from_id`,`id`),
  ADD KEY `order2` (`group_id`,`id`),
  ADD KEY `order3` (`to_id`,`id`),
  ADD KEY `order7` (`seen`,`id`),
  ADD KEY `order8` (`time`,`id`),
  ADD KEY `order4` (`from_id`,`id`),
  ADD KEY `order5` (`group_id`,`id`),
  ADD KEY `order6` (`to_id`,`id`);

--
-- Indexes for table `Wo_MovieCommentReplies`
--
ALTER TABLE `Wo_MovieCommentReplies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comm_id` (`comm_id`),
  ADD KEY `movie_id` (`movie_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_MovieComments`
--
ALTER TABLE `Wo_MovieComments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movie_id` (`movie_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Movies`
--
ALTER TABLE `Wo_Movies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `genre` (`genre`),
  ADD KEY `country` (`country`),
  ADD KEY `release` (`release`);

--
-- Indexes for table `Wo_Notifications`
--
ALTER TABLE `Wo_Notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifier_id` (`notifier_id`),
  ADD KEY `user_id` (`recipient_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `seen` (`seen`),
  ADD KEY `time` (`time`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `group_id` (`group_id`,`seen_pop`),
  ADD KEY `sent_push` (`sent_push`),
  ADD KEY `order1` (`seen`,`id`),
  ADD KEY `order2` (`notifier_id`,`id`),
  ADD KEY `order3` (`recipient_id`,`id`),
  ADD KEY `order4` (`post_id`,`id`),
  ADD KEY `order5` (`page_id`,`id`),
  ADD KEY `order6` (`group_id`,`id`),
  ADD KEY `order7` (`time`,`id`);

--
-- Indexes for table `Wo_PageAdmins`
--
ALTER TABLE `Wo_PageAdmins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `page_id` (`page_id`);

--
-- Indexes for table `Wo_PageRating`
--
ALTER TABLE `Wo_PageRating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `page_id` (`page_id`);

--
-- Indexes for table `Wo_Pages`
--
ALTER TABLE `Wo_Pages`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `registered` (`registered`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `page_category` (`page_category`),
  ADD KEY `active` (`active`),
  ADD KEY `verified` (`verified`),
  ADD KEY `boosted` (`boosted`);

--
-- Indexes for table `Wo_Pages_Invites`
--
ALTER TABLE `Wo_Pages_Invites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_id` (`page_id`,`inviter_id`,`invited_id`);

--
-- Indexes for table `Wo_Pages_Likes`
--
ALTER TABLE `Wo_Pages_Likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `active` (`active`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `page_id` (`page_id`);

--
-- Indexes for table `Wo_Payments`
--
ALTER TABLE `Wo_Payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_PinnedPosts`
--
ALTER TABLE `Wo_PinnedPosts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `active` (`active`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `Wo_Polls`
--
ALTER TABLE `Wo_Polls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `Wo_Posts`
--
ALTER TABLE `Wo_Posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `recipient_id` (`recipient_id`),
  ADD KEY `postFile` (`postFile`),
  ADD KEY `postShare` (`postShare`),
  ADD KEY `postType` (`postType`),
  ADD KEY `postYoutube` (`postYoutube`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `registered` (`registered`),
  ADD KEY `time` (`time`),
  ADD KEY `boosted` (`boosted`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `poll_id` (`poll_id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `videoViews` (`videoViews`),
  ADD KEY `shared_from` (`shared_from`),
  ADD KEY `order1` (`user_id`,`id`),
  ADD KEY `order2` (`page_id`,`id`),
  ADD KEY `order3` (`group_id`,`id`),
  ADD KEY `order4` (`recipient_id`,`id`),
  ADD KEY `order5` (`event_id`,`id`),
  ADD KEY `order6` (`parent_id`,`id`),
  ADD KEY `multi_image` (`multi_image`),
  ADD KEY `album_name` (`album_name`);

--
-- Indexes for table `Wo_Products`
--
ALTER TABLE `Wo_Products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `category` (`category`),
  ADD KEY `price` (`price`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `Wo_Products_Media`
--
ALTER TABLE `Wo_Products_Media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Wo_ProfileFields`
--
ALTER TABLE `Wo_ProfileFields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `registration_page` (`registration_page`),
  ADD KEY `active` (`active`),
  ADD KEY `profile_page` (`profile_page`);

--
-- Indexes for table `Wo_RecentSearches`
--
ALTER TABLE `Wo_RecentSearches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`search_id`),
  ADD KEY `search_type` (`search_type`);

--
-- Indexes for table `Wo_Relationship`
--
ALTER TABLE `Wo_Relationship`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `relationship` (`relationship`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `Wo_Reports`
--
ALTER TABLE `Wo_Reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `seen` (`seen`),
  ADD KEY `profile_id` (`profile_id`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `Wo_SavedPosts`
--
ALTER TABLE `Wo_SavedPosts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Terms`
--
ALTER TABLE `Wo_Terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Wo_Tokens`
--
ALTER TABLE `Wo_Tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `app_id` (`app_id`);

--
-- Indexes for table `Wo_UserAds`
--
ALTER TABLE `Wo_UserAds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appears` (`appears`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `location` (`location`(255)),
  ADD KEY `gender` (`gender`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `Wo_UserFields`
--
ALTER TABLE `Wo_UserFields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `Wo_Users`
--
ALTER TABLE `Wo_Users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `active` (`active`),
  ADD KEY `admin` (`admin`),
  ADD KEY `src` (`src`),
  ADD KEY `gender` (`gender`),
  ADD KEY `avatar` (`avatar`),
  ADD KEY `first_name` (`first_name`),
  ADD KEY `last_name` (`last_name`),
  ADD KEY `registered` (`registered`),
  ADD KEY `joined` (`joined`),
  ADD KEY `phone_number` (`phone_number`) USING BTREE,
  ADD KEY `referrer` (`referrer`),
  ADD KEY `wallet` (`wallet`),
  ADD KEY `friend_privacy` (`friend_privacy`),
  ADD KEY `lat` (`lat`),
  ADD KEY `lng` (`lng`),
  ADD KEY `order1` (`username`,`user_id`),
  ADD KEY `order2` (`email`,`user_id`),
  ADD KEY `order3` (`lastseen`,`user_id`),
  ADD KEY `order4` (`active`,`user_id`),
  ADD KEY `last_data_update` (`last_data_update`);

--
-- Indexes for table `Wo_UsersChat`
--
ALTER TABLE `Wo_UsersChat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `conversation_user_id` (`conversation_user_id`),
  ADD KEY `time` (`time`),
  ADD KEY `order1` (`user_id`,`id`),
  ADD KEY `order2` (`user_id`,`id`),
  ADD KEY `order3` (`conversation_user_id`,`id`),
  ADD KEY `order4` (`conversation_user_id`,`id`);

--
-- Indexes for table `Wo_UserStory`
--
ALTER TABLE `Wo_UserStory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `expires` (`expire`);

--
-- Indexes for table `Wo_UserStoryMedia`
--
ALTER TABLE `Wo_UserStoryMedia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expire` (`expire`),
  ADD KEY `story_id` (`story_id`);

--
-- Indexes for table `Wo_Verification_Requests`
--
ALTER TABLE `Wo_Verification_Requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `page_id` (`page_id`);

--
-- Indexes for table `Wo_VideoCalles`
--
ALTER TABLE `Wo_VideoCalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `called` (`called`),
  ADD KEY `declined` (`declined`);

--
-- Indexes for table `Wo_Votes`
--
ALTER TABLE `Wo_Votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `option_id` (`option_id`);

--
-- Indexes for table `Wo_Wonders`
--
ALTER TABLE `Wo_Wonders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `MAM_City`
--
ALTER TABLE `MAM_City`
  MODIFY `CT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5611;

--
-- AUTO_INCREMENT for table `MAM_Country`
--
ALTER TABLE `MAM_Country`
  MODIFY `SL_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;

--
-- AUTO_INCREMENT for table `MAM_State`
--
ALTER TABLE `MAM_State`
  MODIFY `UF_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `Wo_Activities`
--
ALTER TABLE `Wo_Activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=452;

--
-- AUTO_INCREMENT for table `Wo_AdminInvitations`
--
ALTER TABLE `Wo_AdminInvitations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Wo_Ads`
--
ALTER TABLE `Wo_Ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `Wo_Affiliates_Requests`
--
ALTER TABLE `Wo_Affiliates_Requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_AgoraVideoCall`
--
ALTER TABLE `Wo_AgoraVideoCall`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Albums_Media`
--
ALTER TABLE `Wo_Albums_Media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `Wo_Announcement`
--
ALTER TABLE `Wo_Announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `Wo_Announcement_Views`
--
ALTER TABLE `Wo_Announcement_Views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Wo_Apps`
--
ALTER TABLE `Wo_Apps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Wo_AppsSessions`
--
ALTER TABLE `Wo_AppsSessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=525;

--
-- AUTO_INCREMENT for table `Wo_Apps_Hash`
--
ALTER TABLE `Wo_Apps_Hash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Apps_Permission`
--
ALTER TABLE `Wo_Apps_Permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_AudioCalls`
--
ALTER TABLE `Wo_AudioCalls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Wo_Banned_Ip`
--
ALTER TABLE `Wo_Banned_Ip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Blocks`
--
ALTER TABLE `Wo_Blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Wo_Blog`
--
ALTER TABLE `Wo_Blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `Wo_BlogCommentReplies`
--
ALTER TABLE `Wo_BlogCommentReplies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Wo_BlogComments`
--
ALTER TABLE `Wo_BlogComments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Wo_BlogMovieDisLikes`
--
ALTER TABLE `Wo_BlogMovieDisLikes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_BlogMovieLikes`
--
ALTER TABLE `Wo_BlogMovieLikes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Wo_Codes`
--
ALTER TABLE `Wo_Codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_CommentLikes`
--
ALTER TABLE `Wo_CommentLikes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `Wo_Comments`
--
ALTER TABLE `Wo_Comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=405;

--
-- AUTO_INCREMENT for table `Wo_CommentWonders`
--
ALTER TABLE `Wo_CommentWonders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `Wo_Comment_Replies`
--
ALTER TABLE `Wo_Comment_Replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `Wo_Comment_Replies_Likes`
--
ALTER TABLE `Wo_Comment_Replies_Likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Comment_Replies_Wonders`
--
ALTER TABLE `Wo_Comment_Replies_Wonders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Config`
--
ALTER TABLE `Wo_Config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT for table `Wo_CustomPages`
--
ALTER TABLE `Wo_CustomPages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Wo_Egoing`
--
ALTER TABLE `Wo_Egoing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Einterested`
--
ALTER TABLE `Wo_Einterested`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Einvited`
--
ALTER TABLE `Wo_Einvited`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Wo_Emails`
--
ALTER TABLE `Wo_Emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Events`
--
ALTER TABLE `Wo_Events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Wo_Family`
--
ALTER TABLE `Wo_Family`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Wo_Followers`
--
ALTER TABLE `Wo_Followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `Wo_Forums`
--
ALTER TABLE `Wo_Forums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_ForumThreadReplies`
--
ALTER TABLE `Wo_ForumThreadReplies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Forum_Sections`
--
ALTER TABLE `Wo_Forum_Sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Forum_Threads`
--
ALTER TABLE `Wo_Forum_Threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Games`
--
ALTER TABLE `Wo_Games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Wo_Games_Players`
--
ALTER TABLE `Wo_Games_Players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Wo_GroupAdmins`
--
ALTER TABLE `Wo_GroupAdmins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_GroupChat`
--
ALTER TABLE `Wo_GroupChat`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Wo_GroupChatUsers`
--
ALTER TABLE `Wo_GroupChatUsers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Wo_Groups`
--
ALTER TABLE `Wo_Groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Wo_Group_Members`
--
ALTER TABLE `Wo_Group_Members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `Wo_Hashtags`
--
ALTER TABLE `Wo_Hashtags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `Wo_HiddenPosts`
--
ALTER TABLE `Wo_HiddenPosts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `Wo_Langs`
--
ALTER TABLE `Wo_Langs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1187;

--
-- AUTO_INCREMENT for table `Wo_Likes`
--
ALTER TABLE `Wo_Likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `Wo_Messages`
--
ALTER TABLE `Wo_Messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `Wo_MovieCommentReplies`
--
ALTER TABLE `Wo_MovieCommentReplies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_MovieComments`
--
ALTER TABLE `Wo_MovieComments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Movies`
--
ALTER TABLE `Wo_Movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Notifications`
--
ALTER TABLE `Wo_Notifications`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=653;

--
-- AUTO_INCREMENT for table `Wo_PageAdmins`
--
ALTER TABLE `Wo_PageAdmins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_PageRating`
--
ALTER TABLE `Wo_PageRating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Wo_Pages`
--
ALTER TABLE `Wo_Pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Wo_Pages_Invites`
--
ALTER TABLE `Wo_Pages_Invites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Wo_Pages_Likes`
--
ALTER TABLE `Wo_Pages_Likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `Wo_Payments`
--
ALTER TABLE `Wo_Payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Wo_PinnedPosts`
--
ALTER TABLE `Wo_PinnedPosts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Wo_Polls`
--
ALTER TABLE `Wo_Polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `Wo_Posts`
--
ALTER TABLE `Wo_Posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=411;

--
-- AUTO_INCREMENT for table `Wo_Products`
--
ALTER TABLE `Wo_Products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Wo_Products_Media`
--
ALTER TABLE `Wo_Products_Media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Wo_ProfileFields`
--
ALTER TABLE `Wo_ProfileFields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_RecentSearches`
--
ALTER TABLE `Wo_RecentSearches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `Wo_Relationship`
--
ALTER TABLE `Wo_Relationship`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Reports`
--
ALTER TABLE `Wo_Reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `Wo_SavedPosts`
--
ALTER TABLE `Wo_SavedPosts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Wo_Terms`
--
ALTER TABLE `Wo_Terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Wo_Tokens`
--
ALTER TABLE `Wo_Tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_UserAds`
--
ALTER TABLE `Wo_UserAds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `Wo_UserFields`
--
ALTER TABLE `Wo_UserFields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `Wo_Users`
--
ALTER TABLE `Wo_Users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `Wo_UsersChat`
--
ALTER TABLE `Wo_UsersChat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `Wo_UserStory`
--
ALTER TABLE `Wo_UserStory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_UserStoryMedia`
--
ALTER TABLE `Wo_UserStoryMedia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wo_Verification_Requests`
--
ALTER TABLE `Wo_Verification_Requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `Wo_VideoCalles`
--
ALTER TABLE `Wo_VideoCalles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Wo_Votes`
--
ALTER TABLE `Wo_Votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Wo_Wonders`
--
ALTER TABLE `Wo_Wonders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
